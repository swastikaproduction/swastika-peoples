<?php 
include("../../include/config.php");
$profile = $_GET['profile'];
$employee = $_GET['employee'];
$callbackurl = str_ireplace("profileData.php", "profileData.php", $urltocall);

if($employee==0){
$getData = mysqli_query($con,"SELECT `permission` FROM `profiles` WHERE `id` = '$profile'");
$row = mysqli_fetch_array($getData); 
$permissions = $row[0];
$permissions = explode(",",$permissions); 
}
else{
$getData = mysqli_query($con,"SELECT `permission` FROM `employee_permissions` WHERE employeeid='$employee'");
$row = mysqli_fetch_array($getData); 
$permissions = $row[0];
$permissions = explode(",",$permissions); 
}


?>
       <div class="row">
        <table cellpadding="5" cellspacing="0" class="table table-hover" width="100%">
        <tr>
        <td valign="top"><strong> 
       <input name="Checkbox1" type="checkbox" class="checkInput"  id="perDetail" onchange="chkModule('lds','perDetail','17','1','')"/>Employee Personal Details</strong>
       <input name="Checkbox1" type="checkbox"   style="vertical-align:middle;margin-bottom:7px;display:none" id="lds0" value="null" checked="checked" />
        </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds1" value="emp_name" <?php if(in_array("emp_name",$permissions)) echo "checked='checked'";?> />Name</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds2" value="emp_mobile" <?php if(in_array("emp_mobile",$permissions)) echo "checked='checked'";?> />Mobile</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds3" value="emp_peremail" <?php if(in_array("emp_peremail",$permissions)) echo "checked='checked'";?> />Personal Email</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds4" value="emp_workemail" <?php if(in_array("emp_workemail",$permissions)) echo "checked='checked'";?>/>Work Email</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds5" value="emp_dob" <?php if(in_array("emp_dob",$permissions)) echo "checked='checked'";?> />Date of Birth</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds6" value="emp_username" <?php if(in_array("emp_username",$permissions)) echo "checked='checked'";?> />Username</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds7" value="emp_password" <?php if(in_array("emp_password",$permissions)) echo "checked='checked'";?> />Password</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds8" value="emp_address" <?php if(in_array("emp_address",$permissions)) echo "checked='checked'";?>/>Address</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds9" value="emp_aadhar_name" <?php if(in_array("emp_aadhar_name",$permissions)) echo "checked='checked'";?> />Name (as per Aadhar)</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds10" value="emp_aadhar_number" <?php if(in_array("emp_aadhar_number",$permissions)) echo "checked='checked'";?> />Aadhar No.</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds11" value="emp_aadhar_dob" <?php if(in_array("emp_aadhar_dob",$permissions)) echo "checked='checked'";?> />DOB (as per Aadhar)</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds12" value="emp_aadhar_father" <?php if(in_array("emp_aadhar_father",$permissions)) echo "checked='checked'";?> onchange="chkModule('lds','lds3','16','4','dis')"/>Father Name (as per Aadhar)</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds13" value="emp_uan" <?php if(in_array("emp_uan",$permissions)) echo "checked='checked'";?> />UN Number</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds14" value="emp_update" <?php if(in_array("emp_update",$permissions)) echo "checked='checked'";?> />Update Employee</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="BankDe"  onchange="chkModule('lds','BankDe','17','15','')" />Employee Bank Details</strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds15" value="account_no" <?php if(in_array("account_no",$permissions)) echo "checked='checked'";?> />Account No</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds16" value="Beneficiary_name" <?php if(in_array("Beneficiary_name",$permissions)) echo "checked='checked'";?> />Beneficiary Name</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds17" value="ifsc" <?php if(in_array("ifsc",$permissions)) echo "checked='checked'";?> />IFSC</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="employerDet"  onchange="chkModule('lds','employerDet','40','18','')" />Employment Details</strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds18" value="emp_code" <?php if(in_array("emp_code",$permissions)) echo "checked='checked'";?> />Employee Code</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds19" value="emp_department" <?php if(in_array("emp_department",$permissions)) echo "checked='checked'";?> />Department</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds20" value="emp_designation" <?php if(in_array("emp_designation",$permissions)) echo "checked='checked'";?> />Designation</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds21" value="emp_doj" <?php if(in_array("emp_doj",$permissions)) echo "checked='checked'";?>/>Date of Joining</td>
       </tr>


       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds22" value="emp_branch" <?php if(in_array("emp_branch",$permissions)) echo "checked='checked'";?> />Branch</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds23" value="employee_salary" <?php if(in_array("employee_salary",$permissions)) echo "checked='checked'";?> />Salary</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds24" value="pool_bal" <?php if(in_array("pool_bal",$permissions)) echo "checked='checked'";?> />Pool Balance</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds25" value="pt_deduction_flag" <?php if(in_array("pt_deduction_flag",$permissions)) echo "checked='checked'";?>/>PT Deduction Flag</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td>
                <input name="Checkbox1" type="checkbox" class="checkInput" id="lds26" value="od_flag" <?php if(in_array("od_flag",$permissions)) echo "checked='checked'";?> />Other Deduction Flag</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds27" value="default_pf" <?php if(in_array("default_pf",$permissions)) echo "checked='checked'";?> />Default PF</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds28" value="mark_left" <?php if(in_array("mark_left",$permissions)) echo "checked='checked'";?> />Mark as Left</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds29" value="date_left" <?php if(in_array("date_left",$permissions)) echo "checked='checked'";?>/>Date Left</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds30" value="working_shift" <?php if(in_array("working_shift",$permissions)) echo "checked='checked'";?>/>Shift</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds31" value="emp_hod" <?php if(in_array("emp_hod",$permissions)) echo "checked='checked'";?> />HOD</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds32" value="big_image" <?php if(in_array("big_image",$permissions)) echo "checked='checked'";?> />Big Image</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds33" value="rule_type" <?php if(in_array("rule_type",$permissions)) echo "checked='checked'";?> />Rupe Type</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds34" value="pf_deduction_flag" <?php if(in_array("pf_deduction_flag",$permissions)) echo "checked='checked'";?>/>PF Deduction Flag</td>
       </tr>        


       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds35" value="tally_cast" <?php if(in_array("tally_cast",$permissions)) echo "checked='checked'";?> />Tally cost centre</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds36" value="employee_status" <?php if(in_array("employee_status",$permissions)) echo "checked='checked'";?> />Employee Status</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds37" value="emp_profiles" <?php if(in_array("emp_profiles",$permissions)) echo "checked='checked'";?> />Profile</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds38" value="emp_level" <?php if(in_array("emp_level",$permissions)) echo "checked='checked'";?>/>Levels</td>
       </tr>

       <tr>
        <td valign="top"><strong> </td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds39" value="emp_extra_permission" <?php if(in_array("emp_extra_permission",$permissions)) echo "checked='checked'";?> />Extra Permissions</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds40" value="depart_type" <?php if(in_array("depart_type",$permissions)) echo "checked='checked'";?>/>Department Type (for salary)</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="LeavesDet"  onchange="chkModule('lds','LeavesDet','43','41','')" />Leaves </strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds41" value="manage_leave_request" <?php if(in_array("manage_leave_request",$permissions)) echo "checked='checked'";?> />Manage Requests</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds42" value="all_request_leaves" <?php if(in_array("all_request_leaves",$permissions)) echo "checked='checked'";?> />All Leaves Request</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds43" value="leaves_history" <?php if(in_array("leaves_history",$permissions)) echo "checked='checked'";?> />Leaves History</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="attendanceDet"  onchange="chkModule('lds','attendanceDet','46','44','')" />Attendance </strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds44" value="overtime_request" <?php if(in_array("overtime_request",$permissions)) echo "checked='checked'";?> /> Overtime Request</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds45" value="attendance" <?php if(in_array("attendance",$permissions)) echo "checked='checked'";?> />Attendance</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds46" value="team_attendance" <?php if(in_array("team_attendance",$permissions)) echo "checked='checked'";?> />Team Attendance</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="salaryDet"  onchange="chkModule('lds','salaryDet','49','47','')" />Salary </strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds47" value="view_generated_salary" <?php if(in_array("view_generated_salary",$permissions)) echo "checked='checked'";?> /> View Generated Salaries</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds48" value="salary_slips" <?php if(in_array("salary_slips",$permissions)) echo "checked='checked'";?> />Salary Slips</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds49" value="salary_log" <?php if(in_array("salary_log",$permissions)) echo "checked='checked'";?> />Salary Log</td>
       </tr>

        <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="reportDet"  onchange="chkModule('lds','reportDet','59','50','')" />Reports </strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds50" value="document_report" <?php if(in_array("document_report",$permissions)) echo "checked='checked'";?> /> Documents</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds51" value="branch_report" <?php if(in_array("branch_report",$permissions)) echo "checked='checked'";?> />Branch wise list</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds52" value="employee_report" <?php if(in_array("employee_report",$permissions)) echo "checked='checked'";?> />All Employee List</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds53" value="analytics_report" <?php if(in_array("analytics_report",$permissions)) echo "checked='checked'";?> />Analytics Report</td>
       </tr>
       
       <tr>
        <td valign="top"></td>
        <td> <input name="Checkbox1" type="checkbox" class="checkInput" id="lds54" value="change_log_report" <?php if(in_array("change_log_report",$permissions)) echo "checked='checked'";?> /> Change Log Report</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds55" value="goal_setting" <?php if(in_array("goal_setting",$permissions)) echo "checked='checked'";?> />Goal Setting</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds56" value="appraisal_report" <?php if(in_array("appraisal_report",$permissions)) echo "checked='checked'";?> />Appraisal Report</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds57" value="pool_log_report" <?php if(in_array("pool_log_report",$permissions)) echo "checked='checked'";?> />Pool Log Report</td>
       </tr> 

       <tr>
        <td valign="top"></td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds58" value="salary_log_report" <?php if(in_array("salary_log_report",$permissions)) echo "checked='checked'";?> />Salary Log Report</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds59" value="salary_log_export" <?php if(in_array("salary_log_export",$permissions)) echo "checked='checked'";?> />Salary Log Export</td>
       </tr> 

       <tr>
        <td valign="top"><strong> 
        <input name="Checkbox1" type="checkbox" class="checkInput"  id="Miscellaneous"  onchange="chkModule('lds','Miscellaneous','61','60','')" />Miscellaneous </strong></td>
        <td>
        <input name="Checkbox1" type="checkbox" class="checkInput" id="lds60" value="add_employee" <?php if(in_array("add_employee",$permissions)) echo "checked='checked'";?> /> Add Employee</td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds61" value="delete_employee" <?php if(in_array("delete_employee",$permissions)) echo "checked='checked'";?> />Delete Employee </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds62" value="emp_offer_letter" <?php if(in_array("emp_offer_letter",$permissions)) echo "checked='checked'";?> />Generate Offer Letter </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds63" value="emp_experience_letter" <?php if(in_array("emp_experience_letter",$permissions)) echo "checked='checked'";?> />Experience Letter </td>
       </tr>

       <tr>
        <td valign="top"></td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds64" value="emp_confirmation_letter" <?php if(in_array("emp_confirmation_letter",$permissions)) echo "checked='checked'";?> />Confirmation Letter </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds65" value="emp_relieving_letter" <?php if(in_array("emp_relieving_letter",$permissions)) echo "checked='checked'";?> />Relieving Letter </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds66" value="emp_appointment_letter" <?php if(in_array("emp_appointment_letter",$permissions)) echo "checked='checked'";?> />Appointment Letter </td>
        <!-- <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds67" value="emp_life_cycle" <?php if(in_array("emp_life_cycle",$permissions)) echo "checked='checked'";?> />Life Cycle </td> -->
       </tr> 

       <tr>
        <td valign="top"></td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds67" value="emp_promotion_letter" <?php if(in_array("emp_promotion_letter",$permissions)) echo "checked='checked'";?> />Promotion Letter </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds68" value="emp_increment_letter" <?php if(in_array("emp_increment_letter",$permissions)) echo "checked='checked'";?> />Increment Letter </td>
        <td><input name="Checkbox1" type="checkbox" class="checkInput" id="lds69" value="emp_f_and_f_calculation" <?php if(in_array("emp_f_and_f_calculation",$permissions)) echo "checked='checked'";?> />fnf Calculation </td>
        
       </tr>


       <tr>
         <td colspan="5">
             <input name="Button1" type="button" value="Save Changes" class="btn btn-primary" onclick="SavePermissions('masters/accesscontrol/save.php','<?php echo $callbackurl; ?>');" />
         </td>
       </tr>
       </table>
    </div>
