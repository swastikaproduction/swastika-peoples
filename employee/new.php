<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
?>



<div class="moduleHead">
<br/>

	<div class="moduleHeading" onclick="toogleFormTable();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Employees

	</div>
</div>

<div class="shadow" style="background:#fff">



<div class="row" style="background:">
<div class="col-sm-12 subHead">Add A New Employee</div>
	<div class="col-sm-2 formLeft req">
		Name
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp0" class="inputBox" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Mobile
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp1" class="inputBox" >
	</div>	

</div>



<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Personal Email
	</div>
	<div class="col-sm-4 formRight">
		<input type="email" name="req" title="" id="inp2" class="inputBox" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Work Email
	</div>
	<div class="col-sm-4 formRight">
		<input type="email" name="req" title="" onblur="checkItem('employee/checkemail.php?email='+this.value,'Email Id',this)" id="inp3" class="inputBox" >
	</div>	

</div>

<div class="row" >
	<div class="col-sm-2 formLeft req">
		Emergency Contact No.
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="" title="" id="inp35" class="inputBox" >
	</div>	

</div>


<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Employee Code
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="" id="inp4" class="inputBox"  >
	</div>	
</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Department
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp5" name='req'>
		
		<?php
		$data = getData('departments','*','name','ASC');
		foreach($data as $row1)
		{
		?>
		<option value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
	</div>	
<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft req">
		Designation
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp6" name="req">
		
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	</div>	

</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Date Of Joining 
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="" id="inp7" class="inputBox" onblur="getPoolBalance()" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Branch
	</div>
	<div class="col-sm-4 formRight">
<select class="inputBox" id="inp8" name="req">
		
		<?php
		$data = getData('branch','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	</div>	

</div>

<div class="row" >
	<div class="col-sm-2 formLeft req">
		Shift
	</div>
	<div class="col-sm-10 formRight">
<select class="inputBox" id="inp9" name="req">
		
		<?php
		$data = getData('shift','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	</div>	


</div>


<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Salary
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp16" class="inputBox" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Pool Bal
	</div>
	<div class="col-sm-4 formRight">
<input type="text" name="req" title="" id="inp17" class="inputBox" >
	</div>	

</div>



<div class="row" style="background:">


	<div class="col-sm-2 formLeft req">
	Date of Birth
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="" id="inp11" class="inputBox">
	</div>	

	<div class="col-sm-2 formLeft req">
		Gender
	</div>
	<div class="col-sm-4 formRight">
		<select name="req" id="inp10" class="inputBox">
			<option value="Male">Male</option>
			<option value="Female">Female</option>
		</select>
	</div>

</div>

<div class="row" style="background:">
	
	<div class="col-sm-2 formLeft req">
		 Address
	</div>
	<div class="col-sm-10 formRight">
		<textarea class="inputBox" id="inp12" style="width:100%;height:150px;"></textarea>
	</div>	

</div>


<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2 formLeft req">
		 HOD
	</div>
	<div class="col-sm-10 formRight">
<select class="inputBox" id="inp18" name='req'>
		
		<?php
		$data = getData('employee','*','name','ASC');
		foreach($data as $row1)
		{
		?>
		<option value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
	</div>
	</div>
</div>


<div class="row" style="background:#f7f8f8">
	<div class="col-sm-2 formLeft req">
		Username
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" readonly="readonly" id="inp14" class="inputBox" placeholder="will be workemail" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Password
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp15" class="inputBox" >
	</div>	

</div>



<div class="row">
	<div class="col-sm-2 formLeft req">
		Rule Type
		<br/>
		<span style="font-size:11px;">
		(Only for housekeeping Department)
		</span>
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp19">
	<option value="">Not Applicable</option>
		<option value="9">9 Hours Shift Rule</option>
		<option value="PA">Only Present & Absent</option>
	</select>
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Department Type
				<br/>
		<span style="font-size:11px;">
(For hold salary)
</span>
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp20">
		<option value="Marketing">Marketing</option>
		<option value="Housekeeping">Housekeeping</option>
		<option value="Backoffice">Backoffice</option>
		<option value="Insurance">Swastika Insurance</option>
		<option value="Securities">Swastika Securities</option>
		<option value="Cash">Cash</option>
		<option value="Other">Other</option>
	</select>

	</div>	

</div>


<div class="row">
	<div class="col-sm-2 formLeft">
		PF Deduction
	</div>
	<div class="col-sm-2 formRight">
	<select class="inputBox" id="inp21" onchange="if(this.value == '0') { $('#pfyes').hide();} else {

		 $('#pfyes').show(); }">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>

	</div>	
	<div class="col-sm-2 formLeft">
		PT Deduction
	</div>
	<div class="col-sm-2 formRight">
	<select class="inputBox" id="inp32">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>

	</div>	
	<div class="col-sm-2 formLeft">
		Other Deduction
	</div>
	<div class="col-sm-2 formRight">
	<select class="inputBox" id="inp33">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>

	</div>	
</div>

<div id="pfyes" style="display:none">


<div class="row">
	<div class="col-sm-2 formLeft req" <?php if($row['pf'] == '0') echo "style='display:none'";?>>
		Default PF<br/>
		<span style="font-size:10px">(Only if specific amount)</span>
	</div>
	<div class="col-sm-10 formRight">
	<input type="text" class="inputBox" id="inp31" value=""  name="">
	</div>	
</div>

<div class="row" >
	<div class="col-sm-2 formLeft">
		Name
						<br/>
		<span style="font-size:11px;">
As per aadhar
</span>

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" id="inp22" class="inputBox"  style="border-left:2px #d05419 solid !important;">

	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		Aadhar No.

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" id="inp23" class="inputBox"  style="border-left:2px #d05419 solid !important;" >

	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		DOB
						<br/>
		<span style="font-size:11px;">
As per aadhar
</span>

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" id="inp24" class="inputBox"  style="border-left:2px #d05419 solid !important;" >

	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		Father Name
						<br/>
		<span style="font-size:11px;">
As per aadhar
</span>

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" id="inp25" class="inputBox"  style="border-left:2px #d05419 solid !important;" >

	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		UN Number

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" id="inp26" class="inputBox" >

	</div>	
</div>

</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		Bank Account #

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" title="" name="req" id="inp27" class="inputBox" >

	</div>	
</div>




<div class="row" >
	<div class="col-sm-2 formLeft">
		Beneficiary Name

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" name="req"  title="" id="inp28" class="inputBox" >

	</div>	
</div>





<div class="row" >
	<div class="col-sm-2 formLeft">
		IFSC Code

	</div>
	<div class="col-sm-10 formRight">
	<input type="text" name="req"  title="" id="inp29" class="inputBox" >

	</div>	
</div>



<div class="row">
	<div class="col-sm-2 formLeft">
		Is ICICI Bank
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="inp30">
		<option value="I">YES</option>
		<option value="N">NO</option>
	</select>

	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Profile
		<br/>
		
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox selectInput34border" id="inp34">
		<option value="0">--Please select profile--</option>
	<?php
		$profilequery = mysqli_query($con,"select * from profiles");

	while($row1=mysqli_fetch_array($profilequery))
		{

		?>
		<option <?php if($row['profileid'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
	</div>	


	<div class="col-sm-2 formLeft req">
		Levels
		
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp39">
		<?php $query = mysqli_query($con,"select * from levels");
		while($result = mysqli_fetch_array($query)){
			?>
		
	<option <?php if($row['levelid'] == $result['id']) echo "selected='selected'";?> value="<?php echo $result['id']?>"><?php echo $result['name']?></option>
		
	<?php } ?>
	</select>
	</div>	

</div>





<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp13" style="width:100%;height:150px;"></textarea>
	</div>	


</div>


<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="saveNewEmployee('<?php echo $saveurl;?>','<?php echo $callbackurl;?>')" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			<br/><br/><br/>
	</div>	


</div>





</div>



<br />
<br />
<br />
<br />
<br />
