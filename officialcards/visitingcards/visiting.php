<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Visiting Card</li>
</ol>
</section>
<!-- Main content -->
<section class="content">

            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" data-toggle="modal" data-target="#myModal11">Apply for new visiting cards</span>           
</div>  
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Deleted successfully..!
</div>   <?php
}
            ?>      
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Employee Name</th>            
<th>Designation</th>            
<th>Email</th>            
<th>Mobile</th>            
<th>Branch Address</th>            
<th>Quantity</th>            
<th>Office Landline no.</th>            
<th>Create date</th>
<th>Status</th>
<th>Action</th> 
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../../include/config.php';
$query = mysqli_query($con,"SELECT  v.id,v.status,v.createdate,v.officialnumber,v.quantity,e.name,e.mobile,e.workemail,e.designation,b.name as bname,b.notes , d.name as dname FROM visitingcard v INNER JOIN employee e ON e.id = v.empid INNER JOIN branch b ON b.id = e.branch INNER JOIN designation d ON d.id = e.designation where e.id='$loggeduserid'");
while($row=mysqli_fetch_array($query)){


	if($row['status']==0){
		$status = 'pending';
	}else if($row['status']==1){
		$status = 'Approve';
	}else{
		$status='Rejected';
	}
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['name'];?></td>
<td><?php echo $row['dname'];?></td>                 
<td><?php echo $row['workemail'];?></td>                 
<td><?php echo $row['mobile'];?></td>                 
<td><?php echo $row['notes'];?></td>                 
<td><?php echo $row['quantity'];?></td>                 
<td><?php echo $row['officialnumber'];?></td>                 
<td><?php echo $row['createdate'];?></td> 
<td><span style="background-color: orange;color: white;"><?php echo $status ?></span></td>                 
<td><a onclick="getModule('officialcards/visitingcards/edit-visitingcard.php?id=<?php echo $row['id'];?>','tableDiv','formDiv','loading');"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;<a href="officialcards/visitingcards/delete.php?id=<?php echo $row['id'];?>&message=delete" onclick="return confirm('Are you sure want to delete?')"><i class="fa fa-trash" style="color: red;"></i></a>
<!-- Modal -->
<div class="modal fade" id="myModal55" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Policy Update</h4>
</div>
<form action="Policy/save.php" method="POST" enctype="multipart/form-data">
<div class="modal-body">
	
<div class="row">
	
<div class="col-md-6">
<label>Policy Name</label>
<input type="hidden" name="name" id="policyid">
<input type="text" name="policyname" class="form-control" id="policyname" placeholder="Enter policy name" required="">
</div>
<div class="col-md-6">
	<label>Policy File</label>
<input type="file" class="form-control" name="policyfile" id="policyfile"> 
</div>
</div>

</div>
<div class="modal-footer" >
<button type="submit"  class="btn btn-success">Submit</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</form>
</div>  
</form>
</div>
</div>
</td>

</tr> 
<?php
}
?>                          

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">visiting cards</h4>
</div>
<form action="officialcards/visitingcards/save.php" method="POST" enctype="multipart/form-data">
<div class="modal-body">
	
<div class="row">
	
<div class="col-md-6">
<label>Quantity</label>
<input type="text" name="quantity" class="form-control" id="quantity" placeholder="enter quantity" required="">
</div>
<div class="col-md-6">
	<label>Office Landline Number</label>
<input type="text" class="form-control" name="number" id="number" placeholder="enter number"  required /> 
</div>
</div>


</div>
<div class="modal-footer" >
<button type="submit"  class="btn btn-success">Submit</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</form>
</div>  

</div>  


