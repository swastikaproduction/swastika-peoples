<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Benificiary extends Model {

    //
    protected $table = 'expensebenificiary';

    public $timestamps = false;

}
