<?php
include("../include/config.php");

$branches = Array();
$branches = getData('branch','*','name','ASC');	


$departments = Array();
$departments = getData('departments','*','name','ASC');	


$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>
<div class="moduleHead">
<br/>
Salary Addons
</div>
</div>
<div class="tabelContainer shadow" style="height:auto;background:#fff">

<div class="row">
<div class="col-sm-12 subHead">

<?php
	echo "Select Filters";
	$type = $_GET['type'];
?>
	
</div>

	<div class="col-sm-2 formLeft">
		Employee Id
	</div>
	<div class="col-sm-10 formRight">
	<input type="text" class="inputBox" id="atEmpid" value="" >
</div>

	<div class="col-sm-2 formLeft">
		Branch
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atEmp">
	
	<?php
	foreach($branches as $val)
	{
		?>
			<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
			<?php
	}
	?>
		</select>
	</div>

	<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Department
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atDept">

	<?php
	foreach($departments as $val)
	{
		?>
			<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
			<?php
	}
	?>
		</select>
	</div>	

</div>


<div class="row" style="border-bottom: 1px #eee solid;">
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
<option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
		<br/>

<button  class="btn btn-primary" id="atListViewHere"  onclick="getModule('salaryaddons/getEmployees.php?type=<?php echo $type;?>&branch='+$('#atEmp').val()+'&dept='+$('#atDept').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val()+'&empid='+$('#atEmpid').val(),'atResult','','loading');atchangedone = 0;">
		<i class="fa fa-th-list"></i>&nbsp;&nbsp;GET DETAILS</button>


		

		<br/><br/>
	</div>
</div>

<div id="atResult"></div>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>