
  <?php
  include'../include/config.php';
  $id = $_GET['id'];
  
 $query = mysqli_query($con,"select * from documents where id='$id'")  or die(mysqli_error($con));
 $result = mysqli_fetch_array($query);
 $type = $result['type'];
 if($type==3){
    $name = 'Updated CV';
  
  }else if($type==4){
      $name = 'Duplicate Copy Of Offer Letter';
  }else if($type==5){
      $name = 'References 1 (HR & Reporting Head)';
  }else if($type==6){
      $name = 'References 2 (HR & Reporting Head)';
  }else if($type==7){
      $name = 'SSC Board Mark Sheet';
  }else if($type==8){
      $name = 'HSC Board Mark Sheet';
  }else if($type==9){
      $name = 'Degree Certificate & Mark Sheet';
  }else if($type==10){
      $name = 'Aadhar Card';
  }else if($type==11){
      $name = 'Pan Card';
  }else if($type==12){
      $name = 'Residential Address Proof';
  }else if($type==13){
      $name = 'Last 03 Months Salary Slip (1)';
  }else if($type==14){
      $name = 'Last 03 Months Salary Slip (2)';
  }else if($type==15){
      $name = 'Last 03 Months Salary Slip (3)';
  }else if($type==16){
      $name = 'Relieving Letter';
  }else if($type==17){
      $name = 'Bank Details';
  }else if($type==18){
      $name = 'Last 03 Months Bank Statement (2)';
  }else if($type==19){
      $name = 'Last 03 Months Bank Statement (3)';
  }else if($type==20){
      $name = 'Passbook of Existing Bank Account';
  }
  ?>
       
   <h3 class="box-title">Update Documents</h3>
    <!-- Main content -->
    <section class="content" style="margin-top: 30px;background: white;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="EmployeeDocuments/update-documents.php" role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputEmail1" style="/*font-weight: 700; */" >Documnets Name</label>
                  <input type="hidden" name="docid" value="<?php echo $result['id'];?>">
                  <input type="hidden" name="empid" value="<?php echo $result['dataid'];?>">
                  <input type="hidden" name="typeid" value="<?php echo $type;?>">
                  <input type="text" class="form-control" name="type" value="<?php echo $name;?>" readonly>
                </div>
              </div>
              <div class="col-md-4">	
                <div class="form-group">
                  <label for="exampleInputEmail1">Document File</label>
                 <input type="file" class="form-control" name="docfile">
                </div>
              </div>
                  
            
            </div>     
                                     
            

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" >Update</button>
              
              </div>
            </form>
          </div>           
                
                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
