<?php
include("../include/config.php");
$ids = $_POST['ids'];
$emps = $_POST['emps'];
mysqli_query($con,"UPDATE `attendancelog` SET `released` = '1' WHERE `id` IN ($ids)");
$ids = explode(",",$ids);
$emps = explode(",",$emps);

$faclass = "fa fa-rupee";
$module = 'salary';
$text = "Salary Released: Your salary for last month has been released. Click to view salary slip.";
foreach($ids as $key => $val)
{
	if($val != 0 && $val != '0')
	{
		$url = 'salarycalculation/salary_slip.php?id='.$val;
		$userid = $emps[$key];
		save_notification($text,$url,$faclass,$module,$userid);
	}
}

?>