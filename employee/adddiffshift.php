<?php
include("../include/config.php");
$id = $_GET['id'];
?>
<div class="moduleHead">
	<div class="moduleHeading" onclick="toogleFormTableModalBig();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Temporary Shifts
	</div>
</div>

<div class="shadow" style="background:#fff;">
	
<div class="row" style="background:">
<div class="col-sm-12 subHead">Add New Temporary Shift</div>
	<div class="col-sm-2 formLeft req">
		Fromdate
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="" id="tsht0" class="inputBox" >
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Todate
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="" id="tsht1" class="inputBox" >
	</div>	

</div>
<div class="row">
	<div class="col-sm-12">
	<br/>
	<center>
		<button class="btn btn-sm btn-success" onclick="getModule('employee/getrange.php?empid=<?php echo $id;?>&from='+document.getElementById('tsht0').value+'&to='+document.getElementById('tsht1').value,'dateRange','','loading');$('#saveButton').show();">GET DATE RANGE</button>
</center>
<br/>
<br/>
		<div id="dateRange"></div>

	</div>
</div>

<div class="row" style="background:#f7f8f8">
	<div class="col-sm-2 formLeft req">
		Shift
	</div>
	<div class="col-sm-10 formRight">
<select class="inputBox" id="tsht2" name="req">
		
		<?php
		$data = getData('shift','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option <?php if($row['shift'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	</div>	


</div>


<div class="row" style="display:none" id="saveButton">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('employee/savediffshift.php?id=<?php echo $id;?>','','formModalBig','tsht',4,'','url:employee/diffshift.php?id=<?php echo $id;?>','tableModalBig','formModalBig');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			<br/><br/><br/>
	</div>	


</div>

</div>