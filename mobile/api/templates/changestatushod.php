<?php
include("../apiconfig.php");
$id = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `leaverequests` WHERE `id` = '$id'") or die(mysqli_error($con));
$row= mysqli_fetch_array($getData);
$empid = $row['empid'];

$rtype = Array();
$rtype[1] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Leave Request</span><br/><span style="color:#222">'.date("d-M-y",strtotime($row['fromdate']))." to ".date("d-M-y",strtotime($row['todate'])).'</span>';
$rtype[2] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Timing Change Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';
$rtype[3] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Overtime Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';

$rtype[4] = '<span class="text-primary" style="font-weight:400;font-size:20px;">OD Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';

$rtype[5] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Halfday Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';



?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Status</h4>
      </div>
      <div class="modal-body">

	<div style="float:right;margin-top:20px">
		<?php 
	if($row['hodapproval'] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row['hodapproval'] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row['hodapproval'] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
	</div>

	<div class="subHead" style="padding-left:5px">
	<span style="font-size:15px;">
		<?php echo $rtype[$row['type']];?>
		</div>
		
		</span>
		<br/><br/>
		<div style="background:#f7f8f8;padding:20px;">
<?php echo $row['reason'];?>
		</div>
		<br/>



		Request Status
		<br/>
		<select <?php if($row['hodapproval'] == '1') echo "disabled='true'";?> class="input" id="hodapproval" style="margin-top: 5px !important;
    font-size: 15px !important;
    padding: 10px !important;
    height: 50px;
    border: 1px #eee solid;">
		<option value="">Select Status</option>
			<option <?php if($row['hodapproval'] == '1') echo "selected='selected'";?> value="1">Approved</option>
			<option <?php if($row['hodapproval'] == '2') echo "selected='selected'";?>  value="2">Rejected</option>
		</select>
		<br/><br/>


<?php
if($row['type'] == '4')
{
	?>

<button class="btn btn-primary" onclick="window.open('https://www.google.co.in/maps?q=<?php echo $row['lat'];?>,<?php echo $row['lng'];?>','_blank')">
	<i class="fa fa-map-marker"></i>&nbsp;VIEW ATTACHED LOCATION
</button>
<br/>
<br/>
<button class="btn btn-primary" onclick="$('#attpic').slideDown();">
	<i class="fa fa-image"></i>&nbsp;VIEW ATTACHED PICTURE
</button>
<br/><br/>
<div id="attpic" style="display:none">
<?php
$thispic = str_ireplace('../../../', '', $row['pic']);
?>
<img src="https://www.schoolbirdapp.com/shrm/hrm/<?php echo $thispic;?>" style="width:100%"/>
</div>
<br/><br/>
	<?php
} 
?>



	
		<br/>
		Remark (Optional)
		<br/>
		<textarea class="inputBox" id="hodreason"  <?php if($row['hodapproval'] == '1') echo "readonly='true'";?>   style="height:140px;margin-top:5px;width:100%;padding:10px;border:1px #eee solid !important"><?php echo $row['hodreason'];?></textarea>
		<br/>
		<br/>
		<?php
		if($row['hodapproval'] != '1')
		{
?>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="genAjax(baseUrl+'templates/savestatushod.php?id=<?php echo $id;?>&hodreason='+document.getElementById('hodreason').value+'&hodapproval='+document.getElementById('hodapproval').value,'',function(){ $('#myModal').modal('hide'); showToast('Status Updated') }); goBackToPage();" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<div id="resp">
				
			</div>
<?php			
		}
		else
		{
			?>
<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="showToast('Request already approved. You cannot change an approved request','toast bg-danger','5000')" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<?php
		}
		?>
			<br/><br/><br/>





			      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
