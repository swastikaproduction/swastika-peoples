<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

    //
    protected $table = 'expense';

    public $timestamps = false;

}
