<?php
include('../include/config.php');
$type = $_GET['type'];
$invoiceArray = Array();
$paymentArray = Array();

if($type == 'thisfis')
{
  $from = date("Y")."-04-01 00:00:00";
  $to = (date("Y",(time() + 24 * 60 * 60 * 365)))."-03-31 23:59:59";
}
else if($type == 'prevfis')
{
  $from = (date("Y",(time() - 24 * 60 * 60 * 365)))."-04-01 00:00:00";
  $to = date("Y")."-03-31 23:59:59";
}
else
{
  $from = $_GET['from']." 00:00:00";
$to = $_GET['to']." 23:59:59";
}

function getMonthsInRange($startDate, $endDate) {
$months = array();
while (strtotime($startDate) <= strtotime($endDate)) {
    $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
    $startDate = date('d M Y', strtotime($startDate.
        '+ 1 month'));
}

return $months;
}

$monthRange = getMonthsInRange($from,$to);
$rangeStr = '';

foreach($monthRange as $val)
{
  $str = $val['year']."-".$val['month'];
  $range[] .= date("M-y",strtotime($str));
}

foreach($range as $val)
{
    $paymentArray[$val] = '0';
    $invoiceArray[$val] = '0';
    $rangeStr .= "'".$val."',";
}
  $rangeStr = substr($rangeStr,0,-1);


//$range = Array['Jan','','Jan','Jan','Jan','Jan','Jan','Jan',];



$k=0;
$getData = mysqli_query($con,"SELECT invoicedetails.quantity,invoices.date FROM invoicedetails,invoices WHERE invoices.id = invoicedetails.invoiceid AND  invoices.date BETWEEN '$from' AND '$to' ORDER BY invoices.date ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData[1]));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $invoiceArray[$thisMonth] = $invoiceArray[$thisMonth] + $rowData[0];
  }
  else
  {
    $invoiceArray[$thisMonth] = $rowData[0];
  }
   
}


$getData = mysqli_query($con,"SELECT deliverydetails.quantity,deliveries.createdate FROM deliverydetails,deliveries WHERE deliveries.id = deliverydetails.deliveryid AND  deliveries.createdate BETWEEN '$from' AND '$to' ORDER BY deliveries.createdate ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData[1]));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $paymentArray[$thisMonth] = $paymentArray[$thisMonth] + $rowData[0];
  }
  else
  {
    $paymentArray[$thisMonth] = $rowData[0];
  }
   
}

$saleStr = implode(",",$invoiceArray);
$paymentStr = implode(",",$paymentArray);


?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    #container {
	min-width: 310px;
	max-width: 800px;
	height: 400px;
	margin: 0 auto
}
  </style>

  <title>Delivery Chart</title>

  
</head>

<body>
  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container"></div>

  




<script type='text/javascript'>//<![CDATA[

Highcharts.chart('container', {

    title: {
        text: 'Delivery Chart'
    },

    subtitle: {
        text: 'Invoices Raised Quantity Vs Deliveries Made'
    },
    xAxis: {
        categories: [<?php echo $rangeStr;?>]
    },

    yAxis: {
        title: {
            text: 'Delivery Count'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    series: [{
        name: 'Invoices',
        data: [<?php echo $saleStr;?>]
    }, {
        name: 'Deliveries',
        data: [<?php echo $paymentStr;?>]
    }]

});
//]]> 

</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }
    window.parent.window.document.getElementById('delPreloader').style.display = 'none';
  window.parent.window.document.getElementById('customBoxDel').style.display = 'none'
</script>

</body>

</html>

