<div class="moduleHead">
<div class="moduleHeading">

Dashboard
</div>
</div>
<div class="" style="height:auto">

<div class="row">
	<div class="col-sm-3">
		<div class="dashBox" style="background: #57CFC7">
		<br/>
		
		<i class="fa fa-id-badge" style="font-size:50px;color:#fff;"></i>
		<br/><br/>
<div class="dashBoxBtnHolder">		<button class="btn btn-success btnroundbig" style="margin-top:10px;">TODAY'S ATTENDANCE</button></div>

			<div class="dashBoxInner" style="background: #22cabf">
				2534/2882

			</div>
		</div>
	</div>


	<div class="col-sm-3">
		<div class="dashBox" style="background: #FECB78">

<br/>
		
		<i class="fa fa-bus" style="font-size:50px;color:#fff;"></i>
		<br/><br/>
<div class="dashBoxBtnHolder">		<button class="btn btn-warning btnroundbig" style="margin-top:10px;">BUSES RUNNING</button></div>

			<div class="dashBoxInner" style="background: #fed766">58/65</div>
		</div>
	</div>


	<div class="col-sm-3">
		<div class="dashBox" style="background: #F38173">
<br/>
		
		<i class="fa fa-users" style="font-size:50px;color:#fff;"></i>
		<br/><br/>
<div class="dashBoxBtnHolder">		<button class="btn btn-danger btnroundbig" style="margin-top:10px;">TEACHERS'S LOGGED IN</button></div>
			<div class="dashBoxInner" style="background: #ee5744">121/146</div>
		</div>
	</div>


	<div class="col-sm-3">
		<div class="dashBox" style="background: #57CFC7">
		<br/>
		
		<i class="fa fa-microchip" style="font-size:50px;color:#fff;"></i>
		<br/><br/>
<div class="dashBoxBtnHolder">		<button class="btn btn-success btnroundbig" style="margin-top:10px;">ACTIVE CLASSES</button></div>
			<div class="dashBoxInner" style="background: #22cabf">132/145</div>
		</div>
	</div>

</div>

<br/><br/>



<div class="row">
	<div class="col-sm-9">
<div style="border:1px #ddd solid;padding:10px;">
	<iframe src="dashboard/area-chart.php" style="width:100%;height:400px;" frameborder="0" scrolling="no"></iframe>
</div>		
	</div>

	<div class="col-sm-3">
<div style="border:1px #ddd solid;padding:10px;">
	<iframe src="dashboard/bus-chart.php" style="width:100%;height:400px;" frameborder="0" scrolling="no"></iframe>
</div>		
		

	</div>
</div>
<br/><br/>

<div class="row">
	<div class="row">
		<div class="col-sm-9">
			<table class="table table-striped table-hover fetchDash noShadow" cellpadding="0" cellspacing="0" id="tableDiv">
			
                                                    <thead class="">
                                                        <tr>
                                                            <th class="">Student Id</th>
                                                            <th class=" hidden-xs">Intime</th>
                                                            <th class="">Out Time</th>
                                                            <th class="">Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="wp25">#4551</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">in progress</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4532</td>
                                                            <td class="hidden-xs">23.12.2015</td>
                                                            <td>11.01.2016</td>
                                                            <td><span class="label label-primary">Done</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4536</td>
                                                            <td class="hidden-xs">16.05.2015</td>
                                                            <td>18.06.2015</td>
                                                            <td><span class="label label-danger">Failed</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4552</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">In Progress</span></td>
                                                        </tr>



      <tr>
                                                            <td class="wp25">#4551</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">in progress</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4532</td>
                                                            <td class="hidden-xs">23.12.2015</td>
                                                            <td>11.01.2016</td>
                                                            <td><span class="label label-primary">Done</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4536</td>
                                                            <td class="hidden-xs">16.05.2015</td>
                                                            <td>18.06.2015</td>
                                                            <td><span class="label label-danger">Failed</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4552</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">In Progress</span></td>
                                                        </tr>

   <tr>
                                                            <td class="wp25">#4551</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">in progress</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4532</td>
                                                            <td class="hidden-xs">23.12.2015</td>
                                                            <td>11.01.2016</td>
                                                            <td><span class="label label-primary">Done</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4536</td>
                                                            <td class="hidden-xs">16.05.2015</td>
                                                            <td>18.06.2015</td>
                                                            <td><span class="label label-danger">Failed</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">#4552</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">In Progress</span></td>
                                                        </tr>

    <tr>
                                                            <td class="">#4552</td>
                                                            <td class="hidden-xs">10.02.2016</td>
                                                            <td>10.02.2016</td>
                                                            <td><span class="label label-info">In Progress</span></td>
                                                        </tr>
                                                    </tbody>
                                                
			</table>
		</div>

		<div class="col-sm-3">
			<div style="background:#2e4c56;padding:8px 10px;color:#fff;font-weight:bold">XYZ Statistics</div>
			<div style="border:1px #ddd solid;padding:20px;">

				
			<div class="barUpper">
					<div class="barInner" style="background:#ee5744;width:45%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#ee5744"></i>&nbsp;&nbsp;Yahoo 45%

				<br/>
				<br/>
				<div class="barUpper">
					<div class="barInner" style="background:#22cabf;width:82%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;Google 82%
				<br/>
				<br/>
								<div class="barUpper">
					<div class="barInner" style="background:#ffca4c;width:29%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#ffca4c"></i>&nbsp;&nbsp;Bing 29%
<br/>
				<br/>

		<div class="barUpper">
					<div class="barInner" style="background:#ee5744;width:45%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#ee5744"></i>&nbsp;&nbsp;Yahoo 45%

				<br/>
				<br/>
				<div class="barUpper">
					<div class="barInner" style="background:#22cabf;width:82%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;Google 82%
				<br/>
				<br/>
								<div class="barUpper">
					<div class="barInner" style="background:#ffca4c;width:29%"></div>
				</div>
				<br/>
				<i class="fa fa-circle" style="color:#ffca4c"></i>&nbsp;&nbsp;Bing 29%
			</div>
		</div>
	</div>
</div>


<br/><br/>



<div class="row">
	<div class="col-sm-9">
<div style="border:1px #ddd solid;padding:10px;">
	<iframe src="dashboard/attendance-chart.php" style="width:100%;height:400px;" frameborder="0" scrolling="no"></iframe>
</div>		
	</div>

	<div class="col-sm-3">

<table class="table table-striped table-hover fetchDash noShadow" cellpadding="0" cellspacing="0">
      <tbody><tr>
        <th colspan="2">Latest Parent Message</th>
      </tr>
      <tr>
        <td>
          <strong style="font-size:20px;">Mr. Ashok Sharma</strong>
          <br>
          <span style="font-size:11px;">Sanyam Sharma (2nd B)</span>
          <br>
          I need to get report card again
        </td>
      </tr>
       <tr>
        <td>
          <strong style="font-size:20px;">Mr. Vaibhav Agarwal</strong>
          <br>
          <span style="font-size:11px;">Aadhya Agarwal (4th C)</span>
          <br>
          Can we concentrate more on his maths?
        </td>
      </tr>
      <tr>
        <td>
          <strong style="font-size:20px;">Mr. Ashok Sharma</strong>
          <br>
          <span style="font-size:11px;">Sanyam Sharma (2nd B)</span>
          <br>
          I need to get report card again
        </td>
      </tr>
       <tr>
        <td>
          <strong style="font-size:20px;">Mr. Vaibhav Agarwal</strong>
          <br>
          <span style="font-size:11px;">Aadhya Agarwal (4th C)</span>
          <br>
          Can we concentrate more on his maths?
        </td>
      </tr>
    </tbody></table>
	
		

	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<div style="border:1px #ddd solid;padding:10px;">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3578.9341438278575!2d78.16730165094424!3d26.23132929532212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3976c68ff0a2df73%3A0xebff42167ed98c34!2sGwalior+Fort!5e0!3m2!1sen!2sin!4v1499942972431" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>		

	</div>
</div>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>