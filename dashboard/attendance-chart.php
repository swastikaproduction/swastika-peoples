<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Area Chart</title>
  <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">

  <style type="text/css">
    #container11 {
    min-width: 310px;
    max-width: 800px;
    height: 400px;
    margin: 0 auto
}
  </style>

  <title>Highcharts Demo</title>

  
</head>

<body>
  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container"></div>

  




<script type='text/javascript'>//<![CDATA[

Highcharts.chart('container', {

    title: {
        text: 'Class Wise Attendance'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Attendance %'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            pointStart: 2017-03-04
        }
    },

    series: [{
        name: 'Class 1',
        data: [98,97,99,100,94],
        color:'#ee5744'
    }, {
        name: 'Class 2',
        data: [92,95,97,98,94],
        color:'#22cabf'
    }, {
        name: 'Class 3',
        data: [96,75,88,82,91],
        color:'#ffca4c'
    }, {
        name: 'Class 4',
        data: [99,99,93,94,85],
        color:'#3081b7'
    }, {
        name: 'Class 5',
        data: [87,91,86,98,94],
        color:'#2e4c56'
    }]

});
//]]> 

</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }
</script>

</body>

</html>

