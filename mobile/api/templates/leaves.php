<?php
include("../apiconfig.php");


?>
<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">
<div class="materialButton atPurple" style="color:#fff"  onclick="changePage('viewThree','templates/viewThree.html','templates/new-request.php','viewThreeRes','','strong','darkHeader');globeTitle = ''; globeTitleId = 'viewthreetitle'">
	<i class="fa fa-plus"></i>
</div>

<button class="btn btn-sm btn-primary" style="color:#fff;position:absolute;bottom:15px;left:15px;z-index:20000"  onclick="openofflineod()">
	Offline Od
</button>



<div style="padding:0px;position:relative">

<table style="width:100%">
	<tr>
		<td id="myreqbutton" style="padding:20px 10px;text-align:left;border-bottom:1px #eee solid;background:#ecf2fa;color:#222;width:50%;text-transform:uppercase" onclick="$('#myreq').show();$('#pendreq').hide();$('#myreqbutton').animate({backgroundColor:'#ecf2fa'});$('#pendreqbutton').animate({backgroundColor:'#fff'});">
		My Requests
		</td>
		<td id="pendreqbutton"  style="padding:20px 10px;;text-align:left;border-bottom:1px #eee solid;background:#fff;color:#222;width:50%;text-transform:uppercase" onclick="$('#myreq').hide();$('#pendreq').show();$('#pendreqbutton').animate({backgroundColor:'#ecf2fa'});$('#myreqbutton').animate({backgroundColor:'#fff'});">
		Pending Approvals
		</td>
	</tr>
</table>

<div id="myreq">
	
<?php
$getData = mysqli_query($con,"SELECT * FROM `leaverequests` WHERE `empid` = '$loggeduserid' ORDER BY `id` DESC") or die(mysqli_error($con));
$color[1] = '#e13192';
$color[2] = '#6db3f1';
$color[3] = '#ffcf00';
$color[4] = '#d61212';


$name[1] = 'Leave';
$name[2] = 'Different Timing';
$name[3] = 'Overtime';
$name[4] = 'OD Request';

$status[0] = '<span class="label label-default">HR: STATUS NOT UPDATED</span>';
$status[1] = '<span class="label label-success">HR: APPROVED</span>';
$status[2] = '<span class="label label-danger">HR: REJECTED</span>';

$hodstatus[0] = '<span class="label label-default">HOD: STATUS NOT UPDATED</span>';
$hodstatus[1] = '<span class="label label-success">HOD: APPROVED</span>';
$hodstatus[2] = '<span class="label label-danger">HOD: REJECTED</span>';


if(mysqli_num_rows($getData) == 0)
{
?>
<center>
	<br><br><br><br>
	<i class="fa fa-calendar-check-o" style="font-size:40px;color:#ccc"></i>
	<br>
	<br>
	<span style="color:#ccc">You have not made any leave requests yet.<br>
	Sit back and relax.</span>
</center>
<?php
}
else
{
?>




<div class="" style="height:auto;background:#fff;z-index:10">


<?php
while($row = mysqli_fetch_array($getData))
{
	?>

<div class="calStrip" style="padding:15px 10px;border-bottom:1px #eee solid;">
<table style="width:100%">
	<tr>
		<td style="text-align:left">
	<span style="font-size:16px;font-weight:400;text-transform:capitalize">
		<?php echo $name[$row['type']];?>	</span>
		<div class="halfBorder" style="background:<?php echo $color[$row['type']];?>"></div>
	
<div style="margin:20px 0px;">
<?php
if($row['type'] == 1)
{
	?>

<span class="label label-white">
	<?php echo date("D, d M Y",strtotime($row[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white"><?php echo date("D, d M Y",strtotime($row[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-white">
	<?php echo date("h:i A",strtotime($row['intime']));?> </span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white"><?php echo date("h:i A",strtotime($row['outime']));?></span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white">
<?php echo date("D, d M Y",strtotime($row['ondate']));?>
</span>



	<?php
}
?>
<br/>
</div>
<div style="float:right">
<span style="font-size:12px;">
		<?php echo date("D, d-M h:i A",strtotime($row['createdate']));?></span>	
</div>
<?php echo $row['reason'];?>
<br/>
<br/>
<div style="text-align:left" onclick="displayModal('templates/request-reason.php?id=<?php echo $row['id'];?>')">
<?php
echo $status[$row['status']];
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo $hodstatus[$row['hodapproval']];
?>
	
</div>

</div>
</td>
	</tr>
</tbody></table>


</div>

	<?php
}
?>
</div>
<?php
}
?>




</div>
<div id="pendreq" style="display:none">
	
	
<?php

$empArray = Array();
$getEmployees = mysqli_query($con,"SELECT * FROM `employee`") or die(mysqli_error($con));
while($rowEmployees = mysqli_fetch_array($getEmployees))
{
	$empArray[$rowEmployees['id']] = $rowEmployees['name'];
}

$getData = mysqli_query($con,"SELECT * FROM `leaverequests` WHERE `empid` IN (SELECT `id` FROM `employee` WHERE `imdboss` = '$loggeduserid') ORDER BY `id` DESC") or die(mysqli_error($con));

if(mysqli_num_rows($getData) == 0)
{
?>
<center>
	<br><br><br><br>
	<i class="fa fa-calendar-check-o" style="font-size:40px;color:#ccc"></i>
	<br>
	<br>
	<span style="color:#ccc">You dont have any requests to approve.<br>
	Sit back and relax.</span>
</center>
<?php
}
else
{
?>




<div class="" style="height:auto;background:#fff;z-index:10">


<?php
while($row = mysqli_fetch_array($getData))
{
	?>

<div class="calStrip" style="padding:15px 10px;border-bottom:1px #eee solid;">
<table style="width:100%">
	<tr>
		<td style="text-align:left">

<div style="font-size:20px;font-weight:bold;">
	
	<?php echo $empArray[$row['empid']];?>
</div>



	<span style="font-size:13px;font-weight:400;text-transform:capitalize">
		Requested <?php echo $name[$row['type']];?>	</span>
		<div class="halfBorder" style="background:<?php echo $color[$row['type']];?>"></div>
	
<div style="margin:20px 0px;">
<?php
if($row['type'] == 1)
{
	?>

<span class="label label-white">
	<?php echo date("D, d M Y",strtotime($row[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white"><?php echo date("D, d M Y",strtotime($row[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-white">
	<?php echo date("h:i A",strtotime($row['intime']));?> </span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white"><?php echo date("h:i A",strtotime($row['outime']));?></span><div style="display:inline-block;vertical-align:middle;height:1px;width:30px;margin:10px 0px;background:#1976d2;"></div><span class="label label-white">
<?php echo date("D, d M Y",strtotime($row['ondate']));?>
</span>



	<?php
}
?>
<br/>
</div>
<div style="float:right">
<span style="font-size:12px;">
		<?php echo date("D, d-M h:i A",strtotime($row['createdate']));?></span>	
</div>
<?php echo $row['reason'];?>

<br/>
<br/>
<div style="text-align:left" onclick="displayModal('templates/changestatushod.php?id=<?php echo $row['id'];?>')">
<?php
echo str_ireplace("STATUS NOT UPDATED","CLICK TO UPDATE",$status[$row['status']]);
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo str_ireplace("STATUS NOT UPDATED","CLICK TO UPDATE",$hodstatus[$row['hodapproval']]);
?>
	
</div>
</div>
</td>
	</tr>
</tbody></table>


</div>

	<?php
}
?>
</div>
<?php
}
?>



</div>

		

</div>
<br/><br/><br/><br/><br/><br/>
</div>

</div>

