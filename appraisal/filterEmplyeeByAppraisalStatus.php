<?php
    include'../include/config.php';	
	$status = $_POST['status'];
	    //Do real escaping here
	$query = "SELECT e.id,e.username,e.name,e.mobile,a.created_at ,a.appraised_mode,a.appraised_time,a.appraised_year,au.status FROM appraisalusers au INNER JOIN appraisals a ON a.id = au.appId INNER JOIN employee e ON e.id = au.empId INNER JOIN appraisalstart ast ON ast.appId = a.id";
	$conditions = array();

    if(! empty($deptName)) {
	  $conditions[] = "e.department='$deptName'";
	}
	if(! empty($status)) {
	  $conditions[] = "au.status='$status'";
	}

	$sql = $query;
	if (count($conditions) > 0) {
	  $sql .= " WHERE " . implode(' AND ', $conditions);
	}	
	$result = mysqli_query($con,$sql);
	?>
  <div class="box-body">	
  	<div class="row">
  		<div class="col-md-12">
       <table border='2px;' class="table table-bordered table-striped">
        <thead style="background: #81b7ea;color: white;">
        <tr><td>S.NO</td><td>Name</td><td>Mobile</td><td>Appraisal Time</td><td>Create Date</td><td>Status</td><td>Action</td></tr></thead>
	<?php
    $count = 1;
	while($row = mysqli_fetch_array($result)){
	?>

	<tr>
          <td><?php echo $count++;?></td>
          <td><?php echo $row['name']?></td>
          <td><?php echo $row['mobile']?></td>          
          <td><?php echo $row['appraised_time'] ?> &nbsp;&nbsp;<?php echo $row['appraised_year'] ?></td>
          <td><?php echo $row['created_at']?></td>
          <td><span class="label label-warning"><?php echo $row['status']?></span></td>
         <td><span class="label label-success" onclick="sendEmailEmployee('<?php echo $row['id']?>');">Send Reminder</span></td>

        </tr>

  <?php
	}

	?>

      </table>
      </div>
  </div>
  </div>

    