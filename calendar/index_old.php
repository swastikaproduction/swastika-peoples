<?php
include("../include/config.php");

$newUrl= str_ireplace("index.php", "new.php",$urltocall);
	if(isset($_GET['monthStart']))
	{
		if($_GET['type'] == 'previous')
		{
			$currentStart = $_GET['monthStart'];
			$lastStart = strtotime($currentStart) - (60*60*24*10);
			$month = date("m",$lastStart);
		 $monthStart = date("Y",$lastStart)."-".$month."-01";

		}
		else if($_GET['type'] == 'next')
		{
			$currentStart = $_GET['monthStart'];
			$lastStart = strtotime($currentStart) + (60*60*24*32);
			$month = date("m",$lastStart);
		 echo $monthStart = date("Y",$lastStart)."-".$month."-01";

		}
	}
	else
	{
		$month = date("m");
		$monthStart = date("Y")."-".$month."-01";
	}

$day = date("D",strtotime($monthStart));
$weekdiffarray = Array();
$weekdiffarray['Sun'] = '0';
$weekdiffarray['Mon'] = '1';
$weekdiffarray['Tue'] = '2';
$weekdiffarray['Wed'] = '3';
$weekdiffarray['Thu'] = '4';
$weekdiffarray['Fri'] = '5';
$weekdiffarray['Sat'] = '6';


 $temp = strtotime($monthStart);

 $temp = $temp - (24 * 60 * 60 * $weekdiffarray[$day]);
 $startdate = date("Y-m-d",$temp);

 $fourtyone = strtotime($startdate)+(60*60*24*41);
 $endate = date("Y-m-d",$fourtyone);

$alreadyArray = Array();
$checkAlready = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$startdate' AND '$endate' ORDER BY `date` ASC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($checkAlready))
{
	$alreadyArray[] .= $row['date'];
	$alreadyArray[$row['date']]['type'] =  $row['type'];
	$alreadyArray[$row['date']]['name'] =  $row['name'];
	$alreadyArray[$row['date']]['id'] =  $row['id'];
}




?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<button class="btn btn-sm btn-danger"  onclick="crossCheckDelete('calendar')">DELETE SELECTED</button>


</div>
<div class="moduleHeading">
Calendar
</div>
</div>


<div class="row">
	<div class="col-sm-6 shadow" style="background:#fff;padding:0px;">

	<?php
$caldata = getData("calendar","*","date","ASC");
	?>		
<table class="table table-hover" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:20px;">#</th>
<th style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th style="width:200px;">On Date</th>
<th style="width:200px;">Name</th>
<th>Type</th>

</tr>

<?php
$i=1;
foreach($caldata as $row)
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<input type="checkbox" class="checkInput" id="" name="" value="<?php echo $row['id'];?>">
</td>
<td><?php echo date("d-M-y",strtotime($row['date']));?></td>
<td><?php echo $row['name'];?></td>
<td>
<?php
if($row['type'] == '1')
{
	echo "Holiday";
}
else
{
	echo "Different Timing";
}
?>

</td>


</tr>

	<?php
	$i++;
}
?>
</table>
	</div>
	<div class="col-sm-6">
	
<div class="tabelContainer shadow" style="height:auto;background:#fff;">
<div class="bg-danger" style="padding:20px 0px;text-align:center;">
<div style="float:right;padding-top:6px;height:50px;width:60px;text-align:center;cursor:pointer" onclick="getModule('calendar/index.php?monthStart=<?php echo $monthStart;?>&type=next','tableDiv','formDiv','loading')">
<i class="fa fa-caret-right"></i>	
</div>

<div style="float:left;padding-top:6px;height:50px;width:60px;text-align:center;cursor:pointer" onclick="getModule('calendar/index.php?monthStart=<?php echo $monthStart;?>&type=previous','tableDiv','formDiv','loading')">
<i class="fa fa-caret-left"></i>	
</div>



	<span style="color:#fff;font-size:20px;"><?php echo date("M-Y",strtotime($monthStart));?></span>
</div>
<div style="border-bottom-left-radius:10px;border-bottom-right-radius:10px;" >
<table border="1" class="calendar">
	<tr>
		<th>SUN</th>
		<th>MON</th>
		<th>TUE</th>
		<th>WED</th>
		<th>THU</th>
		<th>FRI</th>
		<th>SAT</th>
	</tr>
	<?php



for($k=0;$k<42;$k++)
{
	$dataPop = '';
	if($k==0)
	{
		?>
<tr>
		<?php
	}
else if($k%7 == 0)
{
	?>
</tr><tr>
	<?php
}

if($month == date("m",strtotime($startdate)))
{
	if($startdate == date("Y-m-d"))
	{
		$class = 'today';
		$msg = 'T';
	}
	else
	{
		$class = 'current';		
	}
}
else
{
	$class = 'past';
}

if(date("D",strtotime($startdate)) == 'Sun')
{
	$class = "sunday";
}

$msg = "";

if(in_array($startdate,$alreadyArray))
{
	if($alreadyArray[$startdate]['type'] == '1')
	{
		$msg =  "H";
		$class= "holiday";
		$thisid = $alreadyArray[$startdate]['id'];
		$dataPop = 'onclick="getModal(\'calendar/date-details.php?id='.$thisid.'\',\'tableModalBig\',\'formModalBig\',\'loading\')"';
	}
	else
	{
		$msg =  "D";
		$class= "different";
		$thisid = $alreadyArray[$startdate]['id'];
		$dataPop = 'onclick="getModal(\'calendar/date-details.php?id='.$thisid.'\',\'tableModalBig\',\'formModalBig\',\'loading\')"';
	
	}
}




	?>
	<td class="<?php echo $class;?>" <?php echo $dataPop;?>>
	<span>
		<?php echo date("d",strtotime($startdate));

		?>
</span>
<?php echo $msg;;?>



	</td>

	<?php

		$t = strtotime($startdate);
		$t = $t+(60*60*24);
		$startdate = date("Y-m-d",$t);
}
	?>
	</tr>
</table>
</div>



</div>	

	</div>
</div>



<br/><br/><br/><br/><br/><br/><br/><br/><br/>