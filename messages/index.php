<?php
include("../include/config.php");
$empArray = Array();
$employees = getData('employee','*','name','ASC');
	foreach($employees as $val)
	{
		$empArray[$val['id']] = trim($val['name']);
	}
?>
<div class="moduleHead">
<br/>
<?php
if($loggeduserid == '124')
{
?>
<div style="float:right">
	<button class="btn btn-primary"  onclick="getModal('messages/new.php','formModalBig','tableModalBig','loading')">
		<i class="fa fa-edit"></i>&nbsp;&nbsp;New Message
	</button>
</div>
<div class="moduleHeading">
Messages
</div>
</div>

<?php	
}
?>

<div class="row shadow" style="background:#fff">
	<div class="col-sm-3" style="background:#fff;padding-right:0px;">
	<div class="subHead" style="margin-bottom:0px;padding:10px 0px !important">
	<i class="fa fa-search"></i>&nbsp;&nbsp;<input type="text" class="inputBox" style="border:0px !important;width:80%" placeholder="Search Here" name="" onkeyup="searchChat(event,this.value)">
	</div>
<div id="calStripContainer" style="height:300px;overflow-y:auto">
<?php
$toch1= $loggeduserid."_";
$toch2= "_".$loggeduserid;
$i=0;
$getData = mysqli_query($con,"SELECT * FROM `conversation` WHERE `userstring` LIKE '$toch1%' OR `userstring` LIKE '%$toch2' ORDER BY `status` ASC, `lastmessagetime` DESC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	$us = explode("_",$row['userstring']);
		if($us[0] == $loggeduserid)
		{
			$emp = $us[1];
		}
		else
		{
			$emp = $us[0];
		}


	?>

<div class="calStCIn" id="calStripContainer<?php echo $row['id'];?>" lang="<?php echo $emp;?>" title="<?php echo $empArray[$emp];?>" onclick="getModule('messages/details.php?id=<?php echo $row['id'];?>&emp=<?php echo $emp;?>&name=<?php echo $empArray[$emp];?>&winheight='+window.innerHeight,'msgBox','','loading');$('#readStatus<?php echo $row['id'];?>').hide();">
<div class="calStrip"  style="padding:15px 10px;" id="calStrip<?php echo $row['id'];?>">

<table>
	<tr>
		<td style="padding-right:10px;text-align:right">
			<div class="letterBox" id="letterBox<?php echo $i;?>">
				<?php
				$temp = explode(" ",$empArray[$emp]); 
				 echo $temp[0][0].$temp[1][0];
				 ?>
			</div>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
	<span class="label label-default">
			<?php echo date("d M, h:i:A",strtotime($row['lastmessagetime']));?> 
			<?php if($row['status'] == '0' && $row['to'] == $loggeduserid) 
				echo '<span id="readStatus'.$row['id'].'">&nbsp;&nbsp;<i class="fa fa-circle text-primary"></i></span>';
			?>
			</span>
	</div>	
	<span style="font-size:16px;font-weight:400">
		<?php 
		

echo $empArray[$emp];
		?>
	</span>
	<br/>
<div style="margin-top:5px">
<?php echo substr($row['lastmessage'],0,80);?>..
</div>
		</td>
	</tr>
</table>


	
</div>
</div>

	<?php
	$i++;
}
?>

</div>
	

	</div>
<div class="col-sm-9" style="background:#fff;padding:0px;border-left:1px #eee solid;">
<div id="msgBox">
	<center>
		<br/><br/><br/>
		<i class="fa fa-envelope" style="font-size:100px;color:#cfecfe"></i>
		<br/>
		<br/>
<?php
if($loggeduserid == '124')
{
?>

		<span style="font-size:20px;">

		Select a conversation from the left <br/>or start a <span class="text-primary" style="cursor:pointer;font-weight:400" onclick="getModal('messages/new.php','formModalBig','tableModalDiv','loading')">new one</span>
		</span>
		<?php
}
else
{
	?>
<span style="font-size:20px;">

		One-to-one Messages from administration will appear here.
		</span>
	<?php
}
		?>
		<br/><br/><br/>
	</center>
</div>
	</div>
</div>





<br/><br/><br/><br/>
<br/><br/>