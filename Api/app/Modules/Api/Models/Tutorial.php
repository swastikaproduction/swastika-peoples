<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model {

    //
    protected $table = 'tutorial';

    public $timestamps = false;

}