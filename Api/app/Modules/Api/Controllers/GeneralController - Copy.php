<?php
namespace App\Modules\Api\Controllers;
use Illuminate\Http\Request;
use App\Modules\Api\Models\Branch;
use App\Http\Controllers\Controller;
use App\Modules\Api\Models\Department;
use App\Modules\Api\Models\Designation;
use App\Modules\Api\Models\Shift;
use App\Modules\Api\Models\Users;
use App\Modules\Api\Models\Leave;
use App\Modules\Api\Models\Calendar;
use App\Modules\Api\Models\Messages;
use App\Modules\Api\Models\Documents;
use App\Modules\Api\Models\Tutorial;
use App\Modules\Api\Models\Policy;
use App\Modules\Api\Models\Notifications;
use Response,Helper,Validator,file,DB,Mail;

class GeneralController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        echo "sharad";
    }

// get all branches list here

    public function  GetBranch(Request $request){

        $branchId = $request->get('branchId'); 
      
        $vArray = array('branchId'=>'required');

        check_validation($request,$vArray);

    	$result=Branch::where('id',$branchId)->get();

      if(!empty($result)){
              
			$res['result']  =$result;
			$res['success'] = true;
			$res['code']    = 200;

        }

        return Response::json($res);     
    }



//get all department list here 

    public function  GetDepartments(Request $request){

        $deptId = $request->get('deptId'); 
      
        $vArray = array('deptId'=>'required');

        check_validation($request,$vArray);

        $result=Department::where('id',$deptId)->get();

        if(!empty($result)){
              
            $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;

        }

        return Response::json($res);     
    }

   
 //get designation list here 

  public function GetDesignation(Request $request){

       $desigId = $request->get('desigId');
       $vArray = array('desigId'=>'required');

       check_validation($request,$vArray);

       $result = Designation::where('id',$desigId)->first();

       if(!empty($desigId)){

            $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;
       }

       return Response::json($res);
  }  



// get shift detailes here

   public function GetShift(Request $request){

       $shiftId = $request->get('shiftId');

       $vArray = array('shiftId'=>'required');

       check_validation($request,$vArray);

       $result = Shift::where('id',$shiftId)->first();

       if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;
       }

       return Response::json($res);
  }  

//change password method here

  public function ChangePassword(Request $request){

          $currentPassword = $request->get('currentPassword');
          $NewPassword = $request->get('NewPassword');
          $userId = $request->get('userId');

          $vArray = array('currentPassword'=>'required','userId'=>'required','NewPassword'=>'required');

          check_validation($request,$vArray);

          $check = Users::where('id',$userId)->where('password',$currentPassword)->first();

          if(empty($check)){
          $res['message']  ='current password not match';         
          $res['success'] = false;
          $res['code']    = 401;

          }else{

          $update = Users::where('id',$userId)->update(['password'=>$NewPassword]);

          if($update){
          Users::where('id',$userId)->update(['passwordflag'=>0]);
          $res['message']  ='Password Updated Successfuly...!';
          $res['success'] = true;
          $res['code']    = 200;
          }
          }
    
    return Response::json($res);

  }

  //send puch  notifications using fire base

   public function sendMessage(Request $request){

       

       $result = file_get_contents('http://183.182.86.91:8088/hrm/appraisal/sendremindermailemployee.php?username=sharad&from=121515&to=4151');
       echo $result;
   }


   //get notifications

   public function GetNotification(Request $request){

       $userId = $request->get('userId');

       $vArray = array('userId'=>'required');

       check_validation($request,$vArray);

        $result =Notifications::where('userid',$userId)->orwhere('userid',0)->get();

         if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;
       }else{
            $res['result']  ='no data found';
            $res['success'] = false;
            $res['code']    = 401;
       }

       return Response::json($res);
   }

   //public method for get circulars

   public function GetCirculars(Request $request){

       $userId = $request->get('userId');

       $vArray = array('userId'=>'required');

       check_validation($request,$vArray);
   }

   //method for leave request

   public function LeaveRequest(Request $request){

           $empId = $request->get('empId');
           $type = $request->get('type');
           $fromDate = $request->get('fromDate');
           $toDate = $request->get('toDate');
           $inTime = $request->get('inTime');
           $outTime = $request->get('outTime');
           $lat = $request->get('lat');
           $long = $request->get('long');
           $reqFile = $request->file('pic');
           $remark = $request->get('remark');

           $checkemp = Users::where('id',$empId)->first();
           $empname = $checkemp['name'];

           if($type=='1'){
            $message  ='A new request is received from '.$empname;
           }else{
            $message  = 'A new request is received from '.$empname;
           }     

         
                
           
           $vArray = array('empId'=>'required','inTime'=>'required','outTime'=>'required','lat'=>'required','long'=>'required','type'=>'required','remark'=>'required');

           check_validation($request,$vArray);
      
             if(!empty($reqFile)){
              
              //$destinationPath = 'uploads';
              $destinationPath = '../od-files';

              $fileName = $reqFile->getClientOriginalName();

              $reqFile->move($destinationPath,$reqFile->getClientOriginalName());
             }else{
              $fileName = '';
             }            
           

           date_default_timezone_set('Asia/Kolkata');
           //$onDate= date('Y-m-d H:i');
           $onDate = $request->get('ondate');
           $insertArray = array('empid'=>$empId,
                                 'type'=>$type,
                                 'fromdate'=>$fromDate,
                                 'todate'=>$toDate,
                                 'intime'=>$inTime,
                                 'outime'=>$outTime,
                                 'ondate'=>$onDate,
                                 'lat'=>$lat,
                                 'lng'=>$long,
                                 'pic'=>$fileName,
                                 'remark'=>$remark,
                                 'status'=>0,
                                 'reason'=>$remark,
                                 'createdate'=>NOW(),
                                 'hodapproval'=>0,
                                 'hodreason'=>'',
                                 'pictime'=>NOW()
                               );

            $check = Leave::insert($insertArray);
            $getimdid = Users::where('id',$empId)->first();
            $imduserid = $getimdid['imdboss'];      

            //for imdboss
            $getimdinfo = Users::where('id',$imduserid)->first();
            $imdemail = $getimdinfo['workemail'];
            $empplayerId = $getimdinfo['tokenId'];          

            SendOneSignalMessage($message,$empplayerId);

        if($type==1){
        $fromDate =$fromDate;
        $toDate = $toDate;
        $onDate = '';
        $reqtext ='leave';
      }else if($type==2){
        $fromDate = $inTime;
        $toDate = $outTime;
        $onDate = $request->get('ondate');
        $reqtext='different timing';
      }else if($type==3){
        $fromDate = $inTime;
        $toDate =  $outTime;
        $onDate = $request->get('ondate');
        $reqtext ='extra working';
      }else{
        $fromDate = $inTime;
        $toDate =  $outTime;
        $onDate = $request->get('ondate');
        $reqtext ='OD';
      }

            $ch = curl_init();
            $curlConfig = array(
            CURLOPT_URL            => "http://183.182.86.91:8088/hrm/appraisal/sendremindermailemployee.php",
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array(
            'email' => $imdemail,
            'fromDate'=>$fromDate,
            'toDate'=>$toDate,
            'empname'=>$empname,
            'leaverequest'=>$reqtext,
             'ondate'=>$onDate,
             'remark'=>$remark
            )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

            if($check){
            
            $res['result']  ='Request Sent Successfully..!';
            $res['success'] = true;
            $res['code']    = 200;            
           }else{
            $res['result']  ='error occur';
            $res['success'] = false;
            $res['code']    = 401;
           }
            return Response::json($res);                
           
               
   }


   //get company calendar
  
  public function GetCompanyCalendar(Request $request){

          $result = Calendar::get();

          if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;   
          }else{
            $res['result']  ='No data found';
            $res['success'] = false;
            $res['code']    = 401;   
          }

      return Response::json($res);
  }
//upload documents method

  public function UploadDocumnets(Request $request){

       date_default_timezone_set('Asia/Kolkata');
       $onDate= date('Y-m-d H:i:s');
       $userId = $request->get('userId');
       $type = $request->get('type');
       $documents = $request->file('documents');
       $vArray = array('userId'=>'required','documents'=>'required','type'=>'required');

       check_validation($request,$vArray);

       if(!empty($documents)){
              
              $destinationPath = 'Documents';

              $fileName = $documents->getClientOriginalName();

              $documents->move($destinationPath,$documents->getClientOriginalName());
             }

       $insertArray = array("type"=>$type,"dataid"=>$userId,"filepath"=>$fileName,"createdate"=>$onDate,"name"=>"null","notes"=>"null","ext"=>"null","updatedby"=>124);

       $insert = Documents::insert($insertArray);

       if($insert){

            $res['result']  ='upload successfuly';
            $res['success'] = true;
            $res['code']    = 200;   
          }else{
            $res['result']  ='Error occur';
            $res['success'] = false;
            $res['code']    = 401;   
          }

      return Response::json($res);
  }


  //send message method

 

 public function SendMessagetoHr(Request $request){


        $userId = $request->get('userId');
        $message = $request->get('message');

        date_default_timezone_set('Asia/Kolkata');
        $onDate= date('Y-m-d H:i');
        $vArray = array("userId"=>"required","message"=>"required");

        check_validation($request,$vArray);

        $insertArray = array("from"=>$userId,"to"=>124,"message"=>$message,"status"=>0,"createdate"=>$onDate,"convoid"=>0);

        $check = Messages::insert($insertArray);

        if($check){

            $res['result']  ='Insert successfuly';
            $res['success'] = true;
            $res['code']    = 200;   
          }else{
            $res['result']  ='Error occur';
            $res['success'] = false;
            $res['code']    = 401;   
          }

      return Response::json($res);
 }

 //get documents 

 public function GetDocument(Request $request){

     $userId = $request->get('userId'); 

     $documentUrl = 'http://183.182.86.91:8088/hrm/Api/Documents/';

     $vArray = array("userId"=>"required");

     check_validation($request,$vArray);

     $result = Documents::where('dataid',$userId)->get();

      if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;
            $res['documentUrl']=$documentUrl;
            $res['code']    = 200;   
          }else{
            $res['result']  ='Document not found';
            $res['success'] = false;
            $res['code']    = 401;   
          }

      return Response::json($res);
 }

 //method for get tutorial link

 public function GetTutorials(Request $request){

      $result = Tutorial::get();

      if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;   
      }
      return Response::json($res);
 }
 //method for demo work

  public function Demo(Request $request){

      $old_date = date('20/06/2019');              // returns Saturday, January 30 10 02:06:34
      $old_date_timestamp = strtotime($old_date);
      echo $new_date = date('Y-m-d', $old_date_timestamp);   

} 
public function GetEmployeeMessages(Request $request){

         $empid = $request->get('empid');

         $vArray = array('empid'=>'required');

         check_validation($request,$vArray);

         $result = Messages::where('to',$empid)->get();

         if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;   
      }
      return Response::json($res);

}
//get my request method 

public function GetmyRequest(Request $request){

       $empId = $request->get('empid');

       $vArray = array('empid'=>'required');

       check_validation($request,$vArray);

       $result = Leave::where('empid',$empId)->get();


       if(!empty($result)){
            
            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;   
      }
      return Response::json($res);

}
public function SendMail(Request $request){
  
 

}

public function GetEmployeeRequrest(Request $request){

      $result = DB::table('leaverequests')
            ->join('employee', 'employee.id', '=', 'leaverequests.empid')
           
            ->select('employee.name','leaverequests.*')
            ->orderBy('createdate','DESC')->get();

        if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;   
       }
      return Response::json($res);

}

public function GetEmployeeRequesttoHod(Request $request){

     
        $empid = $request->get('empid');
        $vArray = array('empid'=>'required');

        check_validation($request,$vArray);

         $result = DB::table('leaverequests')
            ->join('employee', 'employee.id', '=', 'leaverequests.empid')
           
            ->select('employee.name','leaverequests.*')->where('employee.imdboss',$empid)
            ->orderBy('createdate','DESC')->get();


          if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;   
       }
      return Response::json($res);
}

public function AcceptRequestByHr(Request $request){

     $empid = $request->get('empid');
     $status = $request->get('status');

     $vArray = array('empid'=>'required','status'=>'required');

     check_validation($request,$vArray);

     $check = Leave::where('empid',$empid)->update(['status'=>$status]);

     if($check){

            $res['result']  ='Accept/reject successfully';
            $res['success'] = true;            
            $res['code']    = 200;
     }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
     }
     return Response::json($res);
}
  public function AcceptRequestByHOD(Request $request){

  
     $empid = $request->get('empid');
     $status = $request->get('status');
     $requestId = $request->get('requestId');

      $getleaveData = Leave::where('id',$requestId)->first();
      
      $type = $getleaveData['type'];

      if($type==1){
        $fromDate = $getleaveData['fromdate'];
        $toDate = $getleaveData['todate'];
        $onDate = '';
        $reqtext ='leave';
      }else if($type==2){
        $fromDate = $getleaveData['intime'];
        $toDate = $getleaveData['outime'];
        $reqtext='different timing';
        $onDate = date('d/m/Y');
      }else if($type==3){
        $fromDate = $getleaveData['intime'];
        $toDate = $getleaveData['outime'];
        $reqtext ='extra working';
        $onDate = date('d/m/Y');
      }else{
        $fromDate = $getleaveData['intime'];
        $toDate = $getleaveData['outime'];
        $reqtext ='OD';
        $onDate = date('d/m/Y');
      }

     
     if($status==1){
      $text = 'Your '.$reqtext.' application has been approved.';
     }else {
      $text = 'Your '.$reqtext.' application has been rejected.';
     }
           
       

      if($type==1){
        $leavetype='Your leave application';
      }else if($type==2){
        $leavetype='Your different timing application';
      }else if($type==3){
         $leavetype='Your extra working application';
      }else{
        $leavetype = 'Your OD application';
      }

     if($status==1){
      $messsage = 'approved';
     }else{
      $messsage = 'rejected';
     }

     $vArray = array('empid'=>'required','status'=>'required','requestId'=>'required');

     check_validation($request,$vArray);

     $check = Leave::where('empid',$empid)->where('id',$requestId)->update(['hodapproval'=>$status]);
     $checkresult = Users::where('id',$empid)->first(); 

     $empplayerId = $checkresult['tokenId'];
     $empmail = $checkresult['workemail'];
     $empname  =$checkresult['name'];

   

          
     if($check){
            $res['result']  =$text.' successfully';
            $res['success'] = true;            
            $res['code']    = 200;

            SendOneSignalMessage($text,$empplayerId);
            $ch = curl_init();
            $curlConfig = array(
            CURLOPT_URL            => "http://183.182.86.91:8088/hrmtest/appraisal/senAcceptmailtoemployee.php",
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array(
            'email' => $empmail,
            
            'fromDate'=>$fromDate,
            'toDate'=>$toDate,
            'empname'=>$empname,
            'message'=>$messsage,
            'leavetype'=>$leavetype,
            'ondate'=>$onDate
            )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

     }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
     }
     return Response::json($res);

}
public function GetPolicyList(Request $request){

      $result = Policy::get();
      $filepath = 'policy/files';

      if($result){
            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200; 
            $res['filepath']= $filepath;          

     }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
     }
     return Response::json($res);
}
}