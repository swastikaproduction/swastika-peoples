<?php
include('../include/config.php');
$type = $_GET['type'];
$invoiceArray = Array();
$paymentArray = Array();

if($type == 'thisfis')
{
  $from = date("Y")."-04-01 00:00:00";
  $to = (date("Y",(time() + 24 * 60 * 60 * 365)))."-03-31 23:59:59";
}
else if($type == 'prevfis')
{
  $from = (date("Y",(time() - 24 * 60 * 60 * 365)))."-04-01 00:00:00";
  $to = date("Y")."-03-31 23:59:59";
}
else
{
  $from = $_GET['from']." 00:00:00";
$to = $_GET['to']." 23:59:59";
}

function getMonthsInRange($startDate, $endDate) {
$months = array();
while (strtotime($startDate) <= strtotime($endDate)) {
    $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
    $startDate = date('d M Y', strtotime($startDate.
        '+ 1 month'));
}

return $months;
}

$monthRange = getMonthsInRange($from,$to);
$rangeStr = '';

foreach($monthRange as $val)
{
  $str = $val['year']."-".$val['month'];
  $range[] .= date("M-y",strtotime($str));
}

foreach($range as $val)
{
    $paymentArray[$val] = '0';
    $invoiceArray[$val] = '0';
    $rangeStr .= "'".$val."',";
}
  $rangeStr = substr($rangeStr,0,-1);


//$range = Array['Jan','','Jan','Jan','Jan','Jan','Jan','Jan',];



$k=0;
$getData = mysqli_query($con,"SELECT * FROM `invoices` WHERE `date` BETWEEN '$from' AND '$to' ORDER BY `date` ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData['date']));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $invoiceArray[$thisMonth] = $invoiceArray[$thisMonth] + $rowData['grandtotal'];
  }
  else
  {
    $invoiceArray[$thisMonth] = $rowData['grandtotal'];
  }
   
}


$getData = mysqli_query($con,"SELECT * FROM `payments` WHERE `date` BETWEEN '$from' AND '$to'  ORDER BY `date` ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData['date']));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $paymentArray[$thisMonth] = $paymentArray[$thisMonth] + $rowData['amount'];
  }
  else
  {
    $paymentArray[$thisMonth] = $rowData['amount'];
  }
   
}

$saleStr = implode(",",$invoiceArray);
$paymentStr = implode(",",$paymentArray);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Sales Chart</title>
</head>
<body>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="container" style="width:100%; height: 400px; margin: 0px;"></div>
<script type='text/javascript'>//<![CDATA[
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Sales Chart'
    },
    subtitle: {
        text: 'Monthly Sales Vs Payment Received'
    },
    xAxis: {
        categories: [<?php echo $rangeStr;?>],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount In Rupees',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' /-'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Sales',
        data: [<?php echo $saleStr;?>]
    }, {
        name: 'Payment Received',
        data: [<?php echo $paymentStr;?>]
    }]
});
//]]> 

</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }

  window.parent.window.document.getElementById('salePreloader').style.display = 'none';
  window.parent.window.document.getElementById('customBox').style.display = 'none';
  
</script>

</body>

</html>

