<?php
include("../include/config.php");
?>
<div class="moduleHead">
<div class="moduleHeading">
Reports
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto;padding:20px;">
<div class="row">
<div class="col-sm-3">
	For Month
	<br/>
	<input type="month" id="atreport" class="inputBox" name="" required="">
	<br/>

</div>
<div class="col-sm-3">
	From Date
	<br/>
	<input type="date" id="fromdate" class="inputBox" name="" required="">
	<br/>

</div><div class="col-sm-3">
	To Date
	<br/>
	<input type="date" id="todate" class="inputBox" name="" required="">
	<br/>

</div>
<div class="col-sm-3">
	For Branch
	<br/>
<select id="atbranch" class="inputBox">
<?php if($profile==21){ ?>
<option value="ALL">ALL</option>
<?php } ?>
	<?php
	if($profile==21){
        $data = getdata("branch","*","name","ASC");
	}
	else if($profile==20){
      $branch = [];   
      $getZones = mysqli_query($con,"SELECT `zones` FROM employee WHERE id='$loggeduserid'") or die(mysqli_error($con));
      $rowZones = mysqli_fetch_array($getZones);

      $data = mysqli_query($con,"SELECT * FROM `branch` WHERE zoneid IN ($rowZones[0])") or die(mysqli_error($con));
 	}
	foreach($data as $val)
	{
		?>
		<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
		<?php
	}
	?>
</select>
	<br/>

</div>
<div class="col-sm-12">
<br/>
	<button class="btn btn-sm btn-primary" onclick="window.open('reports/misspunch-result.php?date='+document.getElementById('atreport').value+'&branch='+document.getElementById('atbranch').value+'&fromdate='+document.getElementById('fromdate').value+'&todate='+document.getElementById('todate').value,'_blank')">DOWNLOAD REPORT</button>
	<br/><br/>
</div>

</div>
</div>