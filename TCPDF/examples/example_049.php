<?php
ob_start();
include("../../include/config.php");
include("curreny_format.php");

// email library
use PHPMailer\PHPMailer\PHPMailer;

require '../../leaves/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../../leaves/vendor/phpmailer/phpmailer/src/SMTP.php';

$mail = new PHPMailer(true);
require '../../leaves/vendor/autoload.php';

// end email library

   if($_POST['submit']=='GENERATE OFFER LETTER' || $_POST['submit']=='SEND ON EMAIL'){
    $name=ucwords(strtolower($_POST['name']));    
    $email = $_POST['email'];
    $branch = $_POST['branch'];
    $designation = $_POST['designation'];
    $doj = date('d/m/Y',strtotime($_POST['doj']));
    $basic = $_POST['basic'];
    $bonus = $_POST['bonus'];
    $ctc = $_POST['ctc'];
    $pt= $_POST['pt'];
    $annualctc = $_POST['ctc']*12;
    $ctcinwords = currenyformatting($annualctc);
    $da = $_POST['da'];
    $grosssalary = $_POST['grosssalary'];
    $hra = $_POST['hra'];
    $netpay = $_POST['netpay'];
    $otherdeduction = $_POST['otherdeduction'];
    $pfemployer = $_POST['pfemployer'];
    $pfpayable = $_POST['pfpayable'];
    $refreshment = $_POST['refreshment'];
    $totaldeduction = $_POST['totaldeduction'];
	$totalearnings = $_POST['totalearnings'];
	$button = $_POST['submit'];

	// alloted targets
	$revenue = $_POST['revenue'];
	$funded = $_POST['funded'];
	$a_cs = $_POST['a_cs'];
	$margins = $_POST['margins'];
	$franchisee = $_POST['franchisee'];
	$insurance_salary = $_POST['insurance_salary'];
	$cloud_telephony = $_POST['cloud_telephony'];
	$per_day_traded_clients = $_POST['per_day_traded_clients'];
	$total_traded_clients = $_POST['total_traded_clients'];

} else {
	echo 'NOTHINGFOUNDHERE';
	}

	
require_once('tcpdf_include.php');

class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'swastika-logo.jpg';
		$this->setJPEGQuality(60);
        $this->Image($image_file, 30, 10, 50, 9, 'jpg', '', 'T', true, 300, 'R', false, false, 0, false, false, false);

		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
//		$this->Cell(0, 15, 'OFFER LETTER', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-40);
		// Page number
		//$footertext = '<style>'.file_get_contents(K_PATH_IMAGES.'font_tcpdf.css').'</style>';
		$footertext = '<p style="color: #4c7aec;text-align:centre;"><b>Swastika Investmart Limited</b></p>
					   <p style="text-align:centre;"><b>Corp. Off.:</b> 48 Jaora Compound, M.Y.H. Road Indore-452001 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;0731-6644000, 3345000</p>
					   <p style="text-align:centre;"><b>Regd. Off.:</b> Flat No. 18, North Wing, Madhaveshwar Co-op. Hsg. Society, S.V. Road, Andheri (W), Mumbai-40058 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;022-26254568-69</p>
					   <p style="text-align:centre;"><i class="fa fa-envelope fa-11x"></i>&nbsp;&nbsp;info@swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-globe"></i>&nbsp;&nbsp; www.swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-car"></i>&nbsp;&nbsp;0120-4400789 </p>
					   <table cellspacing="4" cellpadding="4">
							<tr>
								<th style="width:100%;height:25px;text-align: center; background-color: #41d7e4;">
									<p style="color:#fff; text-align:centre;padding:50px 50px 600px 666px"><b>Swastika Group: Member of NSE, BSE, NCDEX, MCX, MSEI, DP:NSDL & CDSL</b></p>
								</th>
								
							</tr>
						</table>
				      ';
		$this->writeHTML($footertext, false, true, false, true);
		
		//$this->Cell(0, 10, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 049');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();


// create some HTML content


$date = date('d/m/Y');
//$name = 'Satendra Kumar Shukla';
//$annualctc = 24; $ctcinwords = 'One Thousands';
//$branch = 'Indore'; $doj = '21 june 2021';

$html = '<strong style="font-size:20px;text-align:centre;">OFFER LETTER</strong>
		 <p style="float: left;font-size:15px;"><b>Date: '.$date.'</b></p>
		 <p style="float:left;font-size:15px;"><b>Dear '.$name.',</b></p>
		 <p style="float:left;margin-top:0px;padding-right: 22px;">With reference to your application and subsequent discussions you had with us, we are pleased to offer you an appointment as <b>'. $designation .'</b> at <b>'.$branch.'</b>.For the above mentioned position you will be entitled for monthly CTC of <b>Rs '. $annualctc/100000 .' LPA,</b> in words, ('. $ctcinwords.') Statutory deductions will be as applicable. For detailed break up refer <b>annexure A</b></p>
		 <p>You are required to report on duty on or before <b>'. $doj.'</b> at <b>'.$branch.'</b> Office of failing which this offer will stand automatically cancelled.Please note that this is merely an Offer Letter. The Company’s standard Appointment letter containing general terms and conditions of employment will be issued to you on your joining the Company which shall be binding on you.</p>
		 <p>This letter of offer is based on the information furnished in your application for employment and during the interviews you had with us. If, at any time in future, it comes to light that any of this information is incorrect or any irrelevant information or has been pending, then your employment is liable to be terminated with immediate effect.</p>
		 <p>We look forward for a long and mutually beneficial association with <b>Swastika Investmart Limited</b>.</p>
		 <p>Please share below mentioned documents in HR & provide your acceptance within 24 hrs. from the date of issue .</p>
		 <p><b><u>Documents (Scan or physically) to be submitted at the time of Offer:</u></b></p>
		 <p>
			<ol>
			<li>   Pan Card</li>
			<li>   Aadhar Card (must contain date, month and year) Format should be DD/MM/YY</li>
			<li>   Bank Details:-Personalized cancelled cheque / Bank statement/ Front page of Passbook (Must contain clear Account Holder Name/ IFSC Code/ Account No.)</li>
			<li>   Current Address Proof (If the current address is different from Permanent address)</li>
			<li>   Past Employment Details (Resignation Letter & Relieving/Experience Letter)</li>
			<li>   UAN details if already member of PF</li>
			<li>   Form 11</li>
			<li>   Blood Group(No document Required)</li>
			<li>   Undertaking of Non-submission of documents(If candidate is unable to submit any above of the document )</li>
			</ol>  
		 </p><br>
		';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$html1 = '<strong style="font-size:16px;text-align:centre;">ANNEXURE (A)</strong>
		';

if(!empty($revenue) || !empty($funded) || !empty($a_cs) || !empty($margins) || !empty($franchisee) || !empty($insurance_salary) || !empty($cloud_telephony) || !empty($per_day_traded_clients) || !empty($total_traded_clients) ){
	$html1 .= '<p style="float:left;">Alloted Targets:-</p>';
}
else{
	$html1 .= '<p style="text-align:centre;">As a result of the discussion of your employment, your remuneration details stands described below to:</p>';
}

// output the HTML content
$pdf->writeHTML($html1, true, false, true, false, '');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

IMPORTANT:
If you are printing user-generated content, tcpdf tag can be unsafe.
You can disable this tag by setting to false the K_TCPDF_CALLS_IN_HTML
constant on TCPDF configuration file.

For security reasons, the parameters for the 'params' attribute of TCPDF
tag must be prepared as an array and encoded with the
serializeTCPDFtagParameters() method (see the example below).

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// $basic = 12500; $hra = 5556; $bonus = 2052; $refreshment = 2000; $ctc = 20000; $pfpayable = 400; $otherdeduction = 200; $pt = 100; $netpay = 10000;

$html2 = '';
$html2 .= '<tcpdf method="AddPage" /><style>
table, td, th {  
    border:1px solid #ddd;
}
table {
  width: 100%;
}
th, td {
  border-bottom: 1px solid #ddd;
  border-left: 1px solid #ddd;
  
}
</style>';

if(!empty($revenue) || !empty($funded) || !empty($a_cs) || !empty($margins) || !empty($franchisee) || !empty($insurance_salary) || !empty($cloud_telephony) || !empty($per_day_traded_clients) || !empty($total_traded_clients) ){
	$html2 .= '<table cellpadding="10" cellspacing="0" >
	<tr>
		<th style="font-weight:bold; ">KPI</th>
		<th style="font-weight:bold; ">Designation <hr> Targets</th>
	</tr>
	<tr>
		<td style="font-weight:bold; ">Revenue</td>
		<td >'.$revenue.'</td> 
	</tr>  

	<tr>
		<td style="font-weight:bold">Funded</td>
		<td>'.$funded.'</td> 
	</tr>  

	<tr>
		<td style="font-weight:bold">A/cs</td>
		<td>'.$a_cs.'</td> 
	</tr>  

	<tr>
		<td style="font-weight:bold">Margins</td>
		<td>'.$margins.'</td> 
	</tr>  

	<tr>
		<td style="font-weight:bold">Franchisee Acquisition</td>
		<td>'.$franchisee.'</td> 
	</tr> 

	<tr>
		<td style="font-weight:bold">Insurance 1x of salary</td>
		<td>'.$insurance_salary.'</td> 
	</tr> 

	<tr>
		<td style="font-weight:bold">Cloud Telephony</td>
		<td>'.$cloud_telephony.'</td> 
	</tr> 

	<tr>
		<td style="font-weight:bold">Per Day Traded Clients</td>
		<td>'.$per_day_traded_clients.'</td> 
	</tr> 

	<tr>
		<td style="font-weight:bold">Total traded Clients in Month</td>
		<td>'.$total_traded_clients.'</td> 
	</tr>  
 
</table><br/>
<p style="text-align:centre;">As a result of the discussion of your employment, your remuneration details stands described below to:</p>';
}

$html2 .= '<table cellpadding="10" cellspacing="0" >
				<tr>
					<th style="font-weight:bold; ">COMPENSATION HEADS</th>
					<th style="font-weight:bold; ">PER MONTH</th>
					<th style="font-weight:bold; ">PER ANNUM</th>
				</tr>
				<tr>
					<td style="font-weight:bold; ">Basic</td>
					<td >'.moneyFormatIndia($basic).'</td> 
					<td>'.moneyFormatIndia($basic*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">House Rent Allowance (HRA) </td>
					<td>'.moneyFormatIndia($hra).'</td> 
					<td>'.moneyFormatIndia($hra*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Bonus </td>
					<td>'.moneyFormatIndia($bonus).'</td> 
					<td>'.moneyFormatIndia($bonus*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Employee Refreshment </td>
					<td>'.moneyFormatIndia($refreshment).'</td> 
					<td>'.moneyFormatIndia($refreshment*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Cost to Company</td>
					<td>'.moneyFormatIndia($ctc).'</td> 
					<td>'.moneyFormatIndia($ctc*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">PF Contribution</td>
					<td>'.moneyFormatIndia($pfpayable).'</td> 
					<td>'.moneyFormatIndia($pfpayable*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Training Impartment Contribution</td>
					<td>'.moneyFormatIndia($otherdeduction).'</td> 
					<td>'.moneyFormatIndia($otherdeduction*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Professional Tax Payable</td>
					<td>'.moneyFormatIndia($pt).'</td> 
					<td>'.moneyFormatIndia($pt*12).'</td> 
				</tr>  

				<tr>
					<td style="font-weight:bold">Net Pay</td>
					<td>'.moneyFormatIndia($netpay).'</td> 
					<td>'.moneyFormatIndia($netpay*12).'</td> 
				</tr>  
			</table><br><br>';
if(!empty($revenue) || !empty($funded) || !empty($a_cs) || !empty($margins) || !empty($franchisee) || !empty($insurance_salary) || !empty($cloud_telephony) || !empty($per_day_traded_clients) || !empty($total_traded_clients) ){
	$html2 .= '<br><br>';
}
			
$html2 .= '<p style="float:left;"><b>For Swastika Investmart Limited</b><br/><br/></p>
			<p><span style="font-size:10px;font-weight: bold;float:left;">Authorized Signatory</span><br/><br/></p>
			<p style="float:left;">I have fully understood above & hereby accept the same.</p>
			
		';


// output the HTML content
$pdf->writeHTML($html2, true, 0, true, 0);

// to clean the output buffer just before the method invocation:
ob_end_clean();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
if($button=='GENERATE OFFER LETTER'){
	$pdf->Output($name.'_offerLetter.pdf', 'D');
}else{


$pdfString = $pdf->Output($name.'_offerLetter.pdf', 'S');
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true; 	 
$mail->Username = 'swastika.developer@gmail.com';
$mail->Password = 'sil@321#';
$mail->setFrom('swastika.developer@gmail.com', 'Swastika Investmart Limited');
$mail->addAddress($email);
$mail->Subject = 'Offer Letter';
$mail->Body = 'Offer Letter';
$mail->AltBody = 'Swastika Investmart Limited';
// $mail->msgHTML("<h1>This is an HTML message</h1>");
// $mail->addAttachment('test.txt');
$mail->addStringAttachment($pdfString,$name.'_offerLetter.pdf');

if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo '<script>alert("Mail Sent Successfully.");window.close();</script>';
}

}


//============================================================+
// END OF FILE
//============================================================+

function moneyFormatIndia($num) {
	$explrestunits = "" ;
	if(strlen($num)>3) {
		$lastthree = substr($num, strlen($num)-3, strlen($num));
		$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
		$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
		$expunit = str_split($restunits, 2);
		for($i=0; $i<sizeof($expunit); $i++) {
			// creates each of the 2's group and adds a comma to the end
			if($i==0) {
				$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
			} else {
				$explrestunits .= $expunit[$i].",";
			}
		}
		$thecash = $explrestunits.$lastthree;
	} else {
		$thecash = $num;
	}
	return $thecash; // writes the final format where $currency is the currency symbol.
}

?>
