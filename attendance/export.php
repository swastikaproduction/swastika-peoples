<?php
include("../include/config.php");
$id = $_GET['id'];

$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowData = mysqli_fetch_array($getData);
$month = $rowData['month']; 
$year = $rowData['year'];
$empid = $rowData['empid'];

$tableName = "attendance_".$month."_".$year;
$freezed_table = "freez_attendance_".$month."_".$year;

$getEmp = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($row = mysqli_fetch_array($getTables))
{
	$tables[] = $row[0];
}

if(in_array($freezed_table,$tables)){
	$getData= mysqli_query($con,"SELECT * FROM `$freezed_table` WHERE `empid` = '$empid' ORDER BY `date` ASC") or die(mysqli_error($con));
}
else{
$getData= mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid' ORDER BY `date` ASC") or die(mysqli_error($con));
}

$name = "deductions_".$rowEmp['name'].".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");
?>


<table border="1">
<tr>
<th colspan="15"><?php echo $rowEmp['name'];?></th>
</tr>

	<tr>
		<th>Date</th>
		<th>In</th>
		<th>Out</th>
		<th>Shift End</th>
		<th>Workhrs</th>
		<th>Allotedhrs</th>
		<th>Diff</th>
		<th>Late</th>
		<th>Consecutive</th>
		<th>Status</th>
		<th>Two Hours</th>
		<th>Fifteen Mins</th>
		<th>Deduction</th>
		<th>Remarks</th>
		<th>Forced Dedcution</th>

	</tr>


<?php
while($row= mysqli_fetch_array($getData))
{

	$class = '';
	if($row['status'] == '2')
	{
		$class = '#fecb78';
	}
	else if($row['status'] == '1')
	{
		$class = '';
	}
	else
	{
		$class = '#f28c8c';
	}


	if($row['twohourvalue'] != '0')
{
	$class = '#8fd2f7';
}


if($row['deduction'] != '0')
{
	$class = '#f28c8c';
}









	?>
<tr style="background:<?php echo $class;?>">
	<td><?php echo $row['date'];?></td>
	<td><?php echo $row['intime'];?></td>
	<td><?php echo $row['outime'];?></td>
	<td><?php echo $row['shiftend'];?></td>
	<td><?php echo $row['hours'] * 60;?></td>
	<td><?php echo $row['allotedshifthours'];?></td>
	<td><?php echo $row['difference'];?></td>
	<td><?php echo $row['late'];?></td>
	<td><?php echo $row['consecutive_counter'];?></td>
	<td><?php
	if($row['status'] == '2')
	{
		echo "Holiday";
	}
	else if($row['status'] == '1')
	{
		echo "Present";
	}
	else
	{
		echo "Absent";
	}

	?></td>
	<td><?php echo $row['twohourvalue'];?></td>
	<td><?php echo $row['fifteenminscounter'];?></td>
	<td><?php echo $row['deduction'];?></td>
	<td><?php echo $row['remarks'];?></td>
	<td>

	<?php echo $row['forced'];	?>
</td>
	

</tr>
<?php
}
?>
</table>

