<?php
 include'../include/connection.php';
 $query = mysqli_query($con,"select * from branch");
 $headquery = mysqli_query($con,"select * from expensenature");
 ?>
<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Expense History</li>
</ol>
</section>
<section>
   <div class="row">
      <div class="col-md-2">
        <label>Status</label>
        <select id="expensestatus" class="form-control">
          <option value="">select status</option>
          <option value="0">Pending</option>
          <option value="1">Approved</option>
          <option value="2">Rejected</option>
          <option value="3">Paid</option>
        </select>
      </div>
      <div class="col-md-2">
        <label>From Date</label>
        <input type="date" name="fromdate" id="fromdate" class="form-control">
      </div>
      <div class="col-md-2">
        <label>To Date</label>
        <input type="date" name="todate" id="todate" class="form-control">
      </div>
        <div class="col-md-2">
        <label>Branch Name</label>
        <select name="bname" id="bname" class="form-control">
          <option value="">select branch name</option>
          <?php
         
          while($row=mysqli_fetch_array($query)){
            ?>
            <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php
          }
          ?>
        </select>
      </div>
       <div class="col-md-2">
        <label>Head Name</label>
        <select name="headid" id="headid" class="form-control">
          <option value="">select head name</option>
          <?php
         
          while($row=mysqli_fetch_array($headquery)){
            ?>
            <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="col-md-2">
        <input type="submit" class="btn btn-success" name="submit" value="Submit" style="margin-top:25px; " onclick="GetExpenseFilterDataAll();">
      </div>
    </div>
</section>
<div id="afterexpensefilterdataall">
  
</div>

<div id="beforexpensefilterdataall">
  <!-- Main content -->
<section class="content" style="margin-top: 20px;">
              
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>   -->         
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Branch Name</th>
<th>Benificiary Name</th>
<th>Head Name</th>            
<!-- <th>Type</th>             -->
<th>Amount</th>            
<th>Receipt</th>            
<th>Expense Date</th>            
<th>Create Date</th>            
<th>Approve1</th>            
<th>Approve2</th>            
<th>Action</th>         
            
</tr>
</thead>
<tbody>
<?php  
$count=1; 
         
include'../include/connection.php';
$query = mysqli_query($con,"SELECT e.id,h.name hname,e.paid ,e.amount,e.createdat,b.name bname,e.filename,e.status,br.name as brname,e.expensedate  FROM expense e INNER JOIN expensenature h ON h.id = e.natureid INNER JOIN expensebenificiary b on b.id = e.benificiary INNER JOIN employee ee on ee.id = e.empid INNER JOIN branch br on br.id = ee.branch");
while($row=mysqli_fetch_array($query)){

  if($row['status']==0){

    $status = '<span style="background-color: #81a968;color: white;border-radius: 7px;">Pending</span>';
  }else if($row['status']==1){
    $status = '<span style="background-color: orange;color: white;border-radius: 7px;">Approved</span>';
  }else if($row['status']==2){
    $status = '<span style="background-color: red;color: white;border-radius: 7px;">Rejected</span>';
  }else{
    $status = '<span style="background-color: green;color: white; border-radius: 7px;">Paid</span>';
  }

  if($row['paid']==0){

    $paid = '<span style="background-color: #81a968;color: white;border-radius: 7px;">Pending</span>';
  }else if($row['paid']==1){
    $paid = '<span style="background-color: orange;color: white;border-radius: 7px;">Approved</span>';
  }else {
    $paid = '<span style="background-color: red;color: white;border-radius: 7px;">Rejected</span>';
  }
?>

<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['brname'];?></td>
<td><?php echo $row['bname'];?></td>
<td><?php echo $row['hname'];?></td>
<!-- <td><?php echo $row['type'];?></td> -->
<td><?php echo $row['amount'];?></td>
<td><a href="ExpenseFiles/<?php echo $row['filename'];?>" download>Download</a></td>
<td><?php echo $row['expensedate'];?></td>
<td><?php echo $row['createdat'];?></td>
<td><?php echo $status;?></td>
<td><?php echo $paid;?></td>


<td>
<?php
   if($row['paid'] ==0){
?>
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="http://183.182.86.91:13780/hrm/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=1&empid= <?php echo $empid;?>" onclick="return confirm('Are you sure you want to approve this?');">Approve</a></li>
      <li><a href="http://183.182.86.91:13780/hrm/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=2&empid= <?php echo $empid;?>" onclick="return confirm('Are you sure you want to reject this?');">Reject</a></li>
      <li><a href="http://183.182.86.91:13780/hrm/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=3" onclick="return confirm('Are you sure you want to paid this?');">Paid</a></li>
      <li><a data-toggle="modal" data-target="#myModal12" onclick="UpdateExpense('<?php echo $row['id'];?>');">Edit</a></li>
    </ul>
  </div>
</div>
 <?php
  }?>
</td>
 
  <?php
    }?>                      

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>

</div>
<div class="modal fade" id="myModal12" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/update-expense.php" method="post">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Expense </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Amount</label>
<input type="hidden" name="eupdateid" id="eupdateid">
<input type="text" name="expenseamountupdate" class="form-control" id="expenseamountupdate" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="expenseheadid" name="expenseheadid" class="form-control" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>

<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->