<?php
function has_completed_shift_hours($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	if($rowData['difference'] <=  0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function halfshift($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];

	if(date("D",strtotime($date)) == 'Sat')
	{
		$half = 270;
	}
	else if($shifthours > 300 && $shifthours <= 420)
	{
		$half = 240;
	}
	else if($shifthours < 300)
	{
		$half = 180;
	}
	else
	{
		$half = 270;
	}

	//$half = $shifthours/2;
	$workHours = $rowData['hours'] * 60;
	if($workHours < $half)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function half_to_shift_minus_two($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	$half = $shifthours/2;
	if($rowData['difference'] > 120 && $rowData['difference'] <= $half)
	{
		return true;
	}
	else
	{
		return false;
	}	
}


function shift_minus_two_to_shift_minus_onethirty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 90 && $rowData['difference'] <= 120)
	{
		return true;
	}
	else
	{
		return false;
	}	
}



function shift_minus_onethirty_to_shift_minus_one($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 60 && $rowData['difference'] <= 90)
	{
		return true;
	}
	else
	{
		return false;
	}	
}


function shift_minus_one_to_shift_minus_thirty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 30 && $rowData['difference'] <= 60)
	{
		return true;
	}
	else
	{
		return false;
	}	
}

function shift_minus_thirty_to_shift($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 0 && $rowData['difference'] <= 30)
	{
		return true;
	}
	else
	{
		return false;
	}	
}


function exit_less($emp,$date)
{
	global $con;
	global $rowData;
	/*
	if($rowData['shiftend'] > $rowData['outime'])
	{
		return true;
	}
	else
	{
		return false;
	}
	*/
$shiftend = $rowData['shiftend'];
$outtime = $rowData['outime'];

$shiftendfull = '2017-01-01 '.$shiftend;
$outtimefull = '2017-01-01 '.$outtime;
$shiftendfull = strtotime($shiftendfull);
$outtimefull = strtotime($outtimefull);

$diff = ($shiftendfull - $outtimefull)/3600;
if($diff > 22)
{
$outtimefull = '2017-01-02 '.$outtime;
$outtimefull = strtotime($outtimefull);
}

if($shiftendfull >  $outtimefull)
{
return true;
}
else
{
return false;
}


}

?>