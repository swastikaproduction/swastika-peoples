<?php
include("../include/config.php");
$id = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `leaverequests` WHERE `id` = '$id'") or die(mysqli_error($con));
$row= mysqli_fetch_array($getData);
$empid = $row['empid'];
if($empid == $loggeduserid)
{
	header("location:juststatus.php?id=".$id);
}
$rtype = Array();
$rtype[1] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Leave Request</span><br/><span style="color:#222">'.date("d-M-y",strtotime($row['fromdate']))." to ".date("d-M-y",strtotime($row['todate'])).'</span>';
$rtype[2] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Timing Change Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';
$rtype[3] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Overtime Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';

$rtype[4] = '<span class="text-primary" style="font-weight:400;font-size:20px;">OD Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';

$rtype[5] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Halfday Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';



?>
<div class="row">
	<div class="col-sm-12">
	<div style="float:right;margin-top:20px">
		<?php 
	if($row['hodapproval'] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row['hodapproval'] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row['hodapproval'] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
	</div>

	<div class="subHead" style="padding-left:5px">
	<span style="font-size:15px;">
		<?php echo $rtype[$row['type']];?>
		</div>
		
		</span>
		<span style="font-size:16px;font-weight:bold;">HOD Approval</span>
		<br/><br/>
		<div style="background:#f7f8f8;padding:20px;">
<?php echo $row['reason'];?>
		</div>
		<br/>



		Request Status
		<br/>
		<select <?php if($row['hodapproval'] == '1') echo "disabled='true'";?> class="inputBox" id="lstat0" style="margin-top:5px;">
		<option value="">Select Status</option>
			<option <?php if($row['hodapproval'] == '1') echo "selected='selected'";?> value="1">Approved</option>
			<option <?php if($row['hodapproval'] == '2') echo "selected='selected'";?>  value="2">Rejected</option>
		</select>


<?php
if($row['type'] == '4')
{
	?>
<br/><br/>
<button class="btn btn-primary" onclick="window.open('https://www.google.co.in/maps?q=<?php echo $row['lat'];?>,<?php echo $row['lng'];?>','_blank')">
	<i class="fa fa-map-marker"></i>&nbsp;VIEW ATTACHED LOCATION
</button>
&nbsp;&nbsp;&nbsp;
<?php
$pic = str_ireplace('"','',$row['pic']);
$pic = str_ireplace('../../../od-files/', '',$pic);
?>
<button class="btn btn-primary" onclick="window.open('od-files/<?php echo $pic;?>','_blank')">
	<i class="fa fa-image"></i>&nbsp;VIEW ATTACHED PICTURE
</button>
<br/><br/>

	<?php
} 
?>



	
		<br/><br/>
		Remark (Optional)
		<br/>
		<textarea class="inputBox" id="lstat1"  <?php if($row['hodapproval'] == '1') echo "readonly='true'";?>   style="height:140px;margin-top:5px;"><?php echo $row['hodreason'];?></textarea>
		<br/>
		<br/>
		<?php
		if($row['hodapproval'] != '1')
		{
?>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('leaves/savestatushod.php?id=<?php echo $id;?>','','justCloseModal','lstat',2,'','url:resp','','');" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<div id="resp">
				
			</div>
<?php			
		}
		else
		{
			?>
<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="toast('Request already approved. You cannot change an approved request','toast bg-danger','5000')" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<?php
		}
		?>
			<br/><br/><br/>

<?php
$getPrev = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.id = '$empid' AND leaverequests.id != '$id' ORDER BY `id` DESC") or die(mysqli_error($con));

?>

<table class="table table-hover shadow" cellpadding="0" cellspacing="0">
<tr>
	<th colspan="5" style="background:#fff !important;color:#333 !important;border-top:1px #eee solid !important">Other Requests By This Employee
	</th>
</tr>
<tr>
<th style="width:5%;">#</th>
<th style="width:15%;">Type</th>
<th style="width:30%">Details</th>
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
</tr>

<?php
$i=1;
while($rowPrev = mysqli_fetch_array($getPrev))
{
?>
<tr>
<td><?php echo $i;?></td>
<td>

<?php 
	if($rowPrev[2] == '1')
	{
		?>
	<i class="fa fa-circle" style="color:#ffb22b"></i>&nbsp;&nbsp;&nbsp;
		<?php
		echo "Leave";
	}
	else if($rowPrev[2] == '2')
	{
		?>
	<i class="fa fa-circle" style="color:#1976d2"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Timing Mismatch";
	}

	else if($rowPrev[2] == '3')
	{
		?>
	<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Overtime";
	}
else if($rowPrev[2] == '4')
	{
		?>
	<i class="fa fa-circle" style="color:#e32eab"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "OD Request";
	}

?></td>
<td>
<?php
if($rowPrev[2] == 1)
{
	?>

<span class="label label-default">
	<?php echo date("D, d M Y",strtotime($rowPrev[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo date("D, d M Y",strtotime($rowPrev[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-default">
	<?php echo $rowPrev[9];?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo $rowPrev[10];?></span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default">
<?php echo date("D, d M Y",strtotime($rowPrev[5]));?>
</span>



	<?php
}
?>
</td>


<td style="padding-top:15px;">
	<?php 
	if($rowPrev[6] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($rowPrev[6] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($rowPrev[6] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>


<td>
<?php echo date("d/m/y h:i A",strtotime($rowPrev[7]));?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>




			<br/><br/><br/>
	</div>
</div>