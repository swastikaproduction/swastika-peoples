<?php
include("../apiconfig.php");
$doctypes = Array();
$getAlltypes = mysqli_query($con,"SELECT * FROM `documenttypes` ORDER BY `name`") or die(mysqli_error($con));
while($rowTypes = mysqli_fetch_array($getAlltypes))
{
	$doctypes[$rowTypes['id']] = $rowTypes['name'];
}
?>

<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">
<div style="padding:0px;position:relative">

<table style="width:100%">
	<tr>
		<td id="myreqbutton" style="padding:20px 10px;text-align:left;border-bottom:1px #eee solid;background:#ecf2fa;color:#222;width:50%;text-transform:uppercase" onclick="$('#myreq').show();$('#pendreq').hide();$('#myreqbutton').animate({backgroundColor:'#ecf2fa'});$('#pendreqbutton').animate({backgroundColor:'#fff'});">
		Company Documents
		</td>
		<td id="pendreqbutton"  style="padding:20px 10px;;text-align:left;border-bottom:1px #eee solid;background:#fff;color:#222;width:50%;text-transform:uppercase" onclick="$('#myreq').hide();$('#pendreq').show();$('#pendreqbutton').animate({backgroundColor:'#ecf2fa'});$('#myreqbutton').animate({backgroundColor:'#fff'});">
		Submit Documents
		</td>
	</tr>
</table>
<div id="myreq">
<?php
$alldocsArray = Array();
$getData = mysqli_query($con,"SELECT * FROM `documents` WHERE `dataid` = '$loggeduserid'") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	$alldocsArray[$row['type']]['filename'] = $row['filepath'];
	$alldocsArray[$row['type']]['date'] = $row['createdate'];
}

foreach($doctypes as $key => $val)
{
	?>
<div class="calStrip" style="padding:15px 10px;">

<table style="width:100%">
	<tbody>


	<tr>
		<td class="timeDotTd">
		<div class="timeDotHolder">
		<i class="fa fa-file-pdf-o" style="color:#000"></i>			
		</div>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
			<?php
if(array_key_exists($key, $alldocsArray))
{
?>
	<span class="label label-default">
			<?php echo date("D, d-M h:i A",strtotime($alldocsArray[$key]['date']));?>
			</span>
			<?php
}
			?>
	</div>	
	<span style="font-size:20px;font-weight:400;text-transform:capitalize">
		<?php echo $val;?>	</span>


	<br>
<div style="margin-top:5px;border-bottom:1px #">

<?php
if(array_key_exists($key, $alldocsArray))
{
	?>
	<br/>
<button class="btn btn-primary" style="width:100%;height:50px;" onclick="changePage('viewThree','templates/viewThree.html','templates/document-list.php?type=<?php echo $key;?>','viewThreeRes','','strong','homeHeader');globeTitle = '<?php echo $val;?>'; globeTitleId = 'viewthreetitle'")">CLICK TO VIEW</button>
<br/><br/>
	<?php
}
else
{
	?>
<br/>
<button class="btn btn-danger" id="docUpload<?php echo $key;?>" style="width:100%;height:50px;" onclick="displayModal('templates/select-file-source.php?type=<?php echo $key;?>')">CLICK TO UPLOAD</button>
<button class="btn btn-primary" id="docUploadDone<?php echo $key;?>" style="width:100%;height:50px;display:none" onclick="changePage('viewThree','templates/viewThree.html','templates/document-list.php?type=<?php echo $key;?>','viewThreeRes','','strong','homeHeader');globeTitle = '<?php echo $val;?>'; globeTitleId = 'viewthreetitle'")">CLICK TO VIEW</button>


<br/><br/>
	<?php
}
?>
</div>
<br/>
<div class="halfBorder"></div>
		</td>
	</tr>
</tbody></table>


</div>
	<?php
}
?>
</div>
<div id="pendreq">
	<?php include("company-documents.php");?>

</div>
</div>



</div>
</div>

