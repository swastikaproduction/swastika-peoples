<?php
include("../apiconfig.php");
?>

<!DOCTYPE html>
<html>
<head>
<title>Rate Chart</title>  
<style type="text/css">
    rect
    {
        fill:transparent;
    }


.highcharts-yaxis-grid path
{
stroke:#222;
stroke-width:0;
}

.highcharts-subtitle
{
    display: none;
}

.highcharts-contextbutton
{
    display: none;
}
.highcharts-credits
{
    display: none;
}

.highcharts-yaxis-labels
{
   display:none;
}
.highcharts-xaxis-labels
{
   display:none;
}

.highcharts-xaxis
{
    display: none;
}

</style>
</head>
<body style="margin:0px;">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; height: 200px; margin: 0 0 !important"></div>
<script type='text/javascript'>//<![CDATA[

$.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

    Highcharts.chart('container', {
        chart: {
            zoomType: 'x',
            borderColor: '#222222'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#007599'],
                        [1, '#22cabf']
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            },
            series: {
            lineColor: '#0685a0'
            }
        },

        series: [{
            type: 'area',
            name: 'BTC TO RUPEE',
            data: data
        }]
    });
});
//]]> 

</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }
</script>

</body>

</html>

