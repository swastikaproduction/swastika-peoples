<?php
include("../include/config.php");
$thisdate = $_GET['date'];
$thisdate = $thisdate."-01";
$today = date("d",strtotime($thisdate));
$month = date("m",strtotime($thisdate));
$year = date("Y",strtotime($thisdate));
if($today > 25)
{
	if($month == 12)
	{
		$month = "01";
		$year = $year+1;
		$tableName = "attendance_".$month."_".$year;
	}
	else
	{
		$month = $month+1;
		if($month < 10)
		{
			$month = "0".$month;
		}
		$tableName = "attendance_".$month."_".$year;	
	}
}
else
{
	$tableName = "attendance_".$month."_".$year;
}

$employee = Array();
$data = getdata("employee","*","name","ASC");
foreach($data as $val)
{
	$employee[$val['id']] = $val['name']."-".$val['empid'];
}


$branch = $_GET['branch'];
if($branch == 'ALL')
{
	$getEmp = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` IN (SELECT `id` FROM `employee` WHERE `left` = '0') ORDER BY `empid` ASC") or die(mysqli_error($con));
}
else
{
	$getEmp = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` IN (SELECT `id` FROM `employee` WHERE `branch` = '$branch' AND `left` = '0') ORDER BY `empid` ASC") or die(mysqli_error($con));
}

$already = Array();

$name = "Attendance-report.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>
<table border="1" cellpadding="10">
<?php
while($rowEmp = mysqli_fetch_array($getEmp))
{
	$thisempid = $rowEmp['empid'];
	if(!in_array($thisempid,$already))
	{
?>
<tr>
	<th></th><th></th><th></th>
</tr>
<tr>
	<th></th><th></th><th></th>
</tr>

	<tr>
		<th style="text-align:left" colspan="3"><?php echo $employee[$thisempid];?></th>
		
	</tr>
<tr>
	<th style="text-align:left">Date</th><th style="text-align:left">Intime</th><th style="text-align:left">Outtime</th>
</tr>

<?php
$already[] .= $thisempid;
	}
	
	$diff = $rowEmp['difference'];
	$late = $rowEmp['late'];
	$cons = $rowEmp['consecutive_counter'];
	$status = $rowEmp['status'];

	if($diff > 5 || $late  > 5 || $cons >= 3 || $status == '0')
	{
		$color = "#fff829";
	}
	else
	{
		$color = "white";
	}
	?>


<tr>
	<td style="background:<?php echo $color;?>"><?php echo $rowEmp['date'];?></td>
	<td style="background:<?php echo $color;?>"><?php echo $rowEmp['intime'];?></td>
	<td style="background:<?php echo $color;?>"><?php echo $rowEmp['outime'];?></td>
</tr>
	<?php
}
?>
</table>
