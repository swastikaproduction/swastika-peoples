<?php
include("../include/config.php");
$id = $_GET['id'];
// echo $id; die;
?>


<div class="moduleHead">
	<div class="moduleHeading">
	Promotion Letter
	</div>
</div>
<div class="shadow">

<form target="_blank" method="post" onsubmit="return incrementLetterValidation()"  action="TCPDF/examples/incrementLetter.php" id="myform" >

	<div class="row" style="background:#f8f7f7">
		<div class="col-sm-3 formLeft req">
		 Effect From Date 
		</div>
		<div class="col-sm-9 formRight">
			<input type="date"  name="start_date" title="" id="startDate" class="inputBox input-field"  value="">
			<span id="startDate-info" class="info" style="color:red"></span><br />
			<input type="hidden"  name="emp_id" title=""  value="<?php if(!empty($id)) echo $id; ?>">
		</div>

		<div class="col-sm-3 formLeft req">
		Earlier Date
		</div>
		<div class="col-sm-9 formRight">
			<input type="date"  name="end_date" title="" id="endDate" class="inputBox input-field"  value="">
			<span id="endDate-info" class="info" style="color:red"></span><br />
		</div>

		<div class="col-sm-3 formLeft req">
		Salary
		</div>
		<div class="col-sm-9 formRight">
			<input type="text"  name="emp_salary" title="" id="emp_salary" class="inputBox input-field"  value="">
			<span id="emp_salary-info" class="info" style="color:red"></span><br />

			<input type="hidden" id="inp7" name="basic">
			<input type="hidden" id="inp8" name="bonus">
			<input type="hidden" id="inp9" name="ctc">
			<input type="hidden" id="inp11" name="grosssalary">
			<input type="hidden" id="inp12" name="hra">
			<input type="hidden" id="inp13" name="netpay">
			<input type="hidden" id="inp14" name="otherdeduction">
			<input type="hidden" id="inp15" name="pfemployer">
			<input type="hidden" id="inp16" name="pfpayable">
			<input type="hidden" id="inp17" name="refreshment">
			<input type="hidden" id="inp18" name="totaldeduction">
			<input type="hidden" id="inp19" name="totalearnings">
			<input type="hidden" id="inp20" name="pt">
		</div>	

		<div class="col-sm-3 formLeft req">
		    PF Deduction
		</div>
		<div class="col-sm-9 formRight">
			<select class="inputBox" id="pfDeduction" >
				<option value="0">No</option>
				<option value="1">Yes</option>
			</select>
		</div>

		<div class="col-sm-3 formLeft req">
		Default PF
		</div>
		<div class="col-sm-9 formRight">
			<input type="text"  name="defaultPF" id="defaultPF" class="inputBox"  value="">
		</div>

		<div class="col-sm-3 formLeft req">
		    PT Deduction
		</div>
		<div class="col-sm-9 formRight">
			<select class="inputBox" id="ptDeduction" >
				<option value="0">No</option>
				<option value="1">Yes</option>
			</select>
		</div>

		<div class="col-sm-3 formLeft req">
		    Other Deduction
		</div>
		<div class="col-sm-9 formRight">
			<select class="inputBox" id="otherDeduction" >
				<option value="0">No</option>
				<option value="1">Yes</option>
			</select>
		</div>

		<div class="col-sm-3 formLeft req" style="float: right;">
			<span onclick="salaryBreakdownOnIncrementLetter()" class="label label-info">CALCULATE SALARY BREAKDOWN</span>
		</div>

	</div>

	<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	

	<div class="col-sm-10 formRight">
		<input type="submit" class="btn btn-primary" id="submitIncrementLetter" name="submit" value="GENERATE INCREMENT LETTER" >
		
		<input type="submit" class="btn btn-primary" id="emailIncrementLetter" name="submit" value="SEND ON EMAIL">
		<br><br><br>
	</div>


	</div>

</form>	



</div>
<br />
<br />
<br />
<br />
<br />

