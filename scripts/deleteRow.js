function crossCheckDelete(table)
{
	var comfirmMessage = 'Do you really want to delete selected rows?&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-sm"  onclick="deleteRow(\''+table+'\')" id="confirmDelete"><span class="glyphicon glyphicon-alert"></span>&nbsp;&nbsp;Yes!</button>&nbsp;&nbsp;<button type="button" class="btn btn-default btn-sm"  onclick="toast(\'Good one. There is no point deleting what is already in system. :) :)\',\'toast bg-success\',2000)"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;No</button>';
	toast(comfirmMessage,'toast bg-danger',0);
//showError(comfirmMessage,'',0);
}
function deleteRow(table)
{
	var checkList = "0";
	var itemSearch = $('.checkInput');
	$('#tableDiv').find(itemSearch).each(function(){
		if(this.checked == true)
		{
			var thisVal = this.value;
			if(document.getElementById('tableRow'+thisVal))
			{
				$('#tableRow'+thisVal).hide();
			}
			checkList = checkList+","+thisVal;
		}
			
    });	
//    getModule("deleteRow.php?table="+table+"&ids="+checkList,'','','');
//disableButton('confirmDelete','Deleting..');
showProcessing();
var url = "deleteRow.php?table="+table+"&ids="+checkList;
genAjax(url,'',function(response){
hideProcessing();
//var html = "Successfully deleted all the selected rows.&nbsp;&nbsp;<span style='text-decoration:underline;cursor:pointer' onclick=\"retrieveRow('"+table+"','"+checkList+"')\">Undo</span>";
toast(html,'toast bg-success',0);
});

}

function retrieveRow(table,checkList)
{
	
showProcessing();
var url = "retrieveRow.php?table="+table+"&ids="+checkList;
	genAjax(url,'',function(response){
hideProcessing();
var rowList = checkList.split(",")
	for(j=0;j<rowList.length;j++)
	{
		var thisVal = rowList[j];
			if(document.getElementById('tableRow'+thisVal))
			{
				$('#tableRow'+thisVal).show();
				//document.getElementById('tableRow'+thisVal).style.display = 'table-row';				
			}

	}
	hideProcessing();
	var html = "Successfully retreived all rows deleted in previous action.";
	toast(html,'toast bg-success',0);
	});

}



