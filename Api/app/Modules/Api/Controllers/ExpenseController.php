<?php

namespace App\Modules\Api\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Api\Models\Users;
use App\Modules\Api\Models\Expense;
use App\Modules\Api\Models\Expensetype;
use App\Modules\Api\Models\Expensenature;
use App\Modules\Api\Models\Benificiary;
use Response,Helper,Validator,file,DB;

class ExpenseController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        echo "sharad";
    }


    //get expense nature method here


    public function GetExpensenature(Request $request){
      
           $result = Expensenature::get();

            if($result){
            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);

    }


    //get expense nature type method here

    public function GetExpenseType(Request $request){

        $expensenatureid = $request->get('expensenatureid');
        $vArray  = array('expensenatureid'=>'required');
        check_validation($request,$vArray); 

        $check = Expensetype::where('natureid',$expensenatureid)->get();

        if(!empty($check)){

            $res['result']  =$check;
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);
    }

    //post expense 

    public function PostExpense(Request $request){

          // $expensedate = $request->get('expensedate');
          $natureid = $request->get('natureid');         
          $benificiary = $request->get('benificiary');
          $amount = $request->get('amount');
          $filename = $request->file('filename');
          $remark = $request->get('remark');
          $empid = $request->get('empid');

          $vArray = array('natureid'=>'required','benificiary'=>'required','amount'=>'required','filename'=>'required','remark'=>'required','empid'=>'required');

          check_validation($request,$vArray);

          if(!empty($filename)){
            $destinationPath = '../ExpenseFiles/';
            $filename->move($destinationPath,$filename->getClientOriginalName());
          }else{
            $filename = 'demo.jpg';
          }

            if($benificiary==1){

            $user = DB::table('employee')->where('id', $empid)->first();
            $name = $user->name;            
            $accountnumber = $user->acno;
            $ifsc = $user->ifsc;                     
            $check = DB::table('expensebenificiary')->where('accountnumber', $accountnumber)->first();
            if(!empty($chek)){
            $benificiary = $check->id;
            }else{            
            $input  = array("empid"=>$empid,"name"=>$name,"headid"=>$natureid,"accountnumber"=>$accountnumber,"ifsc"=>$ifsc,"createdate"=>NOW());
             $benificiary = DB::table('expensebenificiary')->insertGetId($input);
            }
            }else{
            $benificiary = $request->get('benificiary');
            }
          
           $crd = date('yy-m-d');
          $insertArray = array('expensedate'=>$crd,'natureid'=>$natureid,'benificiary'=>$benificiary,'amount'=>$amount,'empid'=>$empid,'remark'=>$remark,'filename'=>$filename->getClientOriginalName(),'status'=>0,'paid'=>0,'createdat'=>NOW());


          $insert = Expense::insert($insertArray);

          if($insert){

            $res['result']  ='Expense post successfully';
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);


    }


    //get expense data accroding employee

    public function GetExpenseData(Request $request){

         $empid = $request->get('empid');

         $vArray = array('empid'=>'required');

         check_validation($request,$vArray);

         $result = Expense::join('expensenature','expensenature.id','=','expense.natureid')->join('expensebenificiary','expensebenificiary.id','=','expense.benificiary')->where('expense.empid',$empid)->orderBy('id', 'DESC')->get(['expense.*','expensenature.name as expensehead','expensebenificiary.name as benificiaryname','expense.status']);
         
         if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);

    }

    //Add benificiary method here 


    public function AddBenificiary(Request $request){

            $empid = $request->get('empid');
            $name = $request->get('name');
            $headid = $request->get('headid');
            $typeid = $request->get('typeid');
            $accountnumber = $request->get('accountnumber');
            $ifsc = $request->get('ifsc');
            $bankstatement = $request->file('bankstatement');

            $vArray = array('empid'=>'required','name'=>'required','headid'=>'required','accountnumber'=>'required','ifsc'=>'required',"bankstatement"=>'required');

            check_validation($request,$vArray);

             if(!empty($bankstatement)){
            $destinationPath = '../expense/benificiaryimages/';

            $bankstatement->move($destinationPath,$bankstatement->getClientOriginalName());
          }else{
            $bankstatement = 'demo.jpg';
          }


            $insertArray = array('name'=>$name,'headid'=>$headid,'accountnumber'=>$accountnumber,'empid'=>$empid,'ifsc'=>$ifsc,'createdate'=>NOW(),'bankstatement'=>$bankstatement->getClientOriginalName());

            $check = Benificiary::where('accountnumber',$accountnumber)->where('name',$name)->first();

            if($check){
            
            $res['result']  ='Already have account for this account and name';
            $res['success'] = false;            
            $res['code']    = 401;

            }else{
            $insert = Benificiary::insert($insertArray);
            if($insert){

            $res['result']  ="Add successsfully.";
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
          }
            return Response::json($res);


    }
    
    //get benificiary list

    public function GetBenificiaryList(Request $request){

         $empid = $request->get('empid');

         $vArray = array('empid'=>'required');

         check_validation($request,$vArray);

         $user = DB::table('employee')->where('id', $empid)->first();
         $acno = $user->acno;  


         $result = Benificiary::where('empid',0)->orwhere('empid',$empid)->where('accountnumber','!=',$acno)->join('expensenature','expensebenificiary.headid','=','expensenature.id')->orderBy('id', 'DESC')->get(['expensebenificiary.*','expensenature.name as headname']);
         if(!empty($result)){

            $res['result']  =$result;
            $res['success'] = true;            
            $res['code']    = 200;                      

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);

    }

    //method for delete benificiary 

    public function DeleteBenificiary(Request $request){

          $benificiaryid = $request->get('benificiaryid');

          $vArray = array('benificiaryid'=>'required');

          check_validation($request,$vArray);

          $check = Benificiary::where('id',$benificiaryid)->delete();

          if($check){

            $res['result']  ="Delete successfully";
            $res['success'] = true;            
            $res['code']    = 200; 
                     

            }else{
            $res['result']  ='Error Occur';
            $res['success'] = false;            
            $res['code']    = 401;
            }
            return Response::json($res);

    }

    //approve and reject expense 

    public function ApproveRejectExpense(Request $request){

            $expenseid = $request->get('expenseid');
            $status = $request->get('status');  
            $empid = $request->get('empid');  

            $vArray = array('expenseid'=>'required','status'=>'required');

            check_validation($request,$vArray);

            if($status==3){
                   Expense::where('id',$expenseid)->update(['paid'=>1]);
            }else{
              Expense::where('id',$expenseid)->update(['status'=>$status]);
            }

            // return Redirect('../default.php#&rtl=expense/view-expense-all.php&rtl=tableDiv&rtl=formDiv&rtl=loading');
           if(empty($empid)) {
            return Redirect('../default.php#&rtl=expense/view-expense-all.php&rtl=tableDiv&rtl=formDiv&rtl=loading');

          }else{
            return Redirect('../default.php#&rtl=expense/view-expense.php?empid='.base64_encode($empid).'&rtl=tableDiv&rtl=formDiv&rtl=loading');

          }

          

    }
}
