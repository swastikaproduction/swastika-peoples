<?php

namespace App\Modules\Api\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Api\Models\Users;
use Response,Helper,Validator,file,DB;

class UsersController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        echo "sharad";
    }
    
 public function  checkLogin(Request $request){

        $baseUrl = 'http://localhost/hrms/trunk/hrmold/images/';

        $username = $request->get('username'); 
        $password = $request->get('password');
        $tokenId = $request->get('tokenId');

        $vArray = array('username'=>'required',
                        'password'=>'required',
                        'tokenId'=>'required');

        check_validation($request,$vArray);


        $result=Users::where('username',$username)->where('password',$password)->where('left',0)->first();
        $empid = $result['id'];

        $check = Users::where('imdboss',$empid)->count();

        if($check>0){
            $c ='true';
        }else{
            $c = 'false';
        }

        if(!empty($result)){
            $update = Users::where('workemail',$username)->update(['tokenId'=>$tokenId]);
            $res['message']  ='Login Successfully..!';    
            $res['result']  =$result;
            $res['success'] = true;
            $res['hod']      = $c;
            $res['tokenId'] = $tokenId;
            $res['baseUrl'] =$baseUrl;
            $res['code']    = 200;

        }else{
            $res['message']  ='wrong credentials'; 
            $res['result']  =null;
            $res['success'] = false;
            $res['code']    = 401;
        }

        return Response::json($res);     
    }
 //get employee profile data here
    public function getProfile(Request $request){
           
            $profileUrl = 'http://183.182.86.91:8088/hrm/Api/ProfileImages';
            $userId = $request->get('userId');
            $vArray = array('userId'=>'required');

    	  check_validation($request,$vArray);

    	  $result = Users::where('id',$userId)->first();
          $empid = $result['id'];
           $check = Users::where('imdboss',$empid)->count();

            if($check>0){
                $c ='true';
            }else{
                $c = 'false';
            }

    	  if(!empty($result)){

    	  	$res['message']  ='result found';    
			$res['result']  =$result;
            $res['hod']      = $c;
            $res['profileUrl']=$profileUrl;
			$res['success'] = true;
			$res['code']    = 200;

        }else{

            $res['message']  ='result not found'; 
			$res['result']  =null;
			$res['success'] = false;
			$res['code']    = 401;
        }

     return Response::json($res);

    }
    //method for update profile

   public function ProfileUpdate(Request $request){

         $userId = $request->get('userId');
         $profilePic = $request->file('profilePic');
         $fileName = '';
         
         $vArray = array('userId'=>'required','profilePic'=>'required');
         check_validation($request,$vArray);

         if(!empty($profilePic)){
              
              $destinationPath = 'ProfileImages';

              $fileName = $profilePic->getClientOriginalName();

              $profilePic->move($destinationPath,$profilePic->getClientOriginalName());
             }

         $update = Users::where('id',$userId)->update(['image'=>$fileName]);

         if($update){
           $res['result']  ='Profile Update Successfuly..!';
            $res['success'] = true;
            $res['code']    = 200;            
           }else{
            $res['result']  ='error occur';
            $res['success'] = false;
            $res['code']    = 401;
           }

        return Response::json($res);

   }
  // get profile pic data

   public function GetProfilePic(Request $request){

         $userId = $request->get('userId');

         $vArray = array('userId'=>'required');

         check_validation($request,$vArray);

         $result = Users::where('id',$userId)->first();

         if(!empty($result)){
           
             $res['result']  =$result;
            $res['success'] = true;
            $res['code']    = 200;      

         }
   }
   //forgot password api

   public function ForgotPassword(Request $request){

       $email = $request->get('email');
       $vArray = array('email'=>'required');
       check_validation($request,$vArray);

       $check = Users::where('workemail',$email)->first();

       if(!$check){
            $res['result']  ="This email address is not exist with us..!";
            $res['success'] = false;
            $res['code']    = 401;
       }else{ 
            // $code = rand(100000, 999999);

            // Users::where('workemail',$email)->update(['password'=>$code,'passwordflag'=>0]);

            $ch = curl_init();
            $curlConfig = array(
            CURLOPT_URL            => "http://183.182.86.91:13780/hrm/attendance/cron/sentforgottenmail.php?email='$email'",
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array(
            'email' => $email        
           
            )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

            $res['result']  ="Password will be sent on your email address";
            $res['success'] = true;
            $res['code']    = 200;
       }
       return Response::json($res);
       
   }

   //check login for calling server

    public function  checkLoginCalling(Request $request){

        $baseUrl = 'http://localhost/hrms/trunk/hrmold/images/';

        $username = $request->get('username'); 
        $password = $request->get('password');
        // $tokenId = $request->get('tokenId');

        $vArray = array('username'=>'required',
                        'password'=>'required',
                        );

        check_validation($request,$vArray);


        $result=Users::where('username',$username)->where('password',$password)->where('left',0)->first();
        $empid = $result['id'];

        $check = Users::where('imdboss',$empid)->count();

        if($check>0){
            $c ='true';
        }else{
            $c = 'false';
        }

        if(!empty($result)){
            // $update = Users::where('workemail',$username)->update(['tokenId'=>$tokenId]);
            $res['message']  ='Login Successfully..!';    
            $res['result']  =$result;
            $res['success'] = true;
            $res['hod']      = $c;
            // $res['tokenId'] = $tokenId;
            $res['baseUrl'] =$baseUrl;
            $res['code']    = 200;

        }else{
            $res['message']  ='wrong credentials'; 
            $res['result']  =null;
            $res['success'] = false;
            $res['code']    = 401;
        }

        return Response::json($res);     
    }

 

}
