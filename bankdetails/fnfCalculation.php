<?php
include("../include/config.php");

$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT name,salary,empid FROM `employee` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);


?>
<div class="moduleHead">
<br/>

	
</div>



<div class="shadow" style="background:#fff">
<div class="row" >

	<div class="col-sm-12 subHead">
		FnF Calculation
	</div>

	<div class="col-sm-2 formLeft req">
		Name
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp0" class="inputBox" value="<?php echo $row['name'];?>" readonly>
	</div>

	<div class="col-sm-2 formLeft req">
		Employee Code
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="empcode" class="inputBox" value="<?php echo $row['empid'];?>" readonly>
		<input type="hidden" name="req" id="empid" value="<?php echo $id; ?>">
	</div>

</div>

<div class="row" id="hide1" style="display: none;" >

	<div class="col-sm-2 formLeft req">
		Actual Salary
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="empcode" class="inputBox" value="<?php echo $row['salary'];?>" readonly>
	</div>

	<div class="col-sm-2 formLeft ">
		 Salary Calculated By Attendance
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="salary_calculated_by_attendance" class="inputBox" value="" readonly="readonly" >
	</div>	

</div>

<div class="row" id="hide2" style="display: none;" >

	<div class="col-sm-2 formLeft ">
		 Salary Calculated By Heads Comment
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="salary_calculated_by_heads_comment" class="inputBox" value="" readonly="readonly" >
	</div>

	<div class="col-sm-2 formLeft ">
		Leave Deducton Salary
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="leave_deductun_salary" class="inputBox" value="" readonly="readonly" >
	</div>

</div>

<div class="row" id="hide3" style="display: none;" >
	<div class="col-sm-2 formLeft ">
		OD
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="other_deduction" value="" readonly="readonly" class="inputBox" >
	</div>	

	<div class="col-sm-2 formLeft ">
		PT
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" id="personal_tax" readonly="readonly" value="" class="inputBox" >
	</div>

</div><hr>

<div class="row" >
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
        <option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row" >

	<div class="col-sm-2 formLeft ">
		Loan amount
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="inp5" class="inputBox" value=""  >
	</div>

	<div class="col-sm-2 formLeft ">
		Client debit amount
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="" title="" id="inp4" class="inputBox" value="" >
	</div>

</div>

<div class="row" >

	<div class="col-sm-2 formLeft ">
		RM has a client Debit
	</div>
	<div class="col-sm-4 formRight">
		<select class="inputBox" name="rm_client_debit" id="inp6">
			<option value="1">No</option>
			<option value="2">Yes</option>
		</select>
	</div>

	<div class="col-sm-2 formLeft ">
		 Head's Comment Salary
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="heads_comment_salary" title="" id="heads_comment_salary" class="inputBox" value=""  >
	</div>

	
</div>


<div class="row" style="border-bottom: 1px #eee solid;">

	<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="fnfCalculaton(<?php echo $row['salary'];?>);" type="button">
		<i class="fa fa-check"></i>&nbsp;&nbsp;Calculate</button>
		<br/><br/><br/>
	</div>	
</div> 

</div>

<div class="row" >

	<div class="col-sm-2 formLeft ">
		Salary Released By
	</div>
	<div class="col-sm-4 formRight">
		<select class="inputBox" name="salary_released_by" id="salaryReleasedBy">
			<option value="1">By Attendance Calculation</option>
			<option value="2">By Head's Comment</option>
		</select>
	</div>
	
</div>

<div class="row" >

	<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="saveReleasedBy" class="btn btn-primary" onclick="savefnfCalculaton();" type="button">
		<i class="fa fa-check"></i>&nbsp;&nbsp;Save</button>
		<br/><br/><br/>
	</div>	
</div> 

</div>


</div>
<br />
<br />
<br />
<br />
<br />


