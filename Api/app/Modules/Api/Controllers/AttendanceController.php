<?php

namespace App\Modules\Api\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Api\Models\Branch;
use App\Modules\Api\Models\Department;
use App\Modules\Api\Models\Attendance;
use App\Modules\Api\Models\Shift;
use Response,Helper,Validator,file,DB,Schema;

class AttendanceController extends Controller
{
   
   //get last attenedance month

  public function GetAttendance(Request $request){
 
     $userId = $request->get('userId');
     $vArray = array('userId'=>'required');
     check_validation($request,$vArray);

     $check = Attendance::where('empid',$userId)->orderBy('month','desc')->orderBy('year','desc')->first();

     if(!empty($check)){

     $month = $check['month'];
     $year = $check['year'];

     $tableName = 'attendance_'.$month.'_'.$year;

     $result = DB::table($tableName)->where('empid',$userId)->get();

     if(!empty($result)){
       
      $res['result']  =$result;
      $res['success'] = $tableName;
      $res['code']    = 200;

      }else{
      
      $res['result']  =null;
      $res['success'] = false;
      $res['code']    = 401;
        }
     }else{
      
      $res['result']  ='result not found';
      $res['success'] = false;
      $res['code']    = 401;

     }

     

        return Response::json($res);     
  }

  //get attendeance according to month

  public function GetMonthWiseAttendance(Request $request){

     $userId = $request->get('userId');
     $month = $request->get('month');
     $year = $request->get('year');
     
     $vArray = array('userId'=>'required');

     check_validation($request,$vArray);

     

     $tableName = 'attendance_'.$month.'_'.$year;

     $table = Schema::hasTable($tableName);

     if(!empty($table)){
       
      $result = DB::table($tableName)->where('empid',$userId)->orderBy('date', 'ASC')->get();

     if(!empty($result)){
       
      $res['result']  =$result;
      $res['success'] = true;
      $res['code']    = 200;

      }else{
      
      $res['result']  =null;
      $res['success'] = false;
      $res['code']    = 401;
       }

     }else{

      $res['result']  ='Data not found';
      $res['success'] = false;
      $res['code']    = 401;
     }


        return Response::json($res); 
  }

  //get attendance method here for last moth according to attendance log table


  public function GetSalary(Request $request){

      //variable declare here

     $userId = $request->get('userId');
     $month = $request->get('month');
     $year = $request->get('year');

     $vArray = array('userId'=>'required','month'=>'required','year'=>'required');

     check_validation($request,$vArray);

     $result = Attendance::where('empid',$userId)->where('month',$month)->where('year',$year)->orderBy('id','desc')->get();

     if(!empty($result)){

      $res['result']  =$result;
      $res['success'] = true;
      $res['code']    = 200;

     }else{

      $res['result']  ='No data found';
      $res['success'] = false;
      $res['code']    = 401;
      
     }

     return Response::json($res);
  }
    
    //mehtod for get daily attendance

public function GetDailyAttendance(Request $request){

    $empCode = $request->get('empCode');

    $vArray  = array("empCode"=>'required');

    check_validation($request,$vArray);

    $datetime = date("Y-m-d H:i:s");
    $date = date("d/m/Y");
    $daterange = date("d").date("m").date("Y");
    $daterange = $daterange."-".$daterange;

    $url = "http://183.182.86.91:838/cosec/api.svc/v2/attendance-daily?action=get;format=json;date-range=".$daterange;

    $username = 'sa';
    $password = 'poiuytrewq123';

    $context = stream_context_create(array(
    'http' => array(
    'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
    ));
    $contents = file_get_contents($url, false, $context);

    $contents = json_decode($contents,true);
    $fulldata = $contents['attendance-daily'];

    foreach($fulldata as $val)
    {
    if($val['userid']==$empCode){
    $punchdata  = array("punchin"=>$val['punch1'],"outpunch"=>$val['outpunch']);
    }
    }
    if(!empty($punchdata)){
      return Response::json($punchdata);
    }else{
      $json = '{"punchin":"00\/00\/0000 00:00:00","outpunch":""}';
     echo $json;
    }
    
}  

 //method for get deduction from pool and salry

 public function GetPoolDeduction(Request $request){

       $userId   = $request->get('userId');
       $month  = $request->get('month');
       $year = $request->get('year');

       $vArray = array("userId"=>'required',"month"=>'required',"year"=>'required');

       check_validation($request,$vArray);


      $result = Attendance::where('month',$month)->where('year',$year)->where('empid',$userId)->first();

      if(!empty($result)){

        $res['result'] = $result;
        $res['code']  =200;

      }else{
        $res['result']='Data not found';
        $res['code']  =401;
      }

  return Response::json($res);

 }

}
