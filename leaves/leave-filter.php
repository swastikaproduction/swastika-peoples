<?php
include("../include/config.php");
$data = Array();
if(!isset($_GET['my']))
{
if($loggeduserid == '124')
{
if(isset($_GET['pending']))
{
	$penStr = " AND leaverequests.status = '0'";
}
else
{
	$penStr = " AND (1=1)";	
}
$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id ".$penStr." ORDER BY leaverequests.id DESC") or die(mysqli_error($con));

}
else
{
if(isset($_GET['pending']))
{
	$penStr = " AND leaverequests.status = '0'";
}
else
{
	$penStr = " AND (1=1)";	
}

	$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.imdboss = '$loggeduserid' ".$penStr."  ORDER BY leaverequests.id DESC") or die(mysqli_error($con));

}
}
else
{
	$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval,employee.id FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.id = '$loggeduserid' ".$penStr."  ORDER BY leaverequests.id DESC") or die(mysqli_error($con));
}
$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>


<div class="moduleHead">
<br/>
	<div class="moduleHeading" onclick="toogleFormTable();">
	Request filter page
	</div>
</div>
<section style="background: white;">
	<div class="row">
			<div class="col-md-4">
			<label>Branch</label>
				<select id="branchid" class="form-control" onchange="getBranchwiseEmployee();">
				<option value="">select branch name</option>
				<?php
				$query = mysqli_query($con,"select * from branch order by name");
				 while($row=mysqli_fetch_array($query)){
				?>
				<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
		<?php	}	?>
			</select>
		</div>
		<div class="col-md-4">
			<label>Emplyee Name</label>
			<select id="empid" class="form-control">
				<option value="">select name</option>
				
			</select>
		</div>
	
		<div class="col-md-4">
			<label>Type</label>
			<select name="type" class="form-control" id="type">
				<option value="">select type</option>
				<option value="1">Leave</option>
				<option value="2">Timing Mismatch</option>
				<option value="3">Extra Working</option>
				<option value="4">OD</option>
			</select>
		</div>
	</div>
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-4">
			<label>HOD Approval</label>
			<select name="hodapproval" id="hodapproval" class="form-control">
				<option value="">select</option>
				<option value="0">Pending</option>
				<option value="2">Rejected</option>
				<option value="1">Approved</option>
			</select>
		</div>
		<div class="col-md-4">
			<label>HR Approval</label>
			<select name="hrapproval" id="hrapproval" class="form-control">
				<option value="">select</option>
				<option value="0">Pending</option>
				<option value="2">Rejected</option>
				<option value="1">Approved</option>
			</select>
		</div>
		<div class="col-md-2">
			<label>From Date</label>
			<input type="date" id="fromdate" class="form-control">
		</div>
		<div class="col-md-2">
			<label>To Date</label>
			<input type="date" id="todate" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<button name="submit" class="btn btn-success" style="margin-top: 10px;margin-bottom: 20px;" onclick="GetEmployeeFilterdata();">Submit</button>

			<button class="btn btn-success" onclick="window.open('leaves/getEmployeeRequestReport.php?empid='+$('#empid').val()+'&branchid='+$('#branchid').val()+'&type='+$('#type').val()+'&hodapproval='+$('#hodapproval').val()+'&hrapproval='+$('#hrapproval').val()+'&fromdate='+$('#fromdate').val()+'&todate='+$('#todate').val(),'_blank');" style="margin-top: 10px;margin-bottom: 20px;" ><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT DATA</button>
		</div>
	</div>
</section>

	

<div id="requestfilterdata" style="background: white;">
</div>
<div class="tabelContainer divShadow" style="height:auto;" id="leaverequestsfilter">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:5%;">#</th>
<th style="width:15%">Employee</th>
<th style="width:10%">Branch</th>
<th style="width:15%;">Type</th>
<th style="width:15%;">On Date</th>
<th style="width:30%">Details</th>
<th style="width:10%">HOD Approval</th>
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
?>
<?php
if($row[12] != $loggeduserid && $loggeduserid != '124')
{
	?>
<tr id="tableRow<?php echo $row[8];?>" onclick="getModal('leaves/changestatushod.php?id=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">


	<?php
}
?>
<td><?php echo $i;?></td>
<td><?php echo $row[0];?></td>
<td><?php echo $row[1];?></td>
<td>

<?php 
	if($row[2] == '1')
	{
		?>
	<i class="fa fa-circle" style="color:#ffb22b"></i>&nbsp;&nbsp;&nbsp;
		<?php
		echo "Leave";
	}
	else if($row[2] == '2')
	{
		?>
	<i class="fa fa-circle" style="color:#1976d2"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Timing Mismatch";
	}

	else if($row[2] == '3')
	{
		?>
	<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Overtime";
	}

	else if($row[2] == '4')
	{
		?>
	<i class="fa fa-circle" style="color:#e32eab"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "OD Request";
	}

else if($row[2] == '5')
	{
		?>
	<i class="fa fa-circle" style="color:#fed3c5"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Half Day";
	}
?></td>
<td><?php echo $row['ondate'];?></td>
<td>
<?php
if($row[2] == 1)
{
	?>

<span class="label label-default">
	<?php echo date("D, d M Y",strtotime($row[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo date("D, d M Y",strtotime($row[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-default">
	<?php echo $row[9];?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo $row[10];?></span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default">
<?php echo date("D, d M Y",strtotime($row[5]));?>
</span>



	<?php
}
?>
</td>


<td style="padding-top:15px;" <?php if($loggeduserid == '124')
{
?>

<?php	
}
?>>
	<?php 
	if($row[11] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row[11] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row[11] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>



<td style="padding-top:15px;" <?php if($loggeduserid == '124')
{
?>
onclick="getModal('leaves/changestatus.php?id=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')"
<?php	
}
?>>
	<?php 
	if($row[6] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row[6] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row[6] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>


<td>
<?php echo $row[7];?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>
