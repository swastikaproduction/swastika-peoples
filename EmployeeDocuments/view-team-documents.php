<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Documents</li>
</ol>
</section>
<!-- Main content -->
<section class="content">            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" data-toggle="modal" data-target="#myModal11">Upload New Documents</span>            -->
</div>  
   
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>

<th>S.NO</th>
<th>Document Name</th>
<th>View</th>            
<th>Uploaded date</th>
<!-- <th>Action</th>  -->
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/config.php';
$empid = $_GET['empid'];
$query = mysqli_query($con,"select * from documents where dataid='$empid'") or die(mysqli_error($con));

while($row=mysqli_fetch_array($query)){

	$type = $row['type'];

	if($type==3){
		$name = 'Updated CV';
	
	}else if($type==4){
      $name = 'Duplicate Copy Of Offer Letter';
	}else if($type==5){
      $name = 'References 1 (HR & Reporting Head)';
	}else if($type==6){
      $name = 'References 2 (HR & Reporting Head)';
	}else if($type==7){
      $name = 'SSC Board Mark Sheet';
	}else if($type==8){
      $name = 'HSC Board Mark Sheet';
	}else if($type==9){
      $name = 'Degree Certificate & Mark Sheet';
	}else if($type==10){
      $name = 'Aadhar Card';
	}else if($type==11){
      $name = 'Pan Card';
	}else if($type==12){
      $name = 'Residential Address Proof';
	}else if($type==13){
      $name = 'Last 03 Months Salary Slip (1)';
	}else if($type==14){
      $name = 'Last 03 Months Salary Slip (2)';
	}else if($type==15){
      $name = 'Last 03 Months Salary Slip (3)';
	}else if($type==16){
      $name = 'Relieving Letter';
	}else if($type==17){
      $name = 'Last 03 Months Bank Statement (1)';
	}else if($type==18){
      $name = 'Last 03 Months Bank Statement (2)';
	}else if($type==19){
      $name = 'Last 03 Months Bank Statement (3)';
	}else if($type==20){
      $name = 'Passbook of Existing Bank Account';
	}
	else if($type==21){
      $name = 'Passport Size Photo';
	}
	else if($type==22){
      $name = 'Photo';
	}
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $name;?></td>
<!-- <td><a href="EmployeeDocuments/view_documents.php?id=<?php echo base64_encode($row['id']); ?>">View</a></td> -->
<td><a href="Documentsfiles/<?php echo $row['filepath'];?>" download>View</a></td>
<td><?php echo $row['createdate'];?></td>                 
<!--  -->

</tr> 
<?php
}
?>                          

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Documents Upload</h4>
</div>
<form action="EmployeeDocuments/save.php" method="POST" enctype="multipart/form-data">
<div class="modal-body">
	
<div class="row">
	
<div class="col-md-6">
<label>Document Name</label>
<select name="doctype" class="form-control" required="">
	<option value="">select document name</option>
	<!-- <option value="3">Updated CV</option>
	<option value="4">Duplicate Copy Of Offer Letter</option>
	<option value="5">References 1 (HR & Reporting Head)</option>
	<option value="6">References 2 (HR & Reporting Head)</option>
	<option value="7">SSC Board Mark Sheet</option>
	<option value="8">HSC Board Mark Sheet</option>
	<option value="9">Degree Certificate & Mark Sheet</option> -->
	<option value="10">Aadhar Card</option>
	<option value="11">Pan Card</option>
<!-- 	<option value="12">Residential Address Proof</option>
	<option value="13">Last 03 Months Salary Slip (1)</option>
	<option value="14">Last 03 Months Salary Slip (2)</option>
	<option value="15">Last 03 Months Salary Slip (3)</option>
	<option value="16">Relieving Letter</option> -->
	<option value="17">Bank Details</option>
	<option value="22">Photo</option>
	<!-- <option value="18">Last 03 Months Bank Statement (2)</option>
	<option value="19">Last 03 Months Bank Statement (3)</option>
	<option value="20">Passbook of Existing Bank Account</option>
	<option value="21">Passport Size Photo</option> -->
	
</select>
</div>
<div class="col-md-6">
	<label>Document File</label>
<input type="file" class="form-control" name="docfile" enctype='multipart-formdata' required=""> 
</div>
</div>

</div>
<div class="modal-footer" >
<button type="submit"  class="btn btn-success">Submit</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</form>
</div>  

</div>  


