<?php
include'../include/config.php';
$empid = $_COOKIE['emanagerid'];
?>
 
    <!-- Main content -->
    <section class="content" style="background-color: white;">
      <form action="expense/post-expense.php"  method="post" enctype="multipart/form-data">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Post Expense</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body">

                <div class="row">
                  <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1" style="/*font-weight: 700; */" >Benificiary Name</label>
                  <select class="form-control" name="benificiaryid" id="benificiaryid" onchange="Benificiary();" required="">
                    <option value=''>select benificiary</option>
                    <option value='self'>self</option>
                  <?php 
                    $query = mysqli_query($con,"select * from expensebenificiary where empid='$empid' ");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
                   
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label></label>
                  <span class="btn btn-success" style="margin-top: 25px;" data-toggle="modal" data-target="#myModal12" class="btn btn-success">Add</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputEmail1">Expense Head</label>
                  <select class="form-control" name="expensehead" id="expensehead" required="">
                    <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
                  </select>
                </div>
              </div>
                <div class="col-md-4" id="quater_period"  style="display: none;">
                <div class="form-group">
                  <label for="exampleInputEmail1">Expense Type</label>
  
                  <select class="form-control" name="expensetype" id="expensetype">
                   <option value="">select type</option>
                   <?php 
                    $query = mysqli_query($con,"select * from expensetype");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['type'];?></option>
                
                <?php } ?>

                  </select>
                </div>         

              </div>         
            
            </div>
            
            <div class="row">
              <?php $d = date('Y-m-d'); ?>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Amount</label>

                  <input type="text" name="amount" class="form-control allownumericwithoutdecimal"  required="" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Receipt</label>
                  <input type="file" name="receipt" class="form-control" required="">
                </div>
              </div>
              
            </div>            
  
         </div>
            
 
  </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
               <span class="btn btn-success" onclick="getModule('expense/manager-expense-view.php','formDiv','tableDiv','loading');">Back</span>
              </div>
            </form>
          </div>           
                
                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
 <div class="modal fade" id="myModal12" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/post-benificiary.php" method="post">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Benificiary </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Benificiary Name</label>
<input type="hidden" name="pid" id="pid">
<input type="text" name="name" class="form-control" id="name" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="expenseheadid" name="expenseheadid" class="form-control" onchange="GetExpenseHeadData();" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Account Number</label>
<input type="text" name="accountnumber" class="form-control allownumericwithoutdecimal" required="">
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>IFSC Code</label>
<input type="text" name="ifsccode" class="form-control" required="">
</div>
</div>
</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->