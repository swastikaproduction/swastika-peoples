<?php
include("../apiconfig.php");
?>
<div class="backDropCustom" id="backDrop" >
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">
<div class="purplish" style="height:auto;padding:0px 20px 80px 20px;text-align:left;position:relative">
<span style="color:#fff;font-size:20px;font-weight:bold">Support & Contact</span>	
	<div style="float:right;padding:10px 0px 0px 10px;">

		<div class="purplish shadow" style="width:100px;height:10px;border-radius:20px;margin:0px 20px 0px 0px"></div>
		<div class="purplish shadow" style="width:100px;height:10px;border-radius:20px;margin:10px 0px 0px 20px"></div>
		<div class="purplish shadow" style="width:100px;height:10px;border-radius:20px;margin:10px 20px 0px 0px"></div>
	</div>

</div>
<div style="padding:20px;text-align:left">

For any queries please contact by:
<br/>
<br/>
<a href="tel:0731-6644255" class="button button-positive">
<i class="fa fa-phone"></i>&nbsp;&nbsp;0731-6644255</a>
<br/>
<br/>
<a href="mailto:hr@swastika.co.in" class="button button-positive">
<i class="fa fa-envelope"></i>&nbsp;&nbsp;hr@swastika.co.in
</a>
<br/>
<br/>

<button class="button button-positive" onclick="window.open('https://www.swastika.co.in/privacy-policy','_blank')">
<i class="fa fa-check"></i>&nbsp;&nbsp;Privacy Policy
</button>
<br/>
<br/>
 
<br/>
<br/>
<br/>
<strong style="font-size:25px;">Important Links</strong>
<br/>
<?php
$getdata = mysqli_query($con,"SELECT * FROM `tutorial` ORDER BY `name` ASC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getdata))
{
	?>
	<div style="padding:20px 0px" onclick="window.open('<?php echo $row['notes'];?>','_blank')">
<span style="font-size:14px;font-weight:bold"><?php echo $row['name'];?></span>
<br/>
<button class="btn btn-sm btn-primary">VIEW</button>
<br/>
<br/>
</div>
	<?php
}
?>

<center>

</center>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>



</div>

</div>
</div>

