<?php
include("../include/config.php");
$employees = Array();
if($loggeduserid == '124')
{
$employees = getData('employee','*','name','ASC');	
}
elseif($profile == 20){
      $branch = [];   
      $getZones = mysqli_query($con,"SELECT `zones` FROM employee WHERE id='$loggeduserid'") or die(mysqli_error($con));
      $rowZones = mysqli_fetch_array($getZones);

      $getData = mysqli_query($con,"SELECT id FROM `branch` WHERE zoneid IN ($rowZones[0])") or die(mysqli_error($con));
        while($row=mysqli_fetch_array($getData)){
        $branch[] .= $row[0];
        }
        $branches = implode(',', $branch);
        $employees = mysqli_query($con,"SELECT * FROM employee where `branch` IN ($branches) AND `left`=0 AND employee.profileid NOT IN (2,3,4,22) AND employee.empid NOT IN ('IND8303883') ") or die(mysqli_error($con));
}
else
{
	$employees = getData('employee','**','id',$loggeduserid);
}


$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>
<div class="moduleHead">
<br/>
<?php
if(!isset($_GET['type']) && $loggeduserid == '124'){
?>
<div style="float:right">
	<button class="btn btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 UPLOAD NEW SHEET</button>
</div>
<?php	
}
?>
<div class="moduleHeading">
<?php
if(!isset($_GET['type']))
{
	echo "Attendance";
	$type = '0';
}
else
{
	echo "Salary";
	$type = $_GET['type'];
}
?>

</div>
</div>
<div class="tabelContainer shadow" style="height:auto;background:#fff">

<div class="row">
<div class="col-sm-12 subHead">

<?php
if(!isset($_GET['type']))
{
	echo "Current Attendance";
	$type = '0';
}
else
{
	echo "Select Filters";
	$type = $_GET['type'];
}
?>
	
</div>

	<div class="col-sm-2 formLeft">
		Employee
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="atEmp">
	<?php
if($loggeduserid == '124')
{
?>
	<option value="ALL">ALL Employees</option>
<?php
}
	?>

	<?php
	foreach($employees as $val)
	{
		if($val['left'] == 0)
		{
		?>
			<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
		<?php
		}
	}
	?>
		</select>
	</div>	

</div>


<div class="row" style="border-bottom: 1px #eee solid;">
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" <?php if($loggeduserid==124){ ?> onchange="freezingattendance(this.value,document.getElementById('atYear').value)" <?php } ?> id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
        <option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" <?php if($loggeduserid==124){ ?> onchange="freezingattendance(document.getElementById('atMonth').value,this.value)" <?php } ?> id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
<br/>
<?php if($loggeduserid==124){ ?>
<button  class="btn btn-primary" id="attendance_freez" onclick="freez_attendance(this.value)" style="display: none"><i class="fa fa-th-list"></i>&nbsp;&nbsp;ATTENDANCE FREEZ</button>
&nbsp;&nbsp;
<?php } ?>
<?php if(!isset($_GET['type'])){?>

<button  class="btn btn-primary" id="atListViewHere"  onclick="getModule('attendance/getAttendance.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'atResult','','loading');atchangedone = 0;">
		<i class="fa fa-th-list"></i>&nbsp;&nbsp;GET DETAILS</button>

&nbsp;&nbsp;
	<button class="btn btn-primary"  onclick="window.open('attendance/getAttendanceexport.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT DATA</button>
	<?php
}
else
{
	?>
<button  class="btn btn-primary" id="atListViewHere"  onclick="getModule('attendance/getSalary.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'atResult','','loading');atchangedone = 0;">
		<i class="fa fa-th-list"></i>&nbsp;&nbsp;GET DETAILS</button>

&nbsp;&nbsp;


	<button class="btn btn-primary"  onclick="window.open('attendance/getSalaryExport.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT DATA</button>
	<?php
}
?>


		

		<br/><br/>
	</div>
</div>

<div id="atResult"></div>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>