<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::TRACK VEHICLE::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>  

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>



<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/vendor/jquery.easing.1.3.js"></script>

<!-- we provide marker for google maps, so here it comes  -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- we use markerAnimate to actually animate marker -->
<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/vendor/markerAnimate.js"></script>

<!-- SlidingMarker hides details from you - your markers are just animated automagically -->
<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/SlidingMarker.js"></script>
<script>
  SlidingMarker.initializeGlobally();
</script>

</head>
<body>
<button class="btn btn-primary btn-sm" onclick="getData();">GET DATA</button>
<button class="btn btn-primary btn-sm" onclick="moveit();">MOVE DATA</button>

<div id="map" style="width:100%;height:500px;"></div>
</body>
<script type="text/javascript">

var inHeight = window.innerHeight;
$('#map').animate({height:inHeight},50);
var latArray = [];
var longArray = [];
var busArray = [];
busArray[0] = 'G7D07436';
busArray[1] = 'G7D07662';



  function getData()
  {
    var url = 'trackSample.php';
    genAjax(url,'',function(response)
    {
      response = JSON.parse(response);
      latArray['G7D07436'] = 26.1948;
      longArray['G7D07436'] = 78.2033;


      latArray['G7D07662'] = 26.1951;
      longArray['G7D07662'] = 78.2035;

      placeMarker();

      console.log(response['G7D07436']);
      console.log(response['G7D07662']);      
      console.log(response['G7D07757']);
      console.log(response['G7D07758']);
    });
  }


function moveit()
{

        latArray['G7D07436'] = 26.1920;
      longArray['G7D07436'] = 78.2010;


      latArray['G7D07662'] = 26.1971;
      longArray['G7D07662'] = 78.2065;

placeMarker();
}

var myLat = localStorage.getItem("myLat");
myLat = parseFloat(myLat);

var myLang = localStorage.getItem("myLang");
myLang = parseFloat(myLang);
     

 
 var marker = [];
 var map;
 var myCity = [];
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter=new google.maps.LatLng(myLat,myLang);
  var mapOptions = {center: myCenter, zoom: 15};
  map = new google.maps.Map(mapCanvas, mapOptions);


}


  var direction = 1;
    var rMin = 50, rMax = 200;
    var intVal = 30;

var animCity;
var infowindow = [];
function animateRadius(circleid)
{
  

  
       animCity = setInterval(function() {
            var radius = myCity[circleid].getRadius();
            var zoom = map.getZoom();
            
            if(zoom <= 15)
            {
              rMax = 240;
              rMin = 60;
              intVal = 3;
            }
            else if(zoom == 16)
            {
              rMax = 120;
              rMin = 0;
              intVal = 2;
            }
            else if(zoom == 17)
            {
              rMax = 60;
              rMin = 0;
              intVal = 1;
            }
            else if(zoom == 18)
            {
              rMax = 30;
              rMin = 0;
              intVal = 0.5;
            }
            else if(zoom == 19)
            {
              rMax = 15;
              rMin = 0;
              intVal = 0.25;
            }

            else if(zoom == 20)
            {
              rMax = 7.5;
              rMin = 0;
              intVal = 0.125;
            }


            else if(zoom == 21)
            {
              rMax = 3.75;
              rMin = 0;
              intVal = 0.0625;
            }

            
            if ((radius <= rMax)) {
                myCity[circleid].setRadius(radius + intVal);
                var fillOpacity = 1 - (radius/rMax);
                if(fillOpacity > 0.5)
                {
                  fillOpacity = 0.5;
                }
                myCity[circleid].setOptions({fillOpacity:fillOpacity});
            }
            else 
            {
              myCity[circleid].setRadius(rMin);
            }

        }, 30);

}



function placeMarker() {
    console.log(latArray);
  console.log(busArray.length);
for(k=0;k<busArray.length;k++)
{
console.log(busArray[k]);
  if(marker[busArray[k]])
  {
marker[busArray[k]].setPosition(new google.maps.LatLng(latArray[busArray[k]],longArray[busArray[k]]));
myCity[busArray[k]].setCenter(new google.maps.LatLng(latArray[busArray[k]],longArray[busArray[k]]));;
  }
  else
  {
        thisLoc = new google.maps.LatLng(latArray[busArray[k]],longArray[busArray[k]])

myCity[busArray[k]] = new google.maps.Circle({
  strokeWeight: 0,
            fillColor: '#076bff',
            fillOpacity: 0.3,
            map: map,
            center: thisLoc,
            radius: 20
          });


marker[busArray[k]] = new google.maps.Marker({
    position: thisLoc,
    map: map,
    optimized: false,
    icon:'images/pin-dot.png',
    id:busArray[k]
  });

animateRadius(busArray[k]);





google.maps.event.addListener(marker[busArray[k]], 'click', function() {
  infowindow[this.id].open(map,marker[this.id]);
  });
 infowindow[busArray[k]] = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-danger">USE THIS LOCATION</button>'
  });

  }
}

map.panTo(thisLoc);


}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&callback=myMap"></script>
</html>
