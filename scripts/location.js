var globalLatitude;
var globalLongitude;


var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 10
};


function success(pos) {
document.getElementById('calibrating').innerHTML = '<i class="fa fa-location-arrow"></i>&nbsp; Re-Calibrate My Location';
toast('<i class="fa fa-check"></i>&nbsp;&nbsp;Location Detected','toast bg-success','2000');
var crd = pos.coords;


globalLatitude = crd.latitude;
globalLongitude = crd.longitude;
/*

globalLatitude = 24.179506969967175;
globalLongitude = 78.19750785827637;
*/

localStorage.setItem("myLat",globalLatitude);
localStorage.setItem("myLang",globalLongitude);
myLat = globalLatitude;
myLang = globalLongitude;
};

function error(err) {
globalLatitude = 26.20645432645648;
globalLongitude = 78.19244250655174;


localStorage.setItem("myLat",globalLatitude);
localStorage.setItem("myLang",globalLongitude);
myLat = globalLatitude;
myLang = globalLongitude;
document.getElementById('calibrating').innerHTML = '<i class="fa fa-location-arrow"></i>&nbsp; Re-Calibrate My Location';
toast('<i class="fa fa-check"></i>&nbsp;&nbsp;Location Detected','toast bg-success','2000');


/*
		document.getElementById('calibrating').innerHTML = '<i class="fa fa-spinner fa-spin"></i>&nbsp; Calibrating Location..';

toast('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Location Detection is taking a while..','toast bg-warning','2000');

setTimeout(function(){
  navigator.geolocation.getCurrentPosition(success, error, options); 
},1000);
*/
};


function getFirstLocation()
{
	document.getElementById('calibrating').innerHTML = '<i class="fa fa-spinner fa-spin"></i>&nbsp; Calibrating Location..';
toast('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Detecting Location..','toast bg-warning','0');
	  navigator.geolocation.getCurrentPosition(success, error, options); 
}


