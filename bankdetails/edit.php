<?php
include("../include/config.php");
$id = $_GET['id'];
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "edit.php", $urltocall); 
$getData = mysqli_query($con, "SELECT * FROM `employee` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>


<div class="moduleHead">
	<div class="moduleHeading">
	Bank Details
	</div>
</div>
<div class="shadow">
<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Account No:
	</div>
	<div class="col-sm-10 formRight">
		<?php if(in_array('account_no',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text"  name="req" title="" id="inpbank0" class="inputBox" title="City Name" value="<?php echo $row['acno'];?>">
    	<?php } elseif(in_array('account_no',$permissions)) { ?>
	  	<input type="text" readonly="true"  name="req" title="" id="inpbank0" class="inputBox" title="City Name" value="<?php echo $row['acno'];?>">
 	   <?php } else { ?>
	  	<input type="text" readonly="true" name="req" title="" id="inpbank0" class="inputBox" title="City Name" style="display: none;" value="<?php echo $row['acno'];?>">
 	   <?php } ?>
	</div>	
</div>
<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Beneficiary Name
	</div>
	<div class="col-sm-10 formRight">
	   <?php if(in_array('Beneficiary_name',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text" name="req" title="" id="inpbank3" class="inputBox" title="City Name" value="<?php echo $row['benef'];?>">
    	<?php } elseif(in_array('Beneficiary_name',$permissions)) { ?>
		<input type="text" readonly="true" name="req" title="" id="inpbank3" class="inputBox" title="City Name" value="<?php echo $row['benef'];?>">
 	   <?php } else { ?>
		<input type="text" readonly="true" name="req" title="" id="inpbank3" class="inputBox" title="City Name" style="display: none;" value="<?php echo $row['benef'];?>">
 	   <?php } ?>
	</div>	
</div>


<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		IFSC
	</div>
	<div class="col-sm-10 formRight">
        <?php if(in_array('ifsc',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text" name="req" title="" id="inpbank1" class="inputBox" title="City Name" value="<?php echo $row['ifsc'];?>">
    	<?php } elseif(in_array('ifsc',$permissions)) { ?>
		<input type="text" readonly="true" name="req" title="" id="inpbank1" class="inputBox" title="City Name" value="<?php echo $row['ifsc'];?>">
 	   <?php } else { ?>
		<input type="text" readonly="true" name="req" title="" id="inpbank1" class="inputBox" title="City Name"  style="display: none;" value="<?php echo $row['ifsc'];?>">
 	   <?php } ?>
	</div>	

</div>


<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		ICICI BANK
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="inpbank2">
		<option value="I" <?php if($row['bankpref'] == 'I') echo "selected='selected'";?>>YES</option>
		<option value="N" <?php if($row['bankpref'] == 'N') echo "selected='selected'";?>>NO</option>
	</select>
	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','justCloseModal','inpbank',4,'moduleSaveButtontop:!SCROLL!Saving..','url:','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>
			<br/><br/><br/>
	</div>	


</div>
</div>
<br />
<br />
<br />
<br />
<br />

