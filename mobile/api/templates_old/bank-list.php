<?php
include("../apiconfig.php");
?>
<div class="backDropCustom" id="backDrop">
<div class="menuSpacer"></div>
<div class="contentCustom bluish" id="content">
<div style="position:absolute;top:50px;right:20px;z-index:20" onclick="changePage('viewThree','templates/viewThree.html','templates/bank-new.php','viewThreeRes','','strong','blueheader');globeTitle = 'Add New Bank'; globeTitleId = 'viewthreetitle'">
	<div class="tealCircle" style="background:#fff;color:#004087">+</div>
</div>

<div style="padding:20px;color:#fff;text-align:left">
<br/>
	<span style="font-size:20px;font-weight:bold">Banks</span>
	<br/>
	<strong>
We keep your financial details secure so no one sees your sensitive information.
	</strong>
	<br/>
	<br/>
</div>
<div style="position:relative">
	<table class="bankdocs" onclick="changePage('viewThree','templates/viewThree.html','templates/bank-edit.php','viewThreeRes','','strong','blueheader');globeTitle = 'Bank Details'; globeTitleId = 'viewthreetitle'">
		<tr>
			<td>
				<div class="tealCircle" style="background:#fb746c">H</div>
			</td>
			<td>
			<div style="float:right">
				<i class="fa fa-caret-right" style="font-size:20px;"></i>
			</div>
			<span style="font-size:20px;font-weight:bold">

				HDFC BANK
				</span>
				<br/>
				<span style="font-size:12px;">08374698723</span>
			</td>
		</tr>
	<tr>
			<td>
				<div class="tealCircle" style="">I</div>
			</td>
			<td>
			<div style="float:right">
				<i class="fa fa-caret-right"  style="font-size:20px;"></i>
			</div>
			<span style="font-size:20px;font-weight:bold">
				ICICI BANK
				</span>
				<br/>
				<span style="font-size:12px;">08374698723</span>
			</td>
		</tr>


<tr>
			<td>
				<div class="tealCircle" style="background:#28d2e4">S</div>
			</td>
			<td>
			<div style="float:right">
				<i class="fa fa-caret-right"  style="font-size:20px;"></i>
			</div>
			<span style="font-size:20px;font-weight:bold">
				SBI BANK
				</span>
				<br/>
				<span style="font-size:12px;">08374698723</span>
			</td>
		</tr>


		<tr>
			<td>
				<div class="tealCircle" style="background:#e84b53">K</div>
			</td>
			<td>
			<div style="float:right">
				<i class="fa fa-caret-right"  style="font-size:20px;"></i>
			</div>
			<span style="font-size:20px;font-weight:bold">
				KOTAK MAHIN..
				</span>
				<br/>
				<span style="font-size:12px;">08374698723</span>
			</td>
		</tr>

	</table>
<br/><br/><br/>
</div>

</div>
</div>