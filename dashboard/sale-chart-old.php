<?php
include('../include/config.php');
$from = $_GET['from'];
$to = $_GET['to'];
$type = $_GET['type'];
$invoiceArray = Array();
$paymentArray = Array();

if($type == 'thisfis')
{
  $from = date("Y")."-04-01 00:00:00";
  $to = (date("Y",(time() + 24 * 60 * 60 * 365)))."-03-31 23:59:59";
}


if($type == 'prevfis')
{
  $from = (date("Y",(time() - 24 * 60 * 60 * 365)))."-04-01 00:00:00";
  $to = date("Y")."-03-31 23:59:59";
}

function getMonthsInRange($startDate, $endDate) {
$months = array();
while (strtotime($startDate) <= strtotime($endDate)) {
    $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
    $startDate = date('d M Y', strtotime($startDate.
        '+ 1 month'));
}

return $months;
}

$monthRange = getMonthsInRange($from,$to);

foreach($monthRange as $val)
{
  $str = $val['year']."-".$val['month'];
  $range[] .= date("M-y",strtotime($str));
}

foreach($range as $val)
{
    $paymentArray[$val] = '0';
    $invoiceArray[$val] = '0';
}



//$range = Array['Jan','','Jan','Jan','Jan','Jan','Jan','Jan',];



$k=0;
$getData = mysqli_query($con,"SELECT * FROM `invoices` WHERE `date` BETWEEN '$from' AND '$to' ORDER BY `date` ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData['date']));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $invoiceArray[$thisMonth] = $invoiceArray[$thisMonth] + $rowData['grandtotal'];
  }
  else
  {
    $invoiceArray[$thisMonth] = $rowData['grandtotal'];
  }
   
}


$getData = mysqli_query($con,"SELECT * FROM `payments` WHERE `date` BETWEEN '$from' AND '$to'  ORDER BY `date` ASC") or die(mysqli_error($con));
while($rowData = mysqli_fetch_array($getData))
{
  $thisMonth = date("M-y",strtotime($rowData['date']));
  if(array_key_exists($thisMonth, $invoiceArray))
  {
    $paymentArray[$thisMonth] = $paymentArray[$thisMonth] + $rowData['amount'];
  }
  else
  {
    $paymentArray[$thisMonth] = $rowData['amount'];
  }
   
}




/*

foreach($invoiceArray as $key => $val)
{
  if(!$paymentArray[$key])
  {
    $paymentArray[$key] = '0';
  }
}

foreach($paymentArray as $key => $val)
{
  if(!$invoiceArray[$key])
  {
    $invoiceArray[$key] = '0';
  }
}

*/
?>
<!doctype html>
<html>
<head>
  <script src="https://cdn.anychart.com/js/7.14.0/anychart-bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdn.anychart.com/css/7.14.0/anychart-ui.min.css" />
  <style>
    html, body, #container {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>
</head>
<body>
    <div id="container"></div>
    <script type="text/javascript">
anychart.onDocumentReady(function() {
  // create data set on our data
  chartData = {
    title: 'Sale Chart',
    header: ['#',  'Sale', 'Payment Received'],
    rows: [
    <?php
foreach($invoiceArray as $key => $val)
{
  ?>
['<?php echo $key;?>',  <?php echo $val;?>, <?php echo $paymentArray[$key];?>],
  <?php
}
    ?>


    ]
  };

  // create column chart
  chart = anychart.column();

  // set chart data
  chart.data(chartData);

  // turn on chart animation
  chart.animation(true);

  chart.yAxis().labels().format('Rs.{%Value}{groupsSeparator: }');

  // set titles for Y-axis
  chart.yAxis().title('Revenue');

  chart.labels()
    .enabled(false)
    .position('top')
    .anchor('bottom')
    .format('Rs.{%Value}{groupsSeparator: }');
  chart.hoverLabels(false);

  // turn on legend and tune it
  chart.legend()
    .enabled(true)
    .fontSize(13)
    .padding([0, 0, 20, 0]);

  // interactivity settings and tooltip position
  chart.interactivity().hoverMode('single');

  chart.tooltip()
    .positionMode('point')
    .position('top')
    .anchor('bottom')
    .offsetX(0)
    .offsetY(5)
    .titleFormat('{%X}')
    .format('{%SeriesName} : Rs.{%Value}{groupsSeparator: }');

  // set container id for the chart
  chart.container('container');

  // initiate chart drawing
  chart.draw();
});
    </script>


</body>
</html>

                