<!-- Main content -->
<?php
include'../include/connection.php';
        
$status =  $_GET['status'];          
$fromdate =  $_GET['fromdate']; //date("d-m-Y", strtotime($_GET['fromdate']));         
$todate =   $_GET['todate'];  //date("d-m-Y", strtotime($_GET['todate']));//
$bname =  $_GET['bname']; 
$headid =  $_GET['headid']; 

if($status==1){
  $status = '1';
}else if($status==2){
  $status ='2';
}else{
  $status='0';
}
$query = "SELECT e.id,h.name hname ,e.amount,e.expensedate,e.createdat,b.name bname,e.filename,e.status,e.paid,br.name as brname,e.expensedate  FROM expense e INNER JOIN expensenature h ON h.id = e.natureid  INNER JOIN expensebenificiary b on b.id = e.benificiary INNER JOIN employee ee on ee.id = e.empid INNER JOIN branch br on br.id = ee.branch";
    $conditions = array();    

   
    if(isset($status)) {
      $conditions[] = "e.status='$status'";
    }

    if(!empty($bname)) {
      $conditions[] = "br.id='$bname'";
    }

     if(!empty($headid)) {
      $conditions[] = "h.id='$headid'";
    }
   
    if(!empty($fromdate)) {
      $conditions[] = "e.expensedate BETWEEN '$fromdate' and '$todate'";
    }
  
     $sql = $query;


    if (count($conditions) > 0) {
      $sql .= " WHERE " . implode(' AND ', $conditions);
    } 
 
   
  $result = mysqli_query($con,$sql); 
?>
<section class="content" style="margin-top: 20px;">
              
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>   -->         
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Branch Name</th>
<th>Benificiary Name</th>
<th>Head Name</th>           
<th>Amount</th>            
<th>Receipt</th>            
<th>Expense Date</th>            
<th>Create Date</th>            
<th>Approval 1</th>
<th>Approve2</th>              
<th>Action</th>         
            
</tr>
</thead>
<tbody>
<?php  
$count=1; 


while($row=mysqli_fetch_array($result)){

	if($row['status']==0){

		$status = '<span style="background-color: #81a968;color: white;border-radius: 7px;">Pending</span>';
	}else if($row['status']==1){
		$status = '<span style="background-color: green;color: white;border-radius: 7px;">Approved</span>';
	}else if($row['status']==2){
		$status = '<span style="background-color: red;color: white;border-radius: 7px;">Rejected</span>';
	}else{
		$status = '<span style="background-color: orange;color: white; border-radius: 7px;">Paid</span>';
	}

  if($row['paid']==0){

    $paid = '<span style="background-color: orange;color: white;border-radius: 7px;">Pending</span>';
  }else if($row['status']==1){
    $paid = '<span style="background-color: green;color: white;border-radius: 7px;">Approved</span>';
  }else {
    $paid = '<span style="background-color: red;color: white;border-radius: 7px;">Rejected</span>';
  }
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['brname'];?></td>
<td><?php echo $row['bname'];?></td>
<td><?php echo $row['hname'];?></td>
<td><?php echo $row['amount'];?></td>
<td><a href="ExpenseFiles/<?php echo $row['filename'];?>" download>Download</a></td>
<td><?php echo $row['expensedate'];?></td>
<td><?php echo $row['createdat'];?></td>
<td><?php echo $status;?></td>

<td><?php echo $paid;?></td>
<?php
   if($row['paid'] ==0){
?>
<td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="http://183.182.86.91:13780/hrm-production/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=1" onclick="return confirm('Are you sure you want to approve this?');">Approve</a></li>
      <li><a href="http://183.182.86.91:13780/hrm-production/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=2" onclick="return confirm('Are you sure you want to reject this?');">Reject</a></li>
      <li><a href="http://183.182.86.91:13780/hrm-production/Api/api/approve-reject-expense?expenseid=<?php echo $row['id'];?>&status=3" onclick="return confirm('Are you sure you want to paid this?');">Paid</a></li>
       <li><a data-toggle="modal" data-target="#myModal12" onclick="UpdateExpense('<?php echo $row['id'];?>');">Edit</a></li>
    </ul>
  </div>
</div></td>

    <?php
  }
    }?>                      

</tbody>               
</table>

</div>
<!-- /.box-body -->
</div>
</div>
</div>
<div class="modal fade" id="myModal12" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/update-expense.php" method="post">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Expense </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Amount</label>
<input type="hidden" name="empid" id="empid" value="<?php echo $empid;?>">
<input type="hidden" name="eupdateid" id="eupdateid">

<input type="text" name="expenseamountupdate" class="form-control" id="expenseamountupdate" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="expenseheadid" name="expenseheadid" class="form-control" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>

<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->
