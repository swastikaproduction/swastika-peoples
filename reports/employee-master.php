<?php
include("../include/config.php");
$data = Array();
if(isset($_GET['type']))
{
	$t = $_GET['type'];
	$leftStr = " AND employee.left = '$t'";
}
else
{
	$leftStr = " ";
}

$empArray = Array();
$getEmployees = mysqli_query($con,"SELECT * FROM `employee` ORDER BY `name` ASC") or die(mysqli_error($con));
while($rowEmp = mysqli_fetch_array($getEmployees))
{
	$empArray[$rowEmp['id']] = $rowEmp['name'];
}

if($profile==21){
$permStr='';

}
else if($profile==20){
$branch = [];	
$getZones = mysqli_query($con,"SELECT `zones` FROM employee WHERE id='$loggeduserid'") or die(mysqli_error($con));
$rowZones = mysqli_fetch_array($getZones);

$getData = mysqli_query($con,"SELECT id FROM `branch` WHERE zoneid IN ($rowZones[0])") or die(mysqli_error($con));
while($row=mysqli_fetch_array($getData)){
	$branch[] .= $row[0];
}
$branches = implode(',', $branch);
$permStr= " AND employee.branch IN ($branches) AND employee.profileid NOT IN (2,3,4,22) AND employee.empid NOT IN ('IND8303883')";
}
else {
$employees = [];	
$getData = mysqli_query($con,"select id from (select * from employee order by name,id) employee_sorted, (select @pv := '$loggeduserid') initialisation where find_in_set(imdboss, @pv) and length(@pv := concat(@pv, ',',id))") or die(mysqli_error($con));
while($row=mysqli_fetch_array($getData)){
	$employees[] .= $row[0];
}

$employeeid = implode(',', $employees);
if($employeeid != ''){
	$permStr= " AND employee.id IN ($employeeid)";
}
else{
	$permStr= "";
}

}

// old sql and hide date 13_05_2021
// $sql="SELECT employee.empid,employee.name,employee.username,departments.name,designation.name,employee.id,employee.doj,branch.name,employee.salary,employee.poolbalance,shift.name,employee.mobile,employee.imdboss,employee.pf,employee.leavingdate,employee.leftremark,branch.code,employee.left,employee.tallybranch,employee.branch,branch.regionId,employee.dob,employee.pt,employee.od,employee.accountno,employee.ifsc,employee.bank,employee.defpf,employee.adharnumber,employee.adharfather,employee.unnumber,employee.address,employee.empstatuslive,employee.leavingdate,employee.email,employee.workemail,employee.profileid,employee.depttype FROM departments, designation, employee,branch,shift where employee.department = departments.id AND employee.designation = designation.id AND employee.delete = 0 ".$leftStr."".$permStr." AND employee.shift = shift.id AND employee.branch = branch.id ORDER BY employee.name ASC";

// new sql date 13_05_2021
$sql="SELECT employee.empid,employee.name,employee.username,departments.name,designation.name,employee.id,employee.doj,branch.name,employee.salary,employee.poolbalance,shift.name,employee.mobile,employee.imdboss,employee.pf,employee.leavingdate,employee.leftremark,branch.code,employee.left,employee.tallybranch,employee.branch,branch.regionId,employee.dob,employee.pt,employee.od,employee.accountno,employee.ifsc,employee.bank,employee.defpf,employee.adharnumber,employee.adharfather,employee.unnumber,employee.address,employee.empstatuslive,employee.leavingdate,employee.email,employee.workemail,employee.profileid,employee.depttype FROM employee LEFT JOIN departments ON employee.department = departments.id LEFT JOIN designation ON employee.designation = designation.id LEFT JOIN branch ON employee.branch = branch.id LEFT JOIN shift ON employee.shift = shift.id where employee.delete = 0 ".$leftStr."".$permStr." ORDER BY employee.name ASC";

$getData = mysqli_query($con,$sql) or die(mysqli_error($con));

$name = "Employee_Master.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");
?>
<table border="1">
<tr>
<th style="width:5%">#</th>
<th style="width:15%">Code</th>
<th style="width:15%">Employee Name </th>
<?php if($t==1 || $t==""){
	?>
<th style="width:15%">Date of Leaving </th>
<?php }?>
<th style="width: 15%">Reporting Manager</th>
<th style="width: 15%">Manager Employee Code</th>
<th style="width:30%">Username</th>
<th style="width:30%">Personal Email</th>
<th style="width:30%">Work Email</th>
<th>Mobile</th>
<th style="width:15%">Department</th>
<th style="width:15%">Dept Type</th>
<th style="width:15%">Profile</th>
<th style="width:15%">Designation</th>
<th style="width:15%">Doj</th>
<th style="width:15%">DOB</th>
<th style="width:15%">Branch</th>
<th style="width:15%">Branch Code</th>
<th style="width:15%">Salary</th>
<th style="width:15%">Pool Balance</th>
<th style="width:15%">Shift</th>
<th style="width:15%">HOD</th>
<th style="width:15%">PF</th>
<th style="width:15%">OD</th>
<th style="width:15%">PT</th>
<th style="width:15%">Status</th>
<th style="width:15%">Tally Branch</th>
<th style="width:15%">Branch</th>
<th style="width:15%">Branch Code</th>
<th style="width:15%">Region</th>
<?php if($t==1){
?>
<th style="width:15%">Left Remark</th>
<?php }
?>


<th>Account Number</th>
<th>Bank Name</th>
<th>IFSC Code</th>
<th>Default Pf</th>
<th>Aadhaar Number</th>
<th>Father Name</th>
<th>UN Number</th>
<th>Address</th>
<th>Employee Status</th>
</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{  
	$mid = $row['imdboss'];
	$query = mysqli_query($con,"select name,empid from employee where id='$mid'");
	$result = mysqli_fetch_array($query);

    $leftstatus = $row['left'];

    if($leftstatus==0){
    	$Status = 'Active';
    }else{
    	$Status = 'Left';
    }

   $bid = $row['branch'];
   $bquery = mysqli_query($con,"select * from branch where id='$bid'");
   $brow = mysqli_fetch_array($bquery);
   $rid = $brow['regionId'];
   $rquery = mysqli_query($con,"select * from region where id='$rid'");
   $rrow = mysqli_fetch_array($rquery) ;

   $pid = $row['profileid'];
   $pquery = mysqli_query($con,"select name from profiles where id='$pid'");
   $prow = mysqli_fetch_array($pquery);
   

?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<?php echo $row[0];?>
</td>

<td class="text-primary" onclick="getModule('employee/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row[1];?></td>
<?php if($t==1 || $t==""){
	?>
<td><?php echo $row[14];?></td>
<?php }?>
<td><?php echo $result['name']; ?></td>
<td><?php echo $result['empid']; ?></td>
<td>
	<?php echo $row[2];?>
</td>
<td>	<?php echo $row['email'];?></td>
<td>	<?php echo $row['workemail'];?></td>
<td><?php echo $row[11];?></td>
<td><?php echo $row['depttype']; ?></td>
<td>	<?php echo $row[3];?></td>
<td>	<?php echo $prow[0];?></td>
<td>	<?php  echo $row[4];?></td>
<td><?php echo $row[6];?></td>
<td>
	<?php echo $row['dob'];?>
</td>
<td><?php echo $row[7];?></td>
<td><?php echo $row['code'];?></td>
<td><?php echo $row[8];?></td>

<td><?php echo $row[9];?></td>
<td><?php echo $row[10];?></td>
<td><?php echo $empArray[$row[12]];?></td>
<td><?php 

if($row[13] == '0')
	echo "NO";
else
	echo "YES"; ?></td>
<td><?php 

if($row['od'] == '0')
	echo "NO";
else
	echo "YES"; ?></td>
<td><?php 

if($row['pt'] == '0')
	echo "NO";
else
	echo "YES"; ?></td>

<?php if($t==1){
	?>
<td><?php echo $row[15];?></td>


<?php }?>
<td><?php echo $Status;?></td>
<td><?php echo $row['tallybranch'];?></td>
	<td><?php echo $brow['name'];?></td>
	<td><?php echo $brow['code'];?></td>
	<td><?php echo $rrow['name'];?></td>
<td><?php echo $row['accountno'] ?></td>
<td><?php echo $row['bank'] ?></td>
<td><?php echo $row['ifsc'] ?></td>
<td><?php echo $row['defpf'] ?></td>
<td><?php echo $row['adharnumber'] ?></td>
<td><?php echo $row['father'] ?> </td>
<td><?php echo $row['unnumber'] ?> </td>
<td><?php echo $row['address'] ?> </td>
<?php 
 if($row['empstatuslive']=='CONFIRMED'){
 $livestatus='CONFIRMED';	
 }else if($row['empstatuslive']=='ON PROBATION'){
 $livestatus = 'ON PROBATION';	
 }
else{
$livestatus =  '';	
}
?>
<td><?php echo $livestatus; ?> </td>
</tr>

	<?php
	$i++;
}
?>

