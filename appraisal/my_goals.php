    <!-- Main content -->
    <?php 
    include'../include/config.php';
    $emp_id = $_COOKIE['emanagerid'];
    ?>
    <section class="content" style="background: #fff;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">        
            <div class="box">
            <div class="box-header">
              <h5 class="box-title">Employee goal setting <?php echo $checkresult['id'];?> </h5>
            </div>      
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Weightage value not more than 100.!
</div>   <?php
}
            ?> 
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 2 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Weightage value not equal to 100.!
</div>   <?php
}
            ?> 
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 3 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Submit successfully...!
</div>   <?php
}
            ?>  
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 4 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Save successfully...!
</div>   <?php
}
            ?>              
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                 <th>Appraisal Mode</th>
                  <th>Period</th>                
                  <th>Status</th>
                  <th>Stage</th>
                  <th>Employee End Date</th>
                  <th>Managers End Date</th>  
                  <th>HOD's End Date</th>
                  <th>Create Date</th>
                  <th>Action</th>
                                 
                </tr>
                </thead>
                <tbody>
                  <?php                   
                     
                       $count = 1;                             
                       $query = mysqli_query($con,"call employees_gols($emp_id)");
                       while($row=mysqli_fetch_array($query)){
                        ?>

                <tr>
                  <?php
                     if($row['appraised_mode'=='quaterly']){
                      $mode = 'Quarterly';
                     }else if($row['appraised_mode']=='halfyraly'){
                      $mode = 'Half-Yearly';
                     }else{
                      $mode='Yearly';
                     }

                    if($row['status']=='o'){
                      $status = 'open';
                     }else{
                      $status = 'close';
                    }

                    if($row['issubmitted']=='false'){
                      $stage = 'Goals pending';                      
                      $button = 'Set Goals';               
                    }elseif($row['issubmitted']=='save'){
                      $stage = 'Save By Employee';
                      $button = 'View';
                    }else if($row['issubmitted']=='submitted'){
                         $stage = 'Manager Approval pending';
                         $button = 'View';
                    }
                  if($row['missubmitted']=='submitted'){
                         $stage = 'HOD Approval Completed';
                         $button = 'View';
                    }else if($row['missubmitted']=='save'){
                        $stage = 'Save by Manager';
                         $button = 'View';
                    }
                  

                  if($row['hissubmitted']=='submitted'){
                         $stage = 'HOD Approval Completed';
                         $button = 'View';
                    }else if($row['hissubmitted']=='save'){
                        $stage = 'Save by HOD';
                         $button = 'View';
                    }
                  ?>
                  <td><?php echo $count++; ?></td>
                  <td><?php echo $mode;?></td>
                  <td><?php echo $row['appraised_time'];?> <?php echo $row['appraised_year'];?></td>
                  <td><?php echo $status;?></td>
                  <td><span style="background: orange;color: white;"><?php echo $stage?></span></td>
                  <td><?php echo $row['employee_due_date'];?></td>
                  <td><?php echo $row['managers_due_date'];?></td>
                  <td><?php echo $row['hod_due_date'];?></td>                  
                  <td><?php echo $row['created_at'];?></td>                  
                  <td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/employee_goals.php?issubmitted=<?php echo $row['issubmitted'];?>&app_id=<?php echo $row['id'];?>','formDiv','tableDiv','loading');" ><?php echo $button;?> </span>                       
                  </td>
                   
                  
                  
                              
                </tr> 
                  <?php
                       }
                  ?>                          
                 
                </tbody>
             
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>
</div></section>  
</div>
<script type="text/javascript">
  
  function send_reminder(){

    alert('Reminder send successfully..!');
  }
</script>