

var searchBox;
var map;
var input;
var markers;
var places;
var bounds;
var icon;
var myposition;

var mycity;
var preinfowindow;
var preLat = '0';

var myLat = localStorage.getItem("myLat");
myLat = parseFloat(myLat);

var myLang = localStorage.getItem("myLang");
myLang = parseFloat(myLang);
      function initAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: myLat, lng: myLang},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

         google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });


if(preLat == '1')
{    image = {
  url: '../pin.svg',
  size: new google.maps.Size(24, 24 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(40,40),
  scaledSize: new google.maps.Size(24, 24)
};




    myposition = new google.maps.Marker({
    position: {lat: myLat, lng: myLang},
    map: map,
    optimized: false,
    icon:image
  });     




 preinfowindow = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-success">THIS IS THE CURRENT SPOT</button>'
  });
  preinfowindow.open(map,myposition);
}



//  animateRadius();

        // Create the search box and link it to the UI element.
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location,
              optimized: false
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

 var infowindow;
 var marker;
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter=new google.maps.LatLng(51.508742,-0.120850);
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas, mapOptions);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });
}


  var direction = 1;
    var rMin = 500, rMax = 700;

var animCity;
function animateRadius()
{
       animCity = setInterval(function() {
            var radius = myCity.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            myCity.setRadius(radius + direction * 100);
        }, 100);
}

var image;

function placeMarker(map, location) {
  if(marker)
  {
  marker.setMap(null);    
  }

     image = {
  url: '../pin.svg',
  size: new google.maps.Size(12, 12 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(10,10),
  scaledSize: new google.maps.Size(12, 12)
};



marker = new google.maps.Marker({
    position: location,
    map: map,
    optimized: false,
    icon:image
  });

google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);

  });
 infowindow = new google.maps.InfoWindow({
    content: '<div style="padding:10px;background:#fff;"><input id="stopname" class="input1bdark" placeholder="Enter Stop Name" type="text" value=""/><br/><br/><input id="stopaddress" class="input1bdark" placeholder="Enter Stop Address" type="text" value=""/><br/><br/><input id="stopidenty" class="input1bdark" placeholder="Enter Stop Identifier" type="text" value=""/><br/><br/><br/><button class="btn btn-sm btn-danger" onclick="UseStop('+location.lat()+','+location.lng()+');">USE THIS LOCATION</button></div>',
    pixelOffset: new google.maps.Size(0,0)
  });
  infowindow.open(map,marker);

    setTimeout(function(){
  document.getElementById('stopname').focus();
},500);

}


function sendToTop(lat,lng)
{
 window.top.window.document.getElementById('stop2').readOnly = false;
 window.top.window.document.getElementById('stop3').readOnly = false;


 window.top.window.document.getElementById('stop2').lang = lat;  
 window.top.window.document.getElementById('stop3').lang = lng;
 window.top.window.document.getElementById('stop2').value = lat;  
 window.top.window.document.getElementById('stop3').value = lng;

 }


var selectedMarkers = [];
var selCounter = 0;

 function UseStop(lat,lng)
 {

  var name = document.getElementById('stopname').value;
  var address = document.getElementById('stopaddress').value;
  var identy = document.getElementById('stopidenty').value;

  if(name == '' || address == '' || identy == '')
  {
    toast('Please enter all the values','toast bg-danger','3000');
    return;
  }
  var html = '<div id="div'+selCounter+'" lang="'+lat+':::'+lng+'" style="padding:10px;text-align:left;border-bottom:1px #eee solid;"><div style="float:right;padding:10px;"><span onclick="removeSelected(\''+selCounter+'\')" class="fa-stack fa-lg" style="color:#d95628;font-size:10px;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span></div><strong style="font-size:16px;" id="name'+selCounter+'">'+name+'</strong><br/><span style="font-size:13px;"  id="address'+selCounter+'">'+address+'</span><br/><span style="font-size:13px;"  id="identy'+selCounter+'">'+identy+'</span></div>';
  document.getElementById('stopList').insertAdjacentHTML('afterBegin',html);
placeSelectedMarker(lat,lng);
  

 }



function placeSelectedMarker(lat,lng) {
  if(marker)
  {
  marker.setMap(null);    
  }




selectedMarkers[selCounter] = new google.maps.Marker({
    position: {lat: lat, lng: lng},
    map: map,
    optimized: false,
    icon:'../images/pin-dir.png',
    myi:selCounter
  });

google.maps.event.addListener(selectedMarkers[selCounter], 'click', function() {
for(k=0;k<selCounter;k++)
{
  if(document.getElementById('div'+k))
  {
    if(k != this.myi)
    {
      document.getElementById('div'+k).className= '';      
    }

  }
}


var myElement = document.getElementById('div'+this.myi);
var topPos = myElement.offsetTop;
document.getElementById('stopList').scrollTop = topPos;


  document.getElementById('div'+this.myi).className = 'bg-danger';
  });


selCounter++;

}



var hiderTimeObj;


function toast(html,css,timeout)
{
  if(hiderTimeObj)
  {
    clearTimeout(hiderTimeObj);
  }
  document.getElementById('toastText').innerHTML = html;
  $('#toast').animate({bottom:'0px'});
  document.getElementById('toast').className = css;
  if(timeout != '0')
  {
  hiderTimeObj = setTimeout(function(){
hideToast()     
},timeout);
  }

}

function hideToast()
{
  $('#toast').animate({bottom:'-300px'});
}

function removeSelected(id)
{
  document.getElementById('div'+id).style.display = 'none';

  if(selectedMarkers[id])
  {
  selectedMarkers[id].setMap(null);    
  }
}

function collectValues()
{

  var nameList = '';
  var addressList = '';
  var identyList = '';
  var latList = '';
  var lngList = '';
  for(k=0;k<selCounter;k++)
  {
    ltlng= document.getElementById('div'+k).lang;
    ltlng = ltlng.split(":::");
    nameList += document.getElementById('name'+k).innerHTML+"***$$***";
    addressList += document.getElementById('address'+k).innerHTML+"***$$***";
    identyList += document.getElementById('identy'+k).innerHTML+"***$$***";
    latList  += ltlng[0]+"***$$***";
    lngList += ltlng[1]+"***$$***";

  }

if(nameList != '')
{

var params = "name="+nameList+"&address="+addressList+"&identy="+identyList+"&lat="+latList+"&lng="+lngList;
document.getElementById('saveStop').innerHTML = 'SAVING..';

$.ajax({
  type: "POST",
  url: 'savebulkstop.php',
  data: params,
  success: function(data){
//    alert(data);
   toast('Stops Saved','toast bg-success','4000');
   document.getElementById('saveStop').innerHTML = 'SAVE STOPS';
  resetStops();

  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    document.getElementById('saveStop').innerHTML = 'SAVE STOPS';
    toast('Unable to connect to SchoolBird Server. Please check your network connection.','toast bg-danger','0');
}
  
});
}
else
{
  toast('Please select atleast one stop','toast bg-danger','3000');
}




}

function resetStops()
{

for(k=0;k<selCounter;k++)
{
    selectedMarkers[k].setMap(null);    
}

document.getElementById('stopList').innerHTML = '';


}


