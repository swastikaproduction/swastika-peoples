<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<!-- Content Wrapper. Contains page content -->
<h3 class="box-title">Questions</h3>
<div class="content-wrapper" style="background:#fff;margin-bottom: 60px;">
<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content" >
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">             
</div>
<!-- /.box-header -->
<!-- form start -->
<form action="appraisal/profilekpi.php" role="form" method="post">
<div class="box-body">
<div class="row">
<div class="col-md-2">
<div class="form-group">
<label for="exampleInputEmail1" class="subject">Profile</label>
<select class="form-control" name="profile" required="">
<option value="">select profile type</option>
<?php
include'../include/config.php';
$query = mysqli_query($con,"select * from profile_type where status='Active'");
while($row = mysqli_fetch_array($query)){
?>   

<option value="<?php echo $row['id'];?>"><?php echo $row['profile_name'];?></option>
<?php 
}?>                    
</select>                  
</div>
</div>   
<div class="col-md-0.5">
<div class="form-group">

<p class="btn btn-success" style="margin-top: 23px;" data-toggle="modal" data-target="#myModal11">Add</p>

</div>
</div>        
</div>
<div class="row">                  
<div class="col-md-3">
<div class="form-group">
<label for="exampleInputEmail1" class="subject">Subject</label>
<i class="error-subject" style="color: red;"><strong>*</strong></i>
<textarea  type="text" name="subject[]" class="form-control subject" id="subject"  required=""></textarea>                  
</div>
</div>        
</div>
<div class="row">
<div class="col-md-2">
<div class="form-group">                  
<label for="exampleInputEmail1">Target</label>
<i class="error-subject" style="color: red;"><strong>*</strong></i>
<textarea type="text" name="target[]" class="form-control target" id="trgt" required=""></textarea>
</div>
</div> 
<div class="col-md-1">
<div class="form-group">

<label for="exampleInputEmail1">Weightage</label>
<i class="error-subject" style="color: red;"><strong>*</strong></i>
<input type="text" name="weightage[]" class="form-control allownumeric " id="weightage" maxlength="3" required="">
</div>

</div>  
<div class="col-md-0.5"> 
<label style="display: none">Weightage</label>
<P style="margin-top:30px;"><strong>%</strong></P>
</div>          
</div>
<div class="row">
<div class="col-md-3">
<div class="form-group">                  
<label for="exampleInputEmail1">Description</label>
<i class="error-subject" style="color: red;"><strong>*</strong></i>
<textarea type="text" name="description[]" class="form-control description" id="description" required=""></textarea></div>
</div> 
</div>
</div>


<div id="add"></div>   

<div>
<span id="add_button" onclick="add_button();" style="margin-left: 30px;font-size: 25px;"><i class="fa fa-plus"></i></span>
</div>
<!-- /.box-body -->   
<div class="box-footer buttonBox">               
<button type="submit" class="btn btn-primary">Submit</button>
<button type="reset" class="btn btn-success">Clear</button>
</div>
</form>
</div>
</div>
</div>
</section>
</div>      
<div class="container" style="margin-top: 80px;">  
<!-- Modal -->
<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Profile Type</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<label>Profile Name</label>
<input type="text" name="profile_name" class="form-control" id="profile_name" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">

</div>
</div>

</div>
<div class="modal-footer" >
<input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="profile_type();">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>      
</div>         
</div>
</div>
</div>
</div>

