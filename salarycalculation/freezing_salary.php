<?php
include("../include/config.php");
$tableName = $_POST['tablename'];
$month = $_POST['month'];
$year = $_POST['year'];
$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$valueString = '';
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
}


if($month < 10)
{
$month = "0".$month;    
}

$sql="SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department ,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id  AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC";

$getData = mysqli_query($con,$sql) or die(mysqli_error($con));

if(mysqli_num_rows($getData)!=0){
/**  table salary freeing process **/
$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($row = mysqli_fetch_array($getTables))
{
	$tables[] = $row[0];
}

if(!in_array($tableName,$tables))
{
   mysqli_query($con,"CREATE TABLE IF NOT EXISTS `".$tableName."` (
  `id` int(11) NOT NULL,
  `empid` int(11) NOT NULL,
  `ctc` double NULL,
  `gross` double NULL,
  `basic` double NULL,
  `pfpayable` double NULL,
  `da` double NULL,
  `pfemployer` double NULL,
  `otherdeduction` double NULL,
  `bonus` double NULL,
  `hra` double NULL,
  `refreshment` double NULL,
  `hold` double NULL,
  `loan` double NULL,
  `jvhold` double NULL,
  `leavedeductions` double NULL,
  `calculatedsalary` double NULL,
  `totalearnings` double NULL,
  `totaldeductions` double NULL,
  `netpay` double NULL,
  `pt` double NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1") or die(mysqli_error($con));

mysqli_query($con,"ALTER TABLE `".$tableName."` ADD PRIMARY KEY (`id`)") or die(mysqli_error($con));
mysqli_query($con,"ALTER TABLE `".$tableName."` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT") or die(mysqli_error($con));
}

/**  table salary freeing process **/

}


while($row= mysqli_fetch_array($getData))
{
$eid = $row['empid'];
$employeeid = $row['id'];
$prevsalary = $row['actual_salary'];
$query = mysqli_query($con,"select * from employee where id='$eid'");
$result = mysqli_fetch_array($query);
$currentsalary = $result['salary'];

//flag of increase salary
if($currentsalary>$prevsalary){
	$s = 0;
}else{
	$s = 1;
}

//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}



$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}

$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;



if($row[12] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}

if($row['pt']=='1'){

	$pt = $pt;
}else{
	$pt = 0;
}

$odflag = $row['od'];


if($pfflag == '1')
{

		if($calculatedsalary < 8960)
		{ 

			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);

			if($pfpayable == '')
			{
				$pfpayable = round($gross * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
            $otherdeduction=0;
			$pfemployer = round($pfpayable/2);	
			$basic = $ctc - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;

			$netpay = $totalearnings - $totaldeductions;
		}	

		if($calculatedsalary >= 8961 && $calculatedsalary <= 14292)
		{    
			
			
			$basic = 8000;
			$hra = 0;
			$refreshment=0;
			$otherdeduction=0;
			
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$ctc = $calculatedsalary;

			$pfemployer = round($pfpayable/2);
			$gross = $ctc - $pfemployer;
			$bonus = $ctc - $basic-$pfemployer;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 14293 && $calculatedsalary <= 29680)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross*0.60);
			$hra = round($basic*0.40);
			$otherdeduction=0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = $ctc-$basic-$hra-$pfemployer;
			$refreshment=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary >= 29680)
		{   $ctc = $calculatedsalary;
			$gross = $grosssalary;
			$basic = round($calculatedsalary * 0.52);
			$hra = round($calculatedsalary * 0.4*0.52);
			$bonus = round($calculatedsalary * 0.12);
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($calculatedsalary*0.025);
			}else{
              $otherdeduction=0;
			}
			
			if($pfpayable == '')
			{
			$pfpayable = 3600;
			}
		

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9000)
	{
		    $ctc = $calculatedsalary;		    
			$basic = 8000;
			$hra = 0;
			$da = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - 8000;
			$refreshment=0;
			$otherdeduction=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;

	}else if($calculatedsalary >= 9000 && $calculatedsalary <= 15000)
	{       $ctc = $calculatedsalary;
		    $gross = $grosssalary;
			$basic = 8000;
			$pfpayable = 0;
			$pfemployer = 0;
			$refreshment=0;
			$da=0;
			$otherdeduction=0;
			$bonus = round($ctc * 0.1);
			$hra = $ctc - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000 && $calculatedsalary <=26800)
	{       $ctc = $calculatedsalary;
	        $gross = $grosssalary;
			$basic = round($ctc * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			$otherdeduction=0;
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.4);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}
	else if( $calculatedsalary >26800)
	{       $ctc = $calculatedsalary;
	        $gross = $grosssalary;
			$basic = round($calculatedsalary * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			//other deduction condition when flag is 1
			if($odflag=='1'){
            $otherdeduction= round($calculatedsalary*0.025);
			}else{
            $otherdeduction=0;
			}
			
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.4);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra+$otherdeduction);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns.
			$netpay = $totalearnings - $totaldeductions;
	}
}

/**  Inserting data on freezed table **/
$valueString .= "('','$employeeid', '$ctc', '$gross', '$basic', '$pfpayable', '$da', '$pfemployer', '$otherdeduction', '$bonus', '$hra', '$refreshment',
'$hold','$loan','$jvhold','$leavedeductions','$calculatedsalary','$totalearnings', '$totaldeductions', '$netpay','$pt','$datetime'),";

/**  salary freezed process **/
}

if($valueString!=''){
$valueString  = substr($valueString,0,-1);
mysqli_query($con,"INSERT INTO `$tableName` (`id`, `empid`, `ctc`, `gross`, `basic`, `pfpayable`, `da`, `pfemployer`, `otherdeduction`, `bonus`, `hra`, `refreshment`,`hold`,`loan`,`jvhold`,`leavedeductions`,`calculatedsalary`,`totalearnings`, `totaldeductions`, `netpay`,`pt`,`createdate`) VALUES ".$valueString) or die(mysqli_error($con));
}
?>

