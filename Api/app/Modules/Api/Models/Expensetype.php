<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Expensetype extends Model {

    //
    protected $table = 'expensetype';

    public $timestamps = false;

}
