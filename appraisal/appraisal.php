 <div class="content-wrapper " style="background:#fff">
    <!-- Content Header (Page header) -->
    <section class="content-header">      
      <ol class="breadcrumb">        
        <li class="active">Appraisals History</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">        
            <div class="box">
            <div class="box-header">                         
            </div>            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>  
                  <th>View Users</th>
                  <th>S.NO</th>
                  <th>Appraisal mode</th>            
                  <th>Period</th>                 
                  <th>Status</th>
                  <th>Stage</th>
                  <th>Employee End Date</th>
                  <th>Managers End Date</th>
                  <th>HOD's End Date</th>
                  <th>Create date</th>
                  <th>Action</th>                               
                </tr>
                </thead>
                <tbody>
                	<?php  
                       $count=1;           
                       include'../include/connection.php';
                       $query = mysqli_query($con,"SELECT a.id,ast.eDate,ast.mDate,ast.hDate,a.appraised_mode,a.appraised_time,a.appraised_year,ast.createdAt FROM appraisals a INNER JOIN appraisalstart ast  ORDER BY id DESC ");
                       while($row=mysqli_fetch_array($query)){
                       	?>
                <tr>

                  <?php
                    if($row['status']=='o'){
                      $status = 'open';
                    }else{
                      $status = 'close';
                    }

                    if($row['stage']=='g'){
                      $stage = 'Goals pending';
                    }else if($stage=='a'){
                      $stage = 'Appraisal pending';
                    }else{
                      $stage = 'Completed';
                    }
                   
                  ?>
                  <td><a style="align-items: center;" href="default.php#&rtl=appraisal/appraisal_users.php?startrow=0&app_id=<?php echo $row['id'];?>&rtl=formDiv&rtl=tableDiv&rtl=loading"><i class="fa fa-eye" style="margin-left: 20px;color: #3fbb5b"></i></a></td>
                  <td><?php echo $count++;?></td>
                  <td><?php echo $row['appraised_mode'];?></td>
                  <td><?php echo $row['appraised_time'];?><?php echo $row['appraised_year'];?></td>
                  <td><?php echo $status;?></td>
                  <td><?php echo $stage?></td>
                  <td><?php echo $row['eDate'];?></td>
                  <td><?php echo $row['mDate'];?></td>
                  <td><?php echo $row['hDate'];?></td>                  
                  <td><?php echo $row['createdAt'];?></td>
                  
                  <td><div class="dropdown">
                    <button type="button" class="label  label-primary dropdown-toggle" data-toggle="dropdown">
                      Update
                    </button>
                    <div class="dropdown-menu" style="min-width: auto;">
                     <li data-toggle="modal" data-target="#myModal16" onclick="GetappraisalData(<?php echo $row['id'];?>);"><i class="fa fa-edit" style="color: green;" ></i></li>
                     <li><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i></li>
                    </div>
                    </div>
                    <div class="modal fade" id="myModal16" role="dialog">
                        <div class="modal-dialog" style="margin-top: 150px;">
                        
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Appraisal Update </h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <form action="#" method="post">
                                <div class="col-md-4">                                  
                                  <label>Employees Due Date</label>
                                  <input type="hidden" name="appId" id="appId">
                                  <input type="date" id="edate" class="form-control">
                                </div>
                                <div class="col-md-4">
                                  <label>Manager's Due Date</label>                                  
                                  <input type="date" id="mdate" class="form-control">
                                </div>
                                <div class="col-md-4">
                                  <label>HOD's Due Date</label>                                  
                                  <input type="date" id="hdate" class="form-control">
                                </div>
                              </div>                              
                            </div>
                            <div class="modal-footer">
                              <input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="appraisal_update();">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </form>
                        </div>
                      </div>             
  
                  </td>

                </tr> 
                	<?php
                       }
                	?>                          
                 
                </tbody>               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>