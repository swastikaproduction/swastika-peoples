<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	$typeStr = " AND `status` = '1'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));	
}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));

}

?>
<div style="padding:0px;">
<?php
if($loggeduserid == '124')
{
?>
<div style="float:right;padding:20px;">
<button class="btn btn-warning" onclick="releaseSalary()">Mark As Released</button>

<button class="btn btn-warning" onclick="window.open('salarycalculation/export-cumilative-salary.php?month='+document.getElementById('atMonth').value+'&year='+document.getElementById('atYear').value)">Export Detailed</button>
</div>
<?php
}
?>



<div class="dummyTableHeader" id="forAttendance" style="margin-bottom:0px;position:fixed;top:118px;left:0px;width:100%;padding:0px 21px 0px 15px;z-index:200;display:none">

<table class="table table-hover" cellpadding="0" cellspacing="0" style="">
<tr>
		<th style="width:5%">#</th>
		<th  style="width:5%">

		</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Salary</th>
		<th style="width:10%">Total Deduction</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
		<th style="width:5%">Final Salary</th>
		<th style="width:5%">Details</th>
		<th style="width:10%">Salary Slip</th>
		<th style="width:10%">Released</th>
	</tr>
</table>

</div>


<table class="table table-bordered table-hover"  cellpadding="0" cellspacing="0" id="tableDivAtView">
	<tr>
		<th style="width:5%">#</th>
		<th  style="width:5%">
			<input type="checkbox" name="" onclick="checkAll(this)">
		</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Salary</th>
		<th style="width:10%">Total Deduction</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
		<th style="width:5%">Final Salary</th>
		<th style="width:5%">Details</th>
		<th style="width:10%">Salary Slip</th>
		<th style="width:10%">Released</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td>
		<input type="checkbox" class="checkInput" name="" value="<?php echo $row['id'];?>" lang="<?php echo $row['empid'];?>">
	</td>
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>

	<td>
		
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['actual_salary'];?>
	</td>
	<td>
	
	<?php
	if($row['status'] == '1')
	{
		echo $row['deduction'];
	}
	?>
			<?php
if($row['pool_deduction'] != '')
{
?>
<span style="font-size:11px;">
		(<?php echo $row['pool_deduction'];?> from pool)
		</span>
<?php	
}
		?>

	
	</td>
	<td>
		<?php echo $row['pool_opening'];?>
	</td>
	<td>
		<?php echo $row['pool_opening'] - $row['pool_deduction'];?>

	</td>

	<td>
	<?php
	if($row['edited_salary'] == '')
	{
?>
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['final_salary'];?>
<?php		
}
else
{
	?>
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['edited_salary'];?>
	<?php
}
	
	?>


	</td>



	<td style="padding-top:12px" id="detBut<?php echo $row['id'];?>" onclick="getModule('attendance/details.php?id=<?php echo $row['id'];?>','bottomDiv','','loading')">
		<span class="label label-info">DETAILS</span>
	</td>

	<td style="padding-top:12px" id="" >
	<span class="label label-success" onclick="window.open('salarycalculation/salary_slip_09092020.php?id=<?php echo base64_encode($row['id']);?>','_blank')">
					<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;GENERATE

		</span>
	</td>
	<td>
		<?php if($row['released'] == '1')
		{
			?>
<span class="label label-primary">YES</span>
			<?php
		}
		else
		{
			?>
<span class="label label-danger">NO</span>
			<?php

		}
		?>
	</td>

</tr>
		<?php
		$i++;
	}
	?>
</table>
<input type="text" value="<?php echo $i;?>" id="genTotal" style="display:none">
</div>
<br/><br/><br/><br/><br/>