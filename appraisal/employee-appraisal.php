<style type="text/css">
.top-margin{
margin-top: 15px;
}
.secmargin{      
margin-left:60px;
color: #2dda2a;
}
.col{
color: #2532d6;
}
.sec{
background: white; height: auto;
}
.lab{
color: #d43319;
}
</style>
<section>
<?php 
include'../include/config.php';
$submitted = $_GET['issubmitted'];
$appId = $_GET['app_id'];
$sql = mysqli_query($con,"select * from appraisals where id='$appId'") or die(mysqli_error($con));
$row = mysqli_fetch_array($sql);
$empId = $_COOKIE['emanagerid'];       
$query1 = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
$finalscorresult = mysqli_fetch_array($query1);
?>
<div class="row">
<div class="col-md-12">
<p class="col">Appraisal start for <?php echo $row['appraised_time'] ?> <?php echo $row['appraised_year'] ?></p>
</div>		
</div>
</section>
<section class="sec">
<form action="appraisal/employeeAppraisal.php" enctype="multipart/form-data" method="post" >
<?php 
$query = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
while($result = mysqli_fetch_array($query)){
?>
<div class="row">
<div class="col-md-2">
<label class="top-margin">Subject</label>
<input type="hidden" name="appId" value="<?php echo $appId;?>">
<input type="hidden" name="kpid[]" value="<?php echo $result['id'];?>">
<textarea  type="text" class="form-control" name="subject"  readonly=""><?php echo $result['h_subject'];?></textarea>
</div>
</div>
<div class="row">
<div class="col-md-1">
<label class="top-margin">Target</label>
<textarea type="text" class="form-control" name="target" value="<?php echo $result['h_target'];?>" readonly=""><?php echo $result['h_target'];?></textarea>
</div>
<div class="col-md-1">
<label class="top-margin">Achievement</label>
<textarea type="text" class="form-control" name="achievement[]" required="" <?php if($submitted=='submit' or $submitted='hsubmit'){ echo'readonly';}else{} ?>><?php echo $result['achievement']; ?></textarea>
</div>
<div class="col-md-1">
<label class="top-margin">Weightage</label>
<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'];?>" readonly="">
</div>
<div class="col-md-1">
<label class="top-margin">Your Score</label>
<input type="text" class="form-control allownumeric yourscore" data-weight ="<?php echo $result['h_weightage'];?>" name="yourscore[]" value="<?php echo $result['score'];?>" id="yscore" required <?php if($submitted=='submit'){ echo'readonly';}else{} ?>>		
</div>
</div>
<div class="row">
<div class="col-md-2">
<label class="top-margin">Description</label>
<textarea type="text" class="form-control" name="description" value="<?php echo $result['h_description'];?>" readonly=""><?php echo $result['h_description'];?></textarea>		
</div>
<div class="col-md-2">
<label class="top-margin">Your Comment's</label>
<textarea type="text" class="form-control" name="yourcomment[]" <?php if($submitted=='submit' or $submitted='hsubmit'){ echo'readonly';}else{} ?>><?php echo $result['comments']; ?></textarea>		
</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label class="top-margin">Supporting Document</label>
		<input type="file" class="form-control" name="kpifile[]">
	</div>	
</div>

<?php
}
?>
<div class="row">
<div class="col-md-2">
<label class="top-margin lab">Final Comment's</label>
<textarea type="text" class="form-control" name="finalcommnets" <?php if($submitted=='submit' or $submitted='hsubmit'){ echo'readonly';}else{} ?>><?php echo $finalscorresult['finalcomment'] ?></textarea>		
</div>
<div class="col-md-2">
<label class="top-margin lab">Final Score</label>
<input type="text" class="form-control allownumeric"  name="finalscore" value="<?php echo $finalscorresult['finalscore'] ?>" id="fscore" readonly="">		
</div>
</div>
<?php
$empId = $_COOKIE['emanagerid'];
$check = mysqli_query($con,"select * from appraisalusers where empId='$empId'");
$checkresult = mysqli_fetch_array($check);
$status =$checkresult['status']; 
if( $status !=='submit' && $status !=='msubmit' && $status !=='hsubmit' ){
?>

<div class="row">
<div class="col-md-12">
<input type="submit" class="btn btn-success top-margin" name="submit" value="Save Draft">&nbsp;&nbsp;<input type="submit" class="btn btn-primary top-margin" name="submit" value="Submit">&nbsp;&nbsp;<input type="reset" class="btn btn-info top-margin" name="cancel" value="Cancel">
</div>
</div>
<?php
}
?>
</form>
</section>