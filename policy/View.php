<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Policies</li>
</ol>
</section>
<!-- Main content -->
<section class="content">            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">           
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Policy</th>            
<th>Create date</th> 
<th>View</th> 
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/connection.php';
$query = mysqli_query($con,"select * from policies");
while($row=mysqli_fetch_array($query)){
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['policyname'];?></td>
<td><?php echo $row['createdat'];?></td>                 
<td><span><a href="Policy/files/<?php echo $row['pocliyfetures'];?>" target="_blank">View</a></span></td>                 

</tr> 
<?php
}
?>                          

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Policy</h4>
</div>
<form action="Policy/save.php" method="POST" enctype="multipart/form-data">
<div class="modal-body">
	
<div class="row">
	
<div class="col-md-6">
<label>Policy Name</label>
<input type="text" name="policyname" class="form-control" id="policyname" placeholder="enter policy name" required="">
</div>
<div class="col-md-6">
	<label>Policy File</label>
<input type="file" class="form-control" name="policyfile" id="policyfile" required=""> 
</div>
</div>

</div>
<div class="modal-footer" >
<button type="submit"  class="btn btn-success">Submit</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</form>
</div>
</div>  