var directOpenChat = 0;

function multiSelect(fromId, toId)
{
var x = '';
   var checkboxArray = document.getElementById(fromId);
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
          	if(fromId == 'route3' && toId == 'route4')
          	{
          	 var temp = checkboxArray[i].value;
          	 temp = temp.split(":::");
          	 x += temp[1]+",";
          	}
          	else
          	{
	          	x += checkboxArray[i].value+",";          		
          	}

          }
        }
        document.getElementById(toId).value = x;
}

var valString = '';



function toogleSelect(elm)
{
	if(elm.lang == '1')
	{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;

	}
	else
	{
		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;		
	}
}


function selectAll(selector)
{	
	if(selector.lang == '1')
	{
			todo = '1';
			selector.lang = '0';
	}
	else
	{
		todo = '0';
		selector.lang = '1';
	}

	var total = document.getElementById('totalBusCount').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('buslistRow'+j))
		{
			elm = document.getElementById('buslistRow'+j);		
if(todo == '1')
{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;
}
else
{

		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;
	
}
		}
	}
}


function deleteData(id,table)
{
	var reply = confirm("Are you sure you want to delete this entry?")
	if(reply)
	{
		var url = "deleteData.php?table="+table+"&id="+id;
		genAjax(url,'',function(){
			toast("Successfully Deleted.","toast bg-success","3000");
			document.getElementById('tableRow'+id).className = 'bg-danger';
			setTimeout(function(){
			$('#tableRow'+id).hide();
			},2000);

		})
	}
}

function deleteDataSample(id,table)
{
	toast("Data deletion not activated in demo mode","toast bg-danger",3000);
}


function rotateMenu(id)
{
for(j=0;j<6;j++)
{
	if(document.getElementById('menutr'+j))
	{
		if(j != id)
		{
			document.getElementById('menutr'+j).className = "";	
		}
		if(document.getElementById('submenutr'+j))
		{

			$('#submenutr'+j).slideUp();	
		}
	}
}
document.getElementById('menutr'+id).className = "active";
$('#submenutr'+id).slideDown();


}





function setJsColor()
{

	var itemSearch = $('.jsColor');
		searchList = $('#formDiv').find(itemSearch).each(function(){

	var thisId = $(this).attr('id');
			new jscolor(document.getElementById(thisId));
		});

}


function updateSalary(empid)
{
	var salary = document.getElementById('totalSalary').value;
	if(salary == '')
	{
		toast('Please enter all values','toast bg-danger','5000');
		return false;
	}
	var headId = '';
	var headtype = '';
	var headValue = '';
	var total = document.getElementById('totalHeads').value;
	for(k=0;k<total;k++)
	{
		headId += document.getElementById('headId'+k).value+",";
		headtype += document.getElementById('headtype'+k).value+",";
		headValue += document.getElementById('headValue'+k).value+",";
	}

	var url = "employeesalary/save.php";
	var params = {
		salary:salary,
		empid:empid,
		headId:headId,
		headtype:headtype,
		headValue:headValue
	}

	genAjax(url,params,function(response){
			$('#myModalBig').modal('hide');
			toast('Successfully Updated','toast bg-success','3000');
	});
}

var atchangedone = 0;
function changeAttendance(url,id)
{
var intime = document.getElementById('intime').value;
var outime = document.getElementById('outime').value;
var params = {
	intime:intime,
	outime:outime
}
	genAjax(url,params,function(response){
			$('#detBut'+id).trigger('click');
			atchangedone = 1;
	})
}


function addForced(url,id)
{
var forcedval = document.getElementById('forcedval').value;
var forceremarks = document.getElementById('forceremarks').value;
var params = {
	forcedval:forcedval,
	forceremarks:forceremarks
}
	genAjax(url,params,function(response){
			$('#detBut'+id).trigger('click');
			atchangedone = 1;
	})
}


function checkDateAvailability()
{
	var tocheckDate = document.getElementById('lvr3').value;
	var url = 'leaves/checkdate.php?date='+tocheckDate;
	genAjax(url,'',function(response){
		if(response.indexOf('TRUE') == -1)
		{
			document.getElementById('lvr3').value = '';
			toast("Attendance for given date is not yet uploaded. You can only send request once the attendance is uploaded.","toast bg-danger","5000")
		}
	});
}


var currentCountGen = 1;
function startGen()
{
console.log('start again'+currentCountGen);

if(document.getElementById('genBut'+currentCountGen))
{
document.getElementById('genBut'+currentCountGen).className = 'label label-info';
document.getElementById('genBut'+currentCountGen).innerHTML = 'CALCULATING..';
var thisLang = document.getElementById('genBut'+currentCountGen).lang;
thisLang = thisLang.split("::");
var url = 'salarycalculation/generate_salary.php?id='+thisLang[0]+'&i='+thisLang[1];

var params = '';

$.ajax({
  type: "POST",
  url: url,
  data: params,
  success: function(response){
document.getElementById('atRow'+thisLang[0]).innerHTML = response;
var genTotal = parseInt(document.getElementById('genTotal').value);
if(genTotal > thisLang[1])
{
currentCountGen++;
setTimeout(function(){
startGen();
},300);

}
else
{
console.log('all over');
}
    
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
     toast('Unable to connect to Server. Please check your network connection. Will retry in a few moments','toast bg-danger',2000); 
  }
});
}
else
{

var genTotal = parseInt(document.getElementById('genTotal').value);
if(genTotal > currentCountGen)
{
currentCountGen++;
startGen();
}
}

}


function sendMessage(convoid,touser)
{
	var message = document.getElementById('tomessage').value;
if(message == '')
{
	return false;
}
var params = 
{
	message:message,
	touser:touser,
	convoid:convoid
}

var url = 'messages/savemessage.php';
genAjax(url,params,function(response){

	var x = document.getElementById('calStripContainer'+convoid).innerHTML;
	var thisLang = document.getElementById('calStripContainer'+convoid).lang;
	var thisTitle = document.getElementById('calStripContainer'+convoid).title;
	var thisClick = $('#calStripContainer'+convoid).attr('onclick');
	$('#calStripContainer'+convoid).remove();
	var html= '<div id="calStripContainer'+convoid+'" lang="'+thisLang+'" title="'+thisTitle+'" onclick="'+thisClick+'">'+x+'</div>';
	document.getElementById('calStripContainer').insertAdjacentHTML('afterBegin',html);
	var temp = response;
	temp = temp.split('lang="');
	temp1 = temp[1].split('"');
	bubbleid = temp1[0];
document.getElementById('bubbleHolder').insertAdjacentHTML('beforeEnd',response);
            
            $("#bubbleHolder").animate({ scrollTop: $('#bubbleHolder').prop("scrollHeight")}, 300);
            	
                       // $("#bubbleHolder").scrollTo("#"+bubbleid, 300);
                        document.getElementById('tomessage').value = '';

});
}

flatColors = '#1abc9c,#2ecc71,#3498db,#9b59b6,#34495e,#f1c40f,#e67e22,#e74c3c,#ecf0f1,#95a5a6,#16a085,#27ae60,#2980b9,#8e44ad,#2c3e50,#f39c12,#d35400,#c0392b,#bdc3c7,#7f8c8d';
flatColors = flatColors.split(",");
var curFlat=-1;
function getFlatColor()
{
	if(curFlat >= 19)
	{
		curFlat = -1;	
	}
	curFlat++
	return flatColors[curFlat];
}

function fillLetterBox()
{
		var itemSearch = $('.letterBox');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			
			document.getElementById(thisId).style.backgroundColor = getFlatColor();

    });
		
}

function resetWindow()
{
		$('#sendHereOnly').slideUp();
	$('#simpleProceed').slideDown();
}

function checkMsgWindow()
{
	var found = 0;
var empid = document.getElementById('inmsg0').value;
		var itemSearch = $('.calStCIn');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisLang = $(this).attr('lang');
			
			if(thisLang == empid)
			{
				found++;
				thisId = $(this).attr('id');

				$('#'+thisId).trigger('click');
			}
    });
	

if(found == 0)
{
	$('#sendHereOnly').slideDown();
	$('#simpleProceed').slideUp();
}
else
{
	closeBigModal();
}
}


function searchChat(event,value)
{
	console.log(value);
	value = value.toLowerCase();
	if(value == '')
	{

		var itemSearch = $('.calStCIn');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
				$('#'+thisId).show();
    });
	}
			var itemSearch = $('.calStCIn');
			searchList = $('#calStripContainer').find(itemSearch).each(function(){
				

				thisTitle = $(this).attr('title');
				thisTitle = thisTitle.toLowerCase();

				thisId = $(this).attr('id');			
				if(thisTitle.indexOf(value) != -1)
				{
					$('#'+thisId).show();
				}
				else
				{
					$('#'+thisId).hide();	
				}
	    });
}

function releaseSalary()
{
	var checkList = "0";
	var empList = "0";
	var itemSearch = $('.checkInput');
	$('#tableDiv').find(itemSearch).each(function(){
		if(this.checked == true)
		{
			var thisVal = this.value;
			var thisLang = this.lang;
			if(document.getElementById('tableRow'+thisVal))
			{
				$('#tableRow'+thisVal).hide();
			}
			checkList = checkList+","+thisVal;
			empList = empList+","+thisLang;
		}
			
    });	
showProcessing();
var params =
{
ids:checkList,
emps:empList
}
var url = "attendance/releaseSalary.php";
genAjax(url,params,function(response){
hideProcessing();
var html = "Successfully marked as released. Changes may take a few minutes to reflect";
toast(html,'toast bg-success',5000);
$('#atListViewHere').trigger('click');
});

}


function deleteCircular(id)
{
	var url = "circulars/delete.php?id="+id;
	genAjax(url,'',function(){
		$('#circRow'+id).hide();
	});
}

function saveDocumentTemplate(id)
{
	var name = document.getElementById('doctemp1').value;
	var editorData = CKEDITOR.instances.doctemp0.getData();

	var params = 
	{
		name:name,
		data:editorData,
		id:id
	}
	var url = 'document-templates/save.php'; 
	genAjax(url,params,function(res){
		closeBottom();
		$('#doctemp').trigger('click');

	});
}


function allotDocumentTemplate(id,allotdocid)
{
var docid = document.getElementById('templateid').value;
var editorData = CKEDITOR.instances.doctemp0.getData();

	var params = 
	{
		docid:docid,
		data:editorData,
		id:id,
		allotdocid:allotdocid
	}
	var url = 'document-templates/save-allot.php'; 
	genAjax(url,params,function(){
		closeBottom();
		$('#compdocs').trigger('click');
	});	
}


function toggleShiftDate(elm)
{
	var thisDate = elm.value;
	var curVal = document.getElementById('tsht3').value;
	if(elm.checked == true)
	{
		curVal = curVal+","+thisDate;
	}
	else
	{
		curVal = curVal.replace(thisDate,"");
	}
	 document.getElementById('tsht3').value = curVal;
}

function rotateClass(thisClass)
{
	$('.calendarclass').hide();
	$('.leavesclass').hide();
	$('.circularsclass').hide();
	$('.salaryclass').hide();
	$('.salaryslipclass').hide();

	document.getElementById('calendartd').className= '';
	document.getElementById('leavestd').className= '';
	document.getElementById('circularstd').className= '';
	document.getElementById('salarytd').className= '';
	document.getElementById('salarysliptd').className= '';

	document.getElementById(thisClass+'td').className = 'selected';




	$('.'+thisClass+'class').show();

}

function saveNewEmployee(saveurl,callbackurl)
{
	var profileId = $('#inp34').find(":selected").val();
	if(profileId == '0'){
		toast('Please select the profile','toast bg-danger',3000);
	 	return false;
	}
	if(document.getElementById('inp21').value == '1')
	{
		for(j=22;j<26;j++)
		{

		if(document.getElementById('inp'+j).value == '')
		{
			showError('Please enter all mandatory fields',document.getElementById('inp'+j),1);
			return false;
		}


		}

	}

	savedata(saveurl,'','','inp',36,'','url:'+callbackurl,'tableDiv','formDiv');
}


function UpdateEmployee(saveurl,callbackurl)
{
	var profileId = $('#inp38').find(":selected").val();
	if(profileId == '0'){
		toast('Please select the profile','toast bg-danger',3000);
	 	return false;
	}
	if(document.getElementById('inp18').value == '1')
	{
		for(j=31;j<35;j++)
		{

		if(document.getElementById('inp'+j).value == '')
		{
			showError('Please enter all mandatory fields',document.getElementById('inp'+j),1);
			return false;
		}


		}

	}
savedata(saveurl,'','','inp',47,'','url:'+callbackurl,'tableDiv','formDiv');
}


function getPoolBalance()
{
	var thisdate = document.getElementById('inp7').value;
	var url = "employee/pool-calculator.php?doj="+thisdate;
	genAjax(url,'',function(response){
		document.getElementById('inp17').value = response;
	})
}



/*  Access Control Code */
function chkModule(sub,main,total,fr,dis)
{
fr = parseInt(fr);
total = parseInt(total);

	if(document.getElementById(main).checked == true)
	{
	for(i=fr;i<=total;i++)
		{

				if(document.getElementById(sub+i))
				{
				
					document.getElementById(sub+i).checked = true;
					document.getElementById(sub+i).disabled = false;
				}
		}
	}
	else
	{
	for(i=fr;i<=total;i++)
		{

		if(document.getElementById(sub+i))
				{
					document.getElementById(sub+i).checked = false;
					if(dis != '')
					{
					document.getElementById(sub+i).disabled = true;
					}
				}
		}

	
	}
}

function SavePermissions(saveurl,callbackurl){
	var employeeid= document.getElementById('employeeid').value;
	var profileid= document.getElementById('profileid').value;
	savedata(saveurl+'?profile='+profileid+'&employee='+employeeid,'','','lds',70,'','url:'+callbackurl,'tableDiv','formDiv');
}

function getZoneList(id){
if(id==20){
$("#zone_lists").css("display", "block");
}else{
$("#zone_lists").css("display", "none");
}
}


function SelectAccessLevel(level){
if(level=='profile'){
$("#profile_div").css("display", "block");
$("#employee_div").css("display", "none");
} else if(level=='employee'){
$("#employee_div").css("display", "block");
$("#profile_div").css("display", "none");
}
}

function getEmployeeList(branchid){
showProcessing();
var params =
{
branchid:branchid
}
var url = "masters/accesscontrol/GetEmployees.php";
genAjax(url,params,function(response){
hideProcessing();
$("#employeeid").empty();
$("#employeeid").append(response);
//$('#atListViewHere').trigger('click');
});
}


/*  Access Control Code */

/* salary breakdowns */
function SalaryBreakdowns(){
    showProcessing();
	var params = 
	{
		ctc: document.getElementById('inp2').value,
		pt: document.getElementById('inp5').value,
		pf: document.getElementById('inp3').value,
		od: document.getElementById('inp6').value,
		pffixed: document.getElementById('inp4').value
	}
	var url = 'salarycalculation/calculation_sheet.php'; 
	genAjax(url,params,function(response){
	var data = JSON.parse(response);
	$("#inp7").val(data.basic);
    $("#inp8").val(data.bonus);
    $("#inp9").val(data.ctc);
    $("#inp10").val(data.da);
    $("#inp11").val(data.gross);
    $("#inp12").val(data.hra);
    $("#inp13").val(data.netpay);
    $("#inp14").val(data.otherdeduction);
    $("#inp15").val(data.pfemployer);
    $("#inp16").val(data.pfpayable);
    $("#inp17").val(data.refreshment);
    $("#inp18").val(data.totaldeductions);
    $("#inp19").val(data.totalearnings);
    $("#inp20").val(data.pt);
    hideProcessing();
	});	
}
/* salary breakdowns */


/* freeing salary */
function freezingsalary(month,year){
$("#salary_freez").show();
var months_name = {'1':'JAN', '2':'FEB','3':'MAR','4':'APR','5':'MAY','6':'JUN','7':'JUL','8':'AUG','9':'SEP','10':'OCT','11':'NOV','12':'DEC'};
var button_name = 'FREEZE SALARY FOR '+months_name[month]+" "+year;
if(month < 10){
var month_fix = '0'+month;	
}
else{
var month_fix = month;	
}
var button_value = "freeze_"+month_fix+"_"+year;
$("#salary_freez").attr('value',button_value); //versions older than 1.6
$("#salary_freez").html(button_name);
var params = 
{
tablename:button_value
}
var url = 'salarycalculation/check_table_exist.php'; 
genAjax(url,params,function(response){
hideProcessing();
if(response==0){
var html = "Salary already freezed for "+months_name[month]+" "+year;
toast(html,'toast bg-danger',5000);
$("#salary_freez").prop('disabled', true);
}else{
$("#salary_freez").prop('disabled', false);
}
});	
}

function freezingattendance(month,year){
$("#attendance_freez").show();
var months_name = {'1':'JAN', '2':'FEB','3':'MAR','4':'APR','5':'MAY','6':'JUN','7':'JUL','8':'AUG','9':'SEP','10':'OCT','11':'NOV','12':'DEC'};
var button_name = 'FREEZE ATTENDANCE FOR '+months_name[month]+" "+year;
if(month < 10){
var month_fix = '0'+month;	
}
else{
var month_fix = month;	
}
var button_value = "freez_attendance_"+month_fix+"_"+year;
$("#attendance_freez").attr('value',button_value); //versions older than 1.6
$("#attendance_freez").html(button_name);

var params = 
{
tablename:button_value
}
var url = 'salarycalculation/check_table_exist.php'; 
genAjax(url,params,function(response){
hideProcessing();
if(response==0){
var html = "Attendance already freezed for "+months_name[month]+" "+year;
toast(html,'toast bg-danger',5000);
$("#attendance_freez").prop('disabled', true);
}else{
$("#attendance_freez").prop('disabled', false);
}
});	
}


function finalfreez(value){
    showProcessing();
	var params = 
	{
		tablename:value,
		month: document.getElementById('atMonth').value,
		year: document.getElementById('atYear').value
	}
	var url = 'salarycalculation/freezing_salary.php'; 
	genAjax(url,params,function(){
    hideProcessing();
    var html = "Salary Freezed Successfully.";
    toast(html,'toast bg-success',5000);
    $("#salary_freez").prop('disabled', true);
	});	
}

function freez_attendance(value){
    showProcessing();
	var params = 
	{
		tablename:value,
		month: document.getElementById('atMonth').value,
		year: document.getElementById('atYear').value
	}
	var url = 'salarycalculation/freezing_attendance.php'; 
	genAjax(url,params,function(){
    hideProcessing();
    var html = "Attendance Freezed Successfully.";
    toast(html,'toast bg-success',5000);
    $("#attendance_freez").prop('disabled', true);
	});	
}


/* freeing salary */


