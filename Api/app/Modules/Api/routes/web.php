<?php

Route::group(['module' => 'Api','prefix'=>'api','middleware' => ['web'], 'namespace' => 'App\Modules\Api\Controllers'], function() {

    Route::get('users-login','UsersController@index');
    Route::any('check-login','UsersController@checkLogin');

});
