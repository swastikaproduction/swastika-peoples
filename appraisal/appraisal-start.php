<?php
include'../include/config.php';
$appId = $_GET['appID'];
$query = mysqli_query($con,"select * from appraisals where id='$appId'");
$result = mysqli_fetch_array($query);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background:#fff;height="60%">
<!-- Content Header (Page header) -->   
<!-- Main content -->
<section class="content" style="margin-top: 5px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Start Appraisal</h3>
</div>
<!-- /.box-header -->
<!-- form start -->
<form action="appraisal/appraisalStart.php" role="form" method="post">
<div class="box-body">
<div class="row">
<div class="col-md-2">
<div class="form-group">
<input type="hidden" name="appId" value="<?php echo $_GET['appID'];?>">
<label for="exampleInputEmail1" style="/*font-weight: 700; */" >Appraisal Mode</label>
<span class="form-control"><?php echo $result['appraised_mode']; ?></span>
</div>
</div>
<div class="col-md-2">
<div class="form-group">
<label for="exampleInputEmail1">Year</label>
<span class="form-control"><?php echo $result['appraised_year'];?></span>
</div>
</div>
<div class="col-md-2">
<div class="form-group">
<label for="exampleInputEmail1">Period</label>
<span class="form-control"><?php echo $result['appraised_time'];?></span>
</div>
</div>
</div>         

</div>

<div class="row">
<?php $d = date('Y-m-d'); ?>
<div class="col-md-2">
<div class="form-group">
<label>Employees Due Date</label>

<input type="date" name="eDate" class="form-control" id="datepicker2" min="<?php echo $d;?>"  required="">
</div>
</div>
<div class="col-md-2">
<div class="form-group">
<label>Manager's Due Date</label>
<input type="date" name="mDate" class="form-control" id="datepicker1" min="<?php echo $d;?>" required="">
</div>
</div>
<div class="col-md-2">
<div class="form-group">
<label>HOD's Due Date</label>
<input type="date" name="hDate" class="form-control" id="datepicker" min="<?php echo $d;?>"  required="">
</div>
</div>
</div>             

<!-- /.box-body -->

<div class="box-footer">
<button type="submit" class="btn btn-success" >Start</button>
<span class="btn btn-danger" onclick="getModule('appraisal/appraisal_view.php?appraisal=1','formDiv','tableDiv','loading');">Back</span>
</div>
</form>
</div>  

<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->