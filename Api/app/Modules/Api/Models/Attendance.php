<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model {

    //
    protected $table = 'attendancelog';

    public $timestamps = false;

}