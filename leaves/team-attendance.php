<?php
include("../include/config.php");
$date = date('d-m-Y');

$emp = $loggeduserid;//$_GET['emp'];
 $month = date('m',strtotime($date));//$_GET['month'];

$year = date('Y',strtotime($date));;//$_GET['year'];

//calculation hide code
$d = date('d-m-y');
$c = date('m',strtotime($d));



if($month < 10)
{
	$month = $month;
}

$typeStr = '';
if($_GET['type'] != '0')
{
	$typeStr = " AND `status` = '1'";
}


	$getData = mysqli_query($con,"SELECT a.*,e.name,e.empid FROM `attendancelog` a INNER JOIN employee e ON e.id = a.empid  WHERE a.month = '$month' AND a.year = '$year' ".$typeStr." and e.imdboss='$emp' ORDER BY a.createdate") or die(mysqli_error($con));



?>
<section style="background: white;margin-bottom: 20px;">
	<div class="row">
		<div class="col-md-4">
			<label>Month</label>
			<select class="form-control" id="emonth">
				
				<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
<option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
			</select>

		</div>
		<div class="col-md-4">
			<label>Year</label>
			<select class="form-control" id="eyear">
				<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top: 11px;">
			<input type="button" name="submit" class="btn btn-success" value="Submit" onclick="GetMonthWiseAttendance()">
		</div>
	</div>
</section>
<div id="employeefilterattendance">
	
</div>
<div style="padding:0px;" id="employeeattendance">
<table class="table table-bordered table-hover"  cellpadding="0" cellspacing="0" id="tableDivAtView">
	<tr>
		<th style="width:5%">#</th>
		<th style="width:20%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:15%">Total Deduction</th>
		<th style="width:5%">From Salary</th>
		<th style="width:5%">From Pool</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
		<th style="width:10%">Details</th>
		<th style="width:10%">Export</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		
		<?php
		if($empArray[$row['empid']]['left'] == '1')
		{
			?>
			<div style="float:right;text-align:right;padding-top:5px;">
<span class="label label-danger" onclick="getModule('attendance/delete.php?id=<?php echo $row['id'];?>','atRow<?php echo $row['id'];?>','','loading');$('#atRow<?php echo $row['id'];?>').hide();">
<i class="fa fa-close"></i>
</span>
</div>
			<?php
		}
		?>
		<?php echo $i;?>
	</td>
	<td><?php echo $row['name'];?></td>
	<td><?php echo $row['empid'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>
	<td>
	
	<?php
	if($row['status'] == '1')
	{
		echo $row['deduction'];

		 if($c==$month ){
		?>


	<!-- <div style="float:right;margin-top:5px;" onclick="getModule('salarycalculation/generate_salary.php?id=<?php echo $row['id'];?>&i=<?php echo $i;?>','atRow<?php echo $row['id'];?>','','loading')">
		<span class="label label-success">
	<i class="fa fa-refresh"></i>

</span>

	</div> -->

		<?php
	}
	}
	else
	{ 
		  


		?>
		<div style="float:left;margin-top:5px;">
		<span class="label label-default" id="genBut<?php echo $i;?>" lang="<?php echo $row['id'];?>::<?php echo $i;?>" onclick="getModule('salarycalculation/generate_salary.php?id=<?php echo $row['id'];?>&i=<?php echo $i;?>','atRow<?php echo $row['id'];?>','','loading')">CLICK TO CALCULATE</span>
		</div>
		<?php

	}
	?>
	
	</td>
	<td>
		<?php echo $row['salary_deduction'];?>

	</td>
	<td>
		<?php echo $row['pool_deduction'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'] - $row['pool_deduction'];?>
	</td>



	<td style="padding-top:12px" id="detBut<?php echo $row['id'];?>" onclick="getModule('attendance/details.php?id=<?php echo $row['id'];?>','bottomDiv','','loading')">
		<span class="label label-info">DETAILS</span>
	</td>

	<td style="padding-top:12px" id="" >
	<?php
	if($row['status'] == '1')
	{
		?>
		<span class="label label-success" onclick="window.open('attendance/export.php?id=<?php echo $row['id'];?>','_blank')">
					<i class="fa fa-external-link"></i>&nbsp;EXPORT

		</span>
		<?php
	}
	?>
	</td>

</tr>
		<?php
		$i++;
	}
	?>
</table>
<input type="text" value="<?php echo $i;?>" id="genTotal" style="display:none">
</div>
<br/><br/><br/><br/><br/>