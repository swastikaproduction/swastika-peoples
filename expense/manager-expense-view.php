<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Expense History</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Appraisal mode already Exist
</div>   <?php
}
            ?> 
            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('expense/add-expense.php','formDiv','tableDiv','loading');">Add Expense</span>           
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Benificiary Name</th>
<th>Head Name</th>            
           
<th>Amount</th>            
<th>Receipt</th>            
<th>Create Date</th>            
<th>Status</th>            
<th>Action</th>         
            
</tr>
</thead>
<tbody>
<?php  
$count=1; 
$empid = $_COOKIE['emanagerid'];          
include'../include/connection.php';
$query = mysqli_query($con,"SELECT e.id,h.name hname ,e.amount,e.createdat,b.name bname,e.filename,e.status,e.paid  FROM expense e INNER JOIN expensenature h ON h.id = e.natureid  INNER JOIN expensebenificiary b on b.id = e.benificiary WHERE e.empid = '$empid'");
while($row=mysqli_fetch_array($query)){

	if($row['paid']==0){

		$status = '<span style="background-color: #81a968;color: white;border-radius: 7px;">Pending</span>';
	}else{
		$status = '<span style="background-color: orange;color: white;border-radius: 7px;">Approved</span>';
	}
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['bname'];?></td>

<td><?php echo $row['hname'];?></td>
<td><?php echo $row['amount'];?></td>
<td><a href="ExpenseFiles/<?php echo $row['filename'];?>" download>Download</a></td>
<td><?php echo $row['createdat'];?></td>
<td><?php echo $status;?></td>
<?php 
if($row['paid'] !=1){
    ?>

 <td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="expense/delete-expense.php?exid=<?php echo $row['id'];?>" onclick="return confirm('Are you sure you want to delete this?');">Delete</a></li>
      <li><a href="#" data-toggle="modal" data-target="#myModalUpdate" onclick="UpdateExpense('<?php echo $row['id'];?>');">Edit</a></li>
      
    </ul>
  </div>
</div></td>
<?php } ?>
    <?php
    }?>                      

</tbody>               
</table>

</div>
<!-- /.box-body -->
</div>
</div>
</div>

<!-- //update benificiary data  -->

<div class="modal fade" id="myModalUpdate" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/update-expense-concerns.php" method="post" enctype="multipart/form-data">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update  Benificiary </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Benificiary Name</label>
<input type="hidden" name="keyvalue" value="listing">
<input type="hidden" name="eupdateid" id="eupdateid">
<select class="form-control" name="benificiaryid" id="benificiaryid" onchange="Benificiary();" required="">
                    <option value=''>select benificiary</option>
                  <?php 
                    $query = mysqli_query($con,"select * from expensebenificiary where empid='$empid' ");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
                   
                  </select>
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="expenseheadid" name="expenseheadid" class="form-control" onchange="GetExpenseHeadData();" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Amount</label>
<input type="text" name="expenseamountupdate" id="expenseamountupdate" class="form-control allownumericwithoutdecimal" required="">
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Upload File</label>
<input type="file" name="file" id="file" class="form-control">
</div>
</div>
</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->