var historyVal = 0;
var historyArray = [];
var innerChange = 0;
var outerChange = 0;
var massEditUrl = 0;

function showSearch()
{
$('#searchBox').animate({top:'0'},100);
}

function hideSearch()
{
$('#searchBox').animate({top:'-100px'},100);
}

function closeBigModal()
{
	console.log('called');
$("#myModalBig").modal("hide");
}

function toogleFormTable()
{
	if(document.getElementById('formDiv').style.display == 'none')
	{
		$('#formDiv').show();
		$('#tableDiv').hide();
	}
	else
	{
		$('#formDiv').hide();
		$('#tableDiv').show();		
	}
}

function toogleFormTableModalBig()
{
	if(document.getElementById('formModalBig').style.display == 'none')
	{
		$('#formModalBig').show();
		$('#tableModalBig').hide();
	}
	else
	{
		$('#formModalBig').hide();
		$('#tableModalBig').show();		
	}

}

function addTask()
{
var d = new Date();
var n = d.getTime();
var x= '<div class="checkbox taskList" style="display:none" id="'+n+'">';
x+= '<label><input type="checkbox" value="">'+document.getElementById("taskAdder").value+'</label>';
x+= '</div>';
//var y = document.getElementById('taskList').innerHTML;
document.getElementById('taskList').insertAdjacentHTML('afterBegin',x);
document.getElementById("taskAdder").value = '';
$('#'+n).slideDown('fast');
}

function callCalModal()
{
getModal('projects/calData.php','tableModalBig','formModalBig','loading')
}

function showNotifBar()
{
$('#notifContainer').fadeToggle('fast')
}

function hideNotifBar()
{
$('#notifContainer').fadeOut('fast')
}

function showError(errorMessage,elm,markRed)
{
	if(markRed == 1)
	{
					elm.className = 'inputBoxRed';
					elm.addEventListener('focus',function(){
						this.className = 'inputBox';
					});		
	}
	toast(errorMessage,'toast bg-danger','2000');
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var notifTimeout;
function showBottomNotification(type,message)
{
	clearTimeout(notifTimeout);
	$('#bottomNotification').html("");

	if(type == 'stop')
	{
		toast(message)
		/*
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(216,99,73,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);
	*/

	}


	if(type == 'successdel')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},20000);

	}


	if(type == 'success')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);

	}

}


function hideBottomNotification()
{
	$('#bottomNotification').html("");
			$('#bottomNotification').animate({opacity:0.3},500,function(){
			$('#bottomNotification').animate({opacity:0,width:'0px',padding:'0px'},200);
		});
}

function tableSearch(event,url)
{

	if(event.keyCode == '13')
	{
		var varName = "";
		var varValue = "";
		var i =0;
		var itemSearch = $('.searchInput');
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			if($(this).val() != '')
			{
			varName += $(this).attr('name')+",";				
			varValue += $(this).val()+"!NBCOMNB!";
			}


    });
		
		checkAlready = url.indexOf("varList=");
		if(checkAlready == -1)
		{
			checkQue = url.indexOf("?");
			if(checkQue == -1)
			{
				url = url+'?varList='+varName+'&varValues='+varValue;				
			}
			else
			{
				url = url+'&varList='+varName+'&varValues='+varValue;				
			}

		}
		else
		{
			temp = url.split("varList=");
			url = temp[0]+'varList='+varName+'&varValues='+varValue;
		}
		getModule(url,'tableDiv','fomrDiv','loading');
				
	}			
}

function checkAll(elm)
{
	var itemSearch = $('.checkInput');
	if(elm.checked == true)
	{
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).attr("checked","checked");		
			this.checked = true;	
    });

	}
	else
	{
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).removeAttr("checked");		
			this.checked = false;		
    });		
	}
}

function bulkAction()
{
	var action = document.getElementById('action').value;
	var tempAction = action.split(":");
	if(tempAction[0] == 'D')
	{
		crossCheckDelete(tempAction[1]);
	}
	else
	{
		massEdit();
		//showBottomNotification('stop','Mass edit feature not available yet, but will appear soon');
	}
}

function disableButton(id,loading)
{
	var orgClasName = document.getElementById(id).className;
	var tempClassName = orgClasName+" disabled";
	document.getElementById(id).className = tempClassName;
	if(loading != '')
	{
		document.getElementById(id).innerHTML = "<img src='images/circle-primary.GIF' alt='' style='height:12px;margin-bottom:2px;'/>&nbsp;&nbsp;"+loading;
	}
}

function enableButton(id,orgHTML)
{
	var tempClasName = document.getElementById(id).className;
	var orgClassName = tempClasName.replace(" disabled","");
	document.getElementById(id).className = orgClassName;
	if(orgHTML != '')
	{
		document.getElementById(id).innerHTML = orgHTML;
	}

}

function sortThis(url,tableDiv,formDiv,loading,orderParam)
{
	globalSendParam  = {
		orderby:orderParam
	}
	getModule(url,tableDiv,formDiv,loading);
}





var crtlKey = 0;
function checkMasterKey(e)
{
	if(e.keyCode == '17')
	{
		crtlKey = 1;
	}
	else if(e.keyCode =='77' && crtlKey == 1)
	{
		
		$('#masterKeyModal').modal();
		crtlKey = 0;
	}
	else
	{
		crtlKey = 0;
	}

}

function removeCrtl()
{
	crtlKey = 0;
}


function changeHashValue()
{
var hash = window.location.hash;
if(hash != '')
{
	if(innerChange == 0)
	{
		hash = hash.replace("#","");
		hashVals = hash.split("&rtl=");
		outerChange = 1;
		getModule(hashVals[1],hashVals[2],hashVals[3],hashVals[4])
	}
	else
	{
		innerChange = 0;
	}	

	 $.ajax({
      url:'leaves/get-emplyee.php',
      method:'post',
      data:{},     
      success:function(res){
      
        $('#empid').html(res);
      },
    });
}
else
{
	getModule('dashboard/index.php','tableDiv','formDiv','loading')
}

}


function toggleThings(show,hide,showprop,hideprop)
{

	if(hideprop == 'slide')
	{
		$('#'+hide).slideUp('fast');
	}
	else
	{
		$('#'+hide).fadeOut('fast');
	}

	if(showprop == 'slide')
	{
		$('#'+show).slideDown('fast');
	}
	else
	{
		$('#'+show).fadeIn('fast');
	}	



}

function checkInstallmentAmount(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{

	var currentAmount = parseInt(document.getElementById('mod2').value);
	var maxAmount = parseInt(document.getElementById('mod2').lang);

	if(maxAmount < currentAmount)
	{
		
		var thisurl = url.replace("savepayment","checkPending");
		genAjax(thisurl,'',function(response){
			var temp = parseInt(response);
			if(temp < currentAmount)
			{
				showError('The maximum outstanding amount for this customer is Rs.'+temp);
			}
			else
			{
				toggleThings('reallyPay','simplePay','','slide');
			}
		})				


	}
	else
	{
		savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv);
	}



//	
}


function notWorking()
{
	$('#notWorking').modal();
}


var hiderTimeObj;


function toast(html,css,timeout)
{
	if(hiderTimeObj)
	{
		clearTimeout(hiderTimeObj);
	}
	document.getElementById('toastText').innerHTML = html;
	$('#toast').animate({top:'0px'});
	document.getElementById('toast').className = css;
	if(timeout != '0')
	{
	hiderTimeObj = setTimeout(function(){
hideToast()			
},timeout);
	}

}

function hideToast()
{
	$('#toast').animate({top:'-300px'});
}

var poItemCount=0;

function nextRecord(type,current,url)
{
	var prev = 0;
	var next = 0;
	var thisJ = -1;
	var list = document.getElementById('idList').value;
	list = list.split(",");
	for(j=0;j<list.length;j++)
	{
		if(list[j] == current)
		{
			thisJ = j;
			break;
		}
	}
	if((thisJ == 0 && type == 'prev') || (thisJ == (list.length-1) && type == 'next'))
	{
		toast('Reached end of records in selected view','toast bg-danger',3000);
	}
	else
	{
			if(type == 'next')
			{
				newId = list[j+1];
			}
			else
			{
				newId = list[j-1];
			}	
			url = url.replace('{#IDHERE}',newId)
		getModule(url,'formDiv','tableDiv','loading')
	}
	


}

function sortby(type,item,elm)
{

$('#moduleRow th').each(function() {
	if(this != elm)
	{
		temp = this.innerHTML;
		temp = temp.replace('<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-up"></i></div>',"");
		temp = temp.replace('<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-down"></i></div>',"");
		this.innerHTML = temp;
	}
 
});



var order = 'ASC';
var x = elm.innerHTML;
if(x.indexOf('caret-down') != -1)
{
	order = 'ASC';
	x = x.replace('caret-down','caret-up');
}
else if(x.indexOf('caret-up') != -1)
{
	order = 'DESC';
	x = x.replace('caret-up','caret-down');
}
else
{
	x = x+'<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-up"></i></div>';
}

elm.innerHTML = x;


	if(type == 'leads')
	{
		url = 'leads/subindex.php?sortby='+item+'&order='+order;
	}
	else if(type == 'opportunities')
	{
		url = 'opportunities/subindex.php?sortby='+item+'&order='+order;	
	}
	else if(type == 'deals')
	{
		url = 'deals/subindex.php?sortby='+item+'&order='+order;	
	}

	getModule(url,'moduleData','','loading');
}


function filterActivity(todo)
{

		if(todo != 'all')
		{
			$('#allActivities div.actPanel').each(function(){
				if(this.lang == todo)
				{
					$(this).slideDown();
				}
				else
				{
					$(this).slideUp();
				}			
			})

		}
		else
		{
			$('#allActivities div.actPanel').each(function(){
					$(this).slideDown();
			})

		}


}

function closeBottom()
{
	$('#bottomDivContainer').animate({bottom:'-3000px'});
	document.getElementById('bottomDiv').innerHTML = '';
	if(atchangedone == 1)
	{
		$('#atList').trigger('click');
	}
}

function checkAvailability(table,field,value,responselement,element)
{
	var params = "table="+table+"&field="+field+"&value="+value;
	var url = "checkavailibility.php";
	document.getElementById(responselement).innerHTML = "Checking Availability..";
	genAjax(url, params, function(response){
		document.getElementById(responselement).innerHTML = response;
		if(response.indexOf('!') != -1)
		{
			element.value = '';
			element.focus();
		}

	})
}


function checkHeightBoxes()
{


var fullheightboxes = 'invContainerInner';
fullheightboxes = fullheightboxes.split(",");
for(j=0;j<fullheightboxes.length;j++)
{

	if(document.getElementById(fullheightboxes[j]))
	{
		console.log(document.getElementById(fullheightboxes[j]).style.height);
		$('#'+fullheightboxes[j]).animate({height:window.innerHeight});
        //document.getElementById(fullheightboxes[j]).style.height = window.innerHeight;
		console.log(document.getElementById(fullheightboxes[j]).style.height);
	}

}


}

function toggleDisplay(id,elm)
{
	if(elm.checked == true)
	{
		document.getElementById(id).style.display = 'block';
	}
	else
	{
		document.getElementById(id).style.display = 'none';		
	}
}
 
function setMultiSelectValue(id,elm)
{

var x= '';
	for(k=0;k<elm.options.length;k++)
	{
		if(elm.options[k].selected == true)
		{
			x = x+elm.options[k].value+",";
		}
	}
	$('#'+id).val(x);
}
function showRespectivediv()
{

/*
 var x = document.getElementById("inp1").selectedIndex;
 selected = document.getElementsByTagName("option")[x].value;
*/
selected = document.getElementById("inp1").value;
 if(selected == "Student")
 {
 	document.getElementById('student-div').style.display = 'block';
 	document.getElementById('employee-div').style.display = 'none';

 }

 if(selected == "Employee")
 {
 	document.getElementById('student-div').style.display = 'none';
 	document.getElementById('employee-div').style.display = 'block';

 }

}

function showRespectivedivforhostel()
{
	console.log('ok');
/*
 var x = document.getElementById("inp1").selectedIndex;
 selected = document.getElementsByTagName("option")[x].value;
*/
selected = document.getElementById("inp0").value;
 if(selected == "Student")
 {
 	document.getElementById('student-div').style.display = 'block';
 	document.getElementById('employee-div').style.display = 'none';

 }

 if(selected == "Employee")
 {
 	document.getElementById('student-div').style.display = 'none';
 	document.getElementById('employee-div').style.display = 'block';

 }

}

function getdays()
{	
	days = [];
	selected = document.getElementById("inp0").value;

    $('.timetableday').css({display:'none'});
	var field = selected.split(':');
	var days = field[1];

	var splitted = days.split(',');
		
		for (var i = 0; i < splitted.length; i++) 
		   {
			    id = splitted[i];
			  if(document.getElementById(id).style.display == "none")
			  {
			  	 	document.getElementById(id).style.display = 'block';

			  }

		   }
		  
}





function getAddress(elm)
{
elm.innerHTML = "<i class='fa fa-spin fa-spinner'></i>"
	var geoCodeUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+elm.lang+"&sensor=true";

	 $.ajax({
    type: 'POST',
    dataType:'json',
    url: geoCodeUrl,
    data: '', 
    success: function(responseData) {
    	j=0;
			 for (var key in responseData) {
			 	if(j ==0)
			 	{
			 		var html = "<a href='https://www.google.co.in/maps?q="+elm.lang+"' target='_blank'>"+responseData[key][0].formatted_address+"</a>"
			        elm.innerHTML = html; 
			        elm.className ='';		
			 	}
			j++;
			 	}
    
    },
    error: function() {
    	toast("Unable to get location",'toast bg-danger', '2000');
    }
});
}


function rotateNotBox()
{

}



function showSubmenu(elm)
{
var thisLang = elm.attr('lang');

	$('.submenuItem').hide();

	var itemSearch = $('.submenuItem');
		searchList = $(elm).find(itemSearch).each(function(){

	$(this).fadeToggle();			

		});
}




function toggleCheck(id)
{
	if(document.getElementById(id).checked == true)
	{
		document.getElementById(id).checked = false;
	}
	else
	{
		document.getElementById(id).checked = true;
	}

if(id.indexOf('tosave') != -1)
{
	var total = document.getElementById('totalMatch').value;
	for(j=0;j<total;j++)
	{
		if(id != 'tosave'+j)
		{
			if(document.getElementById('tosave'+j))
			{
				document.getElementById('tosave'+j).checked = false;
			}

		}
	}


}

}


function getTimeStamp()
{
  var d = new Date();
    var n = d.getTime();
    return Date.now();;
}


function setCkeditor()
{

	var eight_height = window.innerHeight - 100;
			var itemSearch = $('[lang="editor"]');
		searchList = $('#formDiv').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			try
			{
				CKEDITOR.replace(thisId);
			}
			catch(e)
			{

			}
		});



searchList = $('#bottomDiv').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			try
			{
				
				CKEDITOR.replace(thisId,{
height: eight_height
});
			}
			catch(e)
			{

			}
		});

$('#docTempCont').animate({height:eight_height});
}

function dynamicSearch(id,term)
{
	term= term.toLowerCase();
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			thisId =  $(this).attr('id');

			if(thisId != 'topLi')
			{
				temp = document.getElementById(thisId).innerHTML.toLowerCase();

				if(temp.indexOf(term) != -1)
				{
					document.getElementById(thisId).style.display = 'list-item';
				}
				else
				{
					document.getElementById(thisId).style.display = 'none';	
				}				
			}


		});
}

function selectAllList(id,putelement)
{
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			var thisId = $(this).attr('id')
			if(thisId != 'topLi')
			{
			thisClass =  $(this).attr('class','list-group-item li-selected');
				itemList+= $(this).attr('lang')+",";
			}


		});

		$('#'+putelement).val(itemList);
}

function deselectAllList(id,putelement)
{
	console.log(id);
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			var thisId = $(this).attr('id')
			if(thisId != 'topLi')
			{
			thisClass =  $(this).attr('class','list-group-item');
			}


		});

		$('#'+putelement).val('');
}




function collectMultiValues(elm,ulclass,putelement)
{
	if(elm.className.indexOf('li-selected') == -1)
	{
		elm.className = 'list-group-item li-selected';
	}
	else
	{
		elm.className = 'list-group-item'
	}
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+ulclass).find(itemSearch).each(function(){
			thisClass =  $(this).attr('class');
			if(thisClass == 'list-group-item li-selected')
			{
				itemList+= $(this).attr('lang')+",";
			}

		});

		$('#'+putelement).val(itemList);

}


function checkItem(url,type,elm)
{
	if(elm.value != '')
	{
		genAjax(url,'',function(response){
			if(response.indexOf('TRUE') == -1)
			{
				var html = "This "+type+ " already exists. Please choose again";
				toast(html,'toast bg-danger',3000);
				elm.value = '';
			}
			else
			{
				if(elm.id == 'inp3' && type == 'Email Id')
				{
					document.getElementById('inp14').value = elm.value;
				}
			}
		})	
	}

}


function checkNotification()
{
$.ajax({
  type: "POST",
  url: 'checknotifications.php',
  data: '',
  success: function(data){
  	temp = data.split("__");
  	if(temp[0].indexOf('YES') != -1)
  	{
  		$('#msgDot').slideDown();
  	}

  	if(temp[1].indexOf('YES') != -1)
  	{
  		$('#notifDot').slideDown();
  	}
    
  }
});

    
}


function insertText(value)
{
	CKEDITOR.instances.doctemp0.insertHtml(value);
}

//branch code check
function branchCheck(branch,currentbranchCode){
		if(currentbranchCode != branch){
		$.ajax({
        url:'masters/branch/getBranch.php',
        method:'get',
        data:{branch_code:branch},
        success:function(res){
	        var data = JSON.parse(res);
         	if(data.status == '200'){
         		$('#inp1').val(currentbranchCode);
         		toast(data.msg,'toast bg-danger',5000);
	 			return false;
         	}
         	else{
         		return true;
         	}

    	}
    })
	}
 }

// fnf calculation
 function fnfCalculaton(salary){
	var empcode = $('#empcode').val();
	var empid = $('#empid').val();
	var loan_amount = Math.round($('#inp5').val());
	var rm_client_debit = $('#inp6').find(":selected").val();
	var month = $('#atMonth').find(":selected").val();
	var year = $('#atYear').find(":selected").val();
	// alert(month); alert(year);

	var head_comment_salary = $('#heads_comment_salary').val();

	if(rm_client_debit == 1){
		var client_debit = Math.round($('#inp4').val());
	}
	else{
		var client_debit = 0;
	}
	
	$.ajax({
        url:'salarycalculation/fnfSalaryCalculationSheet.php',
        method:'get',
        data:{emp_id:empid,emp_code:empcode,actual_salary:salary,loan_amount:loan_amount,client_debit_amount:client_debit,rm_client_debit:rm_client_debit,month:month,year:year,head_comment_salary:head_comment_salary,type:1},
        success:function(res){
	        var data = JSON.parse(res);
         	if(data.status == '200'){
         		$('#hide1').css('display', 'block');
         		$('#hide2').css('display', 'block');
         		$('#hide3').css('display', 'block');
         		$('#salary_calculated_by_attendance').val(data.salary_calculated_by_attendance);
         		$('#salary_calculated_by_heads_comment').val(data.salary_by_heads_comment);
         		$('#leave_deductun_salary').val(data.total_leave_salary_deduction);
         		$('#other_deduction').val(data.training_calculaton);
         		$('#personal_tax').val(data.pt);
         		toast(data.message,'toast bg-success',4000);
         	}
         	else{
         		$('#hide1').css('display', 'hide');
         		$('#hide2').css('display', 'hide');
         		$('#hide3').css('display', 'hide');
         		toast(data.message,'toast bg-danger',4000);
         	}

    	}
    })
}

function savefnfCalculaton(){
	var empid = $('#empid').val();
	var salary_calculated_by_attendance = $('#salary_calculated_by_attendance').val();
	var salary_calculated_by_heads_comment = $('#salary_calculated_by_heads_comment').val();
	var salaryReleasedBy = $('#salaryReleasedBy').find(":selected").val();
	var month = $('#atMonth').find(":selected").val();
	var year = $('#atYear').find(":selected").val();

	if(salaryReleasedBy == 1){
		if(salary_calculated_by_attendance == ""){
			toast('fnf salary not found','toast bg-danger',4000);
			return false;
		}
		var fnf_salary = salary_calculated_by_attendance;
	}
	else if(salaryReleasedBy == 2){
		if(salary_calculated_by_heads_comment == ""){
			toast('fnf salary not found','toast bg-danger',4000);
			return false;
		}
		var fnf_salary = salary_calculated_by_heads_comment;
	}
	
	$.ajax({
        url:'salarycalculation/fnfSalaryCalculationSheet.php',
        method:'get',
        data:{emp_id:empid,fnf_salary:fnf_salary,salaryReleasedBy:salaryReleasedBy,month:month,year:year,type:2},
        success:function(res){
	        var data = JSON.parse(res);
         	if(data.status == '200'){
         		toast(data.message,'toast bg-success',4000);
         	}
         	else{
         		toast(data.message,'toast bg-danger',4000);
         	}

    	}
    })
}

// all letters strat validation 
function changeDefaultPf(id, elementValue) { 
	var pf = document.getElementById("inp3").value; // alert(pf);
	if(pf == 1){
		$("#inp4DefaultPF").attr('style', 'display: block !important');
	}
	else{
		$("#inp4DefaultPF").attr('style', 'display: none !important');
	}
 }

function ValidateOfferLetter(){

var valid = true;

$(".info").html("");
$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');

var userName = $("#inp0").val();
var userEmail = $("#inp1").val();
var userDesignation = $("#inpDesignation").val();
var userBranch = $("#inpBranch").val();
var userDOJ = $("#inpDOJ").val();
var userSallery = $("#inp2").val();

if (userName == "") {
	$("#userName-info").html("Please enter the employee name.");
	$("#inp0").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
}

if (userEmail == "") {
	$("#userEmail-info").html("Please enter the email.");
	$("#inp1").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
}
if (!userEmail.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/))
{
	$("#userEmail-info").empty();
	$("#userEmail-info").html("Invalid email address.");
	$("#inp1").css('border', '#e66262 1px solid');
	valid = false;
}

if (userDesignation == "-- Select Designation --") {
	$("#userDesignation-info").html("Please select the designation.");
	$("#inpDesignation").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
}

if (userBranch == "-- Select Branch --") {
	$("#userBranch-info").html("Please select the branch.");
	$("#inpBranch").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
}

if (userDOJ == "") {
	$("#userDOJ-info").html("Please enter the date of joining.");
	$("#inpDOJ").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
}

if (userSallery == "") {
	$("#userSallery-info").html("Please enter the salary.");
	$("#inp2").attr('style', 'border: #e66262 1px solid !important');
	valid = false;
	//return false;
}

return valid;

}

function salaryFunction(){
	var numbers = /^[0-9]+$/;
	var userSallery = $("#inp2").val();
      if(!userSallery.match(numbers))
      {
		$("#userSallery-info").html("Please enter the number.");
		$("#inp2").attr('style', 'border: #e66262 1px solid !important');

	  }
	  else{
		$("#userSallery-info").empty();
		$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');
	  }
}

function confirmationLetterValidation(){
	var valid = true;

	$(".info").html("");
	$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');

	var confirmationDate = $("#confirmationDate").val();

	if (confirmationDate == "") {
		$("#confirmationLetter-info").html("Please enter the confirmation date.");
		$("#confirmationDate").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	return valid;
}

function GenerateExperienceLetter(employeeId) {
   /// joining date
   var joining_date = $('#inp7').val();
   var gender = $('#inp10').val();
   if(gender == 0){
   		toast('Please update the gender!','toast bg-danger',3000);
		return false
   }
   if(joining_date == ""){
	toast('Joining date not found!','toast bg-danger',3000);
	return false
   }
   //alert($reliving_date);

   var relieving_date = $('#inp37').val();
   if(relieving_date == ""){
	toast('Relieving date not found!','toast bg-danger',3000);
	return false
   }

	swal({
		text: "Generate Experience Letter",
		icon: "info",
		buttons: {
		  // cancel: "Cancel",
		  catch: {
			text: "Send It On Mail",
			value: "catch",
		  },
		  defeat: {
			text: "Download Experience Letter",
			value: "defeat",
		  },
		},
	  })
	  .then((value) => {
		switch (value) {
	   
		  case "defeat":
			window.open('TCPDF/examples/experienceLetter.php?id='+employeeId+'&type=download'+'&joining_date='+joining_date+'&relieving_date='+relieving_date);
			break;
	   
		  case "catch":
			window.open('TCPDF/examples/experienceLetter.php?id='+employeeId+'&type=email'+'&joining_date='+joining_date+'&relieving_date='+relieving_date);
			break;
	   
		  default:
			return false;
		}
	  });

  }

function GenerateRelievingLetter(employeeId) {
	/// joining date
	var joining_date = $('#inp7').val();
 
	if(joining_date == ""){
	 toast('Joining date not found!','toast bg-danger',3000);
	 return false
	}
	//alert($reliving_date);
 
	var relieving_date = $('#inp37').val();
	if(relieving_date == ""){
	 toast('Relieving date not found!','toast bg-danger',3000);
	 return false
	}
 
	 swal({
		 text: "Generate Relieving Letter",
		 icon: "info",
		 buttons: {
		   // cancel: "Cancel",
		   catch: {
			 text: "Send It On Mail",
			 value: "catch",
		   },
		   defeat: {
			 text: "Download Relieving Letter",
			 value: "defeat",
		   },
		 },
	   })
	   .then((value) => {
		 switch (value) {
		
		   case "defeat":
			 window.open('TCPDF/examples/relievingLetter.php?id='+employeeId+'&type=download'+'&joining_date='+joining_date+'&relieving_date='+relieving_date);
			 break;
		
		   case "catch":
			 window.open('TCPDF/examples/relievingLetter.php?id='+employeeId+'&type=email'+'&joining_date='+joining_date+'&relieving_date='+relieving_date);
			 break;
		
		   default:
			 return false;
		 }
	   });
 
   }

function appointmentLetterValidation(){
	var valid = true;

	$(".info").html("");
	$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');

	var emp_salary = $("#emp_salary").val();
	var emp_notice_period = $("#emp_notice_period").val();

	if (emp_salary == "") {
		$("#empSalary-info").html("Please enter the salary.");
		$("#emp_salary").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	if (emp_notice_period == "") {
		$("#empNotice-info").html("Please enter the notice period.");
		$("#emp_notice_period").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	return valid;
}

function promotionLetterValidation(){
	var valid = true;

	$(".info").html("");
	$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');

	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();

	if (startDate == "") {
		$("#startDate-info").html("This field is required");
		$("#startDate").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	if (endDate == "") {
		$("#endDate-info").html("This field is required");
		$("#endDate").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	return valid;
}


function incrementLetterValidation(){
	var valid = true;
	
	$(".info").html("");
	$(".input-field").attr('style', 'border: #b0c0d6 1px solid !important');

	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	var emp_salary = $("#emp_salary").val();
	var ctc = $("#inp9").val();

	if (startDate == "") {
		$("#startDate-info").html("This field is required");
		$("#startDate").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	if (endDate == "") {
		$("#endDate-info").html("This field is required");
		$("#endDate").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	if (emp_salary == "") {
		$("#emp_salary-info").html("This field is required");
		$("#emp_salary").attr('style', 'border: #e66262 1px solid !important');
		valid = false;
	}

	if(ctc != ''){
		$('#submitIncrementLetter').prop('disabled', false);
		$('#emailIncrementLetter').prop('disabled', false);
	}
	else{
		$('#submitIncrementLetter').prop('disabled', true);
		$('#emailIncrementLetter').prop('disabled', true);
	}

	return valid;
}

function salaryBreakdownOnIncrementLetter(){
	var emp_salary = $("#emp_salary").val();
	if(emp_salary != ''){
		var ctc = document.getElementById('emp_salary').value;
		var pt = document.getElementById('ptDeduction').value;
		var pf = document.getElementById('pfDeduction').value;
		var pffixed = document.getElementById('defaultPF').value;
		var od = document.getElementById('otherDeduction').value;
		// alert(ctc);alert(pt);alert(pf);alert(pffixed);
		$.ajax({
	        url:'salarycalculation/calculation_sheet.php',
	        method:'post',
	        data:{ctc:ctc,pt:pt,pf:pf,pffixed:pffixed,od:od},
	        success:function(res){
	        	$('#submitIncrementLetter').prop('disabled', false);
				$('#emailIncrementLetter').prop('disabled', false);
		        var data = JSON.parse(res);
	         	// console.log(data);
         		$("#inp7").val(data.basic);
			    $("#inp8").val(data.bonus);
			    $("#inp9").val(data.ctc);
			    $("#inp11").val(data.gross);
			    $("#inp12").val(data.hra);
			    $("#inp13").val(data.netpay);
			    $("#inp14").val(data.otherdeduction);
			    $("#inp15").val(data.pfemployer);
			    $("#inp16").val(data.pfpayable);
			    $("#inp17").val(data.refreshment);
			    $("#inp18").val(data.totaldeductions);
			    $("#inp19").val(data.totalearnings);
			    $("#inp20").val(data.pt);
	    	}
	    })
	}
}
// end all letters







