<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Appraisals Goal History</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Appraisal mode already Exist
</div>   <?php
}
            ?> 
            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>           
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>View Users</th>
<th>S.NO</th>
<th>Appraisal mode</th>            
<th>Period</th>                 
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create date</th>
<th>Action</th> 
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/connection.php';
$query = mysqli_query($con,"select * from appraisals order by id desc");
while($row=mysqli_fetch_array($query)){
?>
<tr>
<?php
if($row['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}
if($row['stage']=='g'){
$stage = 'Goals pending';
}else if($stage=='a'){
$stage = 'Appraisal pending';
}else{
$stage = 'Completed';
}
$appId = $row['id'];
$sql = mysqli_query($con,"select count(*) as eid from appraisal_users where app_id='$appId' and issubmitted='false'");
$result = mysqli_fetch_array($sql);

$countapp = mysqli_query($con,"SELECT COUNT(*) as usercount FROM appraisal_users where issubmitted='false'");
$countresult = mysqli_fetch_array($countapp);

$countappraisal = mysqli_query($con,"SELECT COUNT(*) as appraisalcount FROM appraisalstart WHERE appId = '$appID'");
$countappraisalresult = mysqli_fetch_array($countappraisal);

?>
<td><a style="align-items: center;" href="default.php#&rtl=appraisal/goal-users.php?startrow=0&app_id=<?php echo $row['id'];?>&rtl=formDiv&rtl=tableDiv&rtl=loading"><i class="fa fa-eye" style="margin-left: 20px;color: #3fbb5b"></i></a></td>
<td><?php echo $count++;?></td>
<td><?php echo $row['appraised_mode'];?></td>
<td><?php echo $row['appraised_time'];?><?php echo $row['appraised_year'];?></td>
<td><?php echo $status;?></td>
<td><?php echo $stage?></td>
<td><?php echo $row['employee_due_date'];?></td>
<td><?php echo $row['managers_due_date'];?></td>
<td><?php echo $row['hod_due_date'];?></td>                  
<td><?php echo $row['created_at'];?></td>
<?php 

if(0==0){
    if($countappraisalresult['appraisalcount']==0){
?>

<td><span class="label label-warning" style="display: block;" ><a href="default.php#&rtl=appraisal/appraisal-start.php?apraisal-start=1&appID=<?php echo $row['id']; ?>&rtl=formDiv&rtl=tableDiv&rtl=loading" style="color: white;">  Start Appraisal </span></a></td>     
<?php
} else{
?>
<td><span class="label label-warning" style="display: block;" ><a href="default.php#&rtl=appraisal/appraisal.php?apraisal-start=1&appID=<?php echo $row['id']; ?>&rtl=formDiv&rtl=tableDiv&rtl=loading" style="color: white;">View Appraisal</span></td>
<?php
}
}else{
?>
<td><span class="lable label-warning" style="color: white;border-radius: 9px;font-style: italic;">Goals Pending</span></td>
<?php
}?>

</tr> 
<?php
}
?>                          

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>