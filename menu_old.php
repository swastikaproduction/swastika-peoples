<div style="text-align:center;background:#2F363D;height:50px;font-size:20px;padding-top:10px;color:#fff;height:52px;width:60px;">
<center>
<i class="fa fa-dropbox"></i>
</center>
</div>
<br/>
<div class="menuItem" data-toggle="tooltip" title="Dashboard"  data-placement="right" onclick="getModule('dashboard/index.php','tableDiv','formDiv','loading');">
  <i class="fa fa-home"></i>
</div>

<div class="menuItem"  data-toggle="tooltip" title="Organization"  data-placement="right" lang="0">
<div class="submenuItem">
<table class="subMenuTable">
<tr onclick="getModule('masters/general/index.php?table=designation&dp=Designation','tableDiv','formDiv','loading');"><td><i class="fa fa-id-card"></i></td><td>Designation</td></tr>
<tr onclick="getModule('masters/general/index.php?table=departments&dp=Department','tableDiv','formDiv','loading');"><td><i class="fa fa-graduation-cap"></i></td><td>Departments</td></tr>
<tr  onclick="getModule('masters/shifts/index.php','tableDiv','formDiv','loading');"><td><i class="fa fa-random"></i></td><td>Shifts</td></tr>
<tr  onclick="getModule('masters/general/index.php?table=branch&dp=Location','tableDiv','formDiv','loading');"><td><i class="fa fa-map-marker"></i></td><td>Branches</td></tr>
<tr onclick="getModule('calendar/index.php','tableDiv','formDiv','loading');"><td><i class="fa fa-calendar"></i></td><td>Holiday Calendar</td></tr>
<tr onclick="getModule('notavailable.php','tableDiv','formDiv','loading');"><td><i class="fa fa-envelope"></i></td><td>Circulars</td></tr>
</table>

</div>
  <i class="fa fa-globe" style=""></i>
<br/>
</div>

<div class="menuItem"  data-toggle="tooltip" title="Employees"  data-placement="right" lang="0">
<div class="submenuItem">
<table class="subMenuTable">
<tr onclick="getModule('employee/index.php','tableDiv','formDiv','loading');"><td><i class="fa fa-user"></i></td><td>All Employees</td></tr>
<tr onclick="getModule('employee/index.php?type=left','tableDiv','formDiv','loading');"><td><i class="fa fa-bicycle"></i></td><td>Left Employees</td></tr>
</table>

</div>
  <i class="fa fa-users" style=""></i>
<br/>
</div>


<div class="menuItem"    data-toggle="tooltip" title="Leaves"  data-placement="right">
<div class="submenuItem">

<table class="subMenuTable">
<tr onclick="getModule('leaves/index.php','tableDiv','formDiv','loading');"><td><i class="fa fa-calendar-plus-o"></i></td><td>Leave & Timing Requests</td></tr>
<tr onclick="getModule('leaves/new.php','formDiv','tableDiv','loading');"><td><i class="fa fa-th-list"></i></td><td>New Requests</td></tr>
</table>
</div>

  <i class="fa fa-calendar-check-o"></i>
</div>


<div class="menuItem"  data-toggle="tooltip" title="Attendance"  data-placement="right">
<div class="submenuItem">

<table class="subMenuTable">
<tr onclick="getModule('attendance/index.php','tableDiv','formDiv','loading');"><td><i class="fa fa-th-large"></i></td><td>View Attendance</td></tr>
<tr onclick="getModule('attendance/new.php','tableDiv','formDiv','loading');"><td><i class="fa fa-cloud-upload"></i></td><td>Upload Attendance</td></tr>
</table>
</div>
  <i class="fa fa-th-list"></i>
</div>




<div class="menuItem"  data-toggle="tooltip" title="Salary"  data-placement="right">
<div class="submenuItem">

<table class="subMenuTable">
<tr onclick="getModule('attendance/index.php?type=salary','tableDiv','formDiv','loading');"><td><i class="fa fa-check-square-o"></i></td><td>View Generated Salaries</td></tr>
<tr onclick="getModule('masters/general/index.php?table=payheads&dp=Payhead','tableDiv','formDiv','loading');"><td><i class="fa fa-gear"></i></td><td>Salary Payheads</td></tr>
</table>
</div>
  <i class="fa fa-rupee"></i>
</div>




<div class="menuItem"  data-toggle="tooltip" title="Reporting"  data-placement="right" onclick="getModule('notavailable.php','tableDiv','formDiv','loading');">
  <i class="fa fa-area-chart"></i>
</div>

<div class="menuItem"  data-toggle="tooltip" title="Logout"  data-placement="right" onclick="window.location = 'logout.php'">
  <i class="fa fa-sign-out"></i>
</div>








