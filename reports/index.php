<?php
include("../include/config.php");
?>
<div class="moduleHead">
<div class="moduleHeading">
Reports
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto;padding:20px;">
<div class="row">
	<div class="col-sm-3">
	<ul class="list-group">
  <li class="list-group-item text-primary" onclick="window.open('reports/employee-master.php?type=0','_blank')">Active Employee Master Sheet</li>
  <li class="list-group-item text-primary" onclick="window.open('reports/employee-master.php?type=1','_blank')">Left Employee Master Sheet</li>
  <li class="list-group-item text-primary" onclick="window.open('reports/employee-master.php','_blank')">All Employees Master Sheet</li>
  <li class="list-group-item text-primary" onclick="getModule('reports/attendance.php','formDiv','tableDiv','loading')">Attendance Report</li>
  <li class="list-group-item text-primary" onclick="getModule('reports/misspunch.php','formDiv','tableDiv','loading')">Miss punch Report</li>
  <li class="list-group-item text-primary" onclick="window.open('reports/document.php?type=1','_blank')">Document Report</li>

</ul>
	</div>
</div>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>