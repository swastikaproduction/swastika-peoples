<?php
$getData = mysqli_query($con,"SELECT * FROM `circulars` ORDER BY `id` DESC LIMIT 5");

if(mysqli_num_rows($getData) == '0')
{
?>
<center>
	<br/><br/><br/><br/>
	<i class="fa fa-coffee" style="font-size:40px;color:#ccc"></i>
	<br/>
	<br/>
	<span style="color:#ccc">There are no circulars yet on this system.<br/>
	Sit back and relax.</span>
</center>
<?php
}
else
{
?>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover" cellpadding="0" cellspacing="0">
<?php
$i=1;

while($row = mysqli_fetch_array($getData))
{
	?>
<tr>
		<td style="padding-right:10px;text-align:center;width:60px;font-size:20px;">
		<br/>
		<i class="fa fa-globe" style="color:#ef5350"></i>
		</td>

	<td style="padding:20px;">
<span style="font-size:16px;font-weight:400;text-transform:capitalize">
	<?php echo $row['title'];?>
	</span>
	<br/>
	<span style="font-size:10px;">
		<?php echo date("d-M-y h:i A",strtotime($row['createdate']));?>
	</span>
		
	</td>
	<td>
	<br/>
		<span class="label label-primary" onclick="getModal('circulars/preview.php?id=<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')">
			<i class="fa fa-external-link"></i>&nbsp;&nbsp;PREVIEW
		</span>

	</td>
	<td></td>
</tr>

	<?php
	$i++;
}
?>
</table>
</div>
<div class="calStrip"  style="padding:15px 10px;text-align:right" onclick="getModule('circulars/index.php','tableDiv','formDiv','loading');">VIEW ALL&nbsp;&nbsp;&nbsp;
<i class="fa fa-arrow-right"></i>
</div>
<?php
}
?>