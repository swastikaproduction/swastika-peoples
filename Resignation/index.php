<?php include'../include/config.php';?>
<style type="text/css">
	label{

	  color: #418ce8;
	}
	span{
		background: gray;
	}
</style>
<h5>Initiate Resignation</h5>
<section style="background: #fff">
	
	<p style="color: green;margin-left: 15px;">Employee Detailes</p>
	<?php
        
        $query = mysqli_query($con,"select e.name as empName,e.doj,ee.name as reportingperson,d.name as departmentName,dg.name as desigName FROM employee e INNER JOIN employee as ee ON ee.id = e.imdboss INNER JOIN departments as d ON d.id = e.department INNER JOIN designation as dg ON dg.id = e.designation where  e.id='$loggeduserid'");
        $result = mysqli_fetch_array($query);
		?>
	<div class="row">		
		<div class="col-md-3">
			<label>Name</label>
			<input type="hidden" name="userId" value="<?php echo $loggeduserid;?>" id="userId">
			<input type="hidden" name="doj" value="<?php echo $result['doj'];?>" id="doj">
			<span class="form-control"><?php echo $result['empName'];?></span>
		</div>
		<div class="col-md-3">
			<label>DOJ:</label>
			<span class="form-control"><?php echo $result['doj'];?></span>
		</div>
		<div class="col-md-3">
			<label>Designation:</label>
			<span class="form-control"><?php echo $result['desigName'];?></span>
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-3">
			<label>Department:</label>
			<span class="form-control"><?php echo $result['departmentName'];?></span>
		</div>
		<div class="col-md-3">
			<label>Location:</label>
			<span class="form-control"><?php echo $result['doj'];?></span>
		</div>
		<div class="col-md-3">
			<label>Reporting:</label>
			<span class="form-control"><?php echo $result['reportingperson'];?></span>
		</div>
	</div>

</section>
<section style="background: #fff;">
	<div class="row">
		<div class="col-md-3">
			<label>Start Date</label>
			<input type="date" name="startDate" class="form-control" onchange="CalculateDays();" id="startDate">
		</div>
		<div class="col-md-3">
			<label>Date of Leaving</label>
			<input type="date" class="form-control" name="DateofLeaving" id="DateofLeaving">
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<label>Reason for Leaving</label>
			<input type="text" name="reasonLeaving" class="form-control" id="reason" placeholder="Reason for Leaving">
		</div>
		<div class="col-md-3">
			<label>Notice Period</label>
			<input type="text" name="reasonLeaving" class="form-control" id="noticePeriod" placeholder="Notice Period" readonly="">
		</div>
		<div class="col-md-3">
			<label>Short days Period</label>
			<span class="form-control">30</span>
		</div>
	</div>

	<div class="row" >
		<div class="col-md-12">
			<input type="submit" class="btn btn-success" name="Submit" onclick="ResignatioProcess();" style="margin-top: 15px;">
		</div>
	</div>
</section>
