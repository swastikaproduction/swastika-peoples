<?php
include("../apiconfig.php");
?>
<div class="backDropCustom" id="backDrop" style="background:#fff !important;">

<div class="menuSpacer"></div>
<div class="contentCustom" id="content" style="background:transparent !important">

<div style="position:relative;padding:0px;color:#2dd393">
<div class="subHeader">
	<i class="fa fa-dashboard"></i>&nbsp;&nbsp;
	HOME

</div>
<center>



<div class="smallheading">
<i class="fa fa-credit-card"></i>&nbsp;&nbsp;
	MY WALLET
</div>

<table class="table" style="width:100%;">
	<tr>
		<td style="text-align:center;padding:30px 20px">
					<i class="fa fa-rupee" style="color:#23B9AF;font-size:14px;"></i>
			<br/>
			<span style="color:#1e2538;font-size:16px;">3784598.45</span>
			<br/>
			<span style="font-size:10px;">INR BALANCE</span>

		</td>

	<td style="text-align:center;padding:30px 20px">
					<i class="fa fa-btc" style="color:#23B9AF;font-size:14px;"></i>
			<br/>
			<span style="color:#1e2538;font-size:16px;">5.21</span>
			<br/>
			<span style="font-size:10px;">BTC BALANCE</span>
	
		</td>
	</tr>
	<tr>
		<td style="padding:20px;border-top:0px" colspan="2">
			<button class="btn btn-sm btn-success" style="width:100%">
<i class="fa fa-plus"></i>
			ADD MONEY</button>
			<br/><br/>
			<button class="btn btn-sm btn-default" style="width:100%">
<i class="fa fa-download"></i>
			WITHDRAW MONEY</button>
		</td>
	</tr>
</table>



</center>

<br/>
<div class="smallheading">
<i class="fa fa-bell"></i>&nbsp;&nbsp;
	RECENT NOTIFICATIONS
</div>
<table class="notifDocks">
	<tr>
		<td>
		<i class="fa fa-file diary"></i>
		<br/>
		
		<span style="font-size:11px;">17th JAN</span>
		</td>
		<td>
		<div class="dot"></div>
		Amit has a new entry in his diary.
		<br/>
		<span class="timeLine">12:30 PM</span>
		</td>
	</tr>
	<tr>
		<td>
		<i class="fa fa-file diary"></i>
		<br/>
		
		<span style="font-size:11px;">17th JAN</span>

		</td>
		<td>
		<div class="dot"></div>
		Anvi has a new diary entry.
		<br/>
		<span class="timeLine">12:30 PM</span>
		</td>
	</tr>
	<tr>
		<td>
		<i class="fa fa-map-marker marker"></i>
		
		<br/>
		<span style="font-size:11px;">17th JAN</span>

		</td>
		<td>
		<div class="dot"></div>
		Amit's Bus has just reached previous stop.
				<br/>
		<span class="timeLine">12:30 PM</span>

		</td>
	</tr>
	<tr>
		<td>
		<i class="fa fa-check remark"></i>
		
		<br/>
		<span style="font-size:11px;">17th JAN</span>

		</td>
		<td>
		<div class="dot"></div>
		Anvi has a new remark entry.
				<br/>
		<span class="timeLine">12:30 PM</span>

		</td>
	</tr>
	<tr>
		<td>
		<i class="fa fa-map-marker marker"></i>
		
		<br/>
		<span style="font-size:11px;">17th JAN</span>

		</td>
		<td>
		<div class="dot"></div>
Anvi's Bus has just reached school.
		<br/>
		<span class="timeLine">12:30 PM</span>

		</td>
	</tr>
</table>


<br/><br/><br/><br/>
<br/><br/><br/><br/>
</div>








</div>


</div>


