<?php 
include("../../include/config.php");
include("curreny_format.php");
error_reporting(-1);
   if($_POST['submit']=='GENERATE OFFER LETTER'){ 
    $name=ucwords(strtolower($_POST['name']));    
    $email = $_POST['email'];
    $branch = $_POST['branch'];
    $designation = $_POST['designation'];
    $doj = date('d/m/Y',strtotime($_POST['doj']));
    $basic = $_POST['basic'];
    $bonus = $_POST['bonus'];
    $ctc = $_POST['ctc'];
    $pt= $_POST['pt'];
    $annualctc = $_POST['ctc']*12;
    $ctcinwords = currenyformatting($annualctc);
    $da = $_POST['da'];
    $grosssalary = $_POST['grosssalary'];
    $hra = $_POST['hra'];
    $netpay = $_POST['netpay'];
    $otherdeduction = $_POST['otherdeduction'];
    $pfemployer = $_POST['pfemployer'];
    $pfpayable = $_POST['pfpayable'];
    $refreshment = $_POST['refreshment'];
    $totaldeduction = $_POST['totaldeduction'];
    $totalearnings = $_POST['totalearnings'];
?>
<!DOCTYPE html>
<html>
<head>
<title>
Offer Letter
</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>

<style type="text/css">
body
{
    overflow: auto !important;
}
    .inTab th
    {
        background:#999;
        color: #fff;
        font-weight: normal;
        text-align: left;
    }

    .inTab tr th:nth-child(2),.inTab tr th:nth-child(4)
    {
        text-align: right;
    }

    .inTab tr td:nth-child(1), .inTab tr td:nth-child(3)
    {
        font-weight: normal;
    }

    .inTab tr td:nth-child(2)
    {
        border-right:1px #eee solid;
        text-align: right;
    }

    .inTab tr td:nth-child(4)
    {
        text-align: right;
    }
    .inTab td
    {
        border-bottom:1px #eee solid; 
    }
</style>

<script type="text/javascript">
    
    function printDoc()
    {
        document.getElementById('ptBox').style.display = 'none';
        window.print();
        setTimeout(function(){
            document.getElementById('ptBox').style.display = 'block';
        },300);
    }
</script>
</head>
<body>
<div style="position:fixed;top:20px;right:20px;" id="ptBox">
<button style="background:rgba(0,0,0,0.5);color:#fff;border:0px;border-radius:2px;" onclick="printDoc();">PRINT</button>
</div>


<center>

    <div style="width:900px;padding:40px;">
        <table cellpadding="10" cellspacing="0" style="width:100%;border:1px #eee solid;">
        <tr>
            <td colspan="2" align="center" style="border-bottom:1px #eee solid;font-size:20px;">
                <strong>OFFER LETTER</strong>
            </td>
        </tr>
            <tr>
                <td style="width:50%;text-align:left;border-right:1px #eee solid;">
                    <table style="width:100%" cellpadding="10" cellspacing="0" class="inTab">
                        <tr>
                          <td style="border-bottom:none;">
                          <p>To,</p>    
                          <p>Ms/Mr <?php echo $name; ?></p> 
                          <p>Address: Jabalpur, 482001</p>  
                          <p><b>Date: <?php echo date('d/m/Y',strtotime($date)); ?></b></p> 
                          </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align:center">
                    <img src="https://www.swastika.co.in/public/webtheme/images/swastika-logo.svg" style="height:40px;"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:0px;border-top:1px #eee solid">
                <br/><br/>
                    <table style="width:100%;" cellpadding="10" cellspacing="0" class="inTab">
                        <tr>
                        <td colspan="4" style="width:50%;text-align:left;border-right:1px #eee solid;">
                        <p><b>Dear Mr./Ms. <?php echo $name; ?>,</b></p>
                        <p>
                        With reference to your application and subsequent discussions you had with us, we are pleased to offer you an appointment as <b><?php echo $designation ?></b> at <b><?php echo $branch; ?></b>.For the above mentioned position you will be entitled for monthly CTC of <b>Rs <?php echo ($annualctc/100000) ?> LPA,</b> in words, (<?php echo $ctcinwords; ?>) Statutory deductions will be as applicable. For detailed break up refer <b>annexure A</b></p>

                        <p>You are required to report on duty on or before <b><?php echo $doj; ?></b> at <b><?php echo $branch ?></b> Office of failing which this offer will stand automatically cancelled.Please note that this is merely an Offer Letter. The Company’s standard Appointment letter containing general terms and conditions of employment will be issued to you on your joining the Company which shall be binding on you.</p>

                        <p>This letter of offer is based on the information furnished in your application for employment and during the interviews you had with us. If, at any time in future, it comes to light that any of this information is incorrect or any irrelevant information or has been pending, then your employment is liable to be terminated with immediate effect.</p>  

                        <p>We look forward for a long and mutually beneficial association with <b>Swastika Investmart Limited</b>.</p>

                        <p>Please share below mentioned documents in HR & provide your acceptance within 24 hrs. from the date of issue .</p>

                        <p><b><u>Documents (Scan or physically) to be submitted at the time of Offer:</u></b></p>

                        <p>
                         <ol>
                         <li>   Pan Card</li>
                         <li>   Aadhar Card (must contain date, month and year) Format should be DD/MM/YY</li>
                         <li>   Bank Details:-Personalized cancelled cheque / Bank statement/ Front page of Passbook (Must contain clear Account Holder Name/ IFSC Code/ Account No.)</li>
                         <li>   Current Address Proof (If the current address is different from Permanent address)</li>
                         <li>   Past Employment Details (Resignation Letter & Relieving/Experience Letter)</li>
                         <li>   UAN details if already member of PF</li>
                         <li>   Form 11</li>
                         <li>   Blood Group(No document Required)</li>
                         <li>   Undertaking of Non-submission of documents(If candidate is unable to submit any above of the document )</li>
                         </ol>  
                        </p>
                        </td>   
                        </tr>
                        <tr class="">
                        <td colspan="4" align="center" style="font-size:16px;">
                        <strong>ANNEXURE (A)</strong>
                        </td>
                        </tr> 
                        <tr>
                        <td colspan="4" align="center"><p>As a result of the discussion of your employment, your remuneration details stands described below to:</p></td>   
                        </tr>
                        <tr>
                            <th style="width:40%;text-align: center">COMPENSATION HEADS</th>
                            <th style="width:30%;text-align: center">PER MONTH</th>
                            <th style="width:30%;text-align: center">PER ANNUM</th>
                        </tr>
                        <tr>
                        <td style="font-weight:bold">Basic</td>
                        <td><?php echo number_format($basic,0); ?></td> 
                        <td><?php echo number_format(($basic*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">House Rent Allowance (HRA) </td>
                        <td><?php echo number_format($hra,0); ?></td> 
                        <td><?php echo number_format(($hra*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Bonus </td>
                        <td><?php echo number_format($bonus,0); ?></td> 
                        <td><?php echo number_format(($bonus*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Employee Refreshment </td>
                        <td><?php echo number_format($refreshment,0); ?></td> 
                        <td><?php echo number_format(($refreshment*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Cost to Company</td>
                        <td><?php echo number_format($ctc,0); ?></td> 
                        <td><?php echo number_format(($ctc*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">PF Contribution</td>
                        <td><?php echo number_format($pfpayable,0); ?></td> 
                        <td><?php echo number_format(($pfpayable*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Training Impartment Contribution</td>
                        <td><?php echo number_format($otherdeduction,0); ?></td> 
                        <td><?php echo number_format(($otherdeduction*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Professional Tax Payable</td>
                        <td><?php echo number_format($pt,0); ?></td> 
                        <td><?php echo number_format(($pt*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td style="font-weight:bold">Net Pay</td>
                        <td><?php echo number_format($netpay,0); ?></td> 
                        <td><?php echo number_format(($netpay*12),0); ?></td> 
                        </tr>  

                        <tr>
                        <td colspan="4">
                        <p><b>For Swastika Investmart Ltd</b></p>
                        <br/><br/>
                        <p><span style="font-size:10px;font-weight: bold;">Authorized Signatory</span></p>
                        <br/><br/>
                        <br/><br/>
                        <br/><br/>
 
                        <p>I have fully understood above & hereby accept the same.</p>    
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>
        
            <tr>
                <td style="font-weight:bold">
                <p>Signature: _____________</p>
                <p>Name: ______________ </p>        
                <p>Date: _____________</p>
                </td>
                <td style="text-align:left;border-top:1px #eee solid;">
                <strong style="font-size:16px;font-weight:bold">Swastika Investmart Ltd.</strong>
                <br/>
                <strong style="font-size:13px;font-weight:bold">
                Head Office</strong>
                <br/>
                <i class="fa fa-map-marker"></i>&nbsp;&nbsp;48, Jaora Compound, M.Y.H. Road, Indore - 452 001<br/>
                <i class="fa fa-phone"></i>&nbsp;&nbsp;0731 - 6644000, <i class="fa fa-globe"></i>&nbsp;&nbsp;www.swastika.co.in<br/>
                </td>
            </tr>
        </table>
    </div>
</center>
</body>
</html>
<?php } else {
echo 'NOTHINGFOUNDHERE';
} ?>