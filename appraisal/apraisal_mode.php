<style type="text/css">
	
	.label{
		font-weight: 700; */
	}
</style>
  <style>

.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
        </style>
         <?php 
            include'../include/config.php';
            $query = mysqli_query($con,"select distinct(name),id from departments where name !='None' order by name asc");
          ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background:#fff">
    <!-- Content Header (Page header) -->
   <?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Appraisal mode already Exist
</div>   <?php
}
            ?> 
               <?php
            if ( isset($_GET['success']) && $_GET['success'] == 2 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Start Successfully..!
</div>   <?php
}
            ?> 
 
    <!-- Main content -->
    <section class="content" style="margin-top: 30px;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Appraisals</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="appraisal/appraisal_save.php" role="form" method="post">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputEmail1" style="/*font-weight: 700; */" >Appraisal Mode</label>
                  <select class="form-control" name="appraised_mode" id="appraised_mode" onchange="appraised();" required="">
                    <option value=''>select appraisal mode</option>
                  
                     <option value="quarterly">Quarterly</option>
                    <option value="halfyearly">Half-Yearly</option>
                      <option value="yearly">Yearly</option>
                   
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputEmail1">Year</label>
                  <select class="form-control" name="year" id="yearly" required="">
                    <option value="">select year</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                
                  </select>
                </div>
              </div>
                <div class="col-md-4" id="quater_period">
                <div class="form-group">
                  <label for="exampleInputEmail1">Period</label>
  
                  <select class="form-control" name="period" id="quater">
                   
                   

                  </select>
                </div>         

              </div>         
            
            </div>
            
            <div class="row">
              <?php $d = date('Y-m-d'); ?>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Employees Due Date</label>

                  <input type="date" name="employee_due_date" class="form-control" id="datepicker2" min="<?php echo $d;?>"  required="" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Manager's Due Date</label>
                  <input type="date" name="managers_due_date" class="form-control" id="datepicker1" min="<?php echo $d;?>" required="">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>HOD's Due Date</label>
                  <input type="date" name="hod_due_date" class="form-control" id="datepicker" min="<?php echo $d;?>"  required="">
                </div>
              </div>
            </div>            
                  
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <div class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
       <label>Department List</label>
      <select class="form-control" >
        <option>Select an option</option>
      </select>
      <div class="overSelect"></div>
    </div>
    <div id="checkboxes">
       <label for="one">
        &nbsp;&nbsp;&nbsp;<input class='deptcheck' type="checkbox" id="department_list" value="-1">All </label>
         <?php 
            while($row = mysqli_fetch_array($query)){
         ?>
        <label for="one">
        &nbsp;&nbsp;&nbsp;<input class='deptcheck' type="checkbox" id="department_list" value="<?php echo $row['id'];?>"><?php echo$row['name'] ?>
      </label>
    <?php }?>
      
    </div>
              </div>
                </div>
              </div>
              <!-- <div class="col-md-2">
                <div class="form-group">
                  <label></label>
                  <p class="btn btn-primary" onclick="department();" style="margin-top: 56px;">Get</p>
                </div>
              </div> -->
              <div class="col-md-3" id="emp" >
                <div class="form-group">
                  <label>Employee List</label>
                  <select id="employee" class="form-control" name="employee_list[]" multiple="yes"></select>
                </div>
               
          
            </div>   
              </div>
            
 
  </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" >Start Goal Setting</button>
               <span class="btn btn-success" onclick="getModule('appraisal/appraisal_view.php?appraisal=1','formDiv','tableDiv','loading');">Back</span>
              </div>
            </form>
          </div>           
                
                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
 