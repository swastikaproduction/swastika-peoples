

    <!-- Main content -->
    <section class="content" style="background: #fff;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
        
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Profile KRA/KPI <a  onclick="getModule('appraisal/profile_kpi.php?profile_kpi_view=1','formDiv','tableDiv','loading');" class="btn btn-success" style="float: right;">Create New</a></h3>
            </div>
            
            <!-- /.box-header -->
            <div class="box-body" style="margin-bottom: 200px;">
              <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>S.No</th>
                  <th>Subject</th>                 
                  <th>Target</th>                 
                  <th>Weightage</th>
                  <th>Description</th>                  
                  <th>Action</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                	<?php  
                       $count = 1;
                       include'../include/config.php';
                       $profileId = $_GET['profileId'];
                       $query = mysqli_query($con,"select * from  profile_kpis where profile='$profileId'");
                       while($row=mysqli_fetch_array($query)){
                       	?>
                <tr>
                  <td><?php echo $count++;?></td>
                  <td><?php echo $row['subject'];?></td>
                  <td><?php echo $row['target'];?> </td>
                  <td><?php echo $row['weightage'];?></td>
                  <td><?php echo $row['description'];?></td>
                   
                  <td><a href="" data-id="<?php echo $row['id'];?>" id="<?php echo $row['id'];?>" data-toggle="modal" data-target="#myModal13" onclick="profilekpi_id('<?php echo $row['id'];?>');">Edit</a></td>                 
                  <td><a href="" data-id="<?php echo $row['id'];?>" id="<?php echo $row['id'];?>" data-toggle="modal" data-target="#myModal14" onclick="getkpiid('<?php echo $row['id'];?>');">Assign</a></td>                 
                
                </tr> 
                	<?php
                       }
                	?>                          
                               <div class="modal fade" id="myModal13" role="dialog">
    <div class="modal-dialog" style="margin-top: 150px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Profile kpi Update </h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <form action="appraisal/profile_name_update.php" method="post">
            <div class="col-md-4">
              
              <label>Subject</label>
              <input type="hidden" name="kpid" id="kpid">
              <input type="text" name="subject" class="form-control" id="subject" placeholder="enter subject" required="">
            </div>
            <div class="col-md-4">
              <label>Target</label>
            
              <input type="text" name="target" class="form-control" id="trgt" placeholder="enter target" required="">
            </div>
             <div class="col-md-4">
              <label>Weightage</label>
            
              <input type="text" name="weightage" class="form-control allownumeric" id="weightage" placeholder="enter weightage" required="" maxlength="3">
            </div>
          </div>
          <div class="row">
            <form action="appraisal/profile_name_update.php" method="post">
            <div class="col-md-4">
              
              <label>Description</label>
              <input type="hidden" name="sid" id="sid">
              <input type="text" name="description" class="form-control" id="description" placeholder="enter description" required="">
            </div>
            <div class="col-md-4">
              <label>Profile</label>
            
              <input type="text" name="profile" class="form-control" id="profile" placeholder="enter profile" required="">
            </div>
             <div class="col-md-4">
              
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="profilekpi_update();">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>

  <div class="modal fade" id="myModal14" role="dialog">
    <div class="modal-dialog" style="margin-top: 150px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign kpi/kra to Employee</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <form action="appraisal/profile_name_update.php" method="post">
            <div class="col-md-6">
              
              <label>Employee List</label>
              <input type="hidden" name="assignkpid" id="assignkpid">
              <select name="employeeid" class="form-control" id="employeeid">
                <option>please select employee name</option>
                <?php 
                $query = mysqli_query($con,"select * from employee"); 
                while($result = mysqli_fetch_array($query)){
                  ?>
                  <option value="<?php echo $result['id']; ?>"> <?php echo $result['name']; ?></option>
                  <?php
                }

                ?>
              </select>
            </div>
            
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="assign();">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>
                </tbody>
               <!--  <tfoot>
                <tr>
                <th>Appraised mode</th>
                  <th>Year</th>
                  <th>Half year</th>
                  <th>Quater</th>
                  <th>Create date</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>
</div></section>
   
</div>
<script type="text/javascript">
  function send_reminder(){

    alert('Reminder send successfully..!');
  }
</script>