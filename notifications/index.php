<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `notifications` WHERE (`userid` = '$loggeduserid' OR `userid` = '0') ORDER BY `id` DESC") or die(mysqli_error($con));
$colorBox = Array();
$colorBox['fa fa-calendar'] = '#1976d2';
$colorBox['fa fa-globe'] = '#ef5350';
$colorBox['fa fa-calendar-check-o'] = '#22cabf';
$colorBox['fa fa-rupee'] = '#fbd696';
$colorBox['fa fa-file-pdf-o'] = '#6649ff';

?>
<div class="moduleHead">
<br/>

<div class="moduleHeading">
Notifications
</div>
</div>
<div class="tabelContainer shadow" style="height:auto;border-radius:5px;background:#fff">
<table style="width:100%" class="table notifTop">
	<tr>
		<td class="selected" id="calendartd" onclick="rotateClass('calendar')">Calendar</td>
		<td id="leavestd"  onclick="rotateClass('leaves')">Leaves</td>
		<td id="circularstd"  onclick="rotateClass('circulars')">Circulars</td>
		<td  id="salarytd" onclick="rotateClass('salary')">Salary</td>
		<td  id="salarysliptd" onclick="rotateClass('salaryslip')">Salaryslip</td>
	</tr>
</table>
<?php

if(mysqli_num_rows($getData) == '0')
{
?>
<center>
	<br/><br/><br/><br/>
	<i class="fa fa-coffee" style="font-size:40px;color:#ccc"></i>
	<br/>
	<br/>
	<span style="color:#ccc">There are no notifications for you yet.<br/>
	Sit back and relax.</span>
		<br/><br/><br/><br/>
</center>
<?php
}
else
{

$firstId = '0';
while($row= mysqli_fetch_array($getData))
{
	if($firstId == '0')
	{
		$firstId = $row['id'];
		mysqli_query($con,"UPDATE `employee` SET `notifcount` = '$firstId' WHERE `id` = '$loggeduserid'") or die(mysqli_error($con));
	}
	if($row['module'] == 'calendar' || $row['module'] == 'leaves')
	{
		$onclick = "getModule('".$row['url']."','tableDiv','formDiv','loading')";
	}
	if($row['module'] == 'circulars')
	{
		$onclick = "getModal('".$row['url']."','tableModalBig','formModalBig','loading')";
	}
	if($row['module'] == 'salary')
	{
		$onclick = "window.open('".$row['url']."','_blank')";
	}
	if($row['module'] == 'salaryslip')
	{
		$onclick = "getModal('".$row['url']."','tableModalBig','formModalBig','loading')";
	}



?>
<div class="calStrip <?php echo $row['module'];?>class"  style="padding:15px 10px;<?php if($row['module'] != 'calendar') echo 'display:none';?>" onclick="<?php echo $onclick;?>">

<table style="width:100%">
	<tr>
		<td style="padding-right:10px;text-align:center;width:60px;font-size:20px;color:">
		<i class="<?php echo $row['faclass'];?>" style="color:<?php echo $colorBox[$row['faclass']];?>"></i>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
	<span class="label label-default">
			<?php echo date("d M, h:i:A",strtotime($row['createdate']));?> 
			</span>
	</div>	
	<span style="font-size:16px;font-weight:400;text-transform:capitalize">
		<?php 
		

echo $row['module'];
		?>
	</span>
	<br/>
<div style="margin-top:5px">
<?php 
echo $row['text'];
?>
</div>
		</td>
	</tr>
</table>


</div>
<?php	
}
}
?>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>