<?php
include("../include/config.php");
$month = $_GET['month'];
$year = $_GET['year'];

$ddate = $year."-".$month."-01";

$tableName = "attendance_".$month."_".$year;
?>
<div style="padding:20px;">

<div class="row">
	<div class='col-sm-6'>
		<span class="subHead">
			Missing Attendance
		</span>
		<br/>
		<br/>
		<br/>

		<table class="table table-bordered table-hover">
		<tr>
			<th>Name</th>
			<th>Employee Code</th>
			<th>Action</th>
		</tr>
		<?php
		$getData = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` NOT IN (SELECT `empid` FROM `attendancelog` WHERE `month`= '$month' AND `year` = '$year') AND `left` = '0'") or die(myqli_error($con));
		while($row = mysqli_fetch_array($getData))
		{
			?>
			<tr>
				<td><?php echo $row['name'];?></td>
				<td><?php echo $row['empid'];?></td>
				<td>
					<button class="btn btn-sm btn-success" onclick="getModule('attendance/manualupload.php?empid=<?php echo $row['id'];?>&month=<?php echo $month;?>&year=<?php echo $year;?>&name=<?php echo $row['name'];?>&code=<?php echo $row['empid'];?>','bottomDiv','','loading');$('#myModalBig').modal('hide')">
						UPLOAD NOW
					</button>
				</td>
			</tr>

			<?php
		}
		?>	
		</table>
	</div>
	<div class="col-sm-6">
				<span class="subHead">
			Double Attendance
		</span>
		<br/>
		<br/>
		<br/>

		<table class="table table-bordered table-hover">
		<tr>
			<th>Name</th>
			<th>Employee Code</th>
		</tr>
		<?php
		$repedata = Array();
		$getData = mysqli_query($con,"SELECT empid, COUNT(*) c FROM `$tableName` GROUP BY date HAVING c > 1");
		while($row = mysqli_fetch_array($getData))
		{

			$repdata[] .= $row['empid'];
		}
		$repdata = array_unique($repdata);
$repdata= implode(",",$repdata);

			$getData = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` IN (".$repdata.")") or die(myqli_error($con));
			while($row = mysqli_fetch_array($getData))
		{


			?>
<tr>
					<td><?php echo $row['name'];?></td>
				<td><?php echo $row['empid'];?></td>

</tr>
			<?php
		}
		?>

		</table>

	</div>
</div>

</div>