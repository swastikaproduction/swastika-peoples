<?php 
//DEFAULT VARIABLES
$grosssalary = $_POST['ctc'];
$ptflag = $_POST['pt'];
$pfflag = $_POST['pf'];
$odflag = $_POST['od'];
$pfpayable = $_POST['pffixed'];
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$calculatedsalary = $grosssalary;

if($grosssalary  <= 18750){
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000){
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333){
	$pt = 167;
}
else {  
	$pt = 208;
}	

if($ptflag==1){
	$pt = $pt;
}else{
	$pt = 0;
}



if($pfflag == 1)
{
		if($calculatedsalary < 9001)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$pfpayable = round($gross * 0.24);		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
            $otherdeduction=0;
			$pfemployer = round($pfpayable/2);	
			$basic = $ctc - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}	

		else if($calculatedsalary >= 9001 && $calculatedsalary < 15000)
		{    
			
			$ctc = $calculatedsalary;
			$basic = 8400;
			$hra = 0;
			$otherdeduction=0;
			
			if($pfpayable == '')
			{
			    $pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}		
			$pfemployer = round($pfpayable/2);			
			$gross = $ctc-$pfemployer;
			$bonus = $ctc - ($basic+$pfemployer);
			$refreshment=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary >= 15000 && $calculatedsalary <= 20000)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = max(round($gross * 0.60),8400);
			$hra = round($basic*0.40);
			$otherdeduction=0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = round($ctc*0.10);
		    $refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary >= 20001 && $calculatedsalary <= 26800)
		{   $ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($calculatedsalary * 0.10);
			//other deduction condition when flag is 1
			if($odflag==1){
              $otherdeduction = round($calculatedsalary*0.025);
			}else{
              $otherdeduction=0;
			}
			
			$pfpayable = round($basic * 0.24);
		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}

		else if($calculatedsalary >26800)
		{   $ctc = $calculatedsalary;
			if($pfpayable == '')
			{
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.10);
			$pfpayable = round($basic * 0.24);
			}
			else
			{
            $gross = $ctc;
			$basic = round($ctc * 0.52);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.12);
			}
			//other deduction condition when flag is 1
			if($odflag==1){
              $otherdeduction = round($ctc*0.025);
			}else{
              $otherdeduction=0;
			}		
			
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9400)
	{
		    $ctc = $calculatedsalary;	
		    $gross = $ctc;	    
			$basic = min($ctc,8400);
			$hra = 0;
			$da = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - $basic;
			$refreshment=0;
			$otherdeduction=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;

	}else if($calculatedsalary >= 9400 && $calculatedsalary <= 15000)
	{       $ctc = $calculatedsalary;
		    $gross = $ctc;
			$basic = max(round($ctc*0.60),8400);
			$pfpayable = 0;
			$pfemployer = 0;
			$refreshment=0;
			$da=0;
			$otherdeduction=0;
			$bonus = round($ctc * 0.10);
			$hra = $ctc - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000 &&  $calculatedsalary <=20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($ctc * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			$otherdeduction=0;
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}
	else if( $calculatedsalary >20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($calculatedsalary * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			//other deduction condition when flag is 1
			if($odflag==1){
            $otherdeduction= round($calculatedsalary*0.025);
			}else{
            $otherdeduction=0;
			}
			
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra+$otherdeduction);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns.
			$netpay = $totalearnings - $totaldeductions;
	}
}

$result  = array(
'ctc' => $ctc,
'gross' => $gross,
'basic' => $basic,
'pfpayable' => $pfpayable,
'da' => $da,
'pfemployer' => $pfemployer,
'otherdeduction' => $otherdeduction,
'bonus' => $bonus,
'hra' => $hra,
'pt' => $pt,
'refreshment' => $refreshment,
'totalearnings' => $totalearnings,
'totaldeductions' => $totaldeductions,
'netpay' => $netpay 
);
echo json_encode($result);
?>