<?php
include("../../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
?>


<div class="moduleHead">
<br/>

	<div class="moduleHeading" onclick="toogleFormTable();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Shifts

	</div>
</div>






<div class="shadow" style="background:#fff;">


<div class="col-sm-12 subHead"> Add New Shift</div>
<div class="row">
	<div class="col-sm-2 formLeft">
		Name
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="Name" id="inp0" class="inputBox">
	</div>	

</div>


<div class="row">
	<div class="col-sm-2 formLeft">
		From Time
	</div>
	<div class="col-sm-4 formRight">
		<input type="time" name="req" title="Name" id="inp1" class="inputBox">
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		To Time
	</div>
	<div class="col-sm-4 formRight">
		<input type="time" name="req" title="Name" id="inp2" class="inputBox">
	</div>


</div>





<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp3" style="width:100%;height:150px;"></textarea>
	</div>	


</div>




<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('<?php echo $saveurl;?>','','','inp',4,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			<br/><br/><br/>
	</div>	


</div>





</div>



<br />
<br />
<br />
<br />
<br />
