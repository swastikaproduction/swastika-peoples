<?php
// error_reporting(-1);
include('../include/config.php');

$emp_id = $_GET['emp_id'];
$year = $_GET['year'];
$month = $_GET['month'];
if($month < 10){
	$month = '0'.$month;
}

if($_GET['type'] == 2){
	
	$fnf_salary = $_GET['fnf_salary'];
	$salaryReleasedBy = $_GET['salaryReleasedBy'];

	$fnfcalculation_details = mysqli_query($con,"SELECT empid FROM `fnfcalculation` where `empid`= '$emp_id' AND `month`= '$month' AND `year`= '$year' ") or die(mysqli_error($con));
	$empid=mysqli_fetch_array($fnfcalculation_details);
	if(!empty($empid)){

		mysqli_query($con,"UPDATE `fnfcalculation` SET `fnf_salary` = '$fnf_salary',`salary_released_by` = '$salaryReleasedBy' WHERE `empid` = '$emp_id' AND `month`= '$month' AND `year`= '$year'") or die(mysqli_error($con));
			$salary = $total_salary;

		$data = array('status' => 200, 'message' => 'fnf salary saved successfully');
	}
	else{
		
		$data = array('status' => 400, 'message' => 'Please try again');
	}
}
else{

	$emp_code = $_GET['emp_code'];
	$actual_salary = $_GET['actual_salary'];
	$loan_amount = $_GET['loan_amount'];
	$client_debit_amount = $_GET['client_debit_amount'];
	$rm_client_debit = $_GET['rm_client_debit'];
	$head_comment_salary = trim(round($_GET['head_comment_salary']));
	$created_date = date('Y-m-d H:i:s');
	//echo $head_comment_salary; die;

	$training_calculaton = round($actual_salary*2.5/100);
	$pt = 0;
	if($actual_salary >= 18750 && $actual_salary <= 25000){
		$pt = '125';
	}
	else if($actual_salary > 25000 && $actual_salary <= 33333){
		$pt = '167';
	}
	else if($actual_salary > 33333){
		$pt = '208';
	}

	global $lastpooldeduction;
	global $rowEmp;
	$getEmp = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$emp_id'") or die(mysqli_error($con));
	$rowEmp = mysqli_fetch_array($getEmp);

	$leavingdate = $rowEmp['leavingdate'];
	$resignation_date = $rowEmp['resignation_date'];
	$left = $rowEmp['left'];
	$resignation_remark = $rowEmp['left_resignation_remark'];
	// echo $leavingdate; echo $resignation_date; die;

	if($left == 0){
		$data = array('status' => 400, 'message' => 'Leaving date is not found!');
		echo json_encode($data);
		exit();
	}

	if($resignation_remark == 0){
		$data = array('status' => 400, 'message' => 'Resignaton date is not found!');
		echo json_encode($data);
		exit();
	}

	$table = 'attendance_'.$month.'_'.$year;

	$getDeduction = mysqli_query($con,"SELECT `date`,`deduction`,`status`  FROM `$table` WHERE `empid` = '$emp_id'") or die(mysqli_error($con));
	while($deduction = mysqli_fetch_array($getDeduction)){

		if($deduction['date'] >=  $resignation_date && $deduction['date'] <= $leavingdate){
			$leave_balance_after_resignation[] = $deduction['deduction'];
		}
		else{
			$leave_balance_before_resignation[] = $deduction['deduction'];
		}

		if($deduction['status'] == 1){
			$working_days[] = $deduction['status'];
		}
		$total_days[] = $deduction;
		
	}
	
	$leave_balance_after_resignation = array_sum($leave_balance_after_resignation);
	$leave_balance_before_resignation = array_sum($leave_balance_before_resignation);
	$total_leave = $leave_balance_after_resignation+$leave_balance_before_resignation;
	$working_days = array_sum($working_days);

	// get pool deduction details
	$lastpooldeduction = '0';
	global $month_end;
	$month_end = $year."-".$month."-25";
	$getLastPool = mysqli_query($con,"SELECT SUM(`deduction`) FROM `pooldeductions` WHERE `empid` = '$emp_id' AND `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
	$rowLastPool = mysqli_fetch_array($getLastPool);
	$lastpooldeduction = $rowLastPool[0];

	if($leave_balance_before_resignation != ""){
		include('poolfunction.php');
		$maxpoolallowed = getMaxPoolAllowed($emp_id);
	}
	else{
		$maxpoolallowed = 0;
	}

	$leave_deduction_before_resignation = $maxpoolallowed-$leave_balance_before_resignation;

	if($leave_deduction_before_resignation < 0){
		$get_leave_balance_before_resignation = abs($leave_deduction_before_resignation);
	}

	$total_leave_deduction = $leave_balance_after_resignation+$get_leave_balance_before_resignation;
	$total_days = count($total_days);
	if($month == '02'){
		if($total_days == '28'){
			$total_days = 30;
		}
		elseif($total_days == '29'){
			$total_days = 30;
		}
		else{
			$total_days = $total_days;
		}
	}
	else{
		if($total_days == '30'){
			$total_days = 30;
		}
		elseif($total_days == '31'){
			$total_days = 30;
		}
		else{
			$total_days = $total_days;
		}
	}

	$actual_working_days_salary = round($total_days*$actual_salary/30);
	$total_leave_salary_deduction = round($actual_salary/30*$total_leave_deduction);
	$other_salary_deduction = round($loan_amount+$client_debit_amount+$training_calculaton+$pt);

	$salary_calculated_by_attendance = $actual_working_days_salary-($total_leave_salary_deduction+$other_salary_deduction);

	if(empty($salary_calculated_by_attendance)){
		$salary_calculated_by_attendance = '0';
	}

	if(!empty($head_comment_salary)){
		$salary_by_heads_comment = round($head_comment_salary-$other_salary_deduction);
	}
	else{
		$salary_by_heads_comment = '0';
	}


	$fnfcalculation_details = mysqli_query($con,"SELECT empid FROM `fnfcalculation` where `empid`= '$emp_id' AND `month`= '$month' AND `year`= '$year' ") or die(mysqli_error($con));
	$empid=mysqli_fetch_array($fnfcalculation_details);
	// echo $empid; die();
	if(!empty($empid)){

		mysqli_query($con,"UPDATE `fnfcalculation` SET `actual_salary` = '$actual_salary',`salary_calculated_by_attendance` = '$salary_calculated_by_attendance',`salary_by_heads_comment` = '$salary_by_heads_comment',`working_days` = '$working_days',`loan_amount` = '$loan_amount', `client_debit_amount`= '$client_debit_amount', `rm_client_debit` = '$rm_client_debit', `pool_balance` = '$maxpoolallowed', `total_leave` = '$total_leave', `leave_balance_before_resignation` = '$leave_balance_before_resignation', `leave_balance_after_resignation` = '$leave_balance_after_resignation', `leave_deduction` = '$total_leave_deduction', `salary_leave_deduction` = '$total_leave_salary_deduction', `training_deduction` = '$training_calculaton', `pt` = '$pt' WHERE `empid` = '$emp_id' AND `month`= '$month' AND `year`= '$year'") or die(mysqli_error($con));
			$salary = $total_salary;

		$data = array('status' => 200, 'salary_calculated_by_attendance' => $salary_calculated_by_attendance, 'salary_by_heads_comment' => $salary_by_heads_comment, 'training_calculaton' => $training_calculaton, 'pt' => $pt, 'total_leave_salary_deduction' => $total_leave_salary_deduction, 'message' => 'FnF calculation successfully generated');
	}
	else{

		mysqli_query($con,"INSERT INTO `fnfcalculation` (`empid`, `month`, `year`, `emp_code`, `actual_salary`, `salary_calculated_by_attendance`, `salary_by_heads_comment`, `working_days`, `loan_amount`, `client_debit_amount`, `rm_client_debit`, `pool_balance`, `total_leave`, `leave_balance_before_resignation`, `leave_balance_after_resignation`, `leave_deduction`, `salary_leave_deduction`, `training_deduction`, `pt`, `created_by`, `created_date`) VALUES ('$emp_id', '$month', '$year', '$emp_code', '$actual_salary', '$salary_calculated_by_attendance', '$salary_by_heads_comment', '$working_days', '$loan_amount', '$client_debit_amount', '$rm_client_debit', '$maxpoolallowed', '$total_leave', '$leave_balance_before_resignation', '$leave_balance_after_resignation', '$total_leave_deduction', '$total_leave_salary_deduction', '$training_calculaton', '$pt', '$loggeduserid', '$created_date')") or die(mysqli_error($con));
			$salary = $total_salary;
		
		$data = array('status' => 200, 'salary_calculated_by_attendance' => $salary_calculated_by_attendance, 'salary_by_heads_comment' => $salary_by_heads_comment, 'training_calculaton' => $training_calculaton, 'pt' => $pt, 'total_leave_salary_deduction' => $total_leave_salary_deduction, 'message' => 'FnF calculation successfully generated');
	}
}

echo json_encode($data);




