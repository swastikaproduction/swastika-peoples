<?php
include("../include/config.php");
$id = $_GET['id'];

?>


<div class="moduleHead">
	<div class="moduleHeading">
	Appointment Letter
	</div>
</div>
<div class="shadow">

<form target="_blank" method="post" onsubmit="return appointmentLetterValidation()"  action="TCPDF/examples/appointmentLetter.php" id="myform" >

	<div class="row" style="background:#f8f7f7">
		<div class="col-sm-3 formLeft req">
		Salary
		</div>
		<div class="col-sm-9 formRight">
			<input type="text"  name="emp_salary" title="" id="emp_salary" class="inputBox input-field"  value="">
			<span id="empSalary-info" class="info" style="color:red"></span><br />
			<input type="hidden"  name="emp_id" title=""  value="<?php if(!empty($id)) echo $id; ?>">
		</div>

		<div class="col-sm-3 formLeft req">
		Designation
		</div>
		<div class="col-sm-9 formRight">
			<select class="inputBox input-field" id="emp_designation" name="emp_designation" >
				<?php
				$data = getData('designation','*','name','ASC');
				foreach($data as $row2)
				{
				?>
				<option value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
				<?php } ?>
			</select>
		</div>	

		<div class="col-sm-3 formLeft req">
			Notice Period
		</div>
		<div class="col-sm-9 formRight">
			<input type="date"  name="emp_notice_period" title="" id="emp_notice_period" class="inputBox input-field"  value="">
			<span id="empNotice-info" class="info" style="color:red"></span><br />
		</div>	

	</div>

	<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	

	<div class="col-sm-10 formRight">
		<input type="submit" class="btn btn-primary" name="submit" value="GENERATE APPOINTMENT LETTER" >
		
		<input type="submit" class="btn btn-primary" name="submit" value="SEND ON EMAIL">
		<br><br><br>
	</div>


	</div>

</form>	



</div>
<br />
<br />
<br />
<br />
<br />

