<?php
$getperms = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$loggeduserid'") or die(mysqli_error($con));
$rowperms= mysqli_fetch_array($getperms);
?>
<ul class="nav navbar-nav">
      <li><a href="#" onclick="getModule('dashboard/index.php','tableDiv','formDiv','loading');">
<i class="fa fa-dashboard"></i>&nbsp;&nbsp;Dashboard</a></li>
<?php
if($loggeduserid == '124' ||  $loggeduserid == '4005'){
?>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-globe"></i>&nbsp;&nbsp;Organization 
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('masters/general/index.php?table=designation&dp=Designation','tableDiv','formDiv','loading');">Designation</li>
          <li onclick="getModule('masters/general/index.php?table=departments&dp=Department','tableDiv','formDiv','loading');">Department</li>
          <li onclick="getModule('masters/general/index.php?table=region&dp=region','tableDiv','formDiv','loading');">Region</li>
          <li onclick="getModule('masters/general/index.php?table=tallybranch&dp=Tally Cost Centers','tableDiv','formDiv','loading');">Tally Cost Centers</li>

          <li onclick="getModule('masters/shifts/index.php','tableDiv','formDiv','loading');">Shifts</li>
          <li onclick="getModule('masters/branch/index.php?table=branch&dp=Location','tableDiv','formDiv','loading');">Branches</li>
           <li onclick="getModule('masters/branchcode/index.php?table=branchcode&dp=Branch Code','tableDiv','formDiv','loading');">Branch Codes</li>
          <li onclick="getModule('calendar/index.php','tableDiv','formDiv','loading');">Holiday Calendar</li>
          <li onclick="getModule('circulars/index.php','tableDiv','formDiv','loading');">Circulars</li>
          <li onclick="getModule('masters/general/index.php?table=payheads&dp=Payhead','tableDiv','formDiv','loading');">Salary Payheads</li>
          <li id="doctemp" onclick="getModule('document-templates/index.php?table=payheads&dp=Payhead','tableDiv','formDiv','loading');">Document Templates</li>
          <li id="doctemp" onclick="getModule('masters/general/index.php?table=tutorial&dp=Tutorial Link','tableDiv','formDiv','loading');">Tutorial Links</li>

        </ul>
      </li>


  <?php
}

?>
      
<?php 
      $getEmp = mysqli_query($con,"SELECT id FROM `employee` WHERE `imdboss` = '$loggeduserid' AND `left`=0") or die(mysqli_error($con));
      $rowcount=mysqli_num_rows($getEmp);
      if($loggeduserid == '124' ||  $loggeduserid == '4005' || $rowcount > 0 || $profile == '20'){ 
?>
<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-users"></i>&nbsp;&nbsp;Employees
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
    <?php if($rowcount > 0 || $profile == '20'){ ?>
          <li  onclick="getModule('employee/index.php','tableDiv','formDiv','loading');">All Employees</li>
        <?php } if($loggeduserid == '124' ||  $loggeduserid == '4005') { ?>
          <li  onclick="getModule('employee/index.php?type=house','tableDiv','formDiv','loading');">Housekeeping Employees</li>
          <li  onclick="getModule('employee/index.php?type=nonhouse','tableDiv','formDiv','loading');">Non-Housekeeping Employees</li>
          <li  onclick="getModule('employee/index.php?type=marketing','tableDiv','formDiv','loading');">Marketing Employees</li>
          <li  onclick="getModule('employee/index.php?type=backoffice','tableDiv','formDiv','loading');">Backoffice Employees</li>
          <?php } if($loggeduserid == '124' ||  $loggeduserid == '4005' || $profile == '20') { ?>
          <li  onclick="getModule('employee/index.php?type=left','tableDiv','formDiv','loading');">Left Employees</li>
        <?php } ?>
        </ul>
</li>
<?php } ?>



<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;Leaves
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('leaves/new.php?leave=1','formDiv','tableDiv','loading');">Request Leave</li>
          <li onclick="getModule('leaves/new.php?leavehf=1','formDiv','tableDiv','loading');">Request Halfday</li>
          <!-- <li onclick="getModule('leaves/index.php?pending=1','tableDiv','formDiv','loading');">Pending Leaves Requests</li> -->
          <?php  if(in_array('manage_leave_request',$permissions)){?> 
          <li onclick="getModule('leaves/index.php?pending=1','tableDiv','formDiv','loading');">Manage Request</li>
          <?php } ?>
          <?php if(in_array('all_request_leaves',$permissions)){?> 
          <li onclick="getModule('leaves/index.php','tableDiv','formDiv','loading');">All Leaves Requested</li>
          <?php } if(in_array('leaves_history',$permissions)){?> 
          <li onclick="getModule('leaves/leave-history.php','tableDiv','formDiv','loading');">Leaves History</li>
          <?php  } ?>
          <?php if($loggeduserid=='124' || $loggeduserid == '4005'){
            ?>
           <li onclick="getModule('leaves/leave-history-hr.php','tableDiv','formDiv','loading');">Leaves History</li>
           <li onclick="getModule('leaves/leave-filter.php','tableDiv','formDiv','loading');">Leaves Filter</li>
           <?php } ?>
        </ul>
      </li>
       <?php
          if($loggeduserid == '124' || $loggeduserid == '4005' )
          {
            ?>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-check"></i>&nbsp;&nbsp;Appraisals
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('appraisal/appraisal_view.php?appraisal=1','formDiv','tableDiv','loading');">HR Appraisals</li>
          <li onclick="getModule('appraisal/profile-kpis-kras.php?profile_kpi=1','formDiv','tableDiv','loading');">Profile Kpi's</li>
         <!--  <li onclick="getModule('appraisal/profile_kpi.php?profile_kpi_view=1','formDiv','tableDiv','loading');">Profile Kpi's View</li> -->
          <li onclick="getModule('appraisal/profile_types.php?profile_types=1','formDiv','tableDiv','loading');">Profile Types</li>
        <!--   <li onclick="getModule('appraisal/appraisal_users.php?appraisal_users=1','formDiv','tableDiv','loading');">Appraisal Users</li> -->
          
        </ul>
      </li>

      <?php
    }?>

     <!-- <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-sign-out"></i>&nbsp;&nbsp;Sepration Process
        <span class="caret"></span></a>
        <?php if($loggeduserid!=='124'){
          ?>
        
        <ul class="dropdown-menu">
          <li onclick="getModule('Resignation/index.php?resignation=1','formDiv','tableDiv','loading');">Resignation</li>
          <li onclick="getModule('Resignation/resignationProcessView.php?resignationprocess=1','formDiv','tableDiv','loading');">Application Status</li>
          <li onclick="getModule('appraisal/profile_kpi.php?profile_kpi=1','formDiv','tableDiv','loading');">Exit Formalities</li>
         
          
        </ul>
        <?php 
      }?>
       <ul class="dropdown-menu">
          <li onclick="getModule('Resignation/index.php?resignation=1','formDiv','tableDiv','loading');">Termination</li>
          <li onclick="getModule('Resignation/resignationProcessView.php?resignationprocess=1','formDiv','tableDiv','loading');">Application Status</li>
         
         
          
        </ul>
      </li> -->
   <?php
          if($loggeduserid !== '124' && $loggeduserid !== '4005')
          {
            ?>
<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-check"></i>&nbsp;&nbsp;Appraisals
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <?php 
            $query = mysqli_query($con,"select COUNT(e.id) as ecount from Employee e inner join Employee m on e.imdboss = m.id and m.id = '$loggeduserid' inner join employee ee on ee.imdboss = e.id");

            $row = mysqli_fetch_array($query);

            $sql = mysqli_query($con,"select count(*) as imd from employee where imdboss='$loggeduserid'");

            $result = mysqli_fetch_array($sql);

            $imdboss = $result['imd'];
            
            if($row['ecount']==0){

            $check2 = mysqli_query($con,"select * from appraisalusers where empId='$loggeduserid'");

           $row = mysqli_fetch_array($check2);

           $appid = $row['appId'];

           $check3 = mysqli_query($con,"select count(appId) as aid from appraisalstart where appId ='$appid'");

           $result = mysqli_fetch_array($check3);

           if($result['aid']>0){

            $my = 'my_appraisal';
            
           }else{
            $my = 'my_goals';
           }     
           ?>
          <li onclick="getModule('appraisal/<?php echo $my;?>.php?<?php echo $my;?>=1','formDiv','tableDiv','loading');">My Appraisals </li>
          <?php
           $check = mysqli_query($con,"select * from employee");
           $result=mysqli_fetch_array($check);
           if($result['imdboss']!==$loggeduserid && $imdboss !=='0'){
           
           $check2 = mysqli_query($con,"select * from appraisal_users where emp_id='$loggeduserid'");

           $row = mysqli_fetch_array($check2);
           $appid = $row['app_id'];

           $check3 = mysqli_query($con,"select count(appId) as aid from appraisalstart where appId ='$appid'");

           $result = mysqli_fetch_array($check3);

           if($result['aid']>0){

            $team = 'team_appraisal';
            
           }else{
            $team = 'team_goals';
           }


          ?>
          <li onclick="getModule('appraisal/<?php echo $team;?>.php?team=1','formDiv','tableDiv','loading');">My Team Appraisals </li>
          <?php
          }?>

          <?php
        }else{

           $check2 = mysqli_query($con,"select * from appraisalusers where empId='$loggeduserid'");

           $row = mysqli_fetch_array($check2);
           $appid = $row['appId'];

           $check3 = mysqli_query($con,"select count(appId) as aid from appraisalstart where appId ='$appid'");

           $result = mysqli_fetch_array($check3);

           if($result['aid']>0){

            $my = 'my_appraisal';
            $team = 'hod_appraisal';
            
           }else{
            $my = 'my_goals';
            $team = 'hod_gols';
           }


          ?>  <li onclick="getModule('appraisal/<?php echo $my;?>.php?<?php echo $my;?>=1','formDiv','tableDiv','loading');">My Appraisals</li>
           <li onclick="getModule('appraisal/<?php echo $team;?>.php?<?php echo $team;?>=1','formDiv','tableDiv','loading');">My Team Appraisals</li>
          <?php

        }
        ?>
         
          
        </ul>
      </li>

<?php  

}?>

<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-th-list"></i>&nbsp;&nbsp;Attendance
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('leaves/new.php?mis=1','formDiv','tableDiv','loading');">Missed Punch</li>
          <?php  if(in_array('overtime_request',$permissions)){?> 
          <li onclick="getModule('leaves/new.php?overtime=1','formDiv','tableDiv','loading');">Overtime Request</li>
          <?php } ?>
          <?php // if(in_array('attendance',$permissions)){?> 
          <li id="menuAttendanceButton" onclick="getModule('attendance/index.php?current=1','tableDiv','formDiv','loading');">Attendance</li>
          <?php // } ?>
          <li onclick="getModule('leaves/index.php?my=1','tableDiv','formDiv','loading');">My Requests</li>
          <?php  if(in_array('team_attendance',$permissions)){ ?> 
          <li onclick="getModule('leaves/team-attendance.php?my=1','tableDiv','formDiv','loading');">Team Attendance</li>
          <?php } ?>
          <?php
          if($loggeduserid == '124' || $loggeduserid == '4005')
          {
            ?>
          <li onclick="getModal('attendance/fetchindex.php','tableModal','formModal','loading');">Fetch Attendance</li>
           <li onclick="getModule('attendance/attendance-corrections.php?my=1','tableDiv','formDiv','loading');">Attendance Corrections</li>
            <?php
          }
          ?>
          
        </ul>
      </li>



<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-rupee"></i>&nbsp;&nbsp;Salary
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <?php  if(in_array('view_generated_salary',$permissions)){?> 
        <li onclick="getModule('attendance/index.php?type=salary','tableDiv','formDiv','loading');">View Generated Salaries</li>
        <?php }
        if(in_array('salary_slips',$permissions)){  ?> 
        <?php } ?>   
         <li onclick="getModule('attendance/index.php?type=salary','tableDiv','formDiv','loading');">Salary Slips</li>
 
          <?php
          if($loggeduserid == '124' || $loggeduserid == '4005')
          {
          ?>
          <li onclick="getModule('salaryaddons/index.php','tableDiv','formDiv','loading');">Salary Addons</li>
          <li onclick="getModule('salarycalculation/bookingindex.php?type=salary','tableDiv','formDiv','loading');">Booking Salary Export</li>
          <li onclick="getModule('attendance/fnf_index.php?type=salary','tableDiv','formDiv','loading');">FnF Calculation Sheet</li>
          <li  onclick="getModule('bankdetails/unapproved.php?type=left','tableDiv','formDiv','loading');">Un approved Bank Details</li>
          <?php if(in_array('salary_log',$permissions)){  ?> 
          <li  onclick="getModule('salarycalculation/salarylog.php?type=left','tableDiv','formDiv','loading');">Salary Log</li>
          <?php } ?>

<?php
}
if($rowperms['holdperm'] == '1' || $rowperms['incentiveperm'] == '1' || $rowperms['loanperm'] == '1')
{
  ?>
<li onclick="getModule('salaryaddons/index.php','tableDiv','formDiv','loading');">Salary Addons</li>
  <?php
}
if($rowperms['bcperm'] == '1')
{
  ?>
                    <li  onclick="getModule('bankdetails/unapproved.php?type=left','tableDiv','formDiv','loading');">Un approved Bank Details</li>

  <?php
}
if($rowperms['jvholdperm'] == '1'  || $loggeduserid == '124' || $loggeduserid == '4005')
{
  ?>
                    <li  onclick="getModule('salaryaddons/index.php','tableDiv','formDiv','loading');">With JV Hold</li>
                     <li  onclick="getModule('salaryaddons/hold-salary.php','tableDiv','formDiv','loading');">Hold Salary</li>
                        <li  onclick="getModule('salaryaddons/ctc-salary.php','tableDiv','formDiv','loading');">CTC Change</li>

  <?php
}
?>


        </ul>
      </li>
      <?php if($loggeduserid!=='124' && $loggeduserid !== '4005'){
        ?>
    
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-user-secret"></i></i>&nbsp;&nbsp;Policies
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('Policy/View.php?mis=1','formDiv','tableDiv','loading');">View</li>
                          
        </ul>
      </li>
           <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-file"></i></i>&nbsp;&nbsp;Documents
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('EmployeeDocuments/index.php?mis=1','formDiv','tableDiv','loading');">View</li>
          <li onclick="getModule('EmployeeDocuments/team-documents.php?mis=1','formDiv','tableDiv','loading');">My Team Docs</li>                          
        </ul>
      </li>
      <?php
       $check = mysqli_query($con,"SELECT * FROM `employee` where id='$loggeduserid'");
       $result = mysqli_fetch_array($check);

       ?>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

       <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Expense
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          
          <?php 
          if($result['expensemanager']==1){?>
          <li onclick="getModule('expense/manager-expense-view.php','tableDiv','formDiv','loading');">My expense View</li>
          <li onclick="getModule('expense/benificiary.php','tableDiv','formDiv','loading');">Benificiary Listing</li>
           <?php }?>
          <?php 

          if($result['expenseadmin']==1){
          ?>
          <li onclick="getModule('expense/view-expense-all.php','tableDiv','formDiv','loading');">View All</li>
          <li onclick="getModule('expense/index.php','tableDiv','formDiv','loading');">View Branch Wise</li>
          <li onclick="getModule('expense/reports.php','tableDiv','formDiv','loading');">Reports</li>
          <li onclick="getModule('expense/manager.php','tableDiv','formDiv','loading');">Concerns Person</li>
          <li onclick="getModule('expense/benificiary-list.php','tableDiv','formDiv','loading');">Benificiary Listing</li>
          <?php
        }?>
 
        </ul>
      </li>
     <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-id-card" aria-hidden="true"></i>&nbsp;&nbsp;Assets
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('asset/view.php?userid=<?php echo $rowperms['empid'];?>','tableDiv','formDiv','loading');">Assign Assets</li>
          <li onclick="getModule('asset/index.php','tableDiv','formDiv','loading');">Requirements</li>
          <li onclick="getModule('asset/manager-view.php','tableDiv','formDiv','loading');">Team Asset</li>
        </ul>
    </li>

      <?php
     /* $check = mysqli_query($con,"SELECT * FROM `employee` where imdboss='$loggeduserid' and `left`=0");
      $result = mysqli_fetch_array($check);
      if(!empty($result)){
        ?> 
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-bar-chart"></i>&nbsp;&nbsp;Reports
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <?php
      $check = mysqli_query($con,"SELECT * FROM `region` where rbhid='$loggeduserid'");
      $result = mysqli_fetch_array($check);
      if(!empty($result)){
        ?> 
          <li onclick="getModule('reports/branchwiselist.php','tableDiv','formDiv','loading');">Branch Wise List</li>
          <li onclick="getModule('reports/employee-list.php','tableDiv','formDiv','loading');">All Employee List</li>

         <?php } ?>
        </ul>
      </li>
      <?php  }  */?>
    <?php 
  }?>

      <?php if($loggeduserid==124){ ?>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user-secret"></i>&nbsp;&nbsp;Policies
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('policy/index.php','tableDiv','formDiv','loading');">View</li>
        </ul>
      </li>
    <?php } ?>

      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-bar-chart"></i>&nbsp;&nbsp;Reports
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <?php  if(in_array('analytics_report',$permissions)){ ?> 
          <li onclick="getModule('reports/index.php','tableDiv','formDiv','loading');">Analytics Reports</li>
          <?php } ?>
          <?php  if(in_array('change_log_report',$permissions)){ ?> 
          <li onclick="getModule('changelog/index.php','tableDiv','formDiv','loading');">Change Log</li>
          <?php } ?>
          <?php  if(in_array('goal_setting',$permissions)){ ?> 
          <li onclick="getModule('appraisal/goal-settings.php','tableDiv','formDiv','loading');">Goal Setting</li>
          <?php } ?>
          <?php  if(in_array('appraisal_report',$permissions)){ ?> 
          <li onclick="getModule('appraisal/index.php','tableDiv','formDiv','loading');">Appraisal</li>
          <?php } ?>
          <?php  if(in_array('document_report',$permissions)){ ?> 
          <li onclick="getModule('reports/documents.php','tableDiv','formDiv','loading');">Documents</li>
          <?php } ?>
          <?php  if(in_array('pool_log_report',$permissions)){ ?> 
          <li onclick="getModule('reports/poollog.php','tableDiv','formDiv','loading');">Pool Log</li>
          <?php } ?>
          <?php  if(in_array('salary_log_report',$permissions)){ ?> 
          <li onclick="getModule('reports/salarylog.php','tableDiv','formDiv','loading');">Salary Log</li>
          <?php } ?>
          <?php  if(in_array('salary_log_export',$permissions)){ ?>
          <li onclick="getModule('reports/salarylogEx.php','tableDiv','formDiv','loading');">Salary Log Export</li>
          <?php } ?>
          <?php  if(in_array('branch_report',$permissions)){ ?>
          <li onclick="getModule('reports/branchwiselist.php','tableDiv','formDiv','loading');">Branch Wise List</li>
          <?php } ?>
        </ul>
      </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <i class="fa fa-id-card" aria-hidden="true"></i>&nbsp;&nbsp;Official Cards
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('officialcards/visitingcards/index.php','tableDiv','formDiv','loading');">Visiting Cards</li>
          <li onclick="getModule('officialcards/identity/index.php','tableDiv','formDiv','loading');">Identity Cards</li>      
 
        </ul>
      </li>

<?php
 $check = mysqli_query($con,"SELECT * FROM `employee` where id='$loggeduserid'");
 $result = mysqli_fetch_array($check);
if($result['expenseadmin']==1){ ?>
<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

       <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Expense
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('expense/view-expense-all.php','tableDiv','formDiv','loading');">View All</li>
          <li onclick="getModule('expense/index.php','tableDiv','formDiv','loading');">View Branch Wise</li>
          <li onclick="getModule('expense/reports.php','tableDiv','formDiv','loading');">Reports</li>
          <li onclick="getModule('expense/manager.php','tableDiv','formDiv','loading');">Concerns Person</li>
          <li onclick="getModule('expense/benificiary-list.php','tableDiv','formDiv','loading');">Benificiary Listing</li>
        </ul>
      </li>
<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

       <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Assets
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('asset/view-all.php','tableDiv','formDiv','loading');">View All</li>       
          <li onclick="getModule('asset/hr-view.php','tableDiv','formDiv','loading');">Requirements</li>       
          <li onclick="getModule('asset/action-log.php','tableDiv','formDiv','loading');">Action Log</li>       
        </ul>
      </li>

      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Masters
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li onclick="getModule('masters/accesscontrol/view-all.php','tableDiv','formDiv','loading');">Access Control</li>       
          <li onclick="getModule('masters/documents/index.php','tableDiv','formDiv','loading');">Generate Offer Letter</li>  
        </ul>
      </li>
<?php } else{ if($profile == '20'){ if(in_array('emp_offer_letter',$permissions)){ ?>

  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Masters
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
               
          <li onclick="getModule('masters/documents/index.php','tableDiv','formDiv','loading');">Generate Offer Letter</li>  
        </ul>
      </li>

<?php } } } ?>
</ul>