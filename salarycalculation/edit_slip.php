<?php
include("../include/config.php");
include("convert-to-word.php");
$id = $_GET['id'];
$getAtlog = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowLog = mysqli_fetch_array($getAtlog);
$empid = $rowLog['empid'];
$salary = $rowLog['final_salary'];

$actualSalary = $rowLog['actual_salary'];;
$calcSalary = $salary;





$getEmp = mysqli_query($con,"SELECT employee.name,designation.name,employee.pf,employee.empid,employee.branch,departments.name FROM employee,designation,departments WHERE employee.designation = designation.id AND employee.id = '$empid' AND employee.department = departments.id") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

$branchid = $rowEmp[4];
$getBranch = mysqli_query($con,"SELECT `name`,`notes` FROM `branch` WHERE `id` = '$branchid'") or die(mysqli_error($con));
$rowBranch = mysqli_fetch_array($getBranch);
$branch = $rowBranch[0];
$branchaddress = $rowBranch[1];

/*
$getData = mysqli_query($con,"SELECT * FROM `bifurcation` WHERE `last` < '$salary' AND `this` >= '$salary'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
*/

$monthArray = Array();
$monthArray[01] = 'January';
$monthArray[02] = 'February';
$monthArray[03] = 'March';
$monthArray[04] = 'April';
$monthArray[05] = 'May';
$monthArray[06] = 'June';
$monthArray[07] = 'July';
$monthArray[08] = 'August';
$monthArray[09] = 'September';
$monthArray[10] = 'October';
$monthArray[11] = 'November';
$monthArray[12] = 'December';


$pfyes=$rowEmp[2];
$mth = "2017-".$rowLog['month']."-01";
include("bifur-logic.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Salary Slip
	</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>


<style type="text/css">
body
{
	overflow: auto !important;
}
	.inTab th
	{
		background:#999;
		color: #fff;
		font-weight: normal;
		text-align: left;
	}

	.inTab tr th:nth-child(2),.inTab tr th:nth-child(4)
	{
		text-align: right;
	}

	.inTab tr td:nth-child(1), .inTab tr td:nth-child(3)
	{
		font-weight: bold;
	}

	.inTab tr td:nth-child(2)
	{
		border-right:1px #eee solid;
		text-align: right;
	}

	.inTab tr td:nth-child(4)
	{
		text-align: right;
	}
	.inTab td
	{
		border-bottom:1px #eee solid;
	}

	.inputBoxSal
	{
		padding: 5px;
	}
</style>
<script type="text/javascript">
	function calculateSalary()
	{
		var totalEarning = 0;
		var totalDeductions = 0;
		var netpay = 0;
		var values = [];
		for(j=0;j<12;j++)
		{
			if(document.getElementById('sal'+j).value == '')
			{
				document.getElementById('sal'+j).value = 0;
			}
			if(j != 1 && j != 4 && j != 6 && j != 10 && j != 11)
			{
			totalEarning += parseInt(document.getElementById('sal'+j).value);				
			}
			else
			{
			totalDeductions += parseInt(document.getElementById('sal'+j).value);				
			}
			console.log(totalEarning);
			console.log(totalDeductions);
		}
		document.getElementById('totalearnings').value= totalEarning;		
		document.getElementById('totaldeductions').value= totalDeductions;
		document.getElementById('netpay').value= totalEarning - totalDeductions;

	}
</script>


<script type="text/javascript">
	
	function printDoc()
	{
		document.getElementById('ptBox').style.display = 'none';
		window.print();
		setTimeout(function(){
			document.getElementById('ptBox').style.display = 'block';
		},300);
	}
</script>
</head>
<body>
<form action="savesalary.php?id=<?php echo $id;?>" method="post">


<center>

	<div style="width:900px;padding:40px;">
		<table cellpadding="10" cellspacing="0" style="width:100%;border:1px #eee solid;">
		<tr>
			<td colspan="2" align="center" style="border-bottom:1px #eee solid;font-size:20px;">
				<strong>Payslip for the Month of <?php echo $monthArray[date("m",strtotime($mth))];?>, <?php echo $rowLog['year'];?></strong>
			</td>
		</tr>
			<tr>
				<td style="width:50%;text-align:left;border-right:1px #eee solid;">
					<table style="width:100%" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<td>Employee Name</td><td style="border-right:0px;font-weight:bold"><?php echo $rowEmp[0];?></td>
						</tr>
<tr>
						<td>
							Department
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[5];?>
						</td>
							
						</tr>						
<tr>
						<td>
							Designation
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[1];?>
						</td>
							
						</tr>
						<tr>
						<td>
							Employee Code
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[3];?>
						</td>
							
						</tr>

<tr>
						<td style="border-bottom:0px;">
Branch
						</td>
						<td style="border-right:0px;border-bottom:0px;font-weight:bold">
							<?php echo $branch;?>
						</td>
							
						</tr>

					</table>
				</td>
				<td style="text-align:center">
					<img src="https://www.swastika.co.in/images/blogo.png" style="height:40px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;border-top:1px #eee solid">
				<br/><br/>
					<table style="width:100%;" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<th style="width:25%">Earnings & Remuneration</th>
							<th style="width:25%">Amount (Rs.)</th>
							<th style="width:25%">Deductions</th>
							<th style="width:25%">Amount (Rs.)</th>
						</tr>
<tr>
							<td>Earned Basic</td>
							<td>
<input type="text" class="inputBoxSal" name="basic" value="<?php echo $basic_real;?>" id="sal0" onkeyup="calculateSalary()">
							</td>
							<td>PF(Employer & Employee)</td>
							<td>
<input type="text" class="inputBoxSal" name="pf" value="<?php echo $twopf;?>"  id="sal1" onkeyup="calculateSalary()">

								
								
							</td>
						</tr>
						<tr>
							<td>Dearness Allowance</td>
							<td>
							<input name="da" type="text" class="inputBoxSal" value="<?php echo $da_real;?>" id="sal2" onkeyup="calculateSalary()">
					</td>
							<td>TDS</td>
							<td>
															<input name="tds" type="text" class="inputBoxSal" value="0" id="sal10" onkeyup="calculateSalary()">


							</td>
						</tr>
						<tr>
							<td>HRA</td>
							<td>

							<input name="hra" type="text" class="inputBoxSal" value="<?php echo $hra;?>"  id="sal3" onkeyup="calculateSalary()">
								

							</td>
							<td>Professional Tax</td>
							<td>
							<input name="pt" type="text" class="inputBoxSal" value="<?php echo $pt;?>"  id="sal4" onkeyup="calculateSalary()">
								
							</td>
						</tr>
						<tr>
							<td>Bonus</td>
							<td>
						<input name="bonus" type="text" class="inputBoxSal" value="<?php echo $bonus;?>"  id="sal5" onkeyup="calculateSalary()">

								

							</td>
							<td>Leave Deductions</td>
							<td>

						<input name="leave" type="text" class="inputBoxSal" value="<?php echo $leavededuction;?>"  id="sal6" onkeyup="calculateSalary()">
							

							</td>
						</tr>
						<tr>
							<td>Domicilliary Allowance</td>
							<td>

<input type="text" name="dom" class="inputBoxSal" value="<?php echo $dom;?>"  id="sal7" onkeyup="calculateSalary()">
</td>
							<td>Other Deductions</td>
							<td>															<input name="other" type="text" class="inputBoxSal" value="0" id="sal11" onkeyup="calculateSalary()">
</td>
						</tr>

						<tr>
							<td>Conveyance Allowance</td>
							<td>
<input type="text" name="convey" class="inputBoxSal" value="<?php echo $conv;?>"  id="sal8" onkeyup="calculateSalary()">
							</td>
							<td></td><td></td>
						</tr>
						<tr>
							<td>Employee Daily Refreshment</td>
							<td>

							<input name="ref" type="text" class="inputBoxSal" value="<?php echo $ref;?>"  id="sal9" onkeyup="calculateSalary()">
							</td>
							<td></td><td></td>
						</tr>
						<tr>
							<td>Total Earning</td>
							<td  style="font-weight:bold">

							<input name="totalearnings" id="totalearnings" readonly="readonly" type="text" class="inputBoxSal" value="<?php echo $total_earnings;?>"  >
							</td>
							<td>Total Deductions</td>
							<td  style="font-weight:bold">
<input type="text" name="totaldeductions"  id="totaldeductions"  readonly="readonly"  class="inputBoxSal" value="<?php echo $total_deductions;?>">
							</td>
						</tr>
						<tr>
							<td>Net Pay</td>
							<td style="font-weight:bold;border-top:1px #eee solid;" colspan="3">
<input type="text" name="netpay" class="inputBoxSal"  readonly="readonly"   id="netpay" value="<?php echo $netpay = $total_earnings - $total_deductions;?>">
							</td>
						</tr>
						<tr>
							
							<td style="font-weight:bold;border-top:1px #eee solid;text-align:right" colspan="4"><span style="text-transform:capitalize">
					<?php echo str_ireplace("thousands","thousand",convert_to_word($netpay));?> Rupees
					</span></td>
						</tr>
						<tr>
							<td colspan="4" style="border-top:1px #eee solid;">
								*This salary (CTC) is subject to achievement of all the targets and submission of reports. In case the same is not achieved or submitted then the salary shall be paid on pro-data basis or the conditions mentioned in the offer letter.
							</td>
						</tr>

					</table>
				</td>
			</tr>



		
			<tr>
				<td style="border-top:1px #eee solid;border-right:1px #eee solid;text-align:left;font-weight:bold">
				For Swastika Investmart Ltd
				<br/><br/><br/><br/>
				<span style="font-size:10px">
				Authorized Signatory
				</span>
					
				</td>
				<td style="text-align:left;border-top:1px #eee solid;">
				<strong style="font-size:16px;font-weight:bold">Swastika Investmart Ltd.</strong>
				<br/>
				<strong style="font-size:13px;font-weight:bold">
Head Office</strong>
				<br/>
				<i class="fa fa-map-marker"></i>&nbsp;&nbsp;48, Jaora Compound, M.Y.H. Road, Indore - 452 001<br/>
<i class="fa fa-phone"></i>&nbsp;&nbsp;0731 - 6644000, <i class="fa fa-globe"></i>&nbsp;&nbsp;www.swastika.co.in<br/>
				<strong style="font-size:13px;font-weight:bold">Reporting Branch</strong>
				<br/>
					<?php echo $branchaddress;?>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="border-top:1px #eee solid;padding:50px;">

					<button style="width:100px;border:0px;background:#1976d2;color:#fff;padding:10px;">SAVE SALARY</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button" style="width:100px;border:0px;background:#ef5350;color:#fff;padding:10px;" onclick="location.reload();">RESET FORM</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button"  style="width:200px;border:0px;background:#ef5350;color:#fff;padding:10px;"  onclick="window.location = 'reset_original.php?id=<?php echo $id;?>'">RESET TO ORIGINAL VERSION</button>

				</td>
			</tr>
		</table>
	</div>
</center>
</form>
</body>
</html>

