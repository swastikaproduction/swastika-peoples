<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active" style="color: green;">Benificiary List</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>   -->         
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Branch Name</th>
<th>Concern Person</th>            
<th>Benificiary Name</th>            
<th>Head Name</th>            
<th>Account Number</th>            
<th>Bank Statment</th>            
<th>IFSC Code</th>            
<th>Create Date</th>            
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/connection.php';
$query = mysqli_query($con,"select eb.name,b.name as bname,e.name as cname,eb.accountnumber,eb.ifsc,eb.createdate,ep.name as headname,eb.bankstatement FROM expensebenificiary eb INNER JOIN employee e on e.id = eb.empid INNER JOIN branch b on b.id = e.branch INNER JOIN expensenature ep on ep.id = eb.headid");
while($row=mysqli_fetch_array($query)){
    
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['bname'];?></a></td>
<td><?php echo $row['cname'];?></a></td>
<td><?php echo $row['name'];?></td>
<td><?php echo $row['headname'];?></td>
<td><?php echo $row['accountnumber'];?></td>
<td><a href="expense/benificiaryimages/<?php echo $row['bankstatement'];?>" download>Download</a></td>
<td><?php echo $row['ifsc'];?></td>
<td><?php echo $row['createdate'];?></td>


    <?php
    }?>                      

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>