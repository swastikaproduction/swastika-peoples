<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active" style="color: green;">Expense Management Panel</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Appraisal mode already Exist
</div>   <?php
}
            ?> 
            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>   -->         
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Concern Person</th>            
<th>Branch Name</th>            
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/connection.php';
$query = mysqli_query($con,"SELECT distinct ee.name,b.name as bname,e.empid FROM expense e INNER JOIN employee ee on ee.id = e.empid INNER JOIN branch b on b.id = ee.branch");
while($row=mysqli_fetch_array($query)){
?>
<tr>
<td><?php echo $count++;?></td>
<td><a href="default.php#&rtl=expense/view-expense.php?empid=<?php echo base64_encode($row['empid']); ?>&rtl=tableDiv&rtl=formDiv&rtl=loading"><?php echo $row['name'];?></a></td>
<td><?php echo $row['bname'];?></td>

    <?php
    }?>                      

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>