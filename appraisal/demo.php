<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <link href="~/Content/bootstrap.min.css" rel="stylesheet" />
    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            position: fixed;
            margin-left: -68px;
            margin-top: -68px;
            top:50%;
            left: 50%;
        }

        @@keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <title>LoginWithToken</title>
</head>
<body>
    <div class="loader"></div>
    <script src="~/Scripts/jquery-3.3.1.min.js"></script>
    <script src="~/Scripts/bootstrap.min.js"></script>


    <script>
        $(document).ready(function () {
            debugger;
            var loginToken = getUrlParameter('token');

            if (!loginToken || loginToken == '' || loginToken=== undefined) {
                window.location.replace("/Account/Login");
                return;
            }
            var serviceURL = '/Account/ValidateToken?token=' + loginToken;

            $.ajax({
                type: "POST",
                url: serviceURL,
                data: param = "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: successFunc,
                error: errorFunc
            });

            function successFunc(data) {
                if (data.UserId == 0) {
                    window.location.replace("/Account/Login");                    
                }
                else {
                    window.location.replace("/Home/Index");
                }
            }

            function errorFunc() {
                window.location.replace("/Account/Login");
            }
        });


        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

    </script>

</body>
</html>