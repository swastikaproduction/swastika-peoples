<?php
include("../include/config.php");
?>
<div class="moduleHead">
<br/>
<div class="moduleHeading">
Attendance Change Log
</div>
</div>
<div class="tabelContainer shadow" style="height:auto;background:#fff">

<div class="row">
<div class="col-sm-12 subHead">
Select Filters
	</div>

		

</div>


<div class="row" style="border-bottom: 1px #eee solid;">
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
<option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
		<br/>



	<button class="btn btn-primary"  onclick="window.open('changelog/export.php?month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT DATA</button>
	

		

		<br/><br/>
	</div>
</div>

<div id="atResult"></div>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>