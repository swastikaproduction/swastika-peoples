<?php
include("../include/config.php");

$newUrl= str_ireplace("index.php", "new.php",$urltocall);
	if(isset($_GET['monthStart']))
	{
		if($_GET['type'] == 'previous')
		{
			$currentStart = $_GET['monthStart'];
			$lastStart = strtotime($currentStart) - (60*60*24*10);
			$month = date("m",$lastStart);
		 $monthStart = date("Y",$lastStart)."-".$month."-01";

		}
		else if($_GET['type'] == 'next')
		{
			$currentStart = $_GET['monthStart'];
			$lastStart = strtotime($currentStart) + (60*60*24*32);
			$month = date("m",$lastStart);
		  $monthStart = date("Y",$lastStart)."-".$month."-01";

		}
	}
	else
	{
		$month = date("m");
		$monthStart = date("Y")."-".$month."-01";
	}

 $monthEnd = date("Y-m",strtotime($monthStart));
 $monthEnd = $monthEnd."-31";


$day = date("D",strtotime($monthStart));
$weekdiffarray = Array();
$weekdiffarray['Sun'] = '0';
$weekdiffarray['Mon'] = '1';
$weekdiffarray['Tue'] = '2';
$weekdiffarray['Wed'] = '3';
$weekdiffarray['Thu'] = '4';
$weekdiffarray['Fri'] = '5';
$weekdiffarray['Sat'] = '6';


 $temp = strtotime($monthStart);

 $temp = $temp - (24 * 60 * 60 * $weekdiffarray[$day]);
 $startdate = date("Y-m-d",$temp);

 $fourtyone = strtotime($startdate)+(60*60*24*41);
 $endate = date("Y-m-d",$fourtyone);


$alreadyArray = Array();
$checkAlready = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$startdate' AND '$endate' ORDER BY `date` ASC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($checkAlready))
{
	$alreadyArray[] .= $row['date'];
	$alreadyArray[$row['date']]['type'] =  $row['type'];
	$alreadyArray[$row['date']]['name'] =  $row['name'];
	$alreadyArray[$row['date']]['id'] =  $row['id'];
}




?>
<div class="moduleHead">
<br/>
<?php
if($loggeduserid == '124')
{
	?>
<div style="float:right">
	<button class="btn btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<button class="btn btn-danger"  onclick="crossCheckDelete('calendar')">DELETE SELECTED</button>


</div>
	<?php
}
?>
<div class="moduleHeading">
Calendar
</div>
</div>

<div class="row shadow" style="background:#fff">
	<div class="col-sm-3" style="background:#fff">
	<div class="subHead">
		Holidays & Timings 
<span style="color:#1976d2;">
		@<?php echo date("M-Y",strtotime($monthStart));?>
		</span>
	</div>

	<?php

$caldata = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$monthStart' AND '$monthEnd'");
$i=1;
while($row = mysqli_fetch_array($caldata))
//foreach($caldata as $row)
{
	if($row['type'] == '1')
	{
		$classDiv = 'calCircleblue';
	}
	else
	{
		$classDiv = 'calCircleorange';	
	}
?>
<div class="calStrip" id="calStrip<?php echo $row['id'];?>">
<?php
if($row['type'] == '2')
{
?>
	<div style="float:right">
	<span class="label label-default">
			<?php echo date("h:i A",strtotime($row['from']));?> to <?php echo date("h:i A",strtotime($row['to']));?>
			</span>
	</div>

<?php	
}
?>
	<div class="<?php echo $classDiv;?>">
		<i class="fa fa-circle"></i>
	</div>

	<span style="font-size:12px;color:#96a0a5">
		<?php echo $row['date'];?>
	</span>
	<br/>

	<span style="font-size:20px;"><?php echo $row['name'];?></span>
<div style="float:right;font-size:11px;" onclick="getModule('calendar/delete.php?del=<?php echo $row['id'];?>','calStrip<?php echo $row['id'];?>','calStrip<?php echo $row['id'];?>','loading')">
<br/>

<?php
if($loggeduserid == '124')
{
	?>
		<span class="label label-danger">
		REMOVE&nbsp;&nbsp;<i class="fa fa-remove"></i>
	</span>

	<?php
}
?>

</div>


	</div>
<?php
}
?>

	


<br/><br/>
	</div>
<div class="col-sm-9" style="background:#FAFAFA;padding:20px;">
<br/>
<div>
<div style="float:left">
	<div class="btn-group">
  <button type="button" class="btn btn-default" onclick="getModule('calendar/index.php?monthStart=<?php echo $monthStart;?>&type=previous','tableDiv','formDiv','loading')">
  	<i class="fa fa-caret-left"></i>
  </button>
  <button type="button" class="btn btn-default" onclick="getModule('calendar/index.php?monthStart=<?php echo $monthStart;?>&type=next','tableDiv','formDiv','loading')">
  	<i class="fa fa-caret-right"></i>
  </button>

</div>		

</div>
	<center>
		<span style="color:#96a0a5;font-size:20px;"><?php echo date("M-Y",strtotime($monthStart));?></span>

	</center>
</div>
<br/><br/>
<div class="tabelContainer" style="height:auto;background:#fff;border:1px #eee solid;">
<div style="" >
<table border="1" class="calendar">
	<tr>
		<th>SUN</th>
		<th>MON</th>
		<th>TUE</th>
		<th>WED</th>
		<th>THU</th>
		<th>FRI</th>
		<th>SAT</th>
	</tr>
	<?php



for($k=0;$k<42;$k++)
{
	$dataPop = '';
	$sunday = '';
	$msg = "";
	if($k==0)
	{
		?>
<tr>
		<?php
	}
else if($k%7 == 0)
{
	?>
</tr><tr>
	<?php
}

if($month == date("m",strtotime($startdate)))
{
	if($startdate == date("Y-m-d"))
	{
		$class = 'today';
		$msg = 'Today';
	}
	else
	{
		$class = 'current';		
	}
}
else
{
	$class = 'past';
}

if(date("D",strtotime($startdate)) == 'Sun')
{
	$sunday = "sunday";
}



if(in_array($startdate,$alreadyArray))
{
	if($alreadyArray[$startdate]['type'] == '1')
	{
		$msg =  "LEAVE";
		$class= "holiday";
		$thisid = $alreadyArray[$startdate]['id'];
		$dataPop = 'onclick="getModal(\'calendar/date-details.php?id='.$thisid.'\',\'tableModalBig\',\'formModalBig\',\'loading\')"';
	}
	else
	{
		$msg =  "DIFF TIME";
		$class= "different";
		$thisid = $alreadyArray[$startdate]['id'];
		$dataPop = 'onclick="getModal(\'calendar/date-details.php?id='.$thisid.'\',\'tableModalBig\',\'formModalBig\',\'loading\')"';
	
	}
}




	?>
	<td class="<?php echo $sunday;?>" <?php echo $dataPop;?>>
	<span>
		<?php echo date("d",strtotime($startdate));?>
</span>
<div class="<?php echo $class;?>">
<?php echo $msg;;?>

</div>


	</td>

	<?php

		$t = strtotime($startdate);
		$t = $t+(60*60*24);
		$startdate = date("Y-m-d",$t);
}
	?>
	</tr>
</table>
</div>



</div>	


	</div>
</div>





<br/><br/><br/><br/><br/><br/><br/><br/><br/>