<?php
include("../../include/config.php");
$data = Array();
$data = getData('shift','*','name','ASC');

$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>
<div class="moduleHead">
<br/>
<div style="float:right">
	<button class="btn btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<button class="btn btn-danger"  onclick="crossCheckDelete('<?php echo $table;?>')">
	<i class="fa fa-remove"></i>&nbsp;&nbsp;DELETE SELECTED</button>


</div>
<div class="moduleHeading">
Shifts
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:20px;">#</th>
<th style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th style="width:200px;">Name</th>
<th>From Time</th>
<th>End Time</th>
<th>Working Hours</th>
<th>Notes</th>
<th style="width:200px;">Createdate</th>
</tr>

<?php
$i=1;
foreach($data as $row)
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<input type="checkbox" class="checkInput" id="" name="" value="<?php echo $row['id'];?>">
</td>
<td class="text-primary" onclick="getModule('masters/shifts/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['name'];?></td>
<td><?php echo date("h:i A",strtotime($row['starttime']));?></td>
<td><?php echo date("h:i A",strtotime($row['endtime']));?></td>
<td><?php echo $row['hours'];?> hrs</td>
<td>
	<?php echo $row['notes'];?>
</td>
<td>
<?php echo date("d/m/y h:i A",strtotime($row['createdate']));?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>