<?php
include("../include/config.php");
$data = Array();
$data = getData('designation','*','name','ASC');

$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<button class="btn btn-sm btn-danger"  onclick="crossCheckDelete('designation')">DELETE SELECTED</button>


</div>
<div class="moduleHeading">
Designations
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:20px;">#</th>
<th style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th>Name</th>
<th>Notes</th>
<th>Createdate</th>
</tr>

<?php
$i=1;
foreach($data as $row)
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<input type="checkbox" class="checkInput" id="" name="" value="<?php echo $row['id'];?>">
</td>
<td class="text-primary" onclick="getModule('designation/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['name'];?></td>
<td>
	<?php echo $row['notes'];?>
</td>
<td>
<?php echo date("d/m/y h:i A",strtotime($row['createdate']));?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>