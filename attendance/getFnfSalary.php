<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];

if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	//$typeStr = " AND `salary_release_status` = '0'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `fnfcalculation` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `created_date`") or die(mysqli_error($con));	
}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `fnfcalculation` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `created_date`") or die(mysqli_error($con));

}
//echo "SELECT * FROM `fnfcalculation` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `created_date`"; die;
?>
<div style="padding:0px;">
<?php
if($loggeduserid == '124')
{
?>
<div style="float:right;padding:20px;">
<button class="btn btn-warning" onclick="fnfReleaseSalary()">Mark As Released</button>

<button class="btn btn-warning" onclick="window.open('attendance/getFnfSalaryReport.php?emp=ALL&month='+document.getElementById('atMonth').value+'&year='+document.getElementById('atYear').value)">Export Detailed</button>
</div>
<?php
}
?>



<div class="dummyTableHeader" id="forAttendance" style="margin-bottom:0px;position:fixed;top:118px;left:0px;width:100%;padding:0px 21px 0px 15px;z-index:200;display:none">

<table class="table table-hover" cellpadding="0" cellspacing="0" style="">
<tr>
		<th style="width:5%">#</th>
		<th  style="width:5%">

		</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Salary</th>
		<th style="width:10%">Total Leave</th>
		<!-- <th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th> -->
		<th style="width:5%">Final Salary</th>
		<!-- /*<th style="width:5%">Details</th>*/ -->
		<!-- <th style="width:10%">Salary Slip</th> -->
		<th style="width:10%">Released</th>
	</tr>
</table>

</div>


<table class="table table-bordered table-hover"  cellpadding="0" cellspacing="0" id="tableDivAtView">
	<tr>
		<th style="width:5%">#</th>
		<th  style="width:5%">
			<input type="checkbox" name="" onclick="checkAll(this)">
		</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Salary</th>
		<th style="width:10%">Total Leave</th>
		<!-- <th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th> -->
		<th style="width:5%">FnF Salary</th>
		<!-- <th style="width:5%">Details</th> -->
		<!-- <th style="width:10%">Salary Slip</th> -->
		<th style="width:10%">Released</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td>
		<input type="checkbox" class="checkInput" name="" value="<?php echo $row['id'];?>" lang="<?php echo $row['empid'];?>">
	</td>
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>

	<td>
		
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['actual_salary'];?>
	</td>
	<td>
		<?php echo $row['total_leave'];?>
	</td>

	<td>
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['fnf_salary'];?>
	</td>

	<td>
		<?php if($row['salary_release_status'] == '1')
		{
			?>
				<span class="label label-primary">YES</span>
			<?php
		}
		else
		{
			?>
				<span class="label label-danger">NO</span>
			<?php

		}
		?>
	</td>

</tr>
		<?php
		$i++;
	}
	?>
</table>
<input type="text" value="<?php echo $i;?>" id="genTotal" style="display:none">
</div>
<br/><br/><br/><br/><br/>