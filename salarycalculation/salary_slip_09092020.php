<?php
include("../include/config.php");
include("convert-to-word.php");
$id = base64_decode($_GET['id']);
$getAtlog = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowLog = mysqli_fetch_array($getAtlog);
/*
if($rowLog['edited_salary'] != '')
{
	header("location:salary_slip_edited.php?id=".$id);
}
*/
$empid = $rowLog['empid'];
$month = $rowLog['month'];
$year = $rowLog['year'];
$freez_table = "freeze_".$month."_".$year;
$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$nonzmonth = $month * 1;
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$nonzmonth' AND `year` = '$year' AND `empid` = '$empid'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
}

$getEmp = mysqli_query($con,"SELECT employee.name,designation.name,employee.pf,employee.empid,employee.branch,departments.name,employee.department,employee.depttype FROM employee,designation,departments WHERE employee.designation = designation.id AND employee.id = '$empid' AND employee.department = departments.id") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

if($rowEmp['department'] == '4')
{
	$type = "Housekeeping";
}
else if($rowEmp['depttype'] == 'Cash')
{
	$type = 'Cash';
}
else if($rowEmp['pf'] == '1')
{
	$type = "PF";
}
else
{
	$type = $rowEmp['depttype'];
}

$branchid = $rowEmp[4];
$getBranch = mysqli_query($con,"SELECT `name`,`notes` FROM `branch` WHERE `id` = '$branchid'") or die(mysqli_error($con));
$rowBranch = mysqli_fetch_array($getBranch);
$branch = $rowBranch[0];
$branchaddress = $rowBranch[1];

/*
$getData = mysqli_query($con,"SELECT * FROM `bifurcation` WHERE `last` < '$salary' AND `this` >= '$salary'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
*/

$monthArray = Array();
$monthArray['01'] = 'January';
$monthArray["02"] = 'February';
$monthArray['03'] = 'March';
$monthArray['04'] = 'April';
$monthArray['05'] = 'May';
$monthArray['06'] = 'June';
$monthArray['07'] = 'July';
$monthArray['08'] = 'August';
$monthArray['09'] = 'September';
$monthArray['10'] = 'October';
$monthArray['11'] = 'November';
$monthArray['12'] = 'December';


$pfyes=$rowEmp[2];
$mth = "2018-".$rowLog['month']."-01";
//include("bifur-logic.php");


//$month = "0".$month;
if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.depttype != 'Cash' AND employee.id = '$empid'   ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else if($type == 'Cash')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = 'Cash' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid'  ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid'   ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}

$row= mysqli_fetch_array($getData);


$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($rowTable = mysqli_fetch_array($getTables))
{
	$tables[] = $rowTable[0];
}


if(in_array($freez_table,$tables)){
$getFreezedData = mysqli_query($con,"SELECT `ctc`, `gross`, `basic`, `pfpayable`, `da`, `pfemployer`, `otherdeduction`, `bonus`, `hra`, `refreshment`,`hold`,`loan`,`jvhold`,`leavedeductions`,`calculatedsalary`,`totalearnings`, `totaldeductions`, `netpay`,`pt` from `$freez_table` WHERE empid='$row[6]'") or die(mysqli_error($con));
$rowFreezed = mysqli_fetch_array($getFreezedData);

$grosssalary = $rowFreezed['ctc'];
$gross = $rowFreezed['gross'];
$leavedeductions = $rowFreezed['leavedeductions'];
$hold = $rowFreezed['hold'];
$loan = $rowFreezed['loan']; 
$jvhold = $rowFreezed['jvhold']; 
$calculatedsalary = $rowFreezed['calculatedsalary'];
$basic = $rowFreezed['basic'];
$da = $rowFreezed['da'];
$hra = $rowFreezed['hra'];
$bonus = $rowFreezed['bonus'];
$refreshment = $rowFreezed['refreshment'];
$pfemployer = $rowFreezed['pfemployer'];
$pfpayable = $rowFreezed['pfpayable'];
$otherdeduction = $rowFreezed['otherdeduction'];
$pt = $rowFreezed['pt'];
$netpay = $rowFreezed['netpay'];
$totalearnings = $rowFreezed['totalearnings'];
$totaldeductions = $rowFreezed['totaldeductions'];
}
else{
//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}

$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;

if($row[7] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}




if($row['pt']=='1'){

	$pt = $pt;
}else{
	$pt = 0;
}

$odflag = $row['od'];


if($pfflag == '1')
{

		if($calculatedsalary < 9001)
		{ 

			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$pfpayable = round($gross * 0.24);		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
            $otherdeduction=0;
			$pfemployer = round($pfpayable/2);	
			$basic = $ctc - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;

			$netpay = $totalearnings - $totaldeductions;
		}	

		else if($calculatedsalary >= 9001 && $calculatedsalary < 15000)
		{    
			
			$ctc = $calculatedsalary;
			$basic = 8400;
			$hra = 0;
			$otherdeduction=0;
			
			if($pfpayable == '')
			{
			    $pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}		
			$pfemployer = round($pfpayable/2);			
			$gross = $ctc-$pfemployer;
			$bonus = $ctc - ($basic+$pfemployer);
			$refreshment=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary >= 15000 && $calculatedsalary <= 20000)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = max(round($gross * 0.60),8400);
			$hra = round($basic*0.40);
			$otherdeduction=0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = round($ctc*0.10);
		    $refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary >= 20001 && $calculatedsalary <= 26800)
		{   $ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($calculatedsalary * 0.10);
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($calculatedsalary*0.025);
			}else{
              $otherdeduction=0;
			}
			
			$pfpayable = round($basic * 0.24);
		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}

		else if($calculatedsalary >26800)
		{   $ctc = $calculatedsalary;
			if($pfpayable == '')
			{
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.10);
			$pfpayable = round($basic * 0.24);
			}
			else
			{
            $gross = $ctc;
			$basic = round($ctc * 0.52);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.12);
			}
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($ctc*0.025);
			}else{
              $otherdeduction=0;
			}		
			
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9400)
	{
		    $ctc = $calculatedsalary;	
		    $gross = $ctc;	    
			$basic = min($ctc,8400);
			$hra = 0;
			$da = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - $basic;
			$refreshment=0;
			$otherdeduction=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;

	}else if($calculatedsalary >= 9400 && $calculatedsalary <= 15000)
	{       $ctc = $calculatedsalary;
		    $gross = $ctc;
			$basic = max(round($ctc*0.60),8400);
			$pfpayable = 0;
			$pfemployer = 0;
			$refreshment=0;
			$da=0;
			$otherdeduction=0;
			$bonus = round($ctc * 0.10);
			$hra = $ctc - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000 &&  $calculatedsalary <=20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($ctc * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			$otherdeduction=0;
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}
	else if( $calculatedsalary >20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($calculatedsalary * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			//other deduction condition when flag is 1
			if($odflag=='1'){
            $otherdeduction= round($calculatedsalary*0.025);
			}else{
            $otherdeduction=0;
			}
			
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra+$otherdeduction);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns.
			$netpay = $totalearnings - $totaldeductions;
	}
}


}

?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Salary Slip
	</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>

<style type="text/css">
body
{
	overflow: auto !important;
}
	.inTab th
	{
		background:#999;
		color: #fff;
		font-weight: normal;
		text-align: left;
	}

	.inTab tr th:nth-child(2),.inTab tr th:nth-child(4)
	{
		text-align: right;
	}

	.inTab tr td:nth-child(1), .inTab tr td:nth-child(3)
	{
		font-weight: normal;
	}

	.inTab tr td:nth-child(2)
	{
		border-right:1px #eee solid;
		text-align: right;
	}

	.inTab tr td:nth-child(4)
	{
		text-align: right;
	}
	.inTab td
	{
		border-bottom:1px #eee solid;
	}
</style>

<script type="text/javascript">
	
	function printDoc()
	{
		document.getElementById('ptBox').style.display = 'none';
		window.print();
		setTimeout(function(){
			document.getElementById('ptBox').style.display = 'block';
		},300);
	}
</script>
</head>
<body>
<div style="position:fixed;top:20px;right:20px;" id="ptBox">
<?php
if($loggeduserid == '124')
{
?>

	<button style="background:rgba(0,0,0,0.5);color:#fff;border:0px;border-radius:2px;" onclick="window.location = 'edit_slip.php?id=<?php echo $id;?>'">EDIT</button>
	&nbsp;&nbsp;

<?php	
}
?>
	<button style="background:rgba(0,0,0,0.5);color:#fff;border:0px;border-radius:2px;" onclick="printDoc();">PRINT</button>

</div>


<center>

	<div style="width:900px;padding:40px;">
		<table cellpadding="10" cellspacing="0" style="width:100%;border:1px #eee solid;">
		<tr>
			<td colspan="2" align="center" style="border-bottom:1px #eee solid;font-size:20px;">
				<strong>Payslip for the Month of <?php echo $monthArray[date("m",strtotime($mth))];?>, <?php echo $rowLog['year'];?></strong>
			</td>
		</tr>
			<tr>
				<td style="width:50%;text-align:left;border-right:1px #eee solid;">
					<table style="width:100%" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<td>Employee Name</td><td style="border-right:0px;font-weight:bold"><?php echo $rowEmp[0];?></td>
						</tr>
<tr>
						<td>
							Department
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[5];?>
						</td>
							
						</tr>						
<tr>
						<td>
							Designation
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[1];?>
						</td>
							
						</tr>
						<tr>
						<td>
							Employee Code
						</td>
						<td style="border-right:0px;font-weight:bold">
							<?php echo $rowEmp[3];?>
						</td>
							
						</tr>

<tr>
						<td style="border-bottom:0px;">
Branch
						</td>
						<td style="border-right:0px;border-bottom:0px;font-weight:bold">
							<?php echo $branch;?>
						</td>
							
						</tr>

					</table>
				</td>
				<td style="text-align:center">
					<img src="https://www.swastika.co.in/public/webtheme/images/swastika-logo.svg" style="height:40px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;border-top:1px #eee solid">
				<br/><br/>
					<table style="width:100%;" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<th style="width:25%">Earnings & Remuneration</th>
							<th style="width:25%">Amount (Rs.)</th>
							<th style="width:25%">Deductions</th>
							<th style="width:25%">Amount (Rs.)</th>
						</tr>
<tr>
							<td>Earned Basic</td>
							<td><?php

							echo $basic;
							?>
							</td>
							<td>PF(Employer & Employee)</td>
							<td>
								<?php echo $pfpayable;?>
								
							</td>
						</tr>
						<tr>
							<td>Dearness Allowance</td>
							<td>
<?php echo $da;?>							</td>
							<td>TDS</td>
							<td><?php echo $jvhold;?></td>
						</tr>
						<tr>
							<td>HRA</td>
							<td>
								<?php echo $hra;?>

							</td>
							<td>Professional Tax</td>
							<td>
								<?php echo $pt;?>
							</td>
						</tr>
						<tr>
							<td>Bonus</td>
							<td>
								<?php echo $bonus;?>

							</td>
							<td>Performance Hold</td>
							<td><?php echo $hold;?></td>
						</tr>
						<tr>
							<td> Employee Daily Refreshment</td>
							<td><?php echo ($refreshment+$otherdeduction);?></td>
							<td>Loan/EMI Deductions</td>
							<td><?php echo $loan;?></td>

						</tr>
						
						<tr>
							<td>PF Employee</td>
							<td><?php echo $pfemployer;?></td>
							<td>Training Impartment Contribution</td>
							<td><?php echo $otherdeduction;?></td>
													
						</tr>

					
						<tr>
							<td style="font-weight:bold">Total Earning</td>
							<td  style="font-weight:bold"><?php echo $basic+$da+$hra+$bonus+$refreshment+$pfemployer+$otherdeduction;?></td>
							<td style="font-weight:bold">Total Deductions</td>
							<td  style="font-weight:bold"><?php echo $pfpayable+$jvhold+$loan+$pt+$otherdeduction;?></td>
						</tr>
						<tr>
						    <td>Leave Deductions</td>
							<td><?php echo $leavedeductions;?></td>
<td></td>
<td></td>
						    
						</tr>
						<tr>
							<td>Monthly CTC</td>
							<td  style="font-weight:bold"><?php echo $grosssalary;?></td>
							<td></td>
							<td></td>
	</tr>
						
						<tr>
							<td>Net Pay</td>
							<td style="font-weight:bold;border-top:1px #eee solid;" colspan="3"><?php echo $netpay= ($basic+$da+$hra+$bonus+$refreshment+$pfemployer+$otherdeduction) - ($pfpayable+$jvhold+$loan+$pt+$otherdeduction);?></td>
						</tr>
						<tr>
							
							<td style="font-weight:bold;border-top:1px #eee solid;text-align:right" colspan="4"><span style="text-transform:capitalize">
					<?php echo str_ireplace("thousands","thousand",convert_to_word($netpay));?> Rupees
					</span></td>
						</tr>
						<tr>
							<td colspan="4" style="border-top:1px #eee solid;">
								*This salary (CTC) is subject to achievement of all the targets and submission of reports. In case the same is not achieved or submitted then the salary shall be paid on pro-data basis or the conditions mentioned in the offer letter.
							</td>
						</tr>

					</table>
				</td>
			</tr>
		
			<tr>
				<td style="border-top:1px #eee solid;border-right:1px #eee solid;text-align:left;font-weight:bold">
				For Swastika Investmart Ltd
				<br/><br/><br/><br/>
				<span style="font-size:10px">
				Authorized Signatory
				</span>
					
				</td>
				<td style="text-align:left;border-top:1px #eee solid;">
				<strong style="font-size:16px;font-weight:bold">Swastika Investmart Ltd.</strong>
				<br/>
				<strong style="font-size:13px;font-weight:bold">
Head Office</strong>
				<br/>
				<i class="fa fa-map-marker"></i>&nbsp;&nbsp;48, Jaora Compound, M.Y.H. Road, Indore - 452 001<br/>
<i class="fa fa-phone"></i>&nbsp;&nbsp;0731 - 6644000, <i class="fa fa-globe"></i>&nbsp;&nbsp;www.swastika.co.in<br/>
				<strong style="font-size:13px;font-weight:bold">Reporting Branch</strong>
				<br/>
					<?php echo $branchaddress;?>
				</td>
			</tr>
		</table>
	</div>
	<div style="text-align:left;width:900px;padding:20px;padding-top:0px;">
	<span style="font-size:13px">This is a computer generated salary slip, hence no signature is required.</span>
	</div>
	<br/>
	<br/>
</center>
</body>
</html>
