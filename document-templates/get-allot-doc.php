<?php
include("../include/config.php");
$id = $_GET['id'];
$empid = $_GET['empid'];
$getdata = mysqli_query($con,"SELECT * FROM `documenttemplates` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getdata);
$data = $row['data'];

$getEmployee = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
	$rowEmployee = mysqli_fetch_array($getEmployee);
	$string = 'name,mobile,phone,workphone,email,workemail,doj,dob,address,shift,imdboss,acno,ifsc,bankpref';
	$stringArray = explode(",",$string);
	foreach($stringArray as $val)
	{
		$tocheck = '<span style="background-color:#d0e5f2">{#'.$val.'#}</span>';
		$thisvalue = $rowEmployee[$val];
		$data = str_ireplace($tocheck,$thisvalue, $data);
	}

	$department = getData('departments',"**","id",$rowEmployee['department']);
	$tocheck = '<span style="background-color:#d0e5f2">{#department#}</span>';
	$data = str_ireplace($tocheck,$department[0]['name'], $data);

	$designation = getData('designation',"**","id",$rowEmployee['designation']);
	$tocheck = '<span style="background-color:#d0e5f2">{#designation#}</span>';
	$data = str_ireplace($tocheck,$designation[0]['name'], $data);

	$branch = getData('branch',"**","id",$rowEmployee['branch']);
	$tocheck = '<span style="background-color:#d0e5f2">{#branch#}</span>';
	$data = str_ireplace($tocheck,$branch[0]['name'], $data);


	$shift = getData('shift',"**","id",$rowEmployee['shift']);
	$tocheck = '<span style="background-color:#d0e5f2">{#shift#}</span>';
	$data = str_ireplace($tocheck,$shift[0]['name'], $data);


	$imdboss = getData('employee',"**","id",$rowEmployee['imdboss']);
	$tocheck = '<span style="background-color:#d0e5f2">{#imdboss#}</span>';
	$data = str_ireplace($tocheck,$imdboss[0]['name'], $data);

	$tocheck = '<span style="background-color:#d0e5f2">{#salary#}</span>';
	$toreplace= "Rs. ".$rowEmployee['salary']."/-";
	$data = str_ireplace($tocheck,$toreplace, $data);

	$tocheck = '<span style="background-color:#d0e5f2">{#salary#}</span>';
	$toreplace= "Rs. ".$rowEmployee['salary']."/-";
	$data = str_ireplace($tocheck,$toreplace, $data);

function convertNumberToWordsForIndia($number){
    //A function to convert numbers into Indian readable words with Cores, Lakhs and Thousands.
    $words = array(
    '0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five',
    '6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten',
    '11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen',
    '16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty',
    '30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy',
    '80' => 'eighty','90' => 'ninty');
    
    //First find the length of the number
    $number_length = strlen($number);
    //Initialize an empty array
    $number_array = array(0,0,0,0,0,0,0,0,0);        
    $received_number_array = array();
    
    //Store all received numbers into an array
    for($i=0;$i<$number_length;$i++){    
  		$received_number_array[$i] = substr($number,$i,1);    
  	}
    //Populate the empty array with the numbers received - most critical operation
    for($i=9-$number_length,$j=0;$i<9;$i++,$j++){ 
        $number_array[$i] = $received_number_array[$j]; 
    }
    $number_to_words_string = "";
    //Finding out whether it is teen ? and then multiply by 10, example 17 is seventeen, so if 1 is preceeded with 7 multiply 1 by 10 and add 7 to it.
    for($i=0,$j=1;$i<9;$i++,$j++){
        //"01,23,45,6,78"
        //"00,10,06,7,42"
        //"00,01,90,0,00"
        if($i==0 || $i==2 || $i==4 || $i==7){
            if($number_array[$j]==0 || $number_array[$i] == "1"){
                $number_array[$j] = intval($number_array[$i])*10+$number_array[$j];
                $number_array[$i] = 0;
            }
               
        }
    }
    $value = "";
    for($i=0;$i<9;$i++){
        if($i==0 || $i==2 || $i==4 || $i==7){    
            $value = $number_array[$i]*10; 
        }
        else{ 
            $value = $number_array[$i];    
        }            
        if($value!=0)         {    $number_to_words_string.= $words["$value"]." "; }
        if($i==1 && $value!=0){    $number_to_words_string.= "Crores "; }
        if($i==3 && $value!=0){    $number_to_words_string.= "Lakhs ";    }
        if($i==5 && $value!=0){    $number_to_words_string.= "Thousand "; }
        if($i==6 && $value!=0){    $number_to_words_string.= "Hundred &amp; "; }            
    }
    if($number_length>9){ $number_to_words_string = "Sorry This does not support more than 99 Crores"; }
    return ucwords(strtolower("Rupees ".$number_to_words_string)." Only.");
}
 
    
 	$tocheck = '<span style="background-color:#d0e5f2">{#salaryinwords#}</span>';
 	$salary = str_ireplace(",", "",$rowEmployee['salary']);
 	$salary = str_ireplace(" ", "", $salary);
	$toreplace= convertNumberToWordsForIndia($salary);
	$data = str_ireplace($tocheck,$toreplace, $data);


	$tocheck = '<span style="background-color:#d0e5f2">{#currentdate#}</span>';
	$toreplace= date("d-M-Y");
	$data = str_ireplace($tocheck,$toreplace, $data);


//echo "56721351.61 = " . convertToIndianCurrency(56721351.61);

/*
department
designation
branch
shift
imdboss
*/
?>
<form id="prevform" name="prevform" action="document-templates/preview.php" target="_blank" method="post">
<textarea name="editor1" lang="editor" id="doctemp0" style="height:1000px;"><?php echo $data;?></textarea>
</form>
