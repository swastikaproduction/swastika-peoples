<style type="text/css">
  .top-margin{
     margin-top: 15px;
  }
  .secmargin{      
    margin-left:60px;
    color: #2dda2a;
  }
  .col{
  	color: #2532d6;
  }
  .sec{
  	background: white; height: auto;
  }
  .lab{
  	color: #d43319;
  }  
</style>
<section>
	<?php 
	include'../include/config.php';
	$appId = $_GET['app_id'];
	$submitted = $_GET['missubmitted'];
	$sql = mysqli_query($con,"select * from appraisals where id='$appId'") or die(mysqli_error($con));
	$row = mysqli_fetch_array($sql);
	$empId = $_GET['emp_id'];       
    $query1 = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
    $finalscorresult = mysqli_fetch_array($query1);
	?>
	<div class="row">
		<div class="col-md-12">
			<p class="col">Appraisal start for <?php echo $row['appraised_time'] ?> <?php echo $row['appraised_year'] ?></p>
		</div>		
	</div>
</section>
<section class="sec">
	<form action="appraisal/hodAsmanagerAppraisal.php" method="post" enctype="multipart/form-data">
    <div class="row">
  	<div class="col-md-6">
  		<?php 
		 $query = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
         while($result = mysqli_fetch_array($query)){
         	?>
  		<div class="row">
	<div class="col-md-4">
		
		<label class="top-margin">Subject</label>
		<input type="hidden" name="empId" value="<?php echo $empId;?>">
		<input type="hidden" name="appId" value="<?php echo $appId;?>">
		<input type="hidden" name="kpid[]" value="<?php echo $result['id'];?>">
		<textarea  type="text" class="form-control" name="subject" readonly=""><?php echo $result['h_subject'] ?></textarea>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label class="top-margin">Target</label>
		<textarea type="text" class="form-control" name="target"  readonly=""><?php echo $result['h_target'] ?></textarea>
	</div>
	<div class="col-md-2">
		<label class="top-margin">Achievement</label>
		<textarea type="text" class="form-control" name="achievement[]" readonly=""><?php echo $result['achievement'] ?></textarea>
	</div>
	<div class="col-md-2">
		<label class="top-margin">Weightage</label>
		<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'] ?>" readonly="">
	</div>
	<div class="col-md-2">
		<label class="top-margin">Your Score</label>
		<input type="text" class="form-control allownumeric" data-weight ="<?php echo $result['h_weightage'];?>" name="yourscore[]" value="<?php echo $result['score'] ?>" id="yscore" readonly="">		
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<label class="top-margin">Description</label>
		<textarea type="text" class="form-control" name="description" value="" readonly=""><?php echo $result['h_description'] ?></textarea>		
	</div>
	<div class="col-md-4">
		<label class="top-margin">Your Comment's</label>
		<textarea type="text" class="form-control" name="yourcomment[]" readonly=""><?php echo $result['comments'] ?></textarea>		
	</div>
 </div>

 <?php
}
 ?>
 <div class="row">
	<div class="col-md-4">
		<label class="top-margin lab">Final Comment's</label>
		<textarea type="text" class="form-control" name="finalcommnets" readonly=""><?php echo $finalscorresult['finalcomment'] ?></textarea>		
	</div>
	<div class="col-md-4">
		<label class="top-margin lab">Final Score</label>
		<input type="text" class="form-control allownumeric"  name="finalscore" value="<?php echo $finalscorresult['finalscore'] ?>" readonly="">		
	</div>
  	</div>
  	</div>
  	<div class="col-md-6">
  		<?php 
		 $query = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
         while($result = mysqli_fetch_array($query)){
         	?>
    <div class="row">
	<div class="col-md-4">
		<label class="top-margin">Subject</label>
		<input type="hidden" name="appId" value="<?php echo $appId;?>">
		<input type="hidden" name="mkpid[]" value="<?php echo $result['id'];?>">
		<textarea  type="text" class="form-control" name="subject"  readonly=""><?php echo $result['h_subject'] ?></textarea>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label class="top-margin">Target</label>
		<textarea type="text" class="form-control" name="target" value="" readonly=""><?php echo $result['h_target'] ?></textarea>
	</div>
	<div class="col-md-2">
		<label class="top-margin">Achievement</label>
		<textarea type="text" class="form-control" name="machievement[]" <?php if($submitted=='msubmit'){ echo'readonly';}else{} ?> ><?php echo $result['machievement'] ?></textarea>
	</div>
	<div class="col-md-2">
		<label class="top-margin">Weightage</label>
		<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'] ?>" readonly="">
	</div>
	<div class="col-md-2">
		<label class="top-margin">Your Score</label>
		<input type="text" class="form-control allownumeric yourscore" data-weight ="<?php echo $result['h_weightage'];?>" value="<?php echo $result['mscore']; ?>" name="mscore[]" id="yscore" <?php if($submitted=='msubmit'){ echo'readonly';}else{} ?>>		
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<label class="top-margin">Description</label>
		<textarea type="text" class="form-control" name="description" value="<?php echo $result['h_description'] ?>" readonly=""><?php echo $result['h_description'] ?></textarea>		
	</div>
	<div class="col-md-4">
		<label class="top-margin">Your Comment's</label>
		<textarea type="text" class="form-control" value="<?php echo $result['mcomment']; ?>" name="mcomment[]" <?php if($submitted=='msubmit'){ echo'readonly';}else{} ?> ><?php echo $result['mcomment']; ?></textarea>		
	</div>

 </div>
 <div class="row">
	<div class="col-md-4">
		<label class="top-margin">Supporting Document</label>
		<input type="file" class="form-control" name="mkpifile[]">
	</div>
	<div class="col-md-4">
		<label class="top-margin">Employee Document</label>
		<a href="kpiImages/<?php echo $result['supportDocument'] ?>" class="form-control" style="color: #38e835;" download> Download</a>
	</div>
</div>
  <?php
}
 ?>
 <div class="row">
	<div class="col-md-4">
		<label class="top-margin lab">Final Comment's</label>
		<textarea type="text" class="form-control" name="mfinalcommnets" <?php if($submitted=='msubmit'){ echo'readonly';}else{} ?>><?php echo $finalscorresult['mfinalcomment']; ?></textarea>		
	</div>
	<div class="col-md-4">
		<label class="top-margin lab">Final Score</label>
		<input type="text" class="form-control allownumeric" value="<?php echo $finalscorresult['mfinalscore']; ?>"  name="mfinalscore" id="fscore" readonly="">		
	</div>
  	</div>
  	</div>

<div class="row">
	<?php
 $empId = $_GET['emp_id'];;
 $check = mysqli_query($con,"select * from appraisalusers where empId='$empId'");
 $checkresult = mysqli_fetch_array($check);
 $status =$checkresult['status']; 
 if( $status !=='msubmit' && $status !=='hsubmit' ){

 	?>
	<div class="col-md-12">
		<input type="submit" class="btn btn-success top-margin" name="submit" value="Save Draft">&nbsp;&nbsp;<input type="submit" class="btn btn-primary top-margin" name="submit" value="Submit">&nbsp;&nbsp;<input type="reset" class="btn btn-info top-margin" name="cancel" value="Cancel">
	</div>

<?PHP }?>
</div>

</form>
</section>
 