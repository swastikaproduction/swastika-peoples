<?php
include("../apiconfig.php");
$month = $_GET['month'];
if($month == '01')
{
$monthStart = "2017-12-26";
}
else
{
    $t = $month -1;
    $t = "0".$t;
$monthStart = date("Y")."-".$t."-26";    
}

$monthEnd = date("Y")."-".$month."-25";




function createDateRangeArray($strDateFrom,$strDateTo)
{
 
    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}
$daterange = createDateRangeArray($monthStart,$monthEnd);
//$categories = implode(",",$daterange);

$seriesData = Array();
foreach($daterange as $val)
{
    $seriesData[$val] = '0';
}


$tableName = 'attendance_'.$month.'_'.date("Y");
$empid = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid' ORDER BY `date` DESC") or die(mysqli_error($con));
//$categories = '';
while($row = mysqli_fetch_array($getData))
{
//$categories .= "'".$row['date']."',";
    $seriesData[$row['date']] = $row['deduction'];

}

foreach($seriesData as $key => $val)
{
    $deduction .= $val.",";
    $tdate = date("D, d-M-y",strtotime($key));
    $categories .= "'".$tdate."',";
}

$deduction = substr($deduction,0,-1);
$categories = substr($categories,0,-1);
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Highcharts Demo</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">



    <link rel="stylesheet" type="text/css" href="/css/result-light.css">
<style type="text/css">
     rect
    {
        fill:transparent;
    }


.highcharts-yaxis-grid path
{
stroke:#fff;
stroke-width:0;
}

.highcharts-subtitle
{
    display: none;
}

.highcharts-contextbutton
{
    display: none;
}
.highcharts-credits
{
    display: none;
}

.highcharts-yaxis-labels
{
   display:none;
}
.highcharts-xaxis-labels
{
   display:none;
}

.highcharts-xaxis
{
    display: none;
}
</style>  
</head>
<body style="">
  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="container" style="width:100%; height: 300px; margin: 0 0 !important"></div>

    <script type="text/javascript">

    //<![CDATA[
        Highcharts.chart('container', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: ''
    },
    
    xAxis: {
        offSet:0,
        categories: [
        <?php echo $categories;?>
        ]
    },
    yAxis: {
        title: {
            text: ' '
        },
        min:0,
        max:1
    },
    tooltip: {
        shared: true,
        valueSuffix: ' of the day.'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5,
                    marker: {
            enabled: true
        }

        },
    },
    series: [{
        name: 'Deduction ',
        data: [<?php echo $deduction;?>]
    }]
});
    //]]>

</script>
  <script>
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: ""
      }], "*")
    }
  </script>
</body>
</html>
