
<!-- Main content -->
<section class="content" style="background: #fff;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<h3 class="box-title">Profile Types <a  data-toggle="modal" data-target="#myModal11" class="btn btn-success" style="float: right;">Create New</a></h3>
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.no</th>
<th>Profile Name</th>
<th>Action</th>
<th>Action</th>       

</tr>
</thead>
<tbody>
<?php  
$count=1;
include'../include/config.php';
$query = mysqli_query($con,"select * from profile_type");
while($row=mysqli_fetch_array($query)){
?>

<tr>

<td><?php echo $count++;?></td>
<td><?php echo $row['profile_name'];?></td>
<td><a href="" data-id="<?php echo $row['id'];?>" id="<?php echo $row['id'];?>" data-toggle="modal" data-target="#myModal12" onclick="profile_id('<?php echo $row['id'];?>');">Edit</a>             <!-- Modal -->
</td>
<td><span onclick="active_profile(<?php echo $row['id'];?>);"><?php echo $row['status'];?></span>             <!-- Modal -->
</td>

</tr> 
<?php
}?>                      
<div class="modal fade" id="myModal12" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Profile Update </h4>
</div>
<div class="modal-body">
<div class="row">
<form action="appraisal/profile_name_update.php" method="post">
<div class="col-md-6">

<label>Profile Name</label>
<input type="hidden" name="pid" id="pid">
<input type="text" name="profile_name" class="form-control" id="profile_name" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">

</div>
</div>

</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="profile_update();">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog">    
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Profile Type</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<label>Profile Name</label>
<input type="text" name="profile_name" class="form-control" id="profile_name1" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">

</div>
</div>

</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="profile_type1();">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>      
</div>         
</div>
</div>
</div>
</div>
<div class="modal fade" id="myModal14" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;width: 296px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<?php
if($row['status'] == 'Active'){

$s = 'Are you sure want to Deactive';
}else{

$s ='Are you sure want to Active';
}
?>
<h4 class="modal-title"><?php echo $s;?></h4>
</div>

<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="active_profile('<?php echo $row['id'];?>');">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
</tbody>

</table>
</div>

</div></section>
</div>
<!-- DataTables -->
<script src="../scripts/jquery.dataTables.min.js"></script>
<script src="../scripts/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
$('#example1').DataTable()
$('#example2').DataTable({
'paging'      : true,
'lengthChange': true,
'searching'   : true,
'ordering'    : true,
'info'        : true,
'autoWidth'   : true,
})
})
</script>
