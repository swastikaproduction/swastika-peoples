<div id="myModal" class="modal fade" role="dialog"   style="z-index:200000">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div id="tableModal">
      </div>
      <div id="formModal"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModalBig" class="modal fade" role="dialog"  style="z-index:200000">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius:0px !important">
    <div id="tableModalBig">
      </div>
      <div id="formModalBig"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="massEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div id="modalData"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="notWorking" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <center style="padding:20px;">
    No Data Available      
    </center>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>



<div style="position:fixed;bottom:-3000px;left:0px;z-index:10000;width:100%;height:800px;background:#fff;" id="bottomDivContainer">
<div style="position:absolute;top:0px;right:0px;height:50px;width:50px;background:#40596b;text-align:center;z-index:200;color:#fff;padding-top:15px;" onclick="closeBottom();">
  <i class="fa fa-remove" style="font-size:20px;color:#fff;"></i>
</div>
<div id="bottomDiv"></div>
</div>  

<div style="position:absolute;bottom:20px;right:20px;z-index:200000;display:none" onclick="hideProcessing();" id="stopLoad">
  <button class="btn btn-sm btn-primary primary-border">
    Request take longer than expected. Click to stop.
  </button>
</div>



<div class="loading" id="loading">

<div style="width:100%;display:inline-block;text-align:left">
 <div style="width:100%;height:7px;background:#1976d2;text-align:left;">
    <div style="width:30%;height:7px;background:#0e4f8c;display:inline-block" id="loadbar"></div>
  </div>
</div>
</div>


