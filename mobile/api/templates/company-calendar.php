<?php
include("../apiconfig.php");
 $monthEnd = date("Y-m",strtotime($monthStart));
 $monthEnd = $monthEnd."-31";
$caldata = mysqli_query($con,"SELECT * FROM `calendar` ORDER BY `date` DESC");
$class[1] = 'fa fa-calendar-minus-o';
$class[2] = 'fa fa-calendar-check-o';

$color[1] = '#e13192';
$color[2] = '#6db3f1';
$type[1] = 'Holiday';
$type[2] = 'Different Timing';

?>

<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">


<div style="padding:0px;">


<div style="position:fixed;top:20px;left: 29px;background:#bddaf5;width: 10px;height:1000px;z-index:1;border-radius:20px;opacity:0.5"></div>
<div class="" style="height:auto;background:#fff;z-index:10">
<?php
while($row= mysqli_fetch_array($caldata))
{
	?>

<div class="calStrip" style="padding:15px 10px;" onclick="displayModal('templates/company-calendar-details.php?id=<?php echo $row['id'];?>')">

<table style="width:100%">
	<tbody><tr>
		<td class="timeDotTd">
		<div class="timeDotHolder">
		<i class="<?php echo $class[$row['type']];?>" style="color:<?php echo $color[$row['type']];?>"></i>			
		</div>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
	<span class="label label-default">
			<?php echo $type[$row['type']];?>
			</span>
	</div>	
	<span style="font-size:16px;font-weight:400;text-transform:capitalize">
		<?php echo date("D, d-M",strtotime($row['date']));?>	</span>
	<br>
<div style="margin-top:5px;border-bottom:1px #">
<?php echo $row['name'];?></div>
<br/>
<div class="halfBorder"></div>
		</td>
	</tr>
</tbody></table>


</div>
	<?php
}
?>

</div>
</div>
<br/><br/><br/><br/>
<br/><br/><br/><br/>

</div>
</div>

