<?php
include("../include/config.php");
$post = Array();
foreach($_POST['tosendparams'] as $val)
{
	$val = str_ireplace('"','',$val);
	$val = str_ireplace("'","",$val);
	$post[] .= $val;
}

/*

mysqli_query($con,"DELETE FROM `attendancelog` WHERE `month` = '$post[0]' AND `year` = '$post[1]'") or die(mysqli_error($con));
echo "INSERT INTO `attendancelog` (`id`, `month`, `year`, `file`, `createdate`, `updatedby`) VALUES ('', '$post[0]', '$post[1]', '$post[2]', '$datetime', '$loggeduserid')";

mysqli_query($con,"INSERT INTO `attendancelog` (`id`, `month`, `year`, `file`, `createdate`, `updatedby`) VALUES ('', '$post[0]', '$post[1]', '$post[2]', '$datetime', '$loggeduserid')") or die(mysqli_error($con));
*/
$month = $post[0];
$year  = $post[1];
if($month < 10)
{
$month = "0".$month;
}

$tableName = 'attendance_'.$month.'_'.$year;


$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($row = mysqli_fetch_array($getTables))
{
	$tables[] = $row[0];
}


if(!in_array($tableName,$tables))
{
	mysqli_query($con,"CREATE TABLE IF NOT EXISTS `".$tableName."` (
  `id` int(11) NOT NULL,
  `empid` int(11) NOT NULL,
  `date` date NOT NULL,
  `intime` time NOT NULL,
  `outime` time NOT NULL,
  `hours` varchar(100) NOT NULL,
  `allotedshifthours` varchar(20) NOT NULL,
  `shiftend` time NOT NULL,
  `difference` varchar(100) NOT NULL,
  `late` varchar(100) NOT NULL,
  `consecutive_counter` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `twohourscounter` varchar(100) NOT NULL,
  `twohourvalue` varchar(20) NOT NULL,
  `fifteenminscounter` varchar(100) NOT NULL,
  `deduction` varchar(100) NOT NULL,
  `remarks` text NOT NULL,
  `forced` varchar(20) NOT NULL,
  `forceremarks` varchar(20) NOT NULL,
  `edited` int(1) NOT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1") or die(mysqli_error($con));


mysqli_query($con,"ALTER TABLE `".$tableName."` ADD PRIMARY KEY (`id`)") or die(mysqli_error($con));
mysqli_query($con,"ALTER TABLE `".$tableName."` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT") or die(mysqli_error($con));

}


if($post[3] == 'MATRIX')
{
include("allotattendance.php");  
}
else
{
  include("allotattendance-elss.php");
}



?>