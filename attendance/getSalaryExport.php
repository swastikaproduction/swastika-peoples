<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	$typeStr = " AND `status` = '1'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));	
}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));

}


$name = "Salary_Cumulative.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>


<table border="1">
	<tr>
		<th>Employee Name</th>
		<th>Employee Code</th>
		<th>Month-Year</th>
		<th>Salary</th>
		<th>Total Deduction</th>
		<th>Pool Opening</th>
		<th>Pool Closing</th>
		<th>Final Salary</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>

	<td>
		
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['actual_salary'];?>
	</td>
	<td>
	
	<?php
	if($row['status'] == '1')
	{
		echo $row['deduction'];
	}
	?>
			<?php
if($row['pool_deduction'] != '')
{
?>
<span style="font-size:11px;">
		(<?php echo $row['pool_deduction'];?> from pool)
		</span>
<?php	
}
		?>

	
	</td>
	<td>
		<?php echo $row['pool_opening'];?>
	</td>
	<td>
		<?php echo $row['pool_opening'] - $row['pool_deduction'];?>

	</td>

	<td>
	<?php
	if($row['edited_salary'] == '')
	{
?>
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['final_salary'];?>
<?php		
}
else
{
	?>
		<i class="fa fa-rupee"></i>&nbsp;<?php echo $row['edited_salary'];?>
	<?php
}
	
	?>


	</td>



</tr>
		<?php
		$i++;
	}
	?>
</table>
