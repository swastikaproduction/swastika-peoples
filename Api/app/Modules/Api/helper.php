<?php

/**
 *	Api Helper  
 */

function check_validation($request, $array){

    $validator = Validator::make($request->all(), $array);

    if ($validator->fails()) {
    	
    	$res['success'] = false;
    	$res['msg'] 	= "Validation failed";
    	$res['result'] 	= $validator->errors();
    	$res['code'] 	= 401;    	
    	echo json_encode($res);
    	exit();
   	}
}

function SendOneSignalMessage($message,$empid){
      // Your code here!
$fields = array(
            'app_id' => 'c91052a2-335a-4900-b4d8-bcf52ba12e6b',
            'include_player_ids' => [$empid],
            'contents' => array("en" =>$message),
            'headings' => array("en"=>"Swastika Investmart"),
            'largeIcon' => 'https://cdn4.iconfinder.com/data/icons/iconsimple-logotypes/512/github-512.png',
            );

 $fields = json_encode($fields);
 //print("\nJSON sent:\n");
 //print($fields);

$ch = curl_init();
 curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                       'Authorization: Basic YzNhYmRlZWEtZjJiOS00YTMxLTlkOTItZmI3ZGFjMjRlMTUw'));
                                       curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                       curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                       curl_setopt($ch, CURLOPT_POST, TRUE);
                                       curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                                       $response = curl_exec($ch);
                                       curl_close($ch);
                                      
}