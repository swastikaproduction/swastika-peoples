<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
</head>

<body>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 250px; height: 400px; width:100%; margin: 0 auto"></div>

  




<script type='text/javascript'>//<![CDATA[


$(document).ready(function () {

    // Build the chart
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
         style: {
            fontFamily: '"Rubik",sans-serif'
        }
        },
        title: {
            text: '<span style="font-weight:bold">Bus Status Today</span>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Total Buses',
                y: 56,
                color:'#22cabf'
            }, {
                name: 'Running',
                y: 24,
                sliced: true,
                selected: true,
                color:'#EE5744'
            }, {
                name: 'Stopped',
                y: 10,
                color:'#ffca4c'
            }, {
                name: 'Critial Status',
                y: 4,
                color:'#F38173'
            }, {
                name: 'Take Action',
                y: 3,
                color:'#41B0A5'
            }, {
                name: 'Leave',
                y: 2,
                color:'#2e4c56'
            }]
        }]
    });
});
//]]> 

</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }
</script>

</body>

</html>

