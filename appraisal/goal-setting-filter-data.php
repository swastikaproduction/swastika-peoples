<?php
    include'../include/config.php';
	$deptId = $_POST['atOptionId'];
	$type   = $_POST['type'];
	
	    //Do real escaping here

	if($type=='department'){

		$query ="SELECT e.id,e.name,e.mobile,au.issubmitted ,d.name as department,a.appraised_mode,a.appraised_time,a.appraised_year,a.created_at,a.id as appId
	            FROM employee e 
				INNER JOIN departments d  ON d.id = e.department 
				INNER JOIN appraisal_users au ON au.emp_id = e.id
				INNER JOIN appraisals a ON a.id = au.app_id where d.id ='$deptId'";
	}else{
		$query ="SELECT e.id,e.name,e.mobile,au.issubmitted ,b.name as department,a.appraised_mode,a.appraised_time,a.appraised_year,a.created_at,a.id as appId
	            FROM employee e 
				INNER JOIN branch b  ON b.id = e.branch 
				INNER JOIN appraisal_users au ON au.emp_id = e.id
				INNER JOIN appraisals a ON a.id = au.app_id where b.id ='$deptId'";
	}
	$result = mysqli_query($con,$query);
	?>
  <div class="box-body">	
  	<div class="row">
  		<div class="col-md-12">
       <table border='2px;' class="table table-bordered table-striped">
        <thead style="background: #81b7ea;color: white;">
        <tr><!-- <td>Action</td> --><td>S.NO</td><td>Name</td><td>Mobile</td><td>Appraisal Time</td><td>Create Date</td><td>Status</td></tr></thead>
	<?php
    $count = 1;
	while($row = mysqli_fetch_array($result)){
	?>

	<tr>  <!-- <td><a href="default.php#&rtl=appraisal/employeeFinalResult.php?empId=<?php echo $row['id'];?>&appId=<?php echo $row['appId']; ?>&rtl=formDiv&rtl=tableDiv&rtl=loading""><i class="fa fa-eye"></i></a></td> -->
          <td><?php echo $count++;?></td>
          <td><?php echo $row['name']?></td>
          <td><?php echo $row['mobile']?></td>          
          <td><?php echo $row['appraised_time'] ?> &nbsp;&nbsp;<?php echo $row['appraised_year'] ?></td>
          <td><?php echo $row['created_at']?></td>
          <td><span class="label label-warning"><?php echo $row['issubmitted']?></span></td>        
        </tr>

  <?php
	}
  ?>

      </table>
      </div>
  </div>
  </div>

    