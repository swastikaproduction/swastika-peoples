<?php
function is_holiday($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['status'] != 2)
	{
		return false;
	}
	else
	{
		return true;
	}
}


function is_present($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['status'] != 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function is_late_five($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] <= 5)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function consecutive_third_late($emp,$date)
{
	global $con;
	global $rowData;
	global $rightBack;
	if($rowData['consecutive_counter'] >= 3)
	{
		if($rightBack < 0.5)
		{
			return true;			
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
}


function is_late_fifteen($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] <= 15 && $rowData['late'] > 5)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_seventh_fifteen($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['fifteenminscounter'] > 6)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_late_fifteen_to_thirty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 15 && $rowData['late'] <= 30)
	{
		return true;
	}
	else
	{
		return false;
	}
}



function is_late_thirty_to_sixty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 30 && $rowData['late'] <= 60)
	{
		return true;
	}
	else
	{
		return false;
	}
}




function is_late_sixty_to_ninety($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 60 && $rowData['late'] <= 90)
	{
		return true;
	}
	else
	{
		return false;
	}
}



function is_late_ninety_to_onetwenty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 90 && $rowData['late'] <= 120)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_late_onetwenty_to_onefifty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 120 && $rowData['late'] <= 150)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function is_late_onefifty_to_oneeighty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['late'] > 150 && $rowData['late'] <= 180)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function late_oneeightty_to_half($emp,$date)
{

	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	$half = $shifthours/2;
	if($rowData['late'] > 180 && $rowData['late'] <= $half)
	{
		return true;
	}
	else
	{
		return false;
	}

}
function late_onetwenty_to_half($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	$half = $shifthours/2;
	if($rowData['late'] > 120 && $rowData['late'] <= $half)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function is_late_more_than_sixty_mins($emp,$date)
{
global $con;
	global $rowData;
	if($rowData['late'] > 60)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function late_more_then_half($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	$half = $shifthours/2;
	if($rowData['late'] > $half)
	{
		return true;
	}
	else
	{
		return false;
	}
}
include("functions_shift_logic.php");
include("functions_two_hours.php");
?>
