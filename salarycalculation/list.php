<?php
include('include/connection.php');
$empdata = Array();
$getData = mysqli_query($con,"SELECT * FROM `employee`") or die(msyqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	$empdata[$row['id']] = $row['name'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::SWASTIKA::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
</head>
<body style="font-family:'Rubik'">

<div style="float:right;padding:20px;">

	<a href="export.php" target="_blank">
<button class="btn btn-sm btn-primary">
	Export Cumilative
</button>
	</a>
	
</div>
<br/><br/>
<table class="table table-hovered table-striped" border="1" style="width:100%" cellpadding="5" >
	<tr>
		<th>Name</th>
		<th>Deduction From Pool</th>
		<th>Deduction From Salary</th>
		<th>Total Deduction</th>
		<th>Details</th>
	</tr>

<?php
$getData = mysqli_query($con,"SELECT * FROM `final_console`");
while($row = mysqli_fetch_array($getData))
{
?>
<tr>
	<td><?php echo $empdata[$row['empid']];?></td>
	<td><?php echo $row['pool'];?></td>
	<td><?php echo $row['salary'];?></td>
	<td><?php echo $row['pool']+$row['salary'];?></td>
	<td>
		<a href="policy/print.php?id=<?php echo $row['empid'];?>" target="_blank">View Details</a>
	</td>
</tr>
<?php
}
?>
</table>
</body>
</html>