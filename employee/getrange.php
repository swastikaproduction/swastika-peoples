<?php
include("../include/config.php");
$from = $_GET['from'];
$to = $_GET['to'];
$empid = $_GET['empid'];


$getBranch = mysqli_query($con,"SELECT `branch` FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
$rowBranch = mysqli_fetch_array($getBranch);
$branch = $rowBranch[0];

$caldates = Array();
$getCaldates = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$from' AND '$to'") or die(mysqli_error($con));
while($rowDates  = mysqli_fetch_array($getCaldates))
{
	$temp = explode(",",$rowDates['branch']);
	foreach($temp as $val)
	{
		if($val == $branch)
		{
			$caldates[] .= $rowDates['date'];
		}
	}
}


function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}

$datestr = '';
$daterangedefault = getDatesFromRange($from,$to);
?>
<table class="table table-bordered">
	<tr>
		<th>To Use</th><th>Date</th>
	</tr>

<?php
foreach($daterangedefault as $val)
{
	if(date("D",strtotime($val)) != 'Sun')
	{
		if(!in_array($val,$caldates))
		{
				$datestr .= $val.",";			
		}

?>
<tr>
<td>
	<input onclick="toggleShiftDate(this)" type="checkbox" <?php if(!in_array($val,$caldates)) echo  'checked="checked"' ;?> value="<?php echo $val;?>"/>
</td>
	<td><?php echo $val;?></td>
</tr>

<?php		
	}
	
}
?>
</table>

<input type="text" id="tsht3" style="display:none"  value="<?php echo $datestr;?>" id="" name="">