<?php
include("../include/config.php");
$month = $_GET['month'];
$year = $_GET['year'];
$dummydate = "01-".$month."-".$year;
$dummytime = strtotime($dummydate);
$dummytime = $dummytime - (10 * 24 * 60 *60);
$lastmonth = date("Y-m",$dummytime);
$startdate = $lastmonth."-26";
$enddate = $year."-".$month."-25";

$startformat = date("Y-m-d",strtotime($startdate));
$endformat = date("Y-m-d",strtotime($enddate));


$empArray = Array();
$getdata = mysqli_query($con,"SELECT * FROM `employee` WHERE  `left`= '0'") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getdata))
{
	$empArray[$row['id']]['name'] = $row['name'];
	$empArray[$row['id']]['code'] = $row['empid'];
	$empArray[$row['id']]['shift'] = $row['shift'];
}

$shiftArray = Array();
$getShifts = mysqli_query($con,"SELECT * FROM `shift` WHERE `delete` = '0'") or die(mysqli_error($con));
while($rowShifts = mysqli_fetch_array($getShifts))
{
	$shiftArray[$rowShifts['id']]['from'] = $rowShifts['starttime'];
	$shiftArray[$rowShifts['id']]['to'] = $rowShifts['endtime'];
}


function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}

$daterangedefault = getDatesFromRange($startdate,$enddate);

$k=0;
foreach($daterangedefault as $key=> $dateval)
{
//	if(date("D",strtotime($dateval)) != 'Sun')
//	{
		$daterange[$k] = date("d/m/Y",strtotime($dateval));
		$k++;		
//	}
}


function checkSunday($givendate)
{
    $t = explode("/",$givendate);
    $format = $t[2]."-".$t[1]."-".$t[0];
    $day = date("D",strtotime($format));
    return $day;
}

$caldates = Array();
$getCalendar= mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$startformat' AND  '$endformat'") or die(mysqli_error($con));
while($rowCalendar = mysqli_fetch_array($getCalendar))
{
$caldates[$rowCalendar['date']]['type'] = $rowCalendar['type'];
$caldates[$rowCalendar['date']]['from'] = $rowCalendar['from'];
$caldates[$rowCalendar['date']]['to'] = $rowCalendar['to'];
}

function changeformat($thisvaldate)
{
	$temp = explode("/",$thisvaldate);
	return $temp[2]."-".$temp[1]."-".$temp[0];
}



$name = "All-Employee-Sheet-".$month."-".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


?>
<table border="1">
<?php



foreach($empArray as $key=> $val)
{
	?>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>User</td><td></td><td><?php echo $val['code'];?></td><td></td><td></td><td><?php echo $val['name'];?></td><td></td><td></td><td></td>
	</tr>
	<?php
	$thisShift = $shiftArray[$val['shift']];
	$thisfrom = $thisShift['from'];
	$thisto = $thisShift['to'];

	foreach($daterange as $thisdate)
	{

		$checkformat= changeformat($thisdate);
		if(array_key_exists($checkformat, $caldates))
		{
			if($caldates[$checkformat]['type'] != '1')
			{
				?>
					<tr>
						<td><?php echo $thisdate;?></td>
						<td></td>
						<td><?php echo $caldates[$checkformat]['from'];?></td>
						<td></td>
						<td><?php echo $caldates[$checkformat]['to'];?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

				<?php
			}
			else
			{
			?>
					<tr>
						<td><?php echo $thisdate;?></td>
						<td></td>
						<td><?php echo '00:00:00';?></td>
						<td></td>
						<td><?php echo '00:00:00';?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

				<?php	
			}
		

		}
		else if(checkSunday($thisdate) == 'Sun')
		{
		    ?>
	<tr>
	<td><?php echo $thisdate;?></td>
	<td></td>
						<td><?php echo '00:00:00';?></td>
						<td></td>
						<td><?php echo '00:00:00';?></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>	    
		    <?php
		}
		else
		{
		?>
<tr>
	<td><?php echo $thisdate;?></td>
	<td></td>
	<td><?php echo $thisfrom;?></td>
	<td></td>
	<td><?php echo $thisto;?></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
		<?php

		}
	}

}


?>

</table>
