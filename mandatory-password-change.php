<?php
include("include/config.php");
if(isset($_POST['p1']))
{
  $p1 = $_POST['p1'];
  mysqli_query($con,"UPDATE `employee` SET `password` = '$p1',`passchange` = '1' WHERE `id` = '$loggeduserid'") or die(mysqli_error($con));
  header("location:default.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::PROTOCOL:.</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/map.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>
<script type="text/javascript">
	jQuery.fn.scrollTo = function(elem, speed) { 
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top 
    }, speed == undefined ? 1000 : speed); 
    return this; 
};

</script>
<script src="scripts/flatcolor.js"></script>
</head>
<body  onhashchange="changeHashValue()" onload="changeHashValue();" onresize="resizeWindow();">
<div style="width:100%;padding:40px">
<div class="row">
  <div class="col-sm-4">
   
<?php
include('modal.php');
?>

<span style="font-size:20px;">
  Change your password
  </span>
  <?php if(!isset($_GET['mand']))
  {
    ?>
  <br/>
  This is a mandatory password change request from administration for the secutiry of your account.
  <br/>
  <br/>
  
    <?php
  }
  ?>
  <br/><br/>  
<form action="mandatory-password-change.php" method="post" id="passForm">
<span style="font-size:11px;text-transform:uppercase">
  Choose a password</span>
  <br/>
  <br/>
  <input type="password" id="p1" name="p1" class="input" style="width:100%" name="">  
<br/><br/>
<span style="font-size:11px;text-transform:uppercase">
  Retype Password</span>

  <br/>
  <br/>
  <input type="password" id="p2" name="p2" class="input" style="width:100%" name="">  
  <br/>
  <br/>
  <br/>
  <button class="btn btn-sm btn-primary" type="button" onclick="checksamepass();">CHANGE PASSWORD</button>

</form>

<script type="text/javascript">
  function checksamepass()
  {
    var p1 = document.getElementById('p1').value;
    var p2 = document.getElementById('p2').value;
    if(p1 == '' || p2 == '')
    {
      toast("Please fill all the fields","toast bg-danger","5000");      
    }
    if(p1 != p2)
    {
      toast("Passwords in both fields must match","toast bg-danger","5000");
    }
    else
    {
      document.getElementById('passForm').submit();
    }
  }

</script>

  <br/><br/>
  



  </div>
  <div class="col-sm-8" style="text-align:right">
       <img src="images/thief.png" alt="" style="width:30%;position:fixed;bottom:0px;right:100px;"/>
  </div>
</div>

</div>
</body>
</html>


