<?php  
include'../include/config.php';
$empid = $_GET['empid'];
$branchid = $_GET['branchid'];
$type = $_GET['type'];
$hodapproval = $_GET['hodapproval'];
$hrapproval = $_GET['hrapproval'];
$fromdate = $_GET['fromdate'];
$todate = $_GET['todate'];

$query = "SELECT lr.id,lr.type,lr.fromdate,lr.todate,lr.ondate,lr.intime,lr.outime,lr.status,lr.hodapproval,lr.remark,lr.approval_date,e.name as ename,lr.lng,lr.lng,lr.lat,b.name as bname,lr.createdate FROM leaverequests as lr INNER JOIN employee as e on e.id=lr.empid INNER JOIN branch b ON b.id = e.branch";

    $conditions = array();    

    if(! empty($empid)) {
      $conditions[] = "e.id='$empid'";
    }
    if(! empty($branchid)) {
      $conditions[] = "b.id='$branchid'";
    }
    if(! empty($type)) {
      $conditions[] = "lr.type='$type'";
    } 
     if(! empty($hodapproval)) {
      $conditions[] = "lr.hodapproval='$hodapproval'";
    }
     if(! empty($hrapproval)) {
      $conditions[] = "lr.status='$hrapproval'";
    }
    if(! empty($fromdate)) {
      $conditions[] = "lr.ondate BETWEEN '$fromdate' and '$todate'";
    }
  
    $sql = $query;

    if (count($conditions) > 0) {
      $sql .= " WHERE " . implode(' AND ', $conditions);
    }   
     $s = $sql ." ORDER BY lr.id DESC";
    $result = mysqli_query($con,$s) or die(mysqli_error($con));   

    $name = "employee_request_reports.xls";
	header("Content-Disposition: attachment; filename=\"$name\"");
	header("Content-Type: application/vnd.ms-excel");
 ?>
<table border="1" style="background-color: #eef5f9;">
<tr>
<th style="width:5%;">#</th>
<th style="width:15%">Employee</th>
<th style="width:10%">Branch</th>
<th style="width:15%;">Type</th>
<th style="width:15%;">On Date</th>
<th style="width:30%">Details</th>
<th style="width:10%">HOD Approval</th>
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
<th style="width:10%">HOD Approval Date</th>
</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($result))
{
?>
<?php
if($row['empid'] != $loggeduserid && $loggeduserid != '124')
{
	?>
<tr >


	<?php
}
?>
<td><?php echo $i;?></td>
<td><?php echo $row['ename'];?></td>
<td><?php echo $row['bname'];?></td>
<td>

<?php 
	if($row['type'] == '1')
	{
		?>
	<i class="fa fa-circle" style="color:#ffb22b"></i>&nbsp;&nbsp;&nbsp;
		<?php
		echo "Leave";
	}
	else if($row['type'] == '2')
	{
		?>
	<i class="fa fa-circle" style="color:#1976d2"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Timing Mismatch";
	}

	else if($row['type'] == '3')
	{
		?>
	<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Overtime";
	}

	else if($row['type'] == '4')
	{
		?>
	<i class="fa fa-circle" style="color:#e32eab"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "OD Request";
	}

else if($row['type'] == '5')
	{
		?>
	<i class="fa fa-circle" style="color:#fed3c5"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Half Day";
	}
?></td>
<td><?php echo $row['ondate'];?></td>
<td>
<?php
if($row['type']== 1)
{
	?>

<span class="label label-default">
	<?php echo date("D, d M Y",strtotime($row['fromdate']))." - ".date("D, d M Y",strtotime($row['todate']));?> </span>
</span>
	<?php
}
else
{
	?>

<span class="label label-default">
	<?php echo $row['intime']." - ".$row['outime']." - ".date("D, d M Y",strtotime($row['ondate']));?> </span>
</span>



	<?php
}
?>
</td>


<td style="padding-top:15px;" <?php if($loggeduserid == '124')
{
?>

<?php	
}
?>>
	<?php 
	if($row['hodapproval'] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row['hodapproval'] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row['hodapproval'] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>



<td style="padding-top:15px;" >
	<?php 
	if($row['status'] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row['status'] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row['status'] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>

<td>
<?php echo $row['createdate'];?>
</td>

<td>
<?php if(!empty($row['approval_date'])){ echo $row['approval_date']; }else{ echo ""; } ?>
</td>

</tr>

	<?php
	$i++;
}
?>



</table>
