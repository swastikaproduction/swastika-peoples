/*
function setCustomView(module)
{
	var checkList = '';
	var itemSearch = $('.checkInput');
			searchList = $('#setCustomView').find(itemSearch).each(function(){
			if(this.checked == true)
			{
				checkList += this.value+"BREAKER";
			}
    });
		var url = "setup/customview/savecustomdata.php";
		var params ={
			data:checkList,
			module:module
		}
		genAjax(url,params,function(response){
			showBottomNotification('success','Preferences successfully set. Please visit module page to see changes.')
		});

}
*/

function setCustomView(module)
{
	var checkList = '';
	var itemSearch = $('.justforcustomvalue');
			searchList = $('#selectedItemsDiv').find(itemSearch).each(function(){
				checkList += this.lang+"BREAKER";
    });
			
		
		var url = "setup/customview/savecustomdata.php";
		var params ={
			data:checkList,
			module:module
		}
		disableButton('setCustomViewButton','Updating values..');
		genAjax(url,params,function(response){
			showBottomNotification('success','Drag any item up or down to set order.');
			enableButton('setCustomViewButton','Proceed to step 2');
			$('#setCustomStep1').slideUp();
			$('#setCustomStep2').slideDown();
			$('#setCustomStep2').html(response);
		    $( "#sortable" ).sortable();
		    $( "#sortable" ).disableSelection();
		});


}

function setCustomOrder(id,module)
{
		var checkList = '';
	var itemSearch = $('.justforcustomvalue2');
			searchList = $('.setCustomOrder').find(itemSearch).each(function(){
				checkList += this.lang+"BREAKER";
    });
		var url = "setup/customview/savecustomdata.php?order=1";
		var params ={
			data:checkList,
			module:module
		}
		disableButton('setCustomOrderButton','Saving');
		genAjax(url,params,function(response){
			showBottomNotification('success','Eveerything set. You can now visit he updated module.');
			enableButton('setCustomOrderButton','All Set. :)');
		});


}


function getCustomView()
{
var value = document.getElementById('customFieldSelector').value;
if(value != '')
{
	$('#getFields').html("<center>Getting..</center>");
	var url = "setup/customview/getCustomField.php?module="+value;
	genAjax(url,'',function(response){
		$('#setCustomView').html(response);
		$('#getFields').html("<center>Get Fields</center>");
	});	
}
else
{
	showBottomNotification('stop','You have to select a module to get started.')
}
}


function allowDrop(ev) {
	ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev,id1,id2) {
	if((ev.target.id == id1) || (ev.target.id == id2))
	{
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));		
	}
	else
	{
		showBottomNotification('stop',"We'll set the order of the view in next step. Thaamba!!")
	}
}
