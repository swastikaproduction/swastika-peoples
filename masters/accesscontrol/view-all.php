<?php
include("../../include/config.php");
 $query = mysqli_query($con,"select * from profiles");
 $headquery = mysqli_query($con,"select * from expensenature");
?>
<br/>
<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Access Control</li>
</ol>
</section>
<section>

   <div class="row">
      <div class="col-md-2">
        <label>Select Level</label>
      </div>
      <div class="col-md-2">
        <label>Profile Level</label>
        <input type="radio" name="accesslevel" value="profile" onclick="SelectAccessLevel(this.value)">
      </div>

      <div class="col-md-2">
        <label>Employee Level</label>
        <input type="radio" name="accesslevel" value="employee" onclick="SelectAccessLevel(this.value)">
      </div>

   </div> 
   <div class="row">
      <div id="profile_div" style="display:none" class="col-md-2">
        <label>Select Profile</label>
        <select id="profileid" class="form-control" onchange="getModule('masters/accesscontrol/profileData.php?profile='+this.value+'&employee=0','profileData','','Access Control')">
          <option value="">select profile</option>
          <?php
           $profilequery = mysqli_query($con,"select * from profiles");
           while($row=mysqli_fetch_array($profilequery)){
          ?>
           <option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
          <?php } ?>
         </select>
       </div>

      <div id="employee_div" style="display:none">
      <div class="col-md-2">
        <label>Select Branch</label>
        <select onchange="getEmployeeList(this.value)" id="branchid" class="form-control">
          <option value="0">select branch</option>
          <?php 
          $data = getData('branch','*','name','ASC');
          foreach($data as $row)
          {
          ?>
           <option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
          <?php } ?>
        </select>
      </div>
      
      <div class="col-md-2">
        <label>Select Employee</label>
        <select id="employeeid" class="form-control" onchange="getModule('masters/accesscontrol/profileData.php?employee='+this.value+'&profile='+document.getElementById('profileid').value,'profileData','','Access Control')">
          <option value="0">select employee</option>
          <?php 
          $data = getData('employee','*','name','ASC');
          foreach($data as $row)
          {
          ?>
           <option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
          <?php } ?>
        </select>
      </div>
      </div>
    </div>
  </br>
  <div id="profileData"></div>
</section>
</section>
<section>
<div id="assetview">
  
</div>

</section>
