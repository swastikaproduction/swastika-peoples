<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];
//calculation hide code
$d = date('d-m-y');
$c = date('m',strtotime($d));



if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
	$empArray[$val['id']]['left'] = $val['left'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	$typeStr = " AND `status` = '1'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));	


}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));

}

?>
<div style="padding:0px;">
<?php
if($loggeduserid == '124')
{
?>
<div style="float:right;padding:20px;">
<button class="btn btn-warning" onclick="startGen()">Auto Generate</button>

<button class="btn btn-danger" onclick="getModal('attendance/missing.php?month=<?php echo $month;?>&year=<?php echo $year;?>','tableModalBig','formModalBig','loading')">View Missing/Double Attendance</button>
</div>

<?php	
}
?>


<div class="dummyTableHeader" id="forAttendance" style="margin-bottom:0px;position:fixed;top:118px;left:0px;width:100%;padding:0px 21px 0px 15px;z-index:200;display:none">

<table class="table table-hover" cellpadding="0" cellspacing="0" style="">
<tr>
		<th style="width:5%">#</th>
		<th style="width:20%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:15%">Total Deduction</th>
		<th style="width:5%">From Salary</th>
		<th style="width:5%">From Pool</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
		<th style="width:10%">Details</th>
		<th style="width:10%">Export</th>
	</tr>
</table>

</div>



<table class="table table-bordered table-hover"  cellpadding="0" cellspacing="0" id="tableDivAtView">
	<tr>
		<th style="width:5%">#</th>
		<th style="width:20%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:15%">Total Deduction</th>
		<th style="width:5%">From Salary</th>
		<th style="width:5%">From Pool</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
		<th style="width:10%">Details</th>
		<th style="width:10%">Export</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		
		<?php
		if($empArray[$row['empid']]['left'] == '1')
		{
			?>
			<div style="float:right;text-align:right;padding-top:5px;">
<span class="label label-danger" onclick="getModule('attendance/delete.php?id=<?php echo $row['id'];?>','atRow<?php echo $row['id'];?>','','loading');$('#atRow<?php echo $row['id'];?>').hide();">
<i class="fa fa-close"></i>
</span>
</div>
			<?php
		}
		?>
		<?php echo $i;?>
	</td>
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>
	<td>
	
	<?php
	if($row['status'] == '1')
	{
		echo $row['deduction'];

		 //if($c==$month ){
		?>


	<div style="float:right;margin-top:5px;" onclick="getModule('salarycalculation/generate_salary.php?id=<?php echo $row['id'];?>&i=<?php echo $i;?>','atRow<?php echo $row['id'];?>','','loading')">
		<span class="label label-success">
	<i class="fa fa-refresh"></i>

</span>

	</div>

		<?php
	//}
	}
	else
	{ 
		  


		?>
		<div style="float:left;margin-top:5px;">
		<span class="label label-default" id="genBut<?php echo $i;?>" lang="<?php echo $row['id'];?>::<?php echo $i;?>" onclick="getModule('salarycalculation/generate_salary.php?id=<?php echo $row['id'];?>&i=<?php echo $i;?>','atRow<?php echo $row['id'];?>','','loading')">CLICK TO CALCULATE</span>
		</div>
		<?php

	}
	?>
	
	</td>
	<td>
		<?php echo $row['salary_deduction'];?>

	</td>
	<td>
		<?php echo $row['pool_deduction'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'] - $row['pool_deduction'];?>
	</td>



	<td style="padding-top:12px" id="detBut<?php echo $row['id'];?>" onclick="getModule('attendance/details.php?id=<?php echo $row['id'];?>','bottomDiv','','loading')">
		<span class="label label-info">DETAILS</span>
	</td>

	<td style="padding-top:12px" id="" >
	<?php
	if($row['status'] == '1')
	{
		?>
		<span class="label label-success" onclick="window.open('attendance/export.php?id=<?php echo $row['id'];?>','_blank')">
					<i class="fa fa-external-link"></i>&nbsp;EXPORT

		</span>
		<?php
	}
	?>
	</td>

</tr>
		<?php
		$i++;
	}
	?>
</table>
<input type="text" value="<?php echo $i;?>" id="genTotal" style="display:none">
</div>
<br/><br/><br/><br/><br/>