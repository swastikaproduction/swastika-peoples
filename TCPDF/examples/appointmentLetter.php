<?php
ob_start();
include("../../include/config.php");
// error_reporting(-1);
 $id = $_POST['emp_id'];
 $emp_designation = $_POST['emp_designation'];
 $emp_notice_period = $_POST['emp_notice_period'];
 $type = $_POST['submit'];

// echo "<pre>"; print_r($_POST); die;	

	$getEmp  = mysqli_query($con,"SELECT * FROM `employee` WHERE id = '$id' ") or die(mysqli_error($con));
	while($empDetails = mysqli_fetch_array($getEmp))
	{
		$emp_empid = $empDetails['empid'];
		$emp_name = $empDetails['name'];
		$designation_id = $empDetails['designation'];
		$branch_id = $empDetails['branch'];
		$emp_salary = $empDetails['salary'];
		$emp_doj = $empDetails['doj'];
		$email = $empDetails['workemail'];
		$gender = $empDetails['gender'];
		
		$emp_doj = date('d/m/Y', strtotime($emp_doj));

	}

	if(!empty($_POST['emp_salary'])){
		 $emp_salary = $_POST['emp_salary'];
	}
	else{
		$emp_salary = $empDetails['salary'];
	}

	if(!empty($designation_id)){
		$getDesignation  = mysqli_query($con,"SELECT `name` FROM `designation` WHERE id = '$designation_id' ") or die(mysqli_error($con));
		while($designationDetails = mysqli_fetch_array($getDesignation))
		{
			$emp_designation = $designationDetails['name'];
		}
	}

	if(!empty($branch_id)){
		$getBranch  = mysqli_query($con,"SELECT `name` FROM `branch` WHERE id = '$branch_id' ") or die(mysqli_error($con));
		while($branchDetails = mysqli_fetch_array($getBranch))
		{
			$emp_branch = $branchDetails['name'];
		}
	}

	// email library
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../leaves/vendor/phpmailer/phpmailer/src/Exception.php';
require '../../leaves/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../../leaves/vendor/phpmailer/phpmailer/src/SMTP.php';

$mail = new PHPMailer(true);
require '../../leaves/vendor/autoload.php';
// end email library
   
require_once('tcpdf_include.php');

class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'swastika-logo.jpg';
		$this->setJPEGQuality(60);
        $this->Image($image_file, 30, 10, 50, 9, 'jpg', '', 'T', true, 300, 'R', false, false, 0, false, false, false);

		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
//		$this->Cell(0, 15, 'OFFER LETTER', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-40);
		// Page number
		//$footertext = '<style>'.file_get_contents(K_PATH_IMAGES.'font_tcpdf.css').'</style>';
		$footertext = '<p style="color: #4c7aec;text-align:centre;"><b>Swastika Investmart Limited</b></p>
					   <p style="text-align:centre;"><b>Corp. Off.:</b> 48 Jaora Compound, M.Y.H. Road Indore-452001 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;0731-6644000, 3345000</p>
					   <p style="text-align:centre;"><b>Regd. Off.:</b> Flat No. 18, North Wing, Madhaveshwar Co-op. Hsg. Society, S.V. Road, Andheri (W), Mumbai-40058 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;022-26254568-69</p>
					   <p style="text-align:centre;"><i class="fa fa-envelope fa-11x"></i>&nbsp;&nbsp;info@swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-globe"></i>&nbsp;&nbsp; www.swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-car"></i>&nbsp;&nbsp;0120-4400789 </p>
					   <table cellspacing="4" cellpadding="4">
							<tr>
								<th style="width:100%;height:25px;text-align: center; background-color: #41d7e4;">
									<p style="color:#fff; text-align:centre;padding:50px 50px 600px 666px"><b>Swastika Group: Member of NSE, BSE, NCDEX, MCX, MSEI, DP:NSDL & CDSL</b></p>
								</th>
								
							</tr>
						</table>
				      ';
		$this->writeHTML($footertext, false, true, false, true);
		
		// $this->Cell(0, 60, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Swastika Investmart Limited');
$pdf->SetTitle('Appointment Letter');
$pdf->SetSubject('Swastika Investmart Limited');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 52);
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();


// create some HTML content
$date = date('d/m/Y'); $emp_city = '';
if($gender == "Male"){
	$prefix_text= 'Mr.';
	$prefix_text1= 'He';
	$gender_prefix1 = 'his';
	$gender_prefix2 = 'him';
}
elseif($gender == "Female"){
	$prefix_text= 'Ms.';
	$prefix_text1= 'She';
	$gender_prefix1 = 'her';
	$gender_prefix2 = 'her';
}
else{
	$prefix_text= 'Gender';
	$prefix_text1= 'Gender';
	$gender_prefix1 = 'his/her';
	$gender_prefix2 = 'him/her';
}

$html = '<br><p style="float: left;font-size:15px;"><b>Date: '.$date.'</b></p>
		 <p style="float:left;"><b>'.$prefix_text.' '. $emp_name.',</b><br><br></p>
		
		 <strong style="font-size:18px;text-align:centre;"><u>Subject: Letter of Appointment </u><br></strong>
		 
		 <p style="float:left;font-size:15px;"><b>Dear '.$emp_name.',</b></p>

		 <p style="float:left;margin-top:0px;padding-right: 22px;">With reference to our recent discussions regarding your possible employment with <b>Swastika Investmart Ltd.</b>, here after referred to as the Company, we are pleased to appoint you as <b>'.$emp_designation.'</b> based at our <b>'.$emp_branch.'</b> on the following terms and conditions defined here in:</p>

		 <p><b>1. &nbsp;&nbsp;<u>Annual Salary:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You will be paid a CTC (Cost To Company) of <b>Rs. '.moneyFormatIndia($emp_salary*12).'/- (Rs. '.getIndianCurrency($emp_salary*12).')</b> per annum and same shall be subject to deductions according to applicable statutory norms and regulations.</p>

		 <p><b>2. &nbsp;&nbsp;<u>Date of Appointment:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your date of appointment as per company records is <b>'.$emp_doj.'</b>.</p>

		 <p><b>3. &nbsp;&nbsp;<u>Transfer:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beyond your posting, your services can be transferred to any other city or from one Company of the Group to another company and/ or one branch to another and/or one Department to another as per the business exigencies and Management’s discretion.</p>

		 <p><b>4. &nbsp;&nbsp;<u>Hours of Work:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The office functions six days a week from Monday to Saturday, the working hours being 9 hours. You may be required to work any additional hours and/or work in shifts, as required by the company based on work exigencies and/or as necessary for the effective performance of your role.</p>

		 <p><b>5. &nbsp;&nbsp;<u>Leave:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In addition to public holidays observed by the Company, you shall be entitled to a casual annual leave for each calendar year in accordance with the existing leave policy, prorated from your date of joining.</p>

		 <p><b>6. &nbsp;&nbsp;<u>Probation Period:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You will be on a Probation period for Three Months and after successful completion of the probation period, your appointment will be confirmed on the basis of probation assessment that will be done by concerned head/manager.</p>

		 <p><b>7. &nbsp;&nbsp;<u>Dress Code:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are required to attend the office in prescribed dress code as per Dress Code Policy.</p>

		 <p><b>8. &nbsp;&nbsp;<u>Travel:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Whenever you are required to undertake travel for company’s work, you will be reimbursed travel expenses as per the company rules with valid travel documents.</p>

		 <p><b>9. &nbsp;&nbsp;<u>Responsibilities:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In view of your office, you must effectively perform to ensure results and during your employment with the Company you shall devote your best efforts for promoting the Company’s (and of any other relevant Group Company, affiliate and/ or business associate of the Company) business.</p>

		 <p><b>10. &nbsp;&nbsp;<u>Own Trade:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You shall not trade in shares for your own without any intimation, while in employment of the company.</p>

		 <p><b>11. &nbsp;&nbsp;<u>Past Records:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This appointment order is made on the understanding that the information and relevant documents provided by you to the company regarding your background and/or previous employment, etc. are correct, true and complete. If it is found at any point of time that the same provided by you are not true, incorrect, incomplete or fraudulent in nature, your employment shall be liable to be terminated without assigning any reasons or notice.</p>

		 <p><b>12. &nbsp;&nbsp;<u>Other Employment:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your position with the Company calls for whole time employment and you will devote yourself exclusively to the business of the Company. You will not take up any other work for remuneration (part time or otherwise) or work on advisory capacity or be interested directly or indirectly (except as shareholder or debenture holder) in any other trade or business during your employment with the Company. If it is found at any point of time that you are seeking full time or part time job or involved in any way with competitor’s business activities either directly or indirectly during your employment with the company, your employment shall be liable to be terminated with immediate effect.</p>

		 <p><b>13. &nbsp;&nbsp;<u>Confidential Information:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You will not, at any time, without the consent of the Company disclose or divulge or make public except under legal obligation, any information regarding Company’s affairs of administration or research carried out, whether the same may be confided to you or become known to you, in the course of your service or otherwise.</p>

		 <p><b>14. &nbsp;&nbsp;<u>Contract or Bond with Previous Employers:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It will be your personal responsibility to discharge all obligations arising out of any contract or bond with previous employers.</p>

		 <p><b>15. &nbsp;&nbsp;<u>Notice Period:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;After confirmation, if you resign from the services of the company then this appointment may only be terminated by giving Thirty days’ notice period and if it is found at any point of time during your employment that if any kind of misconduct, misbehavior or any action done by you against company’s policy then your employment shall be liable to be terminated by the company with immediate effect. If during the course of your employment your performance is found unsatisfactory or if you are not a cultural fit, then the management may ask you to leave as per company policy.<br/>
		 Notice Pay: If you do not serve the notice as mentioned above, then you shall be liable to pay for the shortfall days in the notice period.
		 Note: Notice Period will be considered only if the resignation is accepted by the management and mutually agreed upon by both the employee and the company.</p>

		 <p><b>16. &nbsp;&nbsp;<u>Performance Appraisal:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your first appraisal will take place upon completion of one year from joining and next will be done on pro-rata basis in next financial year.<br>
		 Note: For employees who are coming under Fast Track Career Progression Policy, their appraisal cycle would be different.</p>

		 <p><b>17. &nbsp;&nbsp;<u>Termination:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. &nbsp; If any time during the course of your employment it is found that you have made false or an incomplete declaration as regards to your qualifications/ experience and other details, your appointment will be treated void and you shall cease to be on the rolls of the company with immediate effect. In that case you shall not be entitled to any notice period or payment in lieu there of.<br><br>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;&nbsp;	If any time during the course of your employment it is found that you have: <br>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a)&nbsp; Committed any act of gross misconduct or any serious breach or repeated or continued a material <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; breach of the terms of your employment or been found inefficient or lower performer as compared <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; to other employees of your category; or <br>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b)&nbsp; Been guilty of conduct tending to bring yourself or the company into disrepute; or <br>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c)&nbsp; Been convicted of a criminal offence, other than a road traffic offence for which you are not <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sentenced to a term of imprisonment whether immediate or suspended; or <br>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d)&nbsp; Been absent for a continuous period of 2 days (including absence when leave though applied for, is <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; not granted or when you overstay period of sanctioned leave by 2 days).</p>

		 <p>In all such aforementioned cases you shall not be entitled to any notice period or payment in lieu thereof.</p>

		 <p><b>18. &nbsp;&nbsp;<u>Return of company’s property:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; On termination of this contract, you will immediately give up to the Company all correspondence, specifications, documents, market data, effect or records, etc. belongings of the Company or relating to its business and shall not make or retain any copies of these items.</p>

		 <p><b>19. &nbsp;&nbsp;<u>Change in Personal Details:</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will notify the company, any change of address, telephone number or any other personal details relevant during the tenure of your employment</p>

		 <p>The above terms and conditions are based on Company’s Policy, Procedures and other Rules and Regulations currently applicable to the Company’s employees and are subject to amendments and adjustments from time to time. <br>
		 <b>Please communicate your acceptance of this appointment by reverting on the same trail mail.</b></p>

		 <p>We welcome you to the <b>Swastika Investmart Ltd.</b> family and trust we will have a long and mutually rewarding association.</p>

		 <p><b>Yours truly,</b></p>

		 <p style="font-size:13px;"><b>For Swastika Investmart Ltd. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>  <b> Accepted and I will adhere to Follow the above: </b><br><br></p>
		 
		 <div >
			<b>Sunil Nyati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name: '.$emp_name.'</b><br />

			<b>(Managing Director)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date: '.$date.'</b>
			
		 </div>
		 
		';
		
		// $pdf->setCellMargins( $left, $top = '', $right = '', $bottom = '');
		// $pdf->setCellPaddings(5, 5, 5, 5);
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


// to clean the output buffer just before the method invocation:
ob_end_clean();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


// ---------------------------------------------------------
if($type=='GENERATE APPOINTMENT LETTER'){
	$pdf->Output($emp_name.'_appoinmentLetter.pdf', 'D');
}else{

$pdfString = $pdf->Output($emp_name.'_appoinmentLetter.pdf', 'S');
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true; 	 
$mail->Username = 'swastika.developer@gmail.com';
$mail->Password = 'sil@321#';
$mail->setFrom('swastika.developer@gmail.com', 'Swastika Investmart');
$mail->addAddress($email);
$mail->Subject = 'Appointment Letter';
$mail->Body = 'Appointment Letter';
$mail->AltBody = 'Swastika Investmart Limited';
// $mail->msgHTML("<h1>This is an HTML message</h1>");
// $mail->addAttachment('test.txt');
$mail->addStringAttachment($pdfString,$emp_name.'_appoinmentLetter.pdf');

if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
	echo "<script>alert('Mail Sent Successfully.');</script>";
    echo '<script>setTimeout(function () {window.close(); },3000); </script>';
}

}


//============================================================+
// END OF FILE
//============================================================+

function moneyFormatIndia($num) {
	$explrestunits = "" ;
	if(strlen($num)>3) {
		$lastthree = substr($num, strlen($num)-3, strlen($num));
		$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
		$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
		$expunit = str_split($restunits, 2);
		for($i=0; $i<sizeof($expunit); $i++) {
			// creates each of the 2's group and adds a comma to the end
			if($i==0) {
				$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
			} else {
				$explrestunits .= $expunit[$i].",";
			}
		}
		$thecash = $explrestunits.$lastthree;
	} else {
		$thecash = $num;
	}
	return $thecash; // writes the final format where $currency is the currency symbol.
}

function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}

?>
