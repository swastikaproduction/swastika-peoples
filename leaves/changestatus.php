<?php
include("../include/config.php");
$id = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `leaverequests` WHERE `id` = '$id'") or die(mysqli_error($con));
$row= mysqli_fetch_array($getData);
$empid = $row['empid'];
$rtype = Array();
$rtype[1] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Leave Request</span><br/><span style="color:#222">'.date("d-M-y",strtotime($row['fromdate']))." to ".date("d-M-y",strtotime($row['todate'])).'</span>';
$rtype[2] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Timing Change Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';
$rtype[3] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Overtime Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';

$rtype[4] = '<span class="text-primary" style="font-weight:400;font-size:20px;">OD Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';


$rtype[5] = '<span class="text-primary" style="font-weight:400;font-size:20px;">Halfday Request</span><br/>For '.date("d-M-y",strtotime($row['ondate'])). '<br/><span style="color:#222">Intime: '.$row['intime'].' & Outime: '.$row['outime'].'</span>';


?>
<div class="row">
	<div class="col-sm-12">
	<div style="float:right;margin-top:20px">
		<?php 
	if($row['status'] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row['status'] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row['status'] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
	</div>

	<div class="subHead" style="padding-left:5px">
	<span style="font-size:15px;">
		<?php echo $rtype[$row['type']];?>
		</div>
		
		</span>
				<span style="font-size:16px;font-weight:bold;">HR Approval</span>
		<br/><br/>

		<div style="background:#f7f8f8;padding:20px;">
<?php echo $row['reason'];?>
		</div>
		<br/>



		Request Status
		<br/>
		<select <?php if($row['status'] == '1') echo "disabled='true'";?> class="inputBox" id="lstat0" style="margin-top:5px;" onchange="if(this.value == '1') { $('#moreInputs').slideDown();} else { $('#moreInputs').slideUp();}">
		<option value="">Select Status</option>
			<option <?php if($row['status'] == '1') echo "selected='selected'";?> value="1">Approved</option>
			<option <?php if($row['status'] == '2') echo "selected='selected'";?>  value="2">Rejected</option>
		</select>


<?php
if($row['type'] == '4')
{
	?>
<br/><br/>
<button class="btn btn-primary" onclick="window.open('https://www.google.co.in/maps?q=<?php echo $row['lat'];?>,<?php echo $row['lng'];?>','_blank')">
	<i class="fa fa-map-marker"></i>&nbsp;VIEW ATTACHED LOCATION
</button>
&nbsp;&nbsp;&nbsp;
<?php
$pic = str_ireplace('"','',$row['pic']);
$pic = str_ireplace('../../../od-files/', '',$pic);
?>
<button class="btn btn-primary" onclick="window.open('od-files/<?php echo $pic;?>','_blank')">
	<i class="fa fa-image"></i>&nbsp;VIEW ATTACHED PICTURE
</button>
<br/><br/>

	<?php
} 
?>



		<div id="moreInputs" style="display:none">
		


		<?php 
		if($row['type'] == '1') 
		{
?>
<br/>
<div style="background:#f2f7f8;padding:20px;">
Upon approval, the deduction for above dates will be deduced from Pool balance.
<br/><br/>
<div style="display:none">
			New Intime<br/>
			<input type="time" name="" id="lstat1" class="inputBox" value="<?php echo $row['intime'];?>">	
<br/><br/>

			New Outtime<br/>
			<input type="time" name="" id="lstat2" class="inputBox"  value="<?php echo $row['outime'];?>">
			<input type="date" name="" id="lstat3" class="inputBox" style="display:none">
			</div>
			</div>	
<?php
		}
		if($row['type'] == '5')
		{
?>
<br/>
<div style="background:#f2f7f8;padding:20px;">
Upon approval, half deduction will be done from pool.
<div style="display:none">
			New Intime<br/>
			<input type="time" name="" id="lstat1" class="inputBox" style="display:none" value="<?php echo $row['intime'];?>">	


			New Outtime<br/>
			<input type="time" name="" id="lstat2" class="inputBox" style="display:none"  value="<?php echo $row['outime'];?>">
			<input type="date" name="" id="lstat3" class="inputBox" style="display:none">
			</div>
			</div>	
<?php
		}
		if($row['type'] == '2' || $row['type'] == '4') 
		{
?>
<br/>
<div style="background:#f2f7f8;padding:20px;">
Upon approval, new intime and outime will be alloted to the date in subject.
<br/><br/>
			New Intime<br/>
			<input type="time" name="" id="lstat1" class="inputBox" value="<?php echo $row['intime'];?>">	
<br/><br/>

			New Outtime<br/>
			<input type="time" name="" id="lstat2" class="inputBox"  value="<?php echo $row['outime'];?>">
			<input type="date" name="" id="lstat3" class="inputBox" style="display:none">
			</div>	
<?php
		}
		else if($row['type'] == '3') 
		{

			$getPool = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
			$rowPool = mysqli_fetch_array($getPool);
			$poolbalance = $rowPool['poolbalance'];
			?>
<br/>
<div style="background:#f2f7f8;padding:20px;">
Upon approval, new shift intime and outime will be alloted to the next working day.
<br/>
<br/>

			Shift Intime For Next Working Day<br/>
			<input type="time" name="" id="lstat1" class="inputBox" value="<?php echo $row['intime'];?>">	
<br/><br/>

			Shift Out-time For Next Working Day<br/>
			<input type="time" name="" id="lstat2" class="inputBox" value="<?php echo $row['outime'];?>">
<br/><br/>

			Where Next Working Day is<br/>
			<?php
			$ondate = strtotime($row['ondate']) + (24*60*60);
			
				$nextdate = date("Y-m-d",$ondate);
			?>
			<input type="date" name="" id="lstat3" class="inputBox" value="<?php echo $nextdate;?>">
<br/>
<br/>
You can also change the pool balance:
<br/>
<input type="text" name="" id="lstat5" class="inputBox" value="<?php echo $poolbalance;?>">

			</div>	

			<?php
		}
		?>
		

		</div>
		<br/><br/>
		Remark (Optional)
		<br/>
		<textarea class="inputBox" id="lstat4"  <?php if($row['status'] == '1') echo "readonly='true'";?>   style="height:140px;margin-top:5px;"><?php echo $row['remark'];?></textarea>
		<br/>
		<br/>
		<?php
		if($row['status'] != '1' && $row['type'] != '3')
		{
?>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('leaves/savestatus.php?id=<?php echo $id;?>','','justCloseModal','lstat',5,'','url:resp','','');" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<div id="resp">
				
			</div>
<?php			
		}
		else if($row['status'] != '1' && $row['type'] == '3')
		{
			?>
<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('leaves/savestatus.php?id=<?php echo $id;?>','','justCloseModal','lstat',6,'','url:resp','','');" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<div id="resp">
				
			</div>
			<?php
		}
		else
		{
			?>
<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="toast('Request already approved. You cannot change an approved request','toast bg-danger','5000')" type="button">
			<i class="fa fa-send"></i>&nbsp;&nbsp;UPDATE STATUS</button>
			<?php
		}
		?>
			<br/><br/><br/>

<?php
$getPrev = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.id = '$empid' AND leaverequests.id != '$id' ORDER BY `id` DESC") or die(mysqli_error($con));

?>

<table class="table table-hover shadow" cellpadding="0" cellspacing="0">
<tr>
	<th colspan="5" style="background:#fff !important;color:#333 !important;border-top:1px #eee solid !important">Other Requests By This Employee
	</th>
</tr>
<tr>
<th style="width:5%;">#</th>
<th style="width:15%;">Type</th>
<th style="width:30%">Details</th>
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
</tr>

<?php
$i=1;
while($rowPrev = mysqli_fetch_array($getPrev))
{
?>
<tr>
<td><?php echo $i;?></td>
<td>

<?php 
	if($rowPrev[2] == '1')
	{
		?>
	<i class="fa fa-circle" style="color:#ffb22b"></i>&nbsp;&nbsp;&nbsp;
		<?php
		echo "Leave";
	}
	else if($rowPrev[2] == '2')
	{
		?>
	<i class="fa fa-circle" style="color:#1976d2"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Timing Mismatch";
	}

	else if($rowPrev[2] == '3')
	{
		?>
	<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Overtime";
	}
else if($rowPrev[2] == '4')
	{
		?>
	<i class="fa fa-circle" style="color:#e32eab"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "OD Request";
	}

?></td>
<td>
<?php
if($rowPrev[2] == 1)
{
	?>

<span class="label label-default">
	<?php echo date("D, d M Y",strtotime($rowPrev[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo date("D, d M Y",strtotime($rowPrev[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-default">
	<?php echo $rowPrev[9];?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo $rowPrev[10];?></span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default">
<?php echo date("D, d M Y",strtotime($rowPrev[5]));?>
</span>



	<?php
}
?>
</td>


<td style="padding-top:15px;">
	<?php 
	if($rowPrev[6] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($rowPrev[6] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($rowPrev[6] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>


<td>
<?php echo date("d/m/y h:i A",strtotime($rowPrev[7]));?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>




			<br/><br/><br/>
	</div>
</div>