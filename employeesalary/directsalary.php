<?php
include("../include/config.php");
$saveurl = str_ireplace("getSalarydata.php", "save.php", $urltocall);
$callbackurl = str_ireplace("getSalarydata.php", "index.php", $urltocall);
$id = $_GET['id'];
$data = getData('payheads','*','name','ASC');
?>


<div class="moduleHead">

<div class="moduleHeading">
Employee Salary
</div>
</div>




<div class="row" style="background:#f7f8f8">
	<div class="col-sm-2 formLeft">
		Salary Amount
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="Name" id="cal1" class="inputBox">
	</div>	

</div>


<div class="row" >
	<div class="col-sm-2 formLeft">
		Salary Division
	</div>
	<div class="col-sm-10 formRight">


<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0" id="tableDiv">
		<tr>
			<th>#</th>
			

			<th>Payhead</th>
			<th>Type</th>
			<th>Value</th>
			
		</tr>

		<?php
		$j= 0;
		$k=1;
		 foreach($data as $row)
		  {  
   	
				
		?>
		<tr id="tableRow<?php echo $row['id'];?>">
			<td><?php echo $k;?></td>
			
			<td> <?php echo $row['name']?> 
							<input type="text" readonly="readonly" value="<?php echo $row['id'];?>" style="display:none" id="inp<?php echo $j; $j++;?>">
			</td>
			
			<td>
				<select class="inputBox" id="inp<?php echo $j;$j++?>">
					<option value="">--Select Type--</option>
					<option <?php if($row['type'] == 'Percentage') echo "selected= 'selected' ";?> value="Percentage">Percentage</option>
					<option <?php if($row['type'] == 'Amount') echo "selected= 'selected' ";?> value="Amount">Amount</option>
				</select>
			</td>
			
			<td>
				<input type="text" name="" id="inp<?php echo $j; $j++;?>" class="inputBox"  value="<?php echo $row['value']?>">
			</td>



		</tr>

		<?php
		$k++;
				}
		?>



</table>

	</div>	

</div>






<div class="row">
		<div class="col-sm-2 formLeft">
	
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="updateSalary();" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE SALARY</button>
			<br/><br/><br/>
	</div>	

</div>
