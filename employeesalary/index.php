<?php
include("../include/config.php");
$id = $_GET['id'];
$data = getData('payheads','*','name','ASC');

$getSalary = mysqli_query($con,"SELECT * FROM `salary` WHERE `empid` = '$id'") or die(mysqli_error($con));
$rowSalary = mysqli_fetch_array($getSalary);

$salid = $rowSalary[0];

$headValues = Array();
$getOld = mysqli_query($con,"SELECT * FROM `salarydetails` WHERE `salid` = '$salid'") or die(mysqli_error($con));
while($rowOld = mysqli_fetch_array($getOld))
{
	$headValues[$rowOld['payheadid']]['type'] = $rowOld['type'];
	$headValues[$rowOld['payheadid']]['value'] = $rowOld['value']; 
}
?>


<div class="moduleHead">

<div class="moduleHeading">
Employee Salary
</div>
</div>




<div class="row" style="background:#f7f8f8">
	<div class="col-sm-2 formLeft">
		Salary Amount
	</div>
	<div class="col-sm-10 formRight">
		<input type="number" name="req" title="Name" id="totalSalary" class="inputBox" value="<?php echo $rowSalary['salary'];?>">
	</div>	

</div>


<div class="row" >
	<div class="col-sm-2 formLeft">
		Salary Division
	</div>
	<div class="col-sm-10 formRight">


<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
		<tr>
			<th>#</th>
			

			<th>Payhead</th>
			<th>Type</th>
			<th>Value</th>
			
		</tr>

		<?php
		$j= 0;
		 foreach($data as $row)
		  {  
   	
				
		?>
		<tr id="tableRow<?php echo $row['id'];?>">
			<td><?php echo $j+1;?></td>
			
			<td> <?php echo $row['name']?> 
							<input type="text" readonly="readonly" value="<?php echo $row['id'];?>" style="display:none" id="headId<?php echo $j;?>">
			</td>
			
			<td>
				<select class="inputBox" id="headtype<?php echo $j;?>">
					<option <?php if($headValues[$row['id']]['type'] == 'P') echo "selected= 'selected' ";?> value="P">Percentage</option>
					<option <?php if($headValues[$row['id']]['type'] == 'F') echo "selected= 'selected' ";?> value="F">Flat</option>
				</select>
			</td>
			
			<td>
				<input type="number" name="" id="headValue<?php echo $j;?>" class="inputBox"  value="<?php echo $headValues[$row['id']]['value']?>">
			</td>



		</tr>

		<?php
		$j++;
				}
		?>



</table>
<input type="" id="totalHeads" value="<?php echo $j;?>" name="" style="display:none">

	</div>	

</div>






<div class="row">
		<div class="col-sm-2 formLeft">
	
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="updateSalary('<?php echo $id;?>');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE SALARY</button>
			<br/><br/><br/>
	</div>	

</div>
