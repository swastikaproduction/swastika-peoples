<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Expensenature extends Model {

    //
    protected $table = 'expensenature';

    public $timestamps = false;

}
