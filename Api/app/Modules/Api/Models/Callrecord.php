<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Callrecord extends Model {

    //
    protected $table = 'callrecord';

    public $timestamps = false;

}
