<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	$typeStr = " AND `status` = '1'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));	
}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));

}


$name = "Deductions_Cumilative.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>




<table border="1">
	<tr>
		<th style="width:5%">#</th>
		<th style="width:20%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:15%">Total Deduction</th>
		<th style="width:5%">From Salary</th>
		<th style="width:5%">From Pool</th>
		<th style="width:5%">Pool Opening</th>
		<th style="width:5%">Pool Closing</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>
	<td>
	
	<?php
	if($row['status'] == '1')
	{
		echo $row['deduction'];
		?>

		<?php
	}
	else
	{
		?>
		Not Calculated.
		<?php
	}
	?>
	
	</td>
	<td>
		<?php echo $row['salary_deduction'];?>

	</td>
	<td>
		<?php echo $row['pool_deduction'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'];?>
	</td>

	<td>
		<?php echo $row['pool_opening'] - $row['pool_deduction'];?>
	</td>



</tr>
		<?php
		$i++;
	}
	?>
</table>
