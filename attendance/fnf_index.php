<?php 
include("../include/config.php");
$employees = Array();
if($loggeduserid == '124')
{
$employees = getData('employee','*','name','ASC');	
}
else
{
	$employees = getData('employee','**','id',$loggeduserid);
}

?>
<div class="moduleHead"><br>
<div class="moduleHeading">
	FnF Calculation Details
</div>
</div>
<div class="tabelContainer shadow" style="height:auto;background:#fff">

<div class="row">
<div class="col-sm-12 subHead">

	
</div>

	<div class="col-sm-2 formLeft">
		Left Employee
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="atEmp">
	<?php
if($loggeduserid == '124')
{
?>
	<option value="ALL">ALL Employees</option>
<?php
}
	?>

	<?php
	foreach($employees as $val)
	{
		if($val['left'] == 1)
		{
		?>
			<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
		<?php
		}
	}
	?>
		</select>
	</div>	

</div>


<div class="row" style="border-bottom: 1px #eee solid;">
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
        <option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
<br/>

<button  class="btn btn-primary" id="atListViewHere"  onclick="getModule('attendance/getFnfSalary.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'atResult','','loading');atchangedone = 0;">
		<i class="fa fa-th-list"></i>&nbsp;&nbsp;GET DETAILS</button>

&nbsp;&nbsp;
	<button class="btn btn-primary"  onclick="window.open('attendance/getFnfSalaryReport.php?type=<?php echo $type;?>&emp='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT DATA</button>
	
		

		<br/><br/>
	</div>
</div>

<div id="atResult"></div>

</div>


