<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `circulars` ORDER BY `id` DESC");


?><div class="moduleHead">
<br/>
<?php
if($loggeduserid == '124')
{
?>

<div style="float:right">
	<button class="btn btn-primary"  onclick="getModule('circulars/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<?php
}
?>

	<div class="moduleHeading" onclick="toogleFormTable();">
	Circulars
	</div>
</div>

<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0">
<tr>
<th style="width:50px">#</th>
<th style="width:40%">Title</th>
<th>Preview</th>
<th>Date</th>
<?php
if($loggeduserid == '124')
{
?>
<th>Delete</th>
<?php
}
?>

</tr>
<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
	?>
<tr id="circRow<?php echo $row['id'];?>">
	<td><?php echo $i;?></td>
	<td><?php echo $row['title'];?></td>
	<td>
	<div style="height:5px;"></div>
		<span class="label label-primary" onclick="getModal('circulars/preview.php?id=<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')">
			<i class="fa fa-external-link"></i>&nbsp;&nbsp;PREVIEW
		</span>

	</td>
	<td><?php echo date("d-M-y h:i A",strtotime($row['createdate']));?></td>
	<?php
if($loggeduserid == '124')
{
?>
<td>
	<button class="btn btn-danger btn-sm" onclick="deleteCircular('<?php echo $row['id'];?>')">DELETE</button>

</td>
<?php
}
?>
</tr>

	<?php
	$i++;
}
?>
</table>
</div>
<br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/>