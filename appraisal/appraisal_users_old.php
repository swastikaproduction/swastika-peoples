 <div class="content-wrapper " style="background:#fff;height: 78%;" >
    <!-- Content Header (Page header) -->
    <section class="content-header">     
      <ol class="breadcrumb">        
        <li class="active">Appraisals Users</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <?PHP
            include'../include/config.php';
            //check if the starting row variable was passed in the URL or not
            if (!isset($_GET['startrow']) or !is_numeric($_GET['startrow'])) {
            //we give the value of the starting row to 0 because nothing was found in URL
            $startrow = 0;
            //otherwise we take the value from the URL
            } else {
            $startrow = (int)$_GET['startrow'];
            }
            //this part goes after the checking of the $_GET var
            $fetch = mysqli_query($con,"SELECT e.id,e.username,e.name,a.created_at ,a.appraised_mode,a.appraised_time,a.appraised_year FROM appraisal_users au INNER JOIN appraisals a ON a.id=au.app_id INNER JOIN employee e ON e.id = au.emp_id LIMIT $startrow, 10")or
            die(mysqli_error());
            $num=Mysqli_num_rows($fetch);
          if($num>0)
          {?>
        <div class="box-body">
       <table border='2px;' class="table table-bordered table-striped">
        <thead style="background: #81b7ea;color: white;">
        <tr><td>S.NO</td><td>Name</td><td>Appraisal Time</td><td>Create Date</td><td>Action</td></tr></thead>
        <?php
         $count =1;
        for($i=0;$i<$num;$i++)
        {
        $row=mysqli_fetch_array($fetch);
        ?>
        <tr>
          <td><?php echo $count++;?></td>
          <td><?php echo $row['name']?></td>          
          <td><?php echo $row['appraised_time'] ?> &nbsp;&nbsp;<?php echo $row['appraised_year'] ?></td>
          <td><?php echo $row['created_at']?></td>
         <td><span class="label label-success" onclick="sendHrReminder('<?php echo $row['id']?>');">Send Reminder</span></td>

        </tr>
        <?php
        }//for
        ?>
      </table>
    </div>
        <?php
        }
//now this is the link..
echo '<a href="default.php#&rtl=appraisal/appraisal_users_old.php?startrow='.($startrow+10).'&rtl=formDiv&rtl=tableDiv&rtl=loading"><span class="label label-success">Next</a></span>';

$prev = $_GET['startrow'] - 10;

//only print a "Previous" link if a "Next" was clicked
if ($prev >= 0)
    echo '<a href="default.php#&rtl=appraisal/appraisal_users_old.php?startrow='.$prev.'&rtl=formDiv&rtl=tableDiv&rtl=loading"><span class="label label-success" style="margin-left: 14px;">Previous</a></span>';
?>
            </div>
            <!-- /.box-body -->
      </div>
    </section>
  </div>
