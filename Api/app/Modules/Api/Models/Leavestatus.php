<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Leavestatus extends Model {

    //
    protected $table = 'leavestatus';

    public $timestamps = false;

}