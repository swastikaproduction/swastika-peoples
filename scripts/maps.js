

var searchBox;
var map;
var input;
var markers;
var places;
var bounds;
var icon;
var myposition;

var mycity;
var preinfowindow;
var preLat = '0';

var myLat = localStorage.getItem("myLat");
myLat = parseFloat(myLat);

var myLang = localStorage.getItem("myLang");
myLang = parseFloat(myLang);
      function initAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: myLat, lng: myLang},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

         google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });


if(preLat == '1')
{    image = {
  url: '../pin.svg',
  size: new google.maps.Size(24, 24 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(40,40),
  scaledSize: new google.maps.Size(24, 24)
};




    myposition = new google.maps.Marker({
    position: {lat: myLat, lng: myLang},
    map: map,
    optimized: false,
    icon:image
  });     




 preinfowindow = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-success">THIS IS THE CURRENT SPOT</button>'
  });
  preinfowindow.open(map,myposition);
}



//  animateRadius();

        // Create the search box and link it to the UI element.
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location,
              optimized: false
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

 var infowindow;
 var marker;
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter=new google.maps.LatLng(51.508742,-0.120850);
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas, mapOptions);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });
}


  var direction = 1;
    var rMin = 500, rMax = 700;

var animCity;
function animateRadius()
{
       animCity = setInterval(function() {
            var radius = myCity.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            myCity.setRadius(radius + direction * 100);
        }, 100);
}

var image;

function placeMarker(map, location) {
  if(marker)
  {
  marker.setMap(null);    
  }

     image = {
  url: '../pin.svg',
  size: new google.maps.Size(12, 12 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(10,10),
  scaledSize: new google.maps.Size(12, 12)
};



marker = new google.maps.Marker({
    position: location,
    map: map,
    optimized: false,
    icon:image
  });

google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);
  });
 infowindow = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-danger" onclick="sendToTop('+location.lat()+','+location.lng()+');this.innerHTML = \'SPOT USED\';this.className = \'btn btn-sm btn-success\'">USE THIS LOCATION</button>',
    pixelOffset: new google.maps.Size(0,0)
  });
  infowindow.open(map,marker);
}


function sendToTop(lat,lng)
{
 window.top.window.document.getElementById('stop2').readOnly = false;
 window.top.window.document.getElementById('stop3').readOnly = false;


 window.top.window.document.getElementById('stop2').lang = lat;  
 window.top.window.document.getElementById('stop3').lang = lng;
 window.top.window.document.getElementById('stop2').value = lat;  
 window.top.window.document.getElementById('stop3').value = lng;


 }