<?php
include("../include/config.php");
?>

<div class="moduleHead">
<br/>
	<div class="moduleHeading" onclick="toogleFormTable();">
Dashboard
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		
		<div class="tabelContainer shadow" style="height:592px;background:#fff;border-radius:5px;">
		<div style="height:150px;background:transparent url('images/profile-bg.jpg') no-repeat scroll center center;background-size:cover;border-top-left-radius:5px;border-top-right-radius:5px;position:relative">

				<div style="position:absolute;bottom:-50px;left:0px;width:100%;height:100px;">
				<center>
					<img src="images/default.png" alt="" style="height:100px;"/>
					</center>
				</div>

		</div>
			<?php include('profile.php');?>
		</div>
	</div>
	<div class="col-sm-4">
		
		<div class="tabelContainer shadow" style="height:592px;background:#fff;border-radius:5px">
			<div class="subHead" style="margin-bottom:0px">Recent Notifications</div>
			<?php include('notifications.php');?>
		</div>
	</div>


	<div class="col-sm-4">
		
		<div class="tabelContainer shadow" style="height:592px;background:#fff;border-radius:5px">
			<div class="subHead" style="margin-bottom:0px">Recent Circulars</div>
			<?php include('circulars.php');?>
		</div>
	</div>

</div>
<br/><br/>
<?php if($loggeduserid !=='124'){?>
 <div class="row">
	<div class="col-sm-8">
	<div class="tabelContainer shadow" style="height:420px;background:#fff;border-radius:5px">
		<iframe src="dashboard/deduction-chart.php" frameborder="0" scrolling="no" style="height:400px;width:100%">
		</iframe>
		</div>
	</div>

	<div class="col-sm-4">
	<div class="tabelContainer shadow" style="height:200px;background:#26DAD2;border-radius:5px">
	<div style="padding:40px;color:#fff;font-size:25px">
	<div style="float:right">
				<i class="fa fa-diamond" style=""></i>
	</div>
		<span style="font-size:40px;color:#fff">
		<?php echo $bal;?></span>
		<br/>
		Available Pool Balance
		<br/>
		<div style="display:inline-block;width:70%;height:5px;border-radius:100px;background:#fff;margin-top:10px;"></div>
		</div>
		</div>

<!-- <div class="tabelContainer shadow" style="height:200px;background:#5C4AC7;border-radius:5px;margin-top:20px;">
	<div style="padding:40px;color:#fff;font-size:25px">
	<div style="float:right">
				<i class="fa fa-rupee" style=""></i>
	</div>
		<span style="font-size:40px;color:#fff">
		<?php echo $sal;?>/-</span>
		<br/>
		Current Salary
				<br/>
		<div style="display:inline-block;width:70%;height:5px;border-radius:100px;background:#fff;margin-top:10px;"></div>

		</div>
		</div>

	
	</div>	-->
	</div> 
</div>
 <?php }?>
<br/><br/><br/><br/>
<br/><br/><br/><br/>
<br/><br/><br/><br/>