#!/usr/bin/php
<?php

use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';

error_reporting(0);

date_default_timezone_set('Asia/Kolkata');
global $con;

$con = mysqli_connect("localhost","root","");
if(!$con)
{
  die(mysqli_error($con));
}
mysqli_select_db($con,'hrm');

// $datetime = date("Y-m-d H:i:s");
// $date = date("d/m/Y");
// $daterange = date("d").date("m").date("Y");
// $daterange = $daterange."-".$daterange;

$datetime = date("Y-m-d H:i:s");
$yesterday = time() - (24 * 60 * 60);
$thisdate = date("Y-m-d",$yesterday);
$check =  date('D', strtotime($datetime));
if($check=='Mon'){
 echo "false";  
}else if($check=='Sun'){
 echo "false";
}else{
 
//$thisdate = $_GET['date'];
//$date = "10/12/2018";
$date = date("d/m/Y",strtotime($thisdate));
$daterange = date("d",strtotime($thisdate)).date("m",strtotime($thisdate)).date("Y",strtotime($thisdate));
$daterange = $daterange."-".$daterange;

$url = "http://192.168.0.241/cosec/api.svc/v2/attendance-daily?action=get;format=json;date-range=".$daterange;


$username = 'sa';
$password = 'poiuytrewq123';
 
$context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
));
$contents = file_get_contents($url, false, $context);
$contents = json_decode($contents,true);

$fulldata = $contents['attendance-daily'];




foreach($fulldata as $val)
    {    
   if($val['punch1']==''){

      $userid =  $val['userid']; 

     $query = mysqli_query($con,"select * from employee where empid='$userid' and empid NOT IN('IND1100001') and `left` = '0'")or die(mysqli_error($con));
     $result = mysqli_fetch_array($query);
     $workemail = $result['workemail'];
     $message='<p>Dear '.$result['name'].',</p>
<p>Your punch for the date '.$date.' is missing in HRM which is marking you as absent in HRM.</p>
<p>Please revert to HR at "attendance" (attendance@swastika.co.in); within 48 hours of the mail along with the valid reason for the missing punch.</p>
<p><table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="background: white;"><tbody style=""><tr style=""><td style="padding: 0in;"><table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;"><tbody style=""><tr style=""><td style="padding: 0in;"><table class="MsoNormalTable" border="0" cellpadding="0" style="margin-left: 3.75pt;"><tbody style=""><tr style=""><td style="padding: 0in;"><p class="MsoNormal" style="margin-bottom: 10pt; line-height: 115%;">Thanks &amp; Regards,<span style="font-size: 9pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(84, 141, 212);"></span></p></td><td rowspan="6" style="padding: 0in;"><p class="MsoNormal" style="margin-bottom: 10pt; line-height: 115%;"><span style="color: rgb(31, 73, 125);"><img width="158" height="75" id="Picture_x0020_2" pnsrc="cid:image001.jpg@01D5319B.A2441380" alt="letterhead swastika.jpg" style="" src="https://czimapi.logix.in/service/home/~/?auth=co&amp;id=820&amp;part=2"></span><span style="font-size: 12pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(31, 73, 125);"></span></p></td></tr><tr style=""><td style="padding: 0in;"></td></tr><tr style=""><td style="padding: 0in;"><p class="MsoNormal" style=""><b style=""><span style="font-size: 8.5pt; font-family: arial, sans-serif; color: rgb(73, 64, 60);">HR TEAM </span></b><b style=""><span style="font-size: 8.5pt; font-family: arial, sans-serif; color: rgb(31, 73, 125);">,</span></b><b style=""><span style="font-size: 8.5pt; font-family: arial, sans-serif; color: rgb(73, 64, 60);"> Head Office </span></b><b style=""><span style="font-size: 8.5pt; font-family: arial, sans-serif; color: rgb(73, 64, 60);"></span></b></p><p class="MsoNormal" style="margin-bottom: 10pt; line-height: 115%;"><b style=""><span style="font-size: 8.5pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(73, 64, 60);">Swastika Investmart Ltd.</span></b><i style=""><span style="font-size: 9pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(84, 141, 212);"> </span></i><b style=""><span style="font-size: 8.5pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(73, 64, 60);"></span></b></p></td></tr><tr style=""><td style="padding: 0in;"></td></tr><tr style=""><td style="padding: 0in;"><p class="MsoNormal" style="margin-bottom: 10pt; line-height: 115%;"><span style="font-size: 8.5pt; line-height: 115%; font-family: arial, sans-serif;">Direct No.: <span class="Object" role="link" id="OBJ_PREFIX_DWT241_com_zimbra_phone"><a href="callto:0731-6644255,56,57" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">0731-6644255,56,57</a></span></span><span style="font-size: 12pt; line-height: 115%; font-family: arial, sans-serif;"></span></p></td></tr><tr style=""><td style="padding: 0in;"><p class="MsoNormal" style="margin-bottom: 10pt; line-height: 115%;"><a href="http://www.swastika.co.in/" target="_blank" style=""><span style="font-size: 8.5pt; line-height: 115%; font-family: arial, sans-serif; color: blue;">www.swastika.co.in</span></a><span style="font-size: 8.5pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(31, 73, 125);"> </span><span style="font-size: 12pt; line-height: 115%; font-family: arial, sans-serif; color: rgb(227, 108, 10);"></span></p></td></tr></tbody></table></td></tr><tr style=""><td style="padding: 0in;"></td></tr><tr style=""><td style="padding: 0in;"></td></tr></tbody></table></td></tr></tbody></table></p>
<p>This is a System generated mail, please do not reply.</p>';

$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true;
$mail->Username = 'swastika.developer@gmail.com';
$mail->Password = 'sil@321#';
$mail->setFrom('swastika.developer@gmail.com', 'Swastika Investmart');
//$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
$mail->addAddress($workemail);
$mail->addBCC('swastikamisspunch@gmail.com');
$mail->Subject = 'Miss punch mail';
$mail->Body = $message;
$mail->AltBody = 'Miss puch request';
// $mail->addAttachment('test.txt');
if (!$mail->send()) {

   
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else { 
     date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
     $currentdate = date('d-m-Y H:i:s'); 
     $query = mysqli_query($con,"insert into misspunchlog(empid,status,createdat)values('$userid',1,'$currentdate')");
     echo 'Message sent!';
}
   
 
      }   

 

    }

}




    
  