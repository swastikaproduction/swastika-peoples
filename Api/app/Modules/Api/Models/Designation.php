<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model {

    //
    protected $table = 'designation';

    public $timestamps = false;

}
