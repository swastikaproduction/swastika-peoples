<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model {

    //
    protected $table = 'calendar';

    public $timestamps = false;

}