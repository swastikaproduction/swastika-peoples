<?php
include("../include/config.php");
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
	$typeStr = " AND `status` = '1'";

$name = "Net_Payable_Salary_Cumulative_All_Employees.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>

<table border="1">
	<tr>
		<th>Employee Name</th>
		<th>Employee Designation</th>
		<th>Month</th>
		<th>Year</th>
		<th>Actual Salary</th>
		<th>Deductions</th>
		<th>After attendance deduction</th>
		<th>After PF+PT deductions</th>
		<th>Basic</th>
		<th>HRA</th>
		<th>DA</th>
		<th>Bonus</th>
		<th>Coveyance</th>
		<th>DOM</th>
		<th>Refreshments</th>
		<th>PF</th>
		<th>PT</th>
		<th>Leave Deductions</th>
		<th>Net Pay</th>
	</tr>

<?php
$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `createdate`") or die(mysqli_error($con));
while($rowLog = mysqli_fetch_array($getData))
{
$empid = $rowLog['empid'];
$getEmp = mysqli_query($con,"SELECT employee.name,designation.name,employee.pf FROM employee,designation WHERE employee.designation = designation.id AND employee.id = '$empid'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

if($rowLog['edited_salary'] != '')
{
	$bifurcation = $rowLog['bifurcated'];
	$bifurcation = explode("::",$bifurcation);
	?>
<tr>
	<td><?php echo $rowEmp[0];?></td>
	<td><?php echo $rowEmp[1];?></td>
	<td><?php echo $rowLog['month'];?></td>
	<td><?php echo $rowLog['year'];?></td>
	<td><?php echo $rowLog['actual_salary'];?></td>
	<td><?php echo $rowLog['salary_deduction'];?></td>
	<td><?php echo $rowLog['final_salary'];?></td>
	<td><?php echo $netpay = ($bifurcation[0]+$bifurcation[3]+$bifurcation[2]+$bifurcation[5]+$bifurcation[7]+$bifurcation[6]+$bifurcation[8]) - ($bifurcation[1]+$bifurcation[4]);?></td>
	<td><?php echo $bifurcation[0];?></td>
	<td><?php echo $bifurcation[3];?></td>
	<td><?php echo $bifurcation[2];?></td>
	<td><?php echo $bifurcation[5];?></td>
	<td><?php echo $bifurcation[7];?></td>
	<td><?php echo $bifurcation[6];?></td>
	<td><?php echo $bifurcation[8];?></td>
	<td><?php echo $bifurcation[1];?></td>
	<td><?php echo $bifurcation[4];?></td>
	<td><?php echo $rowLog['actual_salary'] - $netpay;?></td>
	<td><?php echo $netpay;?></td>
</tr>

	<?php
}
else
{
	$pfyes=$rowEmp[2];
	$mth = "2017-".$rowLog['month']."-01";
	$salary = $rowLog['final_salary'];
	$actualSalary = $rowLog['actual_salary'];;
	$calcSalary = $salary;
		include("bifur-logic.php");
	?>
	<tr>
	<td><?php echo $rowEmp[0];?></td>
	<td><?php echo $rowEmp[1];?></td>
	<td><?php echo $rowLog['month'];?></td>
	<td><?php echo $rowLog['year'];?></td>
	<td><?php echo $rowLog['actual_salary'];?></td>
	<td><?php echo $rowLog['salary_deduction'];?></td>
	<td><?php echo $rowLog['final_salary'];?></td>
	<td><?php echo $netpay = $total_earnings-$total_deductions;?></td>
	<td><?php echo $basic_real;?></td>
	<td><?php echo $hra;?></td>
	<td><?php echo $da_real;?></td>
	<td><?php echo $bonus;?></td>
	<td><?php echo $conv;?></td>
	<td><?php echo $dom;?></td>
	<td><?php echo $ref;?></td>
	<td><?php echo $twopf;?></td>
	<td><?php echo $pt;?></td>
	<td><?php echo $rowLog['actual_salary'] - $netpay;?></td>
	<td <?php if($netpay < 0) { echo "style='color:red'";} ?>><?php echo $netpay;?></td>
</tr>

	<?php
}

}






?>

</table>