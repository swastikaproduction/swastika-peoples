<?php
include("../include/config.php");
$type = $_GET['type'];
$month = $_GET['month'];

$year = $_GET['year'];
$loanArray = Array();
$loanledger = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$jvholdledgername = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$loanledger[$rowLoans['empid']] = $rowLoans['loanremark'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
	$jvholdledgername[$rowLoans['empid']] = $rowLoans['jvholdledger'];
}


$name = "tallyexport_".$month."_".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


?>

<table border="1" cellpadding="5">
	<tr>
		<th>DATE</th>
		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>

		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>
		<th>NARRATION</th>
		<th>VOUCHER TYPE</th>
		<th>error message</th>

	</tr>

<?php
if($month < 10)
{
$month = "0".$month;    
}

if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' AND employee.depttype != 'Cash'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));


}
else if($type == 'Cash')
{
		$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));

}
else
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));


}



while($row= mysqli_fetch_array($getData))
{


//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}


$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;


if($type != 'Housekeeping' && $type != 'Cash')
{

if($row[8] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}


if($pfflag == '1')
{

		if($calculatedsalary < 8960)
		{ 

			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);

			if($pfpayable == '')
			{
				$pfpayable = round($gross * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);		

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}	

		if($calculatedsalary >= 8960 && $calculatedsalary <= 14292)
		{    
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - $basic-$pfemployer;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 14293 && $calculatedsalary <= 26800)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross*0.60);
			$hra = round($basic*0.40);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = $ctc-$basic-$hra-$pfemployer;
			
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary > 26801 && $calculatedsalary <= 29999)
		{ $ctc = $calculatedsalary;
			 $gross = $grosssalary;
			$basic = round($calculatedsalary * 0.60);
			$hra = round($basic * 0.40);
			if($pfpayable == '')
			{
			$pfpayable = 3600;
			}
			

						if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = round($calculatedsalary * 0.05);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary >= 30000)
		{   $ctc = $calculatedsalary;
			 $gross = $grosssalary;
			$basic = round($calculatedsalary * 0.60);
			$hra = round($calculatedsalary * 0.24);
			$bonus = round($calculatedsalary * 0.10);
			if($pfpayable == '')
			{
			$pfpayable = 3600;
			}
		

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9000)
	{
		    $ctc = $grosssalary;
		    $gross = $grosssalary;
			$basic = $calculatedsalary;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

	}

	else if($calculatedsalary >= 9000 && $calculatedsalary <= 15000)
	{       $ctc = $grosssalary;
		   $gross = $grosssalary;
			$basic = 8000;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.10);
			$hra = $calculatedsalary - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000)
	{       $ctc = $grosssalary;
	        $gross = $grosssalary;
			$basic = round($calculatedsalary * 0.6);
			$pfpayable = 0;
			$da = 0;
			
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.10);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}
}

}
else
{
	$basic = $calculatedsalary;
}

$dataArray = Array();
for($k=0;$k<84;$k++)
{
	$dataArray[$k] = ''; 
}

$dataArray[0]= 'ABA';
$dataArray[1]= $row[7];

/*
for($l=1;$l<78;$l++)
{
	if($l%4 == 0)
	{
		$dataArray[$l+1] = $row[7];		
	}
}

*/

$dataArray[2] = 'DR.';
$dataArray[6] = 'DR.';
$dataArray[10] = 'DR.';
$dataArray[14] = 'DR.';
$dataArray[18] = 'DR.';
$dataArray[22] = 'DR.';
$dataArray[26] = 'DR.';
$dataArray[30] = 'DR.';
$dataArray[34] = 'DR.';
$dataArray[38] = 'DR.';

if($type == 'Housekeeping' || $type == 'Cash')
{
$dataArray[3] = 'Office Expenses';
}
else
{
$dataArray[3] = 'Basic Salary';	
}


$dataArray[4] = $basic;

if($da != 0)
{
$dataArray[5] = $row[7];
$dataArray[7] = 'Dearness Allowance';
$dataArray[8] = $da;	
}


if($hra != 0)
{
$dataArray[9] = $row[7];
$dataArray[11]  = 'House Rent Allowance';
$dataArray[12] = $hra;

}



if($bonus != 0)
{
$dataArray[13] = $row[7];	
$dataArray[15] = 'Bonus';
$dataArray[16] = $bonus;

}




if($pfemployer != 0)
{
$dataArray[17] = $row[7];	
$dataArray[19] = 'PF Contribution (Employers)';
$dataArray[20] = $pfemployer;

}




if($refreshment != 0)
{
$dataArray[21] = $row[7];		
$dataArray[23] = 'Employee Daily Refreshment';
$dataArray[24] = $refreshment;

}





$dataArray[42] = 'CR.';
$dataArray[46] = 'CR.';
$dataArray[50] = 'CR.';
$dataArray[54] = 'CR.';
$dataArray[58] = 'CR.';
$dataArray[62] = 'CR.';
$dataArray[66] = 'CR.';
$dataArray[70] = 'CR.';
$dataArray[74] = 'CR.';
$dataArray[78] = 'CR.';

$dataArray[41] = $row[7];
$dataArray[43] = $row[1];
$dataArray[44] = round($netpay) - ($loan+$jvhold);

if($pfpayable != 0)
{
$dataArray[45] = $row[7];
$dataArray[47] = 'PF PAYABLE';
$dataArray[48] = $pfpayable;	
}

if($pt != 0)
{
$dataArray[49] = $row[7];	
$dataArray[51] = 'Professional Tax Payable';
$dataArray[52]= $pt;
}





if($jvhold != 0)
{
$dataArray[53] = $row[7];	
$dataArray[55] = $jvholdledgername[$row[6]];
$dataArray[56]= $jvhold;
}



if($loan != 0)
{
$dataArray[57] = $row[7];	
$dataArray[59] = $loanledger[$row[6]];
$dataArray[60]= $loan;
}



$dataArray[81] = 'Salary provided for the month of '.$month.'-'.$year;
$dataArray[82] = 'JOURNAL SIL';

?>
<tr>
	<?php
	for($p=0;$p<count($dataArray);$p++)
	{
		?>
<td><?php echo $dataArray[$p];?></td>
		<?php
	}
	?>
</tr>


<?php
}
?>

</table>