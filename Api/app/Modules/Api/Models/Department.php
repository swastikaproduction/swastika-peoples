<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    //
    protected $table = 'departments';

    public $timestamps = false;

}
