<?php
$getData = mysqli_query($con,"SELECT * FROM `feed` ORDER BY `createdate` DESC") or die(mysqli_error($con));
?>

<div style="background:#f7f7f8;padding:10px 10px 20px 10px;font-size:14px;font-weight:bold">Recent Activities


  <div class="btn-group btn-group-sm" style="float:right">
  <button type="button" class="btn btn-default active" id="feedlist0" onclick="playChart('feedlist','today',this)">TODAY</button>
  <button type="button" class="btn btn-default"  id="feedlist1" onclick="playChart('feedlist','thismonth',this)">THIS MONTH</button>
  <button type="button" class="btn btn-default"  id="feedlist2" onclick="buttonToggle('feedlist',this);$('#customBoxFeed').fadeIn();">CUSTOM DATE</button>

  </div>
    <div style="padding:0px 10px;display:none;background:#f8f7f7;width:100%" class="" id="customBoxFeed">
<div class="row" style="background:#f8f7f7">
<br/><br/>
    From Date

<br/>
    <input type="date" name="fromdate" title="From Date" id="feedFrom" class="inputBox">

<br/><br/>
  
    To Date
<br/>

    <input type="date" name="todate" title="To Date" id="feedTo" class="inputBox">
 <br/><br/>

      <button class="btn btn-sm btn-success" onclick="playChart('feedlist','custom',document.getElementById('feedlist2'))">GET ACTIVITIES&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
      <br/>
  

</div>



  </div>  
<br/></div>
<div style="border:1px #eee solid;height:700px;overflow-y:scroll" id="feedData">
<?php
while($row = mysqli_fetch_array($getData))
{
	?>
<div class="feedBox" onclick="getFeedBox('<?php echo $row['id'];?>')">
	<img src="images/default.png" class="feedImage" alt=""/>
	<div class="feedInner">
	<?php echo $row['text'];?><br/>
		<br/>
	<span class="label label-default"><?php echo $row['comments'];?> Comments</span>
	<div style="float:right">
	<span class="time">
	@ <?php echo $row['createdate'];?></span>
	</div>
	</div>
</div>

	<?php
}
?></div>