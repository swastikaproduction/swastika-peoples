    <!-- Main content -->
    <?php 
    include'../include/config.php';
    $emp_id = $_COOKIE['emanagerid'];
    ?>
   




    <section class="content" style="background: #fff;">
    <?php
    if ( isset($_GET['success']) && $_GET['success'] == 1 )
    {?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Submit Successfully..!
    </div>   <?php
    }
    ?> 
    <?php
    if ( isset($_GET['success']) && $_GET['success'] == 2 )
    {?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Save Successfully..!
    </div>   <?php
    }
    ?> 
            
     
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">        
            <div class="box">
            <div class="box-header">
              <h5 class="box-title">Employee appraisal setting <?php echo $checkresult['id'];?> </h5>
            </div>            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                 <th>Appraisal Mode</th>
                  <th>Period</th>                
                  <th>Status</th>
                  <th>Stage</th>
                  <th>Employee End Date</th>
                  <th>Managers End Date</th>  
                  <th>HOD's End Date</th>
                  <th>Create Date</th>
                  <th>Action</th>
                                 
                </tr>
                </thead>
                <tbody>
                  <?php                   
                     
                       $count = 1;                             
                       $query = mysqli_query($con,"SELECT a.eDate,a.mDate,a.hDate,a.createdAt,ap.appraised_mode,ap.appraised_time,ap.appraised_year,a.status,a.stage,ap.id as appid,au.status as submitstatus FROM appraisalstart a INNER JOIN appraisalusers au ON au.appId = a.appId INNER JOIN appraisals ap ON ap.id = a.appId WHERE au.empId='$emp_id'");
                       while($row=mysqli_fetch_array($query)){
                        ?>

                <tr>
                  <?php
                     if($row['appraised_mode'=='quaterly']){
                      $mode = 'Quarterly';
                     }else if($row['appraised_mode']=='halfyraly'){
                      $mode = 'Half-Yearly';
                     }else{
                      $mode='Yearly';
                     }

                    if($row['status']=='o'){
                      $status = 'open';
                     }else{
                      $status = 'close';
                    }

                    if($row['submitstatus']=='open'){

                      $stage = 'Appraisal Start';                      
                                  
                    }elseif($row['submitstatus']=='save'){
                      $stage = 'Saved By Employee';
                     
                    }else if($row['submitstatus']=='submit'){
                         $stage = 'Submitted by Employee';
                       
                    }else if($row['submitstatus']=='msave'){
                         $stage = 'Saved by Manager';
                       
                    }else if($row['submitstatus']=='msubmit'){
                         $stage = 'Submitted by Manager';
                       
                    }else if($row['submitstatus']=='hsave'){
                         $stage = 'Saved by HOD';
                       
                    }else if($row['submitstatus']=='hsubmit'){
                         $stage = 'Submitted by HOD';
                       
                    }
                  ?>
                  <td><?php echo $count++; ?></td>
                  <td><?php echo $mode;?></td>
                  <td><?php echo $row['appraised_time'];?> <?php echo $row['appraised_year'];?></td>
                  <td><?php echo $row['status'];?></td>
                  <td><span style="background: orange;color: white;"><?php echo $stage?></span></td>
                  <td><?php echo $row['eDate'];?></td>
                  <td><?php echo $row['mDate'];?></td>
                  <td><?php echo $row['hDate'];?></td>                  
                  <td><?php echo $row['createdAt'];?></td>

                  
                               
                    <td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/employee-appraisal.php?issubmitted=<?php echo $row['submitstatus'];?>&app_id=<?php echo $row['appid'];?>','formDiv','tableDiv','loading');" >Appraisal Start </span>
                    </td>
                 
                  
                              
                </tr> 
                  <?php
                       }
                  ?>                          
                 
                </tbody>
             
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
  </div>
</div></section>  
</div>
<script type="text/javascript">
  $('#removeParent').on("click",".removeField", function(e){ //user click on remove text
    $(this).parent('div').remove(); 
  });
</script>
<script type="text/javascript">
  
  function send_reminder(){

    alert('Reminder send successfully..!');
  }
</script>

