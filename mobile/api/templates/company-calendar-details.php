<?php
include("../apiconfig.php");
$id = $_GET['id'];
$branchArray = Array();
$getBranches = mysqli_query($con,"SELECT * FROM `branch` ORDER BY `name` ASC") or die(mysqli_error($con));

while($rowBranches = mysqli_fetch_array($getBranches))
{
	$branchArray[$rowBranches['id']] = $rowBranches['name'];
}
$data = tableData("calendar",$id);

	$tbr = $data['branch'];
	$tbr = explode(",",$tbr);
	foreach($tbr as $tb)
	{
		$brStr .= '<span class="label label-warning" style="font-size:12px;margin:5px;display:inline-block">'.$branchArray[$tb]."</span> ";
	}
	$brStr = substr($brStr, 0,-1);
	$timing= date("h:i A",strtotime('2018-01-01 '.$data['from']))." to ".date("h:i A",strtotime('2018-01-01 '.$data['to']))." : ".round($data['hours'],2)." hours";
	$type = $data['type'];
	$name = $data['name'];

	if($type == '1')
	{
		$type =  "Holiday";
		$dataContent= 'Due to <strong>'.$name.'</strong> <br/><br/> <span style="font-size:12px;color:#999">in branches<span><br/> '.$brStr;

	}
	else
	{
		$type =  "Different Shift Timing";
		$dataContent= $timing. ' due to <strong>'.$name.'</strong> <br/><br/> <span style="font-size:12px;color:#999">in branches<span><br/> '.$brStr;
	}

?>

<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $type;?></h4>
      </div>
      <div class="modal-body">
      
     <?php echo $dataContent;?>
        
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>