<?php
include("../apiconfig.php");
$getData = mysqli_query($con,"SELECT * FROM `circulars` ORDER BY `id` DESC") or die(mysqli_error($con));


?>

<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">


<div style="padding:0px;position:relative">

<?php
if(mysqli_num_rows($getData) == 0)
{
?>
<center>
	<br><br><br><br>
	<i class="fa fa-coffee" style="font-size:40px;color:#ccc"></i>
	<br>
	<br>
	<span style="color:#ccc">There are no circulars yet on this system.<br>
	Sit back and relax.</span>
</center>
<?php
}
else
{
?>

<div style="position:fixed;top:70px;left: 29px;background:#bddaf5;width: 10px;height:1000px;z-index:1;border-radius:20px;opacity:0.5"></div>
<div class="" style="height:auto;background:#fff;z-index:10">



<?php
while($row = mysqli_fetch_array($getData))
{
	?>
<div class="calStrip" style="padding:15px 10px;">

<table style="width:100%">
	<tbody>


	<tr onclick="changePage('viewThree','templates/viewThree.html','templates/circular-details.php?id=<?php echo $row['id'];?>','viewThreeRes','','strong','homeHeader');globeTitle = ''; globeTitleId = 'viewthreetitle'">
		<td class="timeDotTd">
		<div class="timeDotHolder">
		<i class="fa fa-globe" style="color:#41b9e9"></i>			
		</div>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
	<span class="label label-default">
			<?php echo date("D, d-M h:i A",strtotime($row['createdate']));?>
			</span>
	</div>	
	<span style="font-size:16px;font-weight:400;text-transform:capitalize">
		<?php echo $row['title'];?>	</span>
	<br>
<div style="margin-top:5px;border-bottom:1px #">
<?php
$temp = $row['message'];
$temp = strip_tags($temp);
$temp = substr($temp,0,100); 
?>
<?php echo $temp;?>..
</div>
<br/>
<div class="halfBorder"></div>
		</td>
	</tr>
</tbody></table>


</div>
	<?php
}

}
?>



</div>
</div>
</div>
</div>

