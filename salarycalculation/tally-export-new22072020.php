<?php
include("../include/config.php");
$type = $_GET['type'];
$month = $_GET['month'];

$year = $_GET['year'];
$loanArray = Array();
$loanledger = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$jvholdledgername = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$loanledger[$rowLoans['empid']] = $rowLoans['loanremark'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
	$jvholdledgername[$rowLoans['empid']] = $rowLoans['jvholdledger'];
}


 $name = "tallyexport_".$month."_".$year.".xls";
 header("Content-Disposition: attachment; filename=\"$name\"");
 header("Content-Type: application/vnd.ms-excel");


?>

<table border="1" cellpadding="5">
	<tr>
		<th>DATE</th>
		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>

		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>
		<th>NARRATION</th>
		<th>VOUCHER TYPE</th>
		<th>error message</th>

	</tr>

<?php
if($month < 10)
{
$month = "0".$month;    
}

if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' AND employee.depttype != 'Cash'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));


}
else if($type == 'Cash')
{
		$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));

}
else if($type == 'All')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype,employee.department ,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id  AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));


}

$freez_table = 'freeze_'.$month.'_'.$year;
$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($rowTable = mysqli_fetch_array($getTables))
{
	$tables[] = $rowTable[0];
}


while($row= mysqli_fetch_array($getData))
{
if(in_array($freez_table,$tables)){
$getFreezedData = mysqli_query($con,"SELECT `ctc`, `gross`, `basic`, `pfpayable`, `da`, `pfemployer`, `otherdeduction`, `bonus`, `hra`, `refreshment`,`hold`,`loan`,`jvhold`,`leavedeductions`,`calculatedsalary`,`totalearnings`, `totaldeductions`, `netpay`,`pt` from `$freez_table` WHERE empid='$row[6]'") or die(mysqli_error($con));
$rowFreezed = mysqli_fetch_array($getFreezedData);
$grosssalary = $rowFreezed['ctc'];
$gross = $rowFreezed['gross'];
$leavedeductions = $rowFreezed['leavedeductions'];
$hold = $rowFreezed['hold'];
$loan = $rowFreezed['loan']; 
$jvhold = $rowFreezed['jvhold']; 
$calculatedsalary = $rowFreezed['calculatedsalary'];
$basic = $rowFreezed['basic'];
$da = $rowFreezed['da'];
$hra = $rowFreezed['hra'];
$bonus = $rowFreezed['bonus'];
$refreshment = $rowFreezed['refreshment'];
$pfemployer = $rowFreezed['pfemployer'];
$pfpayable = $rowFreezed['pfpayable'];
$otherdeduction = $rowFreezed['otherdeduction'];
$pt = $rowFreezed['pt'];
$netpay = $rowFreezed['netpay'];
$totalearnings = $rowFreezed['totalearnings'];
$totaldeductions = $rowFreezed['totaldeductions'];
}
else{

//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}


$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;


if($type != 'Housekeeping' && $type != 'Cash')
{

if($row[8] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}


if($row['pt']=='1'){

	$pt = $pt;
}else{
	$pt = 0;
}

$odflag = $row['od'];

if($pfflag == '1')
{

		if($calculatedsalary < 9001)
		{ 

			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$pfpayable = round($gross * 0.24);		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
            $otherdeduction=0;
			$pfemployer = round($pfpayable/2);	
			$basic = $ctc - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;

			$netpay = $totalearnings - $totaldeductions;
		}	

		else if($calculatedsalary >= 9001 && $calculatedsalary < 15000)
		{    
			
			$ctc = $calculatedsalary;
			$basic = 8400;
			$hra = 0;
			$otherdeduction=0;
			
			if($pfpayable == '')
			{
			    $pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}		
			$pfemployer = round($pfpayable/2);			
			$gross = $ctc-$pfemployer;
			$bonus = $ctc - ($basic+$pfemployer);
			$refreshment=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary >= 15000 && $calculatedsalary <= 20000)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = max(round($gross * 0.60),8400);
			$hra = round($basic*0.40);
			$otherdeduction=0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = round($ctc*0.10);
		    $refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary >= 20001 && $calculatedsalary <= 26800)
		{   $ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($calculatedsalary * 0.10);
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($calculatedsalary*0.025);
			}else{
              $otherdeduction=0;
			}
			
			$pfpayable = round($basic * 0.24);
		
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}

		else if($calculatedsalary >26800)
		{   $ctc = $calculatedsalary;
			if($pfpayable == '')
			{
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.10);
			$pfpayable = round($basic * 0.24);
			}
			else
			{
            $gross = $ctc;
			$basic = round($ctc * 0.52);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.12);
			}
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($ctc*0.025);
			}else{
              $otherdeduction=0;
			}		
			
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9400)
	{
		    $ctc = $calculatedsalary;	
		    $gross = $ctc;	    
			$basic = min($ctc,8400);
			$hra = 0;
			$da = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - $basic;
			$refreshment=0;
			$otherdeduction=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;

	}else if($calculatedsalary >= 9400 && $calculatedsalary <= 15000)
	{       $ctc = $calculatedsalary;
		    $gross = $ctc;
			$basic = max(round($ctc*0.60),8400);
			$pfpayable = 0;
			$pfemployer = 0;
			$refreshment=0;
			$da=0;
			$otherdeduction=0;
			$bonus = round($ctc * 0.10);
			$hra = $ctc - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000 &&  $calculatedsalary <=20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($ctc * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			$otherdeduction=0;
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}
	else if( $calculatedsalary >20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($calculatedsalary * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			//other deduction condition when flag is 1
			if($odflag=='1'){
            $otherdeduction= round($calculatedsalary*0.025);
			}else{
            $otherdeduction=0;
			}
			
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra+$otherdeduction);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns.
			$netpay = $totalearnings - $totaldeductions;
	}
}


}
else
{
	$basic = $calculatedsalary;
}

}
/*
Here we are checking for column should be less than 84 . 
and storing data in dataArray to display it on report.
*/

$dataArray = Array();
for($k=0;$k<84;$k++)
{
	$dataArray[$k] = ''; 
}

$dataArray[0]= 'ABA';
$dataArray[1]= $row[7];

/*
for($l=1;$l<78;$l++)
{
	if($l%4 == 0)
	{
		$dataArray[$l+1] = $row[7];		
	}
}

*/

$dataArray[2] = 'DR.';
$dataArray[4] = $basic;
$dataArray[6] = 'DR.';
$dataArray[10] = 'DR.';
$dataArray[14] = 'DR.';
$dataArray[18] = 'DR.';
$dataArray[22] = 'DR.';
$dataArray[26] = 'DR.';
$dataArray[30] = 'DR.';
$dataArray[34] = 'DR.';
$dataArray[38] = 'DR.';

if($type == 'Housekeeping' || $type == 'Cash')
{
$dataArray[3] = 'Office Expenses';
}
else
{
$dataArray[3] = 'Basic Salary';	
}

if($otherdeduction != 0){
$dataArray[5] = $row[7];
$dataArray[7] = 'Other Deduction';
$dataArray[8] = $otherdeduction;
}


if($da != 0)
{
$dataArray[5] = $row[7];
$dataArray[7] = 'Dearness Allowance';
$dataArray[8] = $da;	
}


if($hra != 0)
{
$dataArray[9] = $row[7];
$dataArray[11]  = 'House Rent Allowance';
$dataArray[12] = $hra;
}



if($bonus != 0)
{
$dataArray[13] = $row[7];	
$dataArray[15] = 'Bonus';
$dataArray[16] = $bonus;

}




if($pfemployer != 0)
{
$dataArray[17] = $row[7];	
$dataArray[19] = 'PF Contribution (Employers)';
$dataArray[20] = $pfemployer;

}




if($refreshment != 0)
{
$dataArray[21] = $row[7];		
$dataArray[23] = 'Employee Daily Refreshment';
$dataArray[24] = $refreshment;

}





$dataArray[42] = 'CR.';
$dataArray[46] = 'CR.';
$dataArray[50] = 'CR.';
$dataArray[54] = 'CR.';
$dataArray[58] = 'CR.';
$dataArray[62] = 'CR.';
$dataArray[66] = 'CR.';
$dataArray[70] = 'CR.';
$dataArray[74] = 'CR.';
$dataArray[78] = 'CR.';

if((round($netpay) - ($loan+$jvhold))!=0)
{
$dataArray[41] = $row[7];
$dataArray[43] = $row[1];
$dataArray[44] = round($netpay) - ($loan+$jvhold);
}

if($pfpayable != 0)
{
$dataArray[45] = $row[7];
$dataArray[47] = 'PF PAYABLE';
$dataArray[48] = $pfpayable;	
}

if($pt != 0)
{
$dataArray[49] = $row[7];	
$dataArray[51] = 'Professional Tax Payable';
$dataArray[52]= $pt;
}




if($jvhold != 0)
{
$dataArray[53] = $row[7];	
$dataArray[55] = $jvholdledgername[$row[6]];
$dataArray[56]= $jvhold;
}



if($loan != 0)
{
$dataArray[57] = $row[7];	
$dataArray[59] = $loanledger[$row[6]];
$dataArray[60]= $loan;
}

if($otherdeduction!=0)
{
$dataArray[61] = $row[7];	
$dataArray[63]='Other Income';
$dataArray[64]=$otherdeduction;
}

$dataArray[81] = 'Salary provided for the month of '.$month.'-'.$year;
$dataArray[82] = 'JOURNAL SIL';

?>
<tr>
	<?php
	for($p=0;$p<count($dataArray);$p++)
	{
		?>
<td><?php echo $dataArray[$p];?></td>
		<?php
	}
	?>
</tr>


<?php
}
?>

</table>