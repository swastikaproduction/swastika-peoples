0. Mandatory Password Change <-- Done
1.       Add Hierarchy/ HODs for Leave & OD approvals and add option to them for entering any comment.<-- Done
2.       Under the heading Leave, Add an option of ‘New Request’ <-- Done
3.       Add from Time and to time in New Leave Request along with from and to date <-- Done
4.       Add Missed Punch, Overtime & OD under Attendance head at the top & remove Timing mismatch and overtime from Leave. Missed punch would be similar to Timing mismatch. <-- Done
5.       Add an icon after every attendance detail entry to lodge an ‘Issue’ for that particular date. Issue module will be similar to Timing Mismatch. We have to add a drop down first to select issue category – Missed punch, Timing Mismatch, On duty, Other. Next would be In-time entry and exit-time entry followed by comments. <-- Done
6.       Remove Past Attendance <-- Done
7.       Under Attendance, change the text of the button ‘ ‘View Here’ to ‘Get Details’<-- Done
8.       Add ‘forget password’ option in login page.
9.       Add Previous month in mobile calendar.
10.   Calendar change should display what change is been done.
11.   Swastika People in place of Protocol.<-- Done
https://www.flaticon.com/free-icon/teamwork_305112#term=human resources&page=1&position=53<-- Done
12.   Add download app icon in HRM software.
13.   Ekyc.swastika.co.in
14. Add hod request approval in mobile application. 