<?php
include("../include/config.php");
$query = mysqli_query($con,"select * from employee where id NOT IN(1,2)");
?>


<div class="moduleHead">
<br/>
<div style="float:right">
	
</div>


	<div class="moduleHeading" onclick="toogleFormTable();">
	Leave History
	</div>
	<section style="background: white;">
	<div class="row">
		<div class="col-md-4">
			<label>Employee List</label>
			<select name="emplist" class="form-control" id="emplist" required="">
			<option value="">select name</option>
			<?php while($row=mysqli_fetch_array($query)){
				?>
				<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
		<?php	}	?>
			</select>
		</div>
      <div class="col-md-4">
			<button type="submit" name="submit" class="btn btn-success" style="margin-top: 20px;" onclick="GetLeaveHistroyForEmployee();">Submit</button>
		</div>
	</div>

</section>
</div>
<div id="empleavehistorydata">
	
</div>
<div class="tabelContainer divShadow" style="height:auto;" id="alldataleavehistory">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:5%;">#</th>
<th style="width:5%;">Name</th>
<th style="width:5%;">Branch Name</th>
<th style="width:15%;">Type</th>
<th style="width:15%">Details</th>
<!-- <th style="width:10%">HOD Approval</th> -->
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
</tr>
<?php
$count=1;
$query = mysqli_query($con,"select e.name,lr.type,lr.fromdate,lr.reason,lr.status,lr.hodapproval,lr.createdate,b.name as bname from leaverequests as lr  INNER JOIN employee as e ON e.id = lr.empid INNER JOIN branch as b ON b.id=e.branch where lr.type='1' order by lr.id DESC");
while($row=mysqli_fetch_array($query)){


	if($row['hodapproval']==1){
		$status = 'Approved';
		$color='green';
	}else if($row['hodapproval']==2){
		$status = 'Rejected';
		$color='red';
	}else{
		$status = 'Pending';
		$color='orange';
	}

	if($row['type']==1){
		$type = 'Leave';
	}else if($row['type']==2){
		$type = 'Timing Mismath';
	}else if($row['type']==3){
		$type = 'Extra Working';
	}else{
      $type = 'OD';
	}

 ?>	
<tr>


<td><?php echo $count++;?></td>
<td><?php echo $row['name'];?></td>
<td><?php echo $row['bname'];?></td>
<td><?php echo $type;?></td>
<td><?php echo $row['reason'];?></td>
<td><span style="background:<?php echo $color; ?>;color: white;border-radius: 5px;"><?php echo $status;?></span></td>
<td><?php echo $row['createdate'];?></td>

</tr>
<?php
}?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>
