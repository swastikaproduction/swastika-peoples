<?php
include("../include/config.php");
?>
<div class="moduleHead">
<br/>
<div class="moduleHeading">
Salary For Tally Booking
</div>
</div>
<div class="tabelContainer shadow" style="height:auto;background:#fff">

<div class="row">
<div class="col-sm-12 subHead">
Select Filters
	</div>

	<div class="col-sm-2 formLeft">
		Employee Type
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="atEmp">
	<option value="Marketing">Marketing Employees (With Hold Salary Check)</option>
	<option value="Backoffice">Backoffice Employees (Without Hold Salary Check)</option>	>
	<option value="Insurance">Swastika Insurance (Without Hold Salary Check)</option>
	<option value="Securities">Swastika Securities (Without Hold Salary Check)</option>
	<option value="Housekeeping">Housekeeping Employees (Without Hold Salary Check)</option>
	<option value="Other">Other Company Employees</option>
	<option value="All">All Swastika Employees</option>
	<option value="PF">All Pf Employees</option>
	<option value="Cash">All Cash Employees</option>

		</select>
	</div>	

</div>


<div class="row" style="border-bottom: 1px #eee solid;">
	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" <?php if($loggeduserid==124){ ?> onchange="freezingsalary(this.value,document.getElementById('atYear').value)" <?php } ?> id="atMonth">
	<?php
	$month = date("M");
	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		$thisMonth = date("M",strtotime($t));
		?>
<option <?php if($month == $thisMonth) echo "selected='selected'";?> value="<?php echo $j;?>"><?php echo $thisMonth;?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" <?php if($loggeduserid==124){ ?> onchange="freezingsalary(document.getElementById('atMonth').value,this.value)" <?php } ?> id="atYear">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option <?php if(date("Y") ==$j) echo "selected='selected'" ;?> value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
		<br/>
    <?php if($loggeduserid==124){ ?>
	<button class="btn btn-primary" id="salary_freez" style="display: none" onclick="finalfreez(this.value);"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;FREEZE SALARY</button>

	<br/>
	<br/>
	<br/>
    <?php } ?>



	<button class="btn btn-primary"  onclick="window.open('salarycalculation/booking-export-2020.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT CALCULATION REPORT</button>



	<button class="btn btn-primary"  onclick="window.open('salarycalculation/tally-export-new22072020.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT TALLY REPORT</button>


	<button class="btn btn-primary"  onclick="window.open('salarycalculation/bank-export-new22072020.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT BANK REPORT</button>
	

<br/>
<br/>
<br/>
	<button class="btn btn-primary"  onclick="window.open('salarycalculation/tally-export-incentive.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT INCENTIVE MF TALLY REPORT</button>

		<button class="btn btn-primary"  onclick="window.open('salarycalculation/bank-export-incentive.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT INCENTIVE MF BANK REPORT</button>



<br/>
<br/>
<br/>
	<button class="btn btn-primary"  onclick="window.open('salarycalculation/tally-export-incentive-performance.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT INCENTIVE PERFORMANCE TALLY REPORT</button>

		<button class="btn btn-primary"  onclick="window.open('salarycalculation/bank-export-incentive-performance.php?type='+$('#atEmp').val()+'&month='+$('#atMonth').val()+'&year='+$('#atYear').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;EXPORT INCENTIVE PERFORMANCE BANK REPORT</button>






		

		<br/><br/>
	</div>
</div>

<div id="atResult"></div>

</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>