<?php
include("../include/config.php");
$id = $_GET['id'];

if(isset($_GET['delid']))
{
	$delid = $_GET['delid'];
	mysqli_query($con,"DELETE FROM `shiftallot` WHERE `id` = '$delid'") or die(mysqli_error($con));
}

$getData = mysqli_query($con,"SELECT * FROM `shiftallot` WHERE `empid` = '$id'") or die(mysqli_error($con));


$shifts = Array();
$getShifts = mysqli_query($con,"SELECT * FROM `shift`") or die(mysqli_error($con));
while($rowShifts = mysqli_fetch_array($getShifts))
{
$shifts[$rowShifts['id']] = $rowShifts['name'];
}

?>


<div class="moduleHead">

		<div style="float: right">
		<button class="btn btn-primary" onclick="getModule('employee/adddiffshift.php?id=<?php echo $id;?>','formModalBig','tableModalBig','loading')" type="button">
			<i class="fa fa-plus"></i>&nbsp;&nbsp;
			ADD NEW</button>
			

			</div>


	<div class="moduleHeading">
	Temporary Shift Allotment
	</div>
</div>

<div class="shadow" >
<table class="table table-hover">
	<tr>
		<th>From Date</th>
		<th>To Date</th>
		<th>Shift</th>
		<th>
			Remove
		</th>
	</tr>
	<?php
while($row = mysqli_fetch_array($getData))
{
	$dates = str_ireplace(",,",",", $row['dates']);
	$dates = trim($dates,",");
	$dates = explode(",",$dates);
	?>
	<tr>
		<td>
			<?php echo $dates[0];?>
		</td>
		<td>
			<?php echo $dates[count($dates) - 1];?>
		</td>
<td>
	<?php echo $shifts[$row['shiftid']];?>
</td>

<td>
	
	<span class="label label-danger"  onclick="getModal('employee/diffshift.php?id=<?php echo $id;?>&delid=<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')">REMOVE</span>
</td>
	</tr>
	<?php
}
	?>
</table>
</div>