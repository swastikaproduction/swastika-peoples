<?php  
include'../include/config.php';
$count = 1;   
$emp_id=$_COOKIE['emanagerid'];

$query1 = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.employee_due_date,a.managers_due_date,a.hod_due_date,a.created_at,a.id as app_id ,au.mstage as es,au.missubmitted FROM appraisal_users au INNER JOIN employee e ON au.emp_id = e.id INNER JOIN  appraisals a ON a.id = au.app_id where imdboss = '$emp_id' and (issubmitted='false' or issubmitted='save')");

$query = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.employee_due_date,a.managers_due_date,a.hod_due_date,a.created_at,a.id as app_id ,au.mstage as es,au.missubmitted,(select count(appId) from appraisalstart where appId = a.id ) as appraisalcount FROM appraisal_users au INNER JOIN employee e ON au.emp_id = e.id INNER JOIN            appraisals a ON a.id = au.app_id  where imdboss = '$emp_id' and issubmitted='submitted' and missubmitted !='submitted'");

$query2 = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.employee_due_date,a.managers_due_date,a.hod_due_date,a.created_at,a.id as app_id ,au.mstage as es,au.missubmitted ,(select count(appId) from appraisalstart where appId = a.id ) as appraisalcount FROM appraisal_users au INNER JOIN employee e ON au.emp_id = e.id INNER JOIN appraisals a ON a.id = au.app_id  where imdboss = '$emp_id' and issubmitted='submitted' and missubmitted ='submitted'");                 
?>
<style type="text/css">
	.bag{
		background: #cc0404;
	}
</style>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 15px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<h5 class="box-title">HOD  approval pending</h5>
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query2)){

?>

<tr>
<?php
if($row['status']=='o'){

$status = 'open';
}else{
$status = 'close';
}

if($row['es']=='g'){

$stage = 'Goals pending';

}else if($row['es']=='a'){
$stage = 'Appraisal pending';

}else{
$stage = 'HOD Approval Pending';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row['employee_due_date']; ?></td>
<td><?php echo $row['managers_due_date']; ?></td>
<td><?php echo $row['hod_due_date']; ?></td>
<td><?php echo $row['created_at']; ?></td> 
<?php    
$appraisalcount = $row['appraisalcount'];               
if($appraisalcount==0){
?>
<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/managers_goals.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View </span></td> 
<?php
}else{

?>
<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/manager-appraisal.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td> 
<?php }?>               
</tr> 
<?php
}?>                       
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 15px;">
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Weightage value not more than 100.!
</div>   <?php
}
            ?> 
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 2 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Weightage value not equal to 100.!
</div>   <?php
}
            ?> 
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 3 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Submit successfully...!
</div>   <?php
}
            ?>  
            <?php
            if ( isset($_GET['success']) && $_GET['success'] == 4 )
{?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Save successfully...!
</div>   <?php
}
            ?> 
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<h5 class="box-title">Manager approval pending</h5>
</div>

<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th style="background: #b90909;">S.No</th>
<th style="background: #b90909;">Employee name</th>
<th style="background: #b90909;">Appraisal Mode</th>
<th style="background: #b90909;">Period</th>                
<th style="background: #b90909;">Status</th>
<th style="background: #b90909;">Stage</th>
<th style="background: #b90909;"> Employee End Date</th>
<th style="background: #b90909;">Managers End Date</th>
<th style="background: #b90909;">HOD's End Date</th>
<th style="background: #b90909;">Create Date</th>
<th style="background: #b90909;">Action</th>


</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query)){

?>
<tr>
<?php
if($row['status']=='o'){

$status = 'open';
}else{
$status = 'close';
}

if($row['es']=='g'){

$stage = 'Manager Approval pending';

}else if($row['es']=='a'){
$stage = 'Appraisal pending';

}else{
$stage = 'HOD Approval Pending';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row['employee_due_date']; ?></td>
<td><?php echo $row['managers_due_date']; ?></td>
<td><?php echo $row['hod_due_date']; ?></td>

<td><?php echo $row['created_at']; ?></td> 

<?php    
$appraisalcount = $row['appraisalcount'];               
if($appraisalcount==0){
?>

<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/managers_goals.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View & Approve Goals </span></td>
<?php }else{
?>

<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/manager-appraisal.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td> 
<?php 
}?>               
</tr> 
<?php
}?>                       
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 37px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">

<div class="box">
<div class="box-header">
<h5 class="box-title">Employee goal pending</h5>
</div>
<!-- /.box-header -->
<div class="box-body">
<table id="#" class="table table-bordered table-striped">
<thead>   
<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>

</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row1=mysqli_fetch_array($query1)){
?>
<tr>
<?php
if($row1['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}
if($row1['es']=='g'){
$stage = 'Employee Goals Pending';                      
}else if($row1['es']=='a'){
$stage = 'Appraisal Pending';
}else{
$stage = 'Goals Completed';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row1['name']; ?></td>
<td><?php echo $row1['appraised_mode']; ?></td>
<td><?php echo $row1['appraised_time']; ?> <?php echo $row1['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row1['employee_due_date']; ?></td>
<td><?php echo $row1['managers_due_date']; ?></td>
<td><?php echo $row1['hod_due_date']; ?></td>                  
<td><?php echo $row1['created_at']; ?></td> 
<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;"  onclick="sendEmailEmployee('<?php echo $row1['id']; ?>');">Send Reminder</span></td>                
</tr> 
<?php
}?>                   
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
