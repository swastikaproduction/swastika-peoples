<?php  
include'../include/config.php';
$count = 1;   
$emp_id=$_COOKIE['emanagerid'];

$query1 = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.id as app_id ,ast.eDate,ast.mDate,ast.hDate,ast.createdAt FROM appraisalusers  apu INNER JOIN appraisals a ON a.id = apu.appId INNER JOIN employee e ON e.id = apu.empId  INNER JOIN appraisalstart ast on ast.appId = apu.appId  where e.imdboss = '$emp_id'  and (apu.status ='open' or apu.status='save')");

$query = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.employee_due_date,a.managers_due_date,a.hod_due_date,a.created_at,a.id as app_id ,ast.eDate,ast.mDate,ast.hDate,ast.createdAt FROM appraisalusers  apu INNER JOIN appraisals a ON a.id = apu.appId INNER JOIN employee e ON e.id = apu.empId INNER JOIN appraisalstart ast on ast.appId = apu.appId   where e.imdboss = '$emp_id'  and (apu.status = 'msave' or apu.status='submit')");

$query2 = mysqli_query($con,"SELECT e.id,e.name,a.appraised_mode,a.appraised_time,a.appraised_year,a.status,a.employee_due_date,a.managers_due_date,a.hod_due_date,a.created_at,a.id as app_id ,(select count(appId) from appraisalstart where appId = a.id ) as appraisalcount FROM appraisalusers  apu INNER JOIN appraisals a ON a.id = apu.appId INNER JOIN employee e ON e.id = apu.empId   where e.imdboss = '$emp_id'  and apu.status ='msubmit'");                 
?>
<style type="text/css">
	.bag{
		background: #cc0404;
	}
</style>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 15px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<h5 class="box-title">HOD  approval pending</h5>
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query2)){

?>

<tr>
<?php
if($row['status']=='o'){

$status = 'open';
}else{
$status = 'close';
}

if($row['es']=='g'){

$stage = 'Goals pending';

}else if($row['es']=='a'){
$stage = 'Appraisal pending';

}else{
$stage = 'HOD Approval Pending';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row['employee_due_date']; ?></td>
<td><?php echo $row['managers_due_date']; ?></td>
<td><?php echo $row['hod_due_date']; ?></td>
<td><?php echo $row['created_at']; ?></td> 



<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/manager-appraisal.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td> 
              
</tr> 
<?php
}?>                       
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 15px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<h5 class="box-title">Manager approval pending</h5>
</div>

<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th style="background: #b90909;">S.No</th>
<th style="background: #b90909;">Employee name</th>
<th style="background: #b90909;">Appraisal Mode</th>
<th style="background: #b90909;">Period</th>                
<th style="background: #b90909;">Status</th>
<th style="background: #b90909;">Stage</th>
<th style="background: #b90909;"> Employee End Date</th>
<th style="background: #b90909;">Managers End Date</th>
<th style="background: #b90909;">HOD's End Date</th>
<th style="background: #b90909;">Create Date</th>
<th style="background: #b90909;">Action</th>


</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query)){

?>
<tr>
<?php
if($row['status']=='o'){

$status = 'open';
}else{
$status = 'close';
}


?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;">Manager Approval Pending</span></td>
<td><?php echo $row['eDate']; ?></td>
<td><?php echo $row['mDate']; ?></td>
<td><?php echo $row['hDate']; ?></td>

<td><?php echo $row['createdAt']; ?></td> 
<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/manager-appraisal.php?missubmitted=<?php echo $row['missubmitted'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td> 
               
</tr> 
<?php
}?>                       
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 37px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">

<div class="box">
<div class="box-header">
<h5 class="box-title">Employee appraisal pending</h5>
</div>
<!-- /.box-header -->
<div class="box-body">
<table id="#" class="table table-bordered table-striped">
<thead>   
<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>

</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row1=mysqli_fetch_array($query1)){
?>
<tr>
<?php
if($row1['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}

?>
<td><?php echo $count++; ?></td>
<td><?php echo $row1['name']; ?></td>
<td><?php echo $row1['appraised_mode']; ?></td>
<td><?php echo $row1['appraised_time']; ?> <?php echo $row1['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;">Employee Appraisal Pending</span></td>
<td><?php echo $row1['eDate']; ?></td>
<td><?php echo $row1['mDate']; ?></td>
<td><?php echo $row1['hDate']; ?></td>                  
<td><?php echo $row1['createdAt']; ?></td> 
<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;"  onclick="sendEmailEmployee('<?php echo $row1['id']; ?>');">Send Reminder</span></td>                
</tr> 
<?php
}?>                   
</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>
