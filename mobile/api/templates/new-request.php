<?php
include("../apiconfig.php");
?>
<div class="backDropCustom" id="backDrop" style="background:#12163C">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">
<div style="padding:20px;position:relative">
	<center>
		<div style="width:100%;position:relative;height:80px;text-align:center" class="rdCont">
		<div style="position:absolute;top: -20px;
    left: -50%;
    width: 200%;height:60px;display:none" id="undoSet" onclick="decompressLeaveTabs();"></div>
			<table style="width:100%" cellpadding="0" cellspacing="0">
				<tr>
					<td style="width:25%;text-align:center;color:#fff;font-weight:bold;">
						<div style="background:#E13192;" class="rdbox"  onclick="compressLeaveTabs(0);">
							<i class="fa fa-calendar-check-o rdfa"></i>
						</div>
						<div class="rdboxtext">LEAVE</div>

					</td>
					<td style="width:25%;text-align:center;color:#fff;font-weight:bold;">
						<div style="background:#41B9E9;" class="rdbox"  onclick="compressLeaveTabs(1);">
							<i class="fa fa-clock-o rdfa"></i>
						</div>
						<div class="rdboxtext">TIMING</div>
						
					</td>
					<td style="width:25%;text-align:center;color:#fff;font-weight:bold;">
						<div style="background:#fed766;" class="rdbox" onclick="compressLeaveTabs(2);">
							<i class="fa fa-hourglass rdfa"></i>
						</div>
						<div class="rdboxtext">EXTRA WORKING</div>
						
					</td>
					<td style="width:25%;text-align:center;color:#fff;font-weight:bold;">
						<div style="background:#ef5350;" class="rdbox" onclick="compressLeaveTabs(3);">
							<i class="fa fa-handshake-o rdfa"></i>
						</div>
						<div class="rdboxtext">OD</div>
						 
					</td>

				</tr>
			</table>
		</div>

	</center>

</div>
		<div style="padding:20px">



			<?php include("leave-request.php");?>
			<?php include("timing-request.php");?>
			<?php include("overtime-request.php");?>
			<?php include("od-request.php");?>
		</div>

</div>
</div>

