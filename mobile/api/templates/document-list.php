<?php
include("../apiconfig.php");
$type = $_GET['type'];

$doctypes = Array();
$getAlltypes = mysqli_query($con,"SELECT * FROM `documenttypes` WHERE `id` = '$type' ORDER BY `name`") or die(mysqli_error($con));
while($rowTypes = mysqli_fetch_array($getAlltypes))
{
	$doctypes[$rowTypes['id']] = $rowTypes['name'];
}

?>

<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="menuSpacer"></div>
<div class="contentCustom" id="content">
<div style="padding:0px;position:relative">

<?php
$alldocsArray = Array();
$getData = mysqli_query($con,"SELECT * FROM `documents` WHERE `dataid` = '$loggeduserid' AND `type` = '$type'") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
?>
<div class="calStrip" style="padding:15px 10px;">

<table style="width:100%">
	<tbody>


	<tr>
		<td class="timeDotTd">
		<div class="timeDotHolder">
		<i class="fa fa-file-pdf-o" style="color:#000"></i>			
		</div>
		</td>
		<td style="text-align:left">
			<div style="float:right;margin-top:10px">
				<span class="label label-default">
			<?php echo date("D, d-M h:i A",strtotime($row['createdate']));?>
			</span>

	</div>	
	<span style="font-size:20px;font-weight:400;text-transform:capitalize">
		<?php echo $doctypes[$row['type']];?>	</span>


	<br>
<div style="margin-top:5px;border-bottom:1px #">
<br/>
<br/>
<button class="btn btn-primary" id="" style="width:100%;height:50px;" onclick="displayModal('templates/showdocument.php?docid=<?php echo $type;?>&empid=<?php echo $loggeduserid;?>')">CLICK TO VIEW</button>

</div>
<br/>
<br/>
<div class="halfBorder"></div>
		</td>
	</tr>
</tbody></table>


</div>
<?php

}

?>
<br/>
<br/>
<center>
<button class="btn btn-success" id="" style="width:90%;height:50px;" onclick="displayModal('templates/select-file-source.php?type=<?php echo $type;?>');goBackToPage();">UPLOAD MORE</button>
</center>
<br/>
<br/>
<br/>
<br/>

</div>
</div>
</div>

