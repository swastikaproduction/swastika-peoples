<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model {

    //
    protected $table = 'messages';

    public $timestamps = false;

}
