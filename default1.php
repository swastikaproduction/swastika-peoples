<?php
include("include/config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::Human Resource Management:.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/map.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>  
<script type="text/javascript" src="scripts/savedata.js"></script>
<script type="text/javascript" src="scripts/deleteRow.js"></script>  
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="scripts/jscolor.min.js"></script>
<script type="text/javascript" src="scripts/swastika.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>
</head>
<body  onhashchange="changeHashValue()" onload="changeHashValue();" onresize="resizeWindow();">
<?php
include('modal.php');
?>
<div class="loading" id="loading">

<div style="width:100%;display:inline-block;text-align:left">
 <div style="width:100%;height:7px;background:#222;text-align:left;">
    <div style="width:30%;height:7px;background:#22cabf;display:inline-block" id="loadbar"></div>
  </div>
</div>


</div>


<div class="sideMenuContainer" id="sideMenuBlack">
  <?php include("menu.php");?>
</div>


<table style="margin-left:60px" id="topTable0" class="shadow" onclick=" $('.submenuItem').hide();"  onmouseover=" $('.submenuItem').hide();">
  <tr>

    <td>
       <div style=";padding:10px;width:400px;display:inline-block;position:relative">
  </div>

<div style="float:right;margin-top:8px;">
<table style="width:250px;">
  <tr>
      <td style="padding:10px;cursor:pointer;position:relative;color:#8899a0;text-align:center;">
    <div onclick="$('#not2').fadeToggle('fast');rotateNotBox('not2')">
      <i class="fa fa-bell" style="font-size: 13px;color: #8899a0;cursor:pointer"></i>&nbsp;<span style="font-size:12px"> NOTIFICATIONS</span>
      </div>
      <div id="not2" class="topWindows">
      <div style="padding:10px;background:#f8f7f7;text-align:left">
      <h4 style="margin-bottom:0px;">Notifications</h4>
      </div>
            <hr style="width:100%;margin:0px;">

      </div>
    </td>



    <td style="padding:10px;cursor:pointer;color:#8899a0;text-align:center;position:relative;">
      <div onclick="window.location = 'logout.php'">
      <i class="fa fa-sign-out" style="font-size: 13px;color: #8899a0;cursor:pointer"></i>&nbsp;<span style="font-size:12px"> LOGOUT</span>
      </div>
    </td>

  </tr>
</table>

</div>

    </td>
  </tr>
  </table>
  <table style="margin-left:60px" onclick="rotateNotBox(0);$('.submenuItem').hide();" onmouseover=" $('.submenuItem').hide();" id="topTable1">
  <tr>
    <td>
    <div style="padding:0px !important;" id="rightContainer">
    <div id="tableDiv" style="overflow-y:auto;padding:10px">
    </div>
    <div id="formDiv" style="display:none;overflow-y:auto;padding:10px">
      
      
    </div>



    </div>
    </td>
  </tr>
</table>



</div>

<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

<div style="position:fixed;bottom:-3000px;left:0px;z-index:10000;width:100%;height:800px;background:#fff;" id="bottomDivContainer">
<div style="position:absolute;top:0px;right:0px;height:50px;width:50px;background:#40596b;text-align:center;z-index:200;color:#fff;padding-top:15px;" onclick="closeBottom();">
  <i class="fa fa-remove" style="font-size:20px;color:#fff;"></i>
</div>
<div id="bottomDiv"></div>
</div>  

<div style="position:absolute;bottom:20px;right:20px;z-index:200000;display:none" onclick="hideProcessing();" id="stopLoad">
  <button class="btn btn-sm btn-primary primary-border">
    Request take longer than expected. Click to stop.
  </button>
</div>

</body>


<script type="text/javascript">

function resizeWindow()
{
  var inHeight = window.innerHeight;
  $('#sideMenuBig').animate({height:inHeight},50);
  $('#formDiv').animate({height:inHeight},50);
  $('#tableDiv').animate({height:inHeight},50);
  $('#bottomDivContainer').animate({height:inHeight},50);
  $('#sideMenuBlack').animate({height:inHeight},50);
  $('#formDivside').animate({height:inHeight},50);



var tabeWidth = window.innerWidth - 60;


  $('#topTable0').animate({width:tabeWidth});
  $('#topTable1').animate({width:tabeWidth});

}




$(document).ready(function(){
  resizeWindow();
  hideToast();
  $('[data-toggle="tooltip"]').tooltip();   
  $('[data-toggle="tooltip"]').click(function(){
    showSubmenu($(this));
  })
});





</script>

<iframe name="fileuploadframe" style="height:0px;width:0px;display:none" id="fileuploadframe"></iframe>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places"></script>

</html>
