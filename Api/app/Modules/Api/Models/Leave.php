<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model {

    //
    protected $table = 'leaverequests';

    public $timestamps = false;

}
