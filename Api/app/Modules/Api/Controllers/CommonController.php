<?php
namespace App\Modules\Api\Controllers;
use Illuminate\Http\Request;
use App\Modules\Api\Models\Branch;
use App\Http\Controllers\Controller;
use App\Modules\Api\Models\Department;
use App\Modules\Api\Models\Designation;
use App\Modules\Api\Models\Shift;
use App\Modules\Api\Models\Users;
use App\Modules\Api\Models\Leave;
use App\Modules\Api\Models\Calendar;
use App\Modules\Api\Models\Messages;
use App\Modules\Api\Models\Documents;
use App\Modules\Api\Models\Tutorial;
use App\Modules\Api\Models\Policy;
use App\Modules\Api\Models\Notifications;
use Response,Helper,Validator,file,DB,Mail;

class CommonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function GetCommonData(Request $request){
      
            $profileUrl = 'http://183.182.86.91:8088/hrm/Api/ProfileImages';
            $userId = $request->get('userId');
            $vArray = array('userId'=>'required');

          check_validation($request,$vArray);

            $result = Users::where('id',$userId)->first();
          
           $empid = $result['id'];
           $branchid = $result['branch'];
           $designation = $result['designation'];
           $department = $result['department'];


           $checkbenificiarymanager = DB::select("SELECT empid FROM `expensebenificiary` WHERE empid='$empid' GROUP BY empid");

        if($result['expensemanager']==1){

            $branchmanagerstatus = "true";
        }else{
             $branchmanagerstatus = "false";
        }

           $check = Users::where('imdboss',$empid)->count();

            if($check>0){
                $c ='true';
            }else{
                $c = 'false';
            }

            if($branchmanagerstatus=="true" && $c =="true"){
              $branchmanagerstatus = "false";
              $isBankManagerorHod = "true";
              $c = 'false';
            }else{
              $isBankManagerorHod = "false";
            }


            $branch = Branch::where('id',$branchid)->first();
            $Designation = Designation::where('id',$designation)->first();
            $department = Department::where('id',$department)->first();

          if(!empty($result)){

            $res['message']  ='result found';    
            $res['result']  =$result;
            $res['branchdata'] = $branch;
            $res['designationdata'] = $Designation;
            $res['departmentdata'] = $department;
            $res['hod']      = $c;
            $res['branchmanager']      = $branchmanagerstatus;
            $res['isBranchMnagerOrHod']      = $isBankManagerorHod;
            $res['profileUrl']=$profileUrl;
            $res['success'] = true;
            $res['code']    = 200;

        }else{

            $res['message']  ='result not found'; 
            $res['result']  =null;
            $res['success'] = false;
            $res['code']    = 401;
        }

     return Response::json($res);


   }


 public function GetDataEmployee(){
  $result = Users::where('left',0)->get();

if(!empty($result)){

            $res['message']  ='result found';    
            $res['result']  =$result;            
            $res['success'] = true;
            $res['code']    = 200;

        }else{

            $res['message']  ='result not found'; 
            $res['result']  =null;
            $res['success'] = false;
            $res['code']    = 401;
        }

     return Response::json($res);
 }

}