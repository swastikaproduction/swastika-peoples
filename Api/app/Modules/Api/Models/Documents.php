<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model {

    //
    protected $table = 'documents';

    public $timestamps = false;

}