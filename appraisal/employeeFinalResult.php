<?php 
include'../include/config.php';
$empId = $_GET['empId'];
$appId = $_GET['appId'];
$query = mysqli_query($con,"SELECT e.name,a.appraised_mode,a.appraised_year,a.appraised_time,(select name FROM employee where id=eg.m_id) as approvarName,(select name FROM employee where id=eg.h_id) as reviewerNmae FROM employee e INNER JOIN appraisalusers au ON au.empId = e.id  INNER JOIN appraisals a ON a.id =au.appId INNER JOIN employee_goals eg on eg.emp_id = au.empId WHERE au.empId ='$empId' and au.appId='$appId'");
$result = mysqli_fetch_array($query);
?>
<style type="text/css">
	h4{
		color: green;
	}
	.span{
		color: orange;
	}
	table.finalresult tr td, table tr th{
		border: 1px solid #ccc;	
		padding: 5px;
	}
     table.finalresult tr td:first-child{
		width: 50px;
	}
	 table.finalresult tr td{
		background: #ead7db;
	} 
	 table.finalresult tr th {
		background: #b8d4b8;
	}
	.finalresult tbody tr:nth-child(even) td:last-child{
	display: none;
	background:red;
	text-align: center;
	}
	.finalresult tbody tr:nth-child(odd) td:last-child{
	text-align: center;
	background: #dedea1;
	}
</style>
<section style="background: #FFF;height: auto;">
	<div class="row">
		<div class="col-md-12">
			<h4 style="color: green;">Appraisal Final Report</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<label>Employee Name:</label>
			<span class="span"><?php echo $result['name'];?></span>
		</div>
		<div class="col-md-3">
			<label>Appraisal Mode:</label>
			<span class="span"> <?php echo $result['appraised_time'];?> <?php echo $result['appraised_year'];?></span>
		</div>
		<div class="col-md-3">
			<label>Reviewer Name:</label>
			<span class="span"> <?php echo $result['approvarName'];?></span>
		</div>
		<div class="col-md-3">
			<label>Approver Name:</label>
			<span class="span"> <?php echo $result['reviewerNmae'];?></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table border="1" align="center" class="finalresult">
				<thead>
					<tr>
						<th>S.No</th>
						<th>Subject</th>
						<th>Target</th>
						<th>Weightage</th>
						<th>Achievement</th>
						<th>Description</th>
						<th>Final Comments</th>
						<th> Score</th>
						<th>Final Score</th>
						

					</tr>
				</thead>
				<tbody>
					<?php 
											 
                        $count =1;
						$query = "select * from employee_goals where emp_id ='$empId' and app_id='$appId'";
						$result = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($result)){						
						?>						
					<tr>					
						<td><?php echo $count++;?></td>
						<td><?php echo $row['subject'];?></td>
						<td><?php echo $row['target'];?></td>
						<td><?php echo $row['weightage'];?></td>
						<td><?php echo $row['hachievement'];?></td>
						<td><?php echo $row['hcomment'];?></td>
						<td><?php echo $row['hfinalcomment'];?></td>
						<td><?php echo $row['hscore'];?></td>						
                        <td rowspan="2"><?php echo $row['hfinalscore'];?></td>						
					</tr>
				 <?php }?>
				</tbody>
			</table>
		</div>
	</div>
</section>