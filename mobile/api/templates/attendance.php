<?php
include("../apiconfig.php");
if(isset($_GET['month']))
{
	$temp = explode("-",$_GET['month']);
	$mth = $temp[1];
	$year = $temp[0];
	
$getLast = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid` = '$loggeduserid' AND `month` = '$mth' AND `year` = '$year' ORDER BY `id` DESC LIMIT 1") or die(mysqli_error($con));


}
else
{

$getLast = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid` = '$loggeduserid' ORDER BY `id` DESC LIMIT 1") or die(mysqli_error($con));

}
$rowLast = mysqli_fetch_array($getLast);
$month = $rowLast['month'];
$year = $rowLast['year'];
if($month == '01')
{
$monthStart = "2017-12-26";
}
else
{
    $t = $month -1;
    $t = "0".$t;
$monthStart = $year."-".$t."-26";    
}

if(mysqli_num_rows($getLast) > 0)
{


$monthEnd = $year."-".$month."-25";
$tableName = 'attendance_'.$month.'_'.$year;
$deductionarray = Array();
$getLastDate = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$loggeduserid'") or die(mysqli_error($con));

$p=0;
?>

<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="contentCustom" id="content">
<div style="position:relative">
<div style="position:absolute;top:60px;right:20px;">
	<button class="btn btn-sm btn-primary" onclick="displayModal('templates/changeMonth.php');"> Showing for <?php echo date("M-Y",strtotime($monthEnd));?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i></button>

</div>
<div class="atPurple shadow" style="height:auto;padding:10px;text-align:left;color:#fff;overflow:hidden">
<div style="height:60px;"></div>
<?php
while($row = mysqli_fetch_array($getLastDate))
{
	if($row['deduction'] != '0')
	{
		$deductionarray[] .= $row['date'];	
	}
?>
<div id="<?php echo $row['date'];?>atdiv" <?php if($p != 0) { echo 'style="display:none"'; } $p++;?> class="atBoxFull">
<span style="font-size:13px;">On</span>
<br/>
<span style="font-size:25px;"><?php echo date("D, d-M",strtotime($row['date']));?></span>
<br/>
<br/>
<span style="font-size:13px;">Deduction</span>
<br/>
<span style="font-size:40px;"><?php echo $row['deduction'];?>/</span><span style="font-size:13px;">of the day</span>
<br/>
<center onclick="revealMore('<?php echo $row['date'];?>');">
	<i class="fa fa-angle-down atAngle" id="atAngle<?php echo $row['date'];?>" lang="0" style="font-size:30px;"></i>
	<br/>
	<span id="atAngleText<?php echo $row['date'];?>">
	Click to see details.
	</span>

</center>
<div style="display:none" class="atBoxHalf" id="moreAtd<?php echo $row['date'];?>">
<br/>
<table class="atMore">
	<tr>
		<td>
			<span class="up">Remarks</span>
			<br/>
			<span class="down"><?php echo $row['remarks'];?></span>
			<br/>
			<br/>
			<br/>
		</td>
	</tr>
	<?php
if($row['remarks'] == 'Holiday' || $row['status'] == '0')
{
	?>

	<?php
}
else
{
?>

	<tr>
		<td>
			<span class="up">In Time</span>
				<br/>
				<span class="down"><?php echo date("h:i A",strtotime($row['intime']));?></span>
				<br/>
		</td>
		<td>
			<span class="up">Out Time</span>
			<br/>
			<span class="down"><?php echo date("h:i A",strtotime($row['outime']));?></span>
			<br/>
		</td>
	</tr>

<tr>
		<td>
			<span class="up">Shift End</span>
				<br/>
				<span class="down"><?php echo date("h:i A",strtotime($row['shiftend']));?></span>
				<br/>
				<br/>
				<br/>
		</td>
	</tr>

	<tr>
		<td>
			<span class="up">Alloted Working Mins</span>
				<br/>
				<span class="down"><?php echo $row['allotedshifthours'];?></span>
				<br/>
		</td>
		<td>
			<span class="up">Total Working Mins</span>
			<br/>
			<span class="down"><?php echo $row['hours'] * 60;?></span>
			<br/>
		</td>
	</tr>



<tr>
		<td>
			<span class="up">Difference in Mins</span>
				<br/>
				<span class="down"><?php echo $row['difference'];?></span>
				<br/>
		</td>
		<td>
			<span class="up">Late in Morning</span>
			<br/>
			<span class="down"><?php echo $row['late'];?> Mins</span>
			<br/>
		</td>
	</tr>

	<tr>
		<td>
			<span class="up">Conecutive Late #</span>
				<br/>
				<span class="down"><?php echo $row['consecutive_counter'];?></span>
				<br/>
		</td>
		<td>
			<span class="up">Fifteen #</span>
			<br/>
			<span class="down"><?php echo $row['fifteenminscounter'];?></span>
			<br/>
		</td>
		
	</tr>


<tr>
		<td>
			<span class="up">Two Hours #</span>
				<br/>
				<span class="down"><?php echo $row['twohourvalue'];?></span>
				<br/>
		</td>
	</tr>

<?php
}
?>

</table>
</div>
</div>

<?php
}
?>
<br/>
<table style="width:100%;" class="calendar">
	<tr>
		<th>SUN</th>
		<th>MON</th>
		<th>TUE</th>
		<th>WED</th>
		<th>THU</th>
		<th>FRI</th>
		<th>SAT</th>
	</tr>
</table>

</div>
<div style="padding:10px">
<?php
include('calendar.php');
?>
</div>
</div>
</div>
</div>

<?php
}
else
{
	$temp = explode("-",$_GET['month']);
	$mth = $temp[1];
	$year = $temp[0];

	$monthEnd = $year."-".$mth."-25";

	?>
<div class="backDropCustom" id="backDrop" style="background:#fff">
<div class="contentCustom" id="content">
<div style="position:relative">
<div style="position:absolute;top:60px;right:20px;">
	<button class="btn btn-sm btn-primary" onclick="displayModal('templates/changeMonth.php');"> Showing for <?php echo date("M-Y",strtotime($monthEnd));?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i></button>
</div>
<div class="atPurple shadow" style="height:auto;padding:40px;padding-top:100px;text-align:left;color:#fff;overflow:hidden;font-size:20px;color:#fff">
	No attendance found.
	<br/>
	Try changing month or year.
</div>
</div>
</div>
</div>
	<?php

}
?>

