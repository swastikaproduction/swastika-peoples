<?php
include("../include/config.php");
$id = $_GET['id'];
$branchArray = Array();
$branches = getData('branch','*','name','ASC');
foreach($branches as $br)
{
	$branchArray[$br['id']] = $br['name'];
}

$data = getData("calendar","**","id",$id);

	$tbr = $data[0]['branch'];
	$tbr = explode(",",$tbr);
	foreach($tbr as $tb)
	{
		$brStr .= '<span class="label label-warning" style="font-size:12px;margin:5px;display:inline-block">'.$branchArray[$tb]."</span> ";
	}
	$brStr = substr($brStr, 0,-1);
	$timing= date("h:i A",strtotime($data[0]['from']))." to ".date("h:i A",strtotime($data[0]['to']))." : ".$data[0]['hours']." hours";
	$type = $data[0]['type'];
	$name = $data[0]['name'];

	if($type == '1')
	{
		$type =  "Holiday";
		$dataContent= 'Due to <strong>'.$name.'</strong> <br/><br/> <span style="font-size:12px;color:#999">in branches<span><br/> '.$brStr;

	}
	else
	{
		$type =  "Different Shift Timing";
		$dataContent= $timing. ' due to <strong>'.$name.'</strong> <br/><br/> <span style="font-size:12px;color:#999">in branches<span><br/> '.$brStr;
	}
?>

<div class="row">
<div class="col-sm-12 subHead">
	<?php echo $type;?>
</div>
	<div class="col-sm-12">
		<?php echo $dataContent;?>
		<br/><br/>

	</div>
</div>
