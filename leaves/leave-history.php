<?php
include("../include/config.php");
?>


<div class="moduleHead">
<br/>
<div style="float:right">
	
</div>


	<div class="moduleHeading" onclick="toogleFormTable();">
	Leave History
	</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:5%;">#</th>
<th style="width:15%;">Type</th>
<th style="width:15%">Details</th>
<!-- <th style="width:10%">HOD Approval</th> -->
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
</tr>
<?php
$count=1;
$query = mysqli_query($con,"select * from leaverequests where empid='$loggeduserid' and type ='1' order by id DESC ");
while($row=mysqli_fetch_array($query)){
	
	if($row['hodapproval']==1){
		$status = 'Approved';
		$color='green';
	}else if($row['hodapproval']==2){
		$status = 'Rejected';
		$color='red';
	}else{
		$status = 'Pending';
		$color='orange';
	}

	if($row['type']==1){
		$type = 'Leave';
	}else if($row['type']==2){
		$type = 'Timing Mismath';
	}else if($row['type']==3){
		$type = 'Extra Working';
	}else{
      $type = 'OD';
	}

 ?>	
<tr>


<td><?php echo $count++;?></td>
<td><?php echo $type;?></td>
<td><?php echo $row['reason'];?></td>
<td><span style="background:<?php echo $color; ?>;color: white;border-radius: 5px;"><?php echo $status;?></span></td>
<td><?php echo $row['createdate'];?></td>

</tr>
<?php
}?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>
