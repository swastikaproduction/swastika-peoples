<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

    //
    protected $table = 'branch';

    public $timestamps = false;

}
