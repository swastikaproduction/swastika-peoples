<?php

Route::group(['module' => 'Web', 'middleware' => ['web'], 'namespace' => 'App\Modules\Web\Controllers'], function() {

    Route::resource('web', 'WebController');

});
