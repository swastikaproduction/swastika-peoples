<?php
include("../include/config.php");
$branch = $_GET['branch'];
$month = $_GET['month'];
$year = $_GET['year'];
$dept = $_GET['dept'];
$empid = $_GET['empid'];


if($empid != '')
{
$getData = mysqli_query($con,"SELECT * FROM `employee` WHERE `empid` = '$empid' AND `left` = '0'  ORDER BY `name`") or die(mysqli_error($con));	
}
else
{
$getData = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0' AND `branch` = '$branch' AND `department` = '$dept' ORDER BY `name`") or die(mysqli_error($con));

}

$getperms = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$loggeduserid'") or die(mysqli_error($con));
$rowperms= mysqli_fetch_array($getperms);



$previous = Array();
if($empid != '')
{
$getprevious = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year' AND `empid` IN (SELECT `id` FROM `employee` WHERE `empid` = '$empid' )") or die(mysqli_error($con));

}
else
{
$getprevious = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year' AND `empid` IN (SELECT `id` FROM `employee` WHERE `left` = '0' AND `branch` = '$branch' AND `department` = '$dept')") or die(mysqli_error($con));

}
while($rowprevious = mysqli_fetch_array($getprevious))
{
	$previous[$rowprevious['empid']]['hold'] = $rowprevious['hold'];
	$previous[$rowprevious['empid']]['holdremark'] = $rowprevious['holdremark'];
	$previous[$rowprevious['empid']]['incentive'] = $rowprevious['incentive'];
	$previous[$rowprevious['empid']]['incentiveremark'] = $rowprevious['incentiveremark'];
	$previous[$rowprevious['empid']]['incentiveform'] = $rowprevious['incentiveform'];
	$previous[$rowprevious['empid']]['incentiveformremark'] = $rowprevious['incentiveformremark'];

	$previous[$rowprevious['empid']]['loan'] = $rowprevious['loan'];
	$previous[$rowprevious['empid']]['loanremark'] = $rowprevious['loanremark'];
	$previous[$rowprevious['empid']]['jvhold'] = $rowprevious['jvhold'];
	$previous[$rowprevious['empid']]['jvholdledger'] = $rowprevious['jvholdledger'];
	
}

?>





<div class="dummyTableHeader" id="forAttendance" style="margin-bottom:0px;position:fixed;top:118px;left:0px;width:100%;padding:0px 21px 0px 15px;z-index:200;display:none">

<table class="table table-hover" cellpadding="0" cellspacing="0" style="">
<tr>
		<th style="width:5%">#</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Hold Salary</th>
		<th style="width:10%">Incentive MF</th>
		<th style="width:10%">Incentive PERF</th>
		<th style="width:10%">Loan</th>
		<th style="width:10%">Tds</th>
	</tr>
</table>

</div>



<table class="table table-bordered table-hover"  cellpadding="0" cellspacing="0" id="tableDivAtView">
	<tr>
		<th style="width:5%">#</th>
		<th style="width:10%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:10%">Hold Salary</th>
		<th style="width:10%">Incentive MF</th>
		<th style="width:10%">Incentive PERF</th>
		<th style="width:10%">Loan</th>
		<th style="width:10%">Tds</th>
	</tr>
	<?php
	$i=1;
	$inp = 0;
	while($row = mysqli_fetch_array($getData))
	{
		$thisempid = $row['id'];
		if($month < 10)
		{
			$chkmonth = '0'.$month;
		}
		else
		{
			$chkmonth = $month;
		}
		$getSalaryDetails = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid` = '$thisempid' AND `month` = '$chkmonth' AND `year` = '$year'") or die(mysqli_error($con));
		
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td><?php echo $row['name'];?>
		<input type="text" class="inputBox" style="display:none" value="<?php echo $row['id'];?>" id="salad<?php echo $inp; $inp++?>" />

	</td>
	<td><?php echo $row['empid'];?>
		<br/>
		<br/>
		
		<span class="label label-success" style="cursor:pointer;" onclick="$('#saldet<?php echo $thisempid;?>').slideToggle();">SALARY DETAILS</span>
		<div style="font-size:12px;line-height:30px;display:none" id="saldet<?php echo $thisempid;?>">
		
		<?php
		$rowSal = mysqli_fetch_array($getSalaryDetails);
		?>
		Salary: <?php echo $rowSal['actual_salary'];?>
		<br/>
		Deduction: <?php echo $rowSal['actual_salary'] - $rowSal['final_salary'];?>
		<br/>
		Netpay: <?php echo $rowSal['final_salary'];?>
		</div>

	</td>
	<td><?php 

	$temp = $year."-".$month."-01";
	echo date("M-Y",strtotime($temp));
	?></td>

	<td>

	<?php 
	if($rowperms['holdperm'] == '1' || $loggeduserid == '124')
	{
		$str = "";
	}
	else
	{
		$str = "readonly='readonly'";
	}


	?>
		<input type="text" <?php echo $str;?> class="inputBox" id="salad<?php echo $inp; $inp++?>" onclick="$('#salad<?php echo $inp;?>').slideDown();" onblur="if(this.value == '0') { $('#salad<?php echo $inp;?>').slideUp();}" value="<?php echo $previous[$thisempid]['hold'];?>" />
		<br/>
		<textarea  <?php echo $str;?>  class="inputBox" style="height:100px;display:none" placeholder="Enter Remark"  id="salad<?php echo $inp; $inp++?>"><?php echo $previous[$thisempid]['holdremark'];?></textarea>

	</td>
	<td>
	<?php 
	if($rowperms['incentiveperm'] == '1' || $loggeduserid == '124')
	{
		$str = "";
	}
	else
	{
		$str = "readonly='readonly'";
	}
	?>
		<input type="text"  <?php echo $str;?> class="inputBox" id="salad<?php echo $inp; $inp++?>" onclick="$('#salad<?php echo $inp;?>').slideDown();" onblur="if(this.value == '0') { $('#salad<?php echo $inp;?>').slideUp();}"  value="<?php echo $previous[$thisempid]['incentive'];?>" />
		<br/>
		<textarea class="inputBox"  <?php echo $str;?> style="height:100px;display:none" placeholder="Enter Remark"  id="salad<?php echo $inp; $inp++?>"  value="" ><?php echo $previous[$thisempid]['incentiveremark'];?></textarea>
	</td>

<td>
	<?php 
	if($rowperms['incentivepermform'] == '1' || $loggeduserid == '124')
	{
		$str = "";
	}
	else
	{
		$str = "readonly='readonly'";
	}
	?>
		<input type="text"  <?php echo $str;?> class="inputBox" id="salad<?php echo $inp; $inp++?>" onclick="$('#salad<?php echo $inp;?>').slideDown();" onblur="if(this.value == '0') { $('#salad<?php echo $inp;?>').slideUp();}"  value="<?php echo $previous[$thisempid]['incentiveform'];?>" />
		<br/>
		<textarea class="inputBox"  <?php echo $str;?> style="height:100px;display:none" placeholder="Enter Remark"  id="salad<?php echo $inp; $inp++?>"  value="" ><?php echo $previous[$thisempid]['incentiveformremark'];?></textarea>
	</td>

	<td>

	<?php 
	if($rowperms['jvholdperm'] == '1' || $loggeduserid == '124')
	{
		$str = "";
	}
	else
	{
		$str = "readonly='readonly'";
	}
	?>
		<input type="text" <?php echo $str;?>  class="inputBox" id="salad<?php echo $inp; $inp++?>" onclick="$('#salad<?php echo $inp;?>').slideDown();" onblur="if(this.value == '0') { $('#salad<?php echo $inp;?>').slideUp();}" value="<?php echo $previous[$thisempid]['loan'];?>"  />
		<br/>
		<textarea class="inputBox" <?php echo $str;?>  style="height:100px;display:none" placeholder="Enter Ledger Name"  id="salad<?php echo $inp; $inp++?>" value="" ><?php echo $previous[$thisempid]['loanremark'];?></textarea>

	</td>



<td>

	<?php 
	if($rowperms['jvholdperm'] == '1' || $loggeduserid == '124')
	{
		$str = "";
	}
	else
	{
		$str = "readonly='readonly'";
	}
	?>
		<input type="text" <?php echo $str;?>  class="inputBox" id="salad<?php echo $inp; $inp++?>" onclick="$('#salad<?php echo $inp;?>').slideDown();" onblur="if(this.value == '0') { $('#salad<?php echo $inp;?>').slideUp();}" value="<?php echo $previous[$thisempid]['jvhold'];?>"  />
		<br/>
		<textarea class="inputBox" <?php echo $str;?>  style="height:100px;display:none" placeholder="Enter Ledger Name"  id="salad<?php echo $inp; $inp++?>"><?php 
			if($previous[$thisempid]['jvholdledger'] == '')
			{
				echo "TDS on Salary (192-B)";
			}
			else
			{
				echo $previous[$thisempid]['jvholdledger'];
			}
?></textarea>

	</td>


</tr>
		<?php
		$i++;
	}
	
	?>
</table>
<div style="padding:20px;text-align:right">
		
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('salaryaddons/save.php?year=<?php echo $year;?>&month=<?php echo $month;?>','','justCloseModal','salad',<?php echo $inp;?>,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE ADDONS</button>
		

</div>

</div>
<br/><br/><br/><br/><br/>