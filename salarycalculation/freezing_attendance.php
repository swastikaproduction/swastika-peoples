n<?php
include("../include/config.php");
$tableName = $_POST['tablename'];
$month = $_POST['month'];
$year = $_POST['year'];

$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($row = mysqli_fetch_array($getTables))
{
  $tables[] = $row[0];
}

if(!in_array($tableName,$tables)){
  mysqli_query($con,"CREATE TABLE IF NOT EXISTS `".$tableName."` (
  `id` int(11) NOT NULL,
  `empid` int(11) NOT NULL,
  `date` date NOT NULL,
  `intime` time NOT NULL,
  `outime` time NOT NULL,
  `hours` varchar(100) NOT NULL,
  `allotedshifthours` varchar(20) NOT NULL,
  `shiftend` time NOT NULL,
  `difference` varchar(100) NOT NULL,
  `late` varchar(100) NOT NULL,
  `consecutive_counter` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `twohourscounter` varchar(100) NOT NULL,
  `twohourvalue` varchar(20) NOT NULL,
  `fifteenminscounter` varchar(100) NOT NULL,
  `deduction` varchar(100) NOT NULL,
  `remarks` text NOT NULL,
  `forced` varchar(20) NOT NULL,
  `forceremarks` varchar(20) NOT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1") or die(mysqli_error($con));


mysqli_query($con,"ALTER TABLE `".$tableName."` ADD PRIMARY KEY (`id`)") or die(mysqli_error($con));
mysqli_query($con,"ALTER TABLE `".$tableName."` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT") or die(mysqli_error($con));

if($month < 10)
{
      $month = "0".$month;
}

$current_table = "attendance_".$month."_".$year;
if(in_array($current_table,$tables)){
mysqli_query($con, "INSERT INTO `".$tableName."` (`id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend`)  SELECT `id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend` FROM `".$current_table."`") or die(mysqli_error($con));
}

}

?>

