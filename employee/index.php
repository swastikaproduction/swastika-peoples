<?php
include("../include/config.php");
$data = Array();

if(isset($_GET['type']))
{
	if($_GET['type'] == 'house')
	{
			$leftStr = " AND e.department = '4' AND `e`.`left` = '0'";
			$heading = "Housekeeping Employees";
	}
	else if($_GET['type'] == 'nonhouse')
	{
			$leftStr = " AND e.department != '4' AND `e`.`left` = '0'";
			$heading = "Non-Housekeeping Employees";
	}
	else if($_GET['type'] == 'marketing')
	{
			$leftStr = " AND e.depttype = 'Marketing' AND `e`.`left` = '0'";
			$heading = "Marketing Employees";
	}
	else if($_GET['type'] == 'backoffice')
	{
			$leftStr = " AND e.depttype = 'Backoffice' AND `e`.`left` = '0'";
			$heading = "Backoffice Employees";
	}

	else
	{
			$leftStr = " AND e.left = '1'";	
			$heading = "Left Employees";	
	}

}
else
{
	$leftStr = " AND e.left = '0'";
	$heading = "All Employees";
}

if($profile==21){
$permStr='';
$sql="SELECT e.empid,e.name,e.email,d.name,ds.name,e.id
FROM employee e LEFT JOIN departments d ON e.department=d.id LEFT JOIN designation ds ON e.designation=ds.id where e.delete = 0 ".$leftStr."".$permStr." ORDER BY e.name ASC";
}
else if($profile==20){
$branch = [];	
$getZones = mysqli_query($con,"SELECT `zones` FROM employee WHERE id='$loggeduserid'") or die(mysqli_error($con));
$rowZones = mysqli_fetch_array($getZones);

$getData = mysqli_query($con,"SELECT id FROM `branch` WHERE zoneid IN ($rowZones[0])") or die(mysqli_error($con));
while($row=mysqli_fetch_array($getData)){
	$branch[] .= $row[0];
}
$branches = implode(',', $branch);
$permStr= " AND e.branch IN ($branches)";
$sql="SELECT e.empid,e.name,e.email,d.name,ds.name,e.id
FROM employee e LEFT JOIN departments d ON e.department=d.id LEFT JOIN designation ds ON e.designation=ds.id where e.delete = 0 ".$leftStr."".$permStr." AND e.profileid NOT IN (2,3,4,22) AND e.empid NOT IN ('IND8303883') ORDER BY e.name ASC";
}
else {
$employees = [];	
$getData = mysqli_query($con,"select id from (select * from employee order by name,id) employee_sorted, (select @pv := '$loggeduserid') initialisation where find_in_set(imdboss, @pv) and length(@pv := concat(@pv, ',',id))") or die(mysqli_error($con));
while($row=mysqli_fetch_array($getData)){
	$employees[] .= $row[0];
}


$employeeid = implode(',', $employees);
if(empty($employees)){
echo 'No employee list found';	
die();
}
else{
$permStr= " AND e.id IN ($employeeid)";
}
$sql="SELECT e.empid,e.name,e.email,d.name,ds.name,e.id FROM employee e LEFT JOIN departments d ON e.department=d.id LEFT JOIN designation ds ON e.designation=ds.id where e.delete = 0 ".$leftStr."".$permStr." ORDER BY e.name ASC";
}

$getData = mysqli_query($con,$sql) or die(mysqli_error($con));
$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>

<div class="moduleHead">
<br/>
<div style="float:right">
	<?php if(in_array('add_employee',$permissions)){ ?>
	<button class="btn btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
   <?php } ?>
   <?php if(in_array('delete_employee',$permissions)){ ?>
	<button class="btn btn-danger"  onclick="crossCheckDelete('employee')">
	<i class="fa fa-remove"></i>&nbsp;&nbsp;DELETE SELECTED</button>
   <?php } ?>

</div>
<div class="moduleHeading">
<?php echo $heading;?>
</div>
</div>



<div class="tabelContainer divShadow shadow" style="height:auto;">

<div class="dummyTableHeader" style="margin-bottom:0px;position:fixed;top:118px;left:0px;width:100%;padding:0px 21px 0px 15px;z-index:200;display:none">

<table class="table table-hover" cellpadding="0" cellspacing="0" style="">
<tr>
<th style="width:5%">#</th>
<th style="width:5%">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th style="width:15%">Code</th>
<th style="width:15%">Employee Name</th>
<th style="width:30%">Email</th>
<th style="width:15%">Department</th>
<th style="width:15%">Designation</th>
</tr>
</table>
<div class="dummybutton" style="position:fixed;bottom:20px;right:30px;z-index:200">
		<?php if(in_array('add_employee',$permissions)){ ?>
		<button class="btn btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<?php } ?>

</div>

</div>


<table class="table table-hover" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:5%">#</th>
<th style="width:5%">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th style="width:15%">Code</th>
<th style="width:15%">Employee Name </th>
<th style="width:30%">Email</th>
<th style="width:15%">Department</th>
<th style="width:15%">Designation</th>


</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<input type="checkbox" class="checkInput" id= name="" value="<?php echo $row['id'];?>">
</td>
<td>
	<?php echo $row[0];?>
</td>

<td class="text-primary" onclick="getModule('employee/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row[1];?></td>


<td>
	<?php echo $row[2];?>
</td>
<td>
	<?php echo $row[3];?>
</td>
<td>
	<?php echo $row[4];?>
</td>

</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>