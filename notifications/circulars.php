<?php
$getData = mysqli_query($con,"SELECT * FROM `circulars` ORDER BY `id` DESC");
?>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0">
<tr>
<th style="width:50px">#</th>
<th style="width:40%">Title</th>
<th>Preview</th>
<th>Date</th>
</tr>
<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
	?>
<tr>
	<td><?php echo $i;?></td>
	<td><?php echo $row['title'];?></td>
	<td>
		<span class="label label-primary" onclick="getModal('circulars/preview.php?id=<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')">
			<i class="fa fa-external-link"></i>&nbsp;&nbsp;PREVIEW
		</span>

	</td>
	<td><?php echo date("d-M-y h:i A",strtotime($row['createdate']));?></td>
</tr>

	<?php
	$i++;
}
?>
</table>
</div>
<div class="calStrip"  style="padding:15px 10px;text-align:right" onclick="getModule('circulars/index.php','tableDiv','formDiv','loading');">VIEW ALL&nbsp;&nbsp;&nbsp;
<i class="fa fa-arrow-right"></i>
</div>