<?php
session_start();
ob_start();
error_reporting(0);
$ip = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
$token = time().rand(100000,9999999).$ip;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Swastika People</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>
<link rel="icon" type="image/png" sizes="192x192"  href="images/favicon.ico">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
<script type="text/javascript">
function loginNow()
{
  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  if(username == '' || password == '')
  {
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      
      return false;
  }
  var url = "login.php";
  var params = {
    username:username,
    password:password
  }
  document.getElementById('logButton').innerHTML = 'AUTHENTICATING..';
  genAjax(url,params,function(response){
    if(response.indexOf('SUCCESS') != -1)
    {
      document.getElementById('logButton').innerHTML = 'REDIRECTING..';
     window.location = "default.php";


    }
    else
    {
      document.getElementById('logButton').innerHTML = 'RETRY LOGIN <i class="fa fa-arrow-right"></i>';
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      

    }
  });

}


function sendOtp()
{
  var username = document.getElementById('username').value;
  var mobile = document.getElementById('mobile').value;
  if(mobile == '')
  {
      toast("How can we send an OTP on a blank mobile number?",'toast bg-danger','3000');      
      return false;
  }
  else if(mobile < 7000000000 || mobile > 9999999999)
  {
      toast("Invalid mobile number. Call you friend from your mobile and ask them to tell you the number displayed on your screen when your call was appearing.",'toast bg-danger','10000');    
      return false;    
  }
  else if(isNaN(mobile))
  {
      toast("Invalid mobile number. You really have "+mobile+" as your number? Seriously? With alphabets or symbols in it?",'toast bg-danger','10000');      
      return false;    
  }
  var url = "sendotp.php";
  var params = {
    mobile:mobile,
    username:username,
    token:'<?php echo $token; ?>'
  }
  document.getElementById('optbutton').innerHTML = 'SENDING OTP..';
  
  genAjax(url,params,function(response){
       document.getElementById('optbutton').innerHTML = 'OTP SENT..MOVING NOW';
        setTimeout(function(){
     
      $('#login_window_2').hide();
      $('#login_window_3').show();
            },1000);



      setTimeout(function(){
              document.getElementById('optbutton').innerHTML = 'SEND OTP <i class="fa fa-arrow-right"></i>';
            },2000);
  });

  
}

function resend()
{
  document.getElementById('mobile').value = '';
         $('#login_window_2').show();
      $('#login_window_3').hide(); 
}



function verifyOtp()
{
  var username = document.getElementById('username').value;
  var mobile = document.getElementById('mobile').value;
  var otp = document.getElementById('otp').value;
  if(otp == '')
  {
      toast("Invalid OTP. Did we really send a blank OTP?",'toast bg-danger','3000');      
      return false;
  }
  var url = "checkOtp.php";
  var params = {
    mobile:mobile,
    username:username,
    token:'<?php echo $token; ?>',
    otp:otp
  }
  document.getElementById('confirmbutton').innerHTML = 'CHECKING OTP..';
  
  genAjax(url,params,function(response){
    if(response.indexOf('FALSE') == -1)
    {
       document.getElementById('confirmbutton').innerHTML = 'REDIRECTING..';
       window.location = 'default.php';
    }
    else
    {
            toast("Invalid OTP. Please try again.",'toast bg-danger','3000'); 
        document.getElementById('confirmbutton').innerHTML = 'VERIFY <i class="fa fa-arrow-right"></i>';     
    }

  });

  
}

</script>
</head>
<body style="background:transparent url('images/login.jpg') no-repeat scroll right top;background-size:cover">
<div class="loading" id="loading">

<div style="width:100%;display:inline-block;text-align:left">
 <div style="width:100%;height:7px;background:#222;text-align:left;">
    <div style="width:30%;height:7px;background:#1976d2;display:inline-block" id="loadbar"></div>
  </div>
</div>


</div>

<div class="row footer">
  <div class="col-sm-12" style="font-size:10px;color:#fff;padding-left:45px;">
  <div style="float:right;color:#fff">
    <!-- <&nbsp;parxlab/&nbsp;> -->
  </div>
    
    Copyright 2017, All Rights Reserved.<br/>
    <a href="http://www.swastika.co.in" style="color:#fff">
    Swastika Investmart Ltd.
    </a>
    <br/>
    <br/>
  
  </div>
</div>
<div class="row">
  <div class="col-sm-12" style="text-align:left;padding-top:40px;;padding-left:40px">
    <div style="width:500px;text-align:left;display:inline-block;background:#fff;padding:40px;border-radius:5px;" class="shadow">


    <img src="images/people.png" style="height:20px;vertical-align:text-bottom">&nbsp;&nbsp;<div style="display:inline-block;font-weight:400;font-size:20px;vertical-align:-webkit-baseline-middle">
    Swastika People</div>
<br/><br/>    <br/>
    <h2>Human Resource Management System</h2>
    <hr style="width:200px;">
    <div id="login_window_1">
    <br/>

    <h4>Login to your account.</h4>
    <br/>
          <input class="w3-input input1b" type="text" style="width:100%" id="username"  onkeyup="if(event.keyCode == '13') { $('#logButton').trigger('click'); }" required="" placeholder="Enter Email">

<br/>
      <input class="w3-input input1b" type="password" style="width:100%" id="password" required="" onkeyup="if(event.keyCode == '13') { $('#logButton').trigger('click'); }" placeholder="Enter Password">
      <div style="float:right;padding:5px" onclick="openPassword();" class="text-primary">
        Forgot Password
      </div>
<br/><br/>
 <button class="btn btn-primary" style="width:50%" id="logButton" onclick="loginNow();">TAKE ME IN <i class="fa fa-arrow-right"></i></button>
 


</div>

    






    </div>
  </div>
</div>


<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

<div id="forgotPasswordModal" class="modal fade" role="dialog"   style="z-index:200000">
  <div class="modal-dialog">
    <div class="modal-content">


    <div style="padding:20px;display:none" id="emailDone">
    We have send you an email with instructions to get your password. 
    </div>

    <div style="padding:20px;" id="emailNotDone">
    <br/>
    <br/>
    <span style="font-size:11px;text-transform:uppercase">
    Please enter username
    </span>
    <br/>
    <br/>
    <input type="text" class="input" id="fpusername" style="width:100%" placeholder="Enter Username" name="">
    <br/>
    <br/>
    <button class="btn btn-primary" onclick="forgotPassword();" id="fpButton">
<i class="fa fa-send"></i>&nbsp;&nbsp;
SEND INSTRUCTIONS
    </button>
    <br/><br/>
    We will send an email to your registered email id with instructions.
    <br/>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  hideToast();


  function openPassword()
  {
      document.getElementById('emailNotDone').style.display = 'block';
      document.getElementById('emailDone').style.display = 'none';
      document.getElementById('fpButton').innerHTML = '<i class="fa fa-send"></i>&nbsp;&nbsp;SEND INSTRUCTIONS';
      $('#forgotPasswordModal').modal();

  }

function forgotPassword()
{
  var username = document.getElementById('fpusername').value;
  if(username == '')
  {
    toast("Please enter username","toast bg-danger","3000");
    return false;
  }

var params = "username="+username;
document.getElementById('fpButton').innerHTML = 'SENDING..';
url = "forgotpassword.php";
 genAjax(url,params,function(response){
    if(response.indexOf('SUCCESS') != -1)
    {
      document.getElementById('emailNotDone').style.display = 'none';
      document.getElementById('emailDone').style.display = 'block';


    }
    else if(response.indexOf('WRONG') != -1)
    {
      document.getElementById('fpButton').innerHTML = 'RETRY <i class="fa fa-arrow-right"></i>';
      toast("Invalid username. Please enter a valid email id.",'toast bg-danger','3000');      

    }
    else
    { 
     

      document.getElementById('fpButton').innerHTML = 'RETRY <i class="fa fa-arrow-right"></i>';
      // toast("Unable to process your request right now. Please try again after some time.",'toast bg-danger','3000');
      toast("Please check your email address, we have sent password on your email address.",'toast bg-success','3000');

           

    }

    if(username !=''){

      $.ajax({
        url:'http://183.182.86.91:13780/hrm/attendance/cron/sentforgottenmail.php?email='+username,
        method:'get',
        data:{},
        success:function(res){

        }
      })
    }


  });


}

</script>
</body>
</html>
