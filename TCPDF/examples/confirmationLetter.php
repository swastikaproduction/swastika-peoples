<?php
ob_start();
include("../../include/config.php");

// $id = $_GET['id'];
// $id = 4790;

if($_POST['submit']=='GENERATE CONFIRMATION LETTER' || $_POST['submit']=='SEND ON EMAIL'){

	// echo "<pre>"; print_r($_POST); die;	

	if(!empty($_POST['emp_id'])){

		$id = $_POST['emp_id'];
		
		if(!empty($_POST['start_date'])){
			$start_date = $_POST['start_date'];
			$start_date = date('d/m/Y', strtotime($start_date));
		}
		else{
			$start_date = "Start Date";
		}

		if(!empty($_POST['end_date'])){
			$end_date = $_POST['end_date'];
			$end_date = date('d/m/Y', strtotime($end_date));
		}
		else{
			$end_date = "End Date";
		}

		if(!empty($_POST['confirmation_date'])){
			$emp_date_of_confirmation = $_POST['confirmation_date'];
			$emp_date_of_confirmation = date('d/m/Y', strtotime($emp_date_of_confirmation));
		}
		else{
			$emp_date_of_confirmation = "Confirmation Date";
		}

		$getEmp  = mysqli_query($con,"SELECT * FROM `employee` WHERE id = '$id' ") or die(mysqli_error($con));
		while($empDetails = mysqli_fetch_array($getEmp))
		{
			$emp_name = $empDetails['name'];
			$emp_designation = $empDetails['designation'];
			$emp_department = $empDetails['department'];
			$emp_empid = $empDetails['empid'];
			$email = $empDetails['workemail'];
			// echo $emp_name; die;

		}

		if(!empty($emp_designation)){
			$getDesignation  = mysqli_query($con,"SELECT * FROM `designation` WHERE id = '$emp_designation' ") or die(mysqli_error($con));
			while($designationDetails = mysqli_fetch_array($getDesignation))
			{
				$emp_designation = $designationDetails['name'];
			}
		}

		if(!empty($emp_department)){
			$getDepartment  = mysqli_query($con,"SELECT * FROM `departments` WHERE id = '$emp_department' ") or die(mysqli_error($con));
			while($departmentDetails = mysqli_fetch_array($getDepartment))
			{
				$emp_department = $departmentDetails['name'];
			}
		}
	}
	else{
		echo 'NOTHINGFOUNDHERE';
	}

}
else{
	echo 'NOTHINGFOUNDHERE';
}


error_reporting(-1);

// email library
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../leaves/vendor/phpmailer/phpmailer/src/Exception.php';
require '../../leaves/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../../leaves/vendor/phpmailer/phpmailer/src/SMTP.php';

$mail = new PHPMailer(true);
require '../../leaves/vendor/autoload.php';
// end email library

   if(!empty($emp_name) && !empty($emp_designation) && !empty($emp_department) && !empty($emp_empid) && !empty($emp_date_of_confirmation) ){

    
	// $button = $_POST['submit'];

	$button = $_POST['submit'];

	} else {
		echo 'NOTHINGFOUNDHERE';
	}
// echo $button; die;
	
require_once('tcpdf_include.php');

class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'swastika-logo.jpg';
		$this->setJPEGQuality(60);
        $this->Image($image_file, 30, 10, 50, 9, 'jpg', '', 'T', true, 300, 'R', false, false, 0, false, false, false);

		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
//		$this->Cell(0, 15, 'OFFER LETTER', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-40);
		// Page number
		//$footertext = '<style>'.file_get_contents(K_PATH_IMAGES.'font_tcpdf.css').'</style>';
		$footertext = '<p style="color: #4c7aec;text-align:centre;"><b>Swastika Investmart Limited</b></p>
					   <p style="text-align:centre;"><b>Corp. Off.:</b> 48 Jaora Compound, M.Y.H. Road Indore-452001 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;0731-6644000, 3345000</p>
					   <p style="text-align:centre;"><b>Regd. Off.:</b> Flat No. 18, North Wing, Madhaveshwar Co-op. Hsg. Society, S.V. Road, Andheri (W), Mumbai-40058 &nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;022-26254568-69</p>
					   <p style="text-align:centre;"><i class="fa fa-envelope fa-11x"></i>&nbsp;&nbsp;info@swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-globe"></i>&nbsp;&nbsp; www.swastika.co.in  &nbsp;&nbsp;&nbsp;<i class="fa fa-car"></i>&nbsp;&nbsp;0120-4400789 </p>
					   <table cellspacing="4" cellpadding="4">
							<tr>
								<th style="width:100%;height:25px;text-align: center; background-color: #41d7e4;">
									<p style="color:#fff; text-align:centre;padding:50px 50px 600px 666px"><b>Swastika Group: Member of NSE, BSE, NCDEX, MCX, MSEI, DP:NSDL & CDSL</b></p>
								</th>
								
							</tr>
						</table>
				      ';
		$this->writeHTML($footertext, false, true, false, true);
		
		//$this->Cell(0, 10, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Swastika Investmart Limited');
$pdf->SetTitle('Confirmation Letter');
$pdf->SetSubject('Swastika Investmart Limited');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$certificate = K_PATH_IMAGES.'phone_icon.png';
//echo $certificate; die;

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();




// create some HTML content

	//$emp_date_of_confirmation = $enpDetails['doj'];

$date = date('d/m/Y');

$html = <<<EOF
<strong style="font-size:20px;text-align:centre;">CONFIRMATION LETTER</strong>
<p style="float: left;font-size:15px;"><b>Date: $date</p>
<div style="float:left;font-size:15px;">
Dear $emp_name<br />
Designation: $emp_designation<br />
Department: $emp_department<br />
Employee Code: $emp_empid
</div>

<p style="float:left;">With reference to the review of your performance during the probation period from <b>$start_date</b> to <b>$end_date</b>, we are pleased to inform you that your employment is being confirmed as <b>$emp_designation</b> effective from <b>$emp_date_of_confirmation</b>.The terms and conditions as per mentioned in your appointment letter will remain unchanged.</p>

<p>We expect same enthusiasm and passion towards performing your job in future too. We wish you good luck for your future performance. <br/><br/></p>

<p style="float:left;"><b>For Swastika Investmart Limited</b><br/><br/></p>
<p><span style="font-size:10px;font-weight: bold;float:left;">Authorized Signatory</span><br/><br/></p>

EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');



// to clean the output buffer just before the method invocation:
ob_end_clean();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


// ---------------------------------------------------------

//Close and output PDF document
if($button=='GENERATE CONFIRMATION LETTER'){
	$pdf->Output($emp_name.'_confirmationLetter.pdf', 'D');
}else{


$pdfString = $pdf->Output($emp_name.'_confirmationLetter.pdf', 'S');
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true; 	 
$mail->Username = 'swastika.developer@gmail.com';
$mail->Password = 'sil@321#';
$mail->setFrom('swastika.developer@gmail.com', 'Swastika Investmart');
$mail->addAddress($email);
$mail->Subject = 'Confirmation Letter';
$mail->Body = 'Confirmation Letter';
$mail->AltBody = 'Swastika Investmart Private Limited';
// $mail->msgHTML("<h1>This is an HTML message</h1>");
// $mail->addAttachment('test.txt');
$mail->addStringAttachment($pdfString,$emp_name.'_confirmationLetter.pdf');

if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo '<script>alert("Mail Sent Successfully.");window.close();</script>';
}

}


//============================================================+
// END OF FILE
//============================================================+


?>
