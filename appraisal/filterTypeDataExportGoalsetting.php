<?php
    include'../include/config.php';
    
    $deptId = $_POST['atOptionId'];
    $type   = $_POST['type'];
    
        //Do real escaping here

    if($type=='department'){

         $query ="SELECT e.id,e.name,e.mobile,au.issubmitted ,d.name as department,a.appraised_mode,a.appraised_time,a.appraised_year,a.created_at,a.id as appId
                FROM employee e 
                INNER JOIN departments d  ON d.id = e.department 
                INNER JOIN appraisal_users au ON au.emp_id = e.id
                INNER JOIN appraisals a ON a.id = au.app_id where d.id ='$deptId'";
    }else{
         $query ="SELECT e.id,e.name,e.mobile,au.issubmitted ,b.name as department,a.appraised_mode,a.appraised_time,a.appraised_year,a.created_at,a.id as appId
                FROM employee e 
                INNER JOIN branch b  ON b.id = e.branch 
                INNER JOIN appraisal_users au ON au.emp_id = e.id
                INNER JOIN appraisals a ON a.id = au.app_id where b.id ='$deptId'";
    }
    
    $result = mysqli_query($con,$query);
    

if(mysqli_num_rows($result) > 0){
    $delimiter = ",";
    $filename = "members_" . date('Y-m-d') . ".csv";
    

    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('ID', 'Name','Mobile','Appraisal Time','Create Date','Status');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = mysqli_fetch_array($result)){
         
         $time = $row['appraised_time'].'-'.$row['appraised_year'];

        $lineData = array($row['id'], $row['name'],$row['mobile'],$time,$row['created_at'],$row['issubmitted']);
        fputcsv($f, $lineData, $delimiter);
    }

    
    //move back to beginning of file
    fseek($f, 0);
      
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f); 
  

}



exit;

?>
