<table width="100%" class="formCrtl">
<tr>
<td style="width:50%;vertical-align:middle" >
	<span style="font-size:10px;color:#222;font-weight:bold">BTC</span>
	
	<input type="text" class="input" placeholder="0.0348456" style="border-radius:0px;padding:0px;font-size:14px !important;width:100%;padding:0px !important;margin:0px !important"/>
</td>

<td style="width:50%;vertical-align:middle;border-left:1px #eee solid;padding-left:10px;">
	<span style="font-size:10px;color:#222;font-weight:bold">INR</span>
	
	<input type="text" class="input" placeholder="10000.00" style="border-radius:0px;padding:0px;font-size:14px !important;width:100%;padding:0px !important;margin:0px !important"/>
</td>
</tr>
<tr>
<td style="width:100%;vertical-align:middle;position:relative" colspan="2">
	<span style="font-size:10px;color:#222;font-weight:bold">PAYMENT DESTINATION</span>
<div style="position:absolute;right:10px;bottom:10px">
	<i class="fa fa-caret-down" style="font-size:15px;"></i>
</div>
<select class="input">
<option value="My Wallet">BANK ACCOUNT</option>
<option value="BTC">CREDIT BALANCE</option>
	
</select>	
</td>
</tr>


<tr>
<td style="width:50%;vertical-align:middle" >
	<span style="font-size:10px;color:#222;font-weight:bold">GREENPAY FEE</span>
	<br/>
	Rs. 400
</td>

<td style="width:50%;vertical-align:middle;border-left:1px #eee solid;padding-left:10px;">
	<span style="font-size:10px;color:#222;font-weight:bold">TAX</span>
	<br/>
	18%
</td>
</tr>
<tr>
<td style="width:100%;vertical-align:middle" colspan="2">
	<span style="font-size:10px;color:#222;font-weight:bold">TOTAL</span>
<br/>
<span style="font-size:30px;color:#007599">Rs. 12453.45</span>
</td>
</tr>

</table>
<div style="padding:10px;">
<br/>
<button class="btn btn-primary" style="width:100%;width:100%;font-weight:bold;height:50px;">CONFIRM SALE</button>
</div>

