<?php
include("../include/config.php");
$data = Array();
$account = Array();
$accountArray = getData('account','*','name','ASC');
foreach($accountArray as $val)
{
	$account[$val['id']] = $val['name'];
}
$data = getData('expenses','*','name','ASC');

$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-primary"  onclick="getModule('<?php echo $newUrl;?>','formDiv','tableDiv','loading')">+1 ADD NEW</button>
	<button class="btn btn-sm btn-danger"  onclick="crossCheckDelete('fee_category')">DELETE SELECTED</button>


</div>
<div class="moduleHeading">
Expenses</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:20px;">#</th>
<th style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this)">

</th>
<th>Name</th>
<th>Amount</th>
<th>Date</th>
<th>From Account</th>
<th>Createdate</th>
</tr>

<?php
$i=1;
foreach($data as $row)
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $i;?></td>
<td>
	<input type="checkbox" class="checkInput" id= name="" value="<?php echo $row['id'];?>">
</td>
<td class="text-primary" onclick="getModule('expences/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['name'];?></td>
<td>
	<?php echo $row['amount'];?>
</td>

<td>
	<?php echo $row['date'];?>
</td>

<td>
	<?php echo $account[$row['account']];?>
</td>


<td>
<?php echo date("d/m/y h:i A",strtotime($row['createdate']));?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>