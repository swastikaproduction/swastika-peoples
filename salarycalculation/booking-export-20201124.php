<?php
include("../include/config.php");
$type = $_GET['type'];
$month = $_GET['month'];
$year = $_GET['year'];
$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
}

$name = "calculationsheet_".$month."_".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>

<table border="1">

<tr>
	<th>Employee Name</th>
	<th>Employee Code</th>
	<th>Employee Type</th>
	<th>CTC</th>	
	<th>Gross Salary</th>
	<th>Leave Deductions</th>
	<th>Hold Deductions</th>
	<th>Other Deductions</th>
	<th>Loan</th>
	<th>Tds</th>
	<th>Calculated Salary</th>
	<th>Basic</th>
	<th>Da</th>
	<th>Hra</th>
	<th>Bonus</th>
	<th>Refreshment</th>
	<th>Pf Contribution Employee</th>
	<th>Other Deduction</th>
	<th>Total Pf Payable</th>
	<th>Other Income</th>
	<th>Professional Tax</th>
	<th>Netpay</th>
	<th>Branch</th>
	<th>Branch Code</th>
	<th>Region</th>


</tr>

<?php
if($month < 10)
{
$month = "0".$month;	
}

if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' AND employee.depttype != 'Cash'  ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else if($type == 'Cash')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.branch ,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = 'Cash' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else if($type == 'All')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department ,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id  AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department ,employee.branch,employee.od,employee.pt FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}

while($row= mysqli_fetch_array($getData))
{  $bid = $row['branch'];
   $bquery = mysqli_query($con,"select * from branch where id='$bid'");
   $brow = mysqli_fetch_array($bquery);
   $rid = $brow['regionId'];
   $rquery = mysqli_query($con,"select * from region where id='$rid'");
   $rrow = mysqli_fetch_array($rquery) ;

//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$odflag = $row['od'];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}

$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;

if($row[7] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{  
	//for march month deduction 
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}

if($row['pt']=='1'){

	$pt = $pt;
}else{
	$pt = 0;
}



if($pfflag == '1')
{

		if($calculatedsalary < 9001)
		{ 

			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);

			if($pfpayable == '')
			{
				$pfpayable = round($gross * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
            $otherdeduction=0;
			$pfemployer = round($pfpayable/2);	
			$basic = $ctc - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;

			$netpay = $totalearnings - $totaldeductions;
		}	

		else if($calculatedsalary > 9001 && $calculatedsalary < 14999)
		{    
			
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			
			$basic = 8400;
			$hra = 0;
			
			$otherdeduction=0;
			
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}		
            
			$pfemployer = round($pfpayable/2);			
			$bonus = $ctc - ($basic+$pfemployer);
			$refreshment=$ctc-($basic+$pfemployer);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 14999 && $calculatedsalary <= 20000)
		{ 
			$ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);

			if($basic>8400){
				$basic = 8400;
			}else{
				$basic = $basic;
			}
			$hra = round($basic*0.40);
			$otherdeduction=0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = round($ctc*0.10);;
		    $refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary > 20001 && $calculatedsalary < 26800)
		{   $ctc = $calculatedsalary;
			$gross = round(($ctc*93.28358209)/100);
			$basic = round($gross * 0.60);
			$hra = round($basic * 0.40);
			$bonus = round($calculatedsalary * 0.10);
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($calculatedsalary*0.025);
			}else{
              $otherdeduction=0;
			}
			
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
		

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}

		else if($calculatedsalary >26801)
		{   $ctc = $calculatedsalary;
			$gross = $calculatedsalary;
			$basic = round($ctc * 0.52);
			$hra = round($basic * 0.40);
			$bonus = round($ctc * 0.12);
			//other deduction condition when flag is 1
			if($odflag=='1'){
              $otherdeduction = round($ctc*0.025);
			}else{
              $otherdeduction=0;
			}		
			
			$pfpayable = 3600;	

			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da+$otherdeduction);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 9400)
	{
		    $ctc = $calculatedsalary;	
		    $gross = $ctc;	    
			$basic = 8400;
			$hra = 0;
			$da = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - $basic;
			$refreshment=0;
			$otherdeduction=0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;

	}else if($calculatedsalary > 9400 && $calculatedsalary < 15000)
	{       $ctc = $calculatedsalary;
		    $gross = $ctc;
			$basic = round($ctc * 0.60);
			if($basic>8400){
				$basic = 8400;
			}else{
				$basic = $basic;
			}
			$pfpayable = 0;
			$pfemployer = 0;
			$refreshment=0;
			$da=0;
			$otherdeduction=0;
			$bonus = round($ctc * 0.10);
			$hra = $ctc - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary >= 15000 &&  $calculatedsalary <=20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($ctc * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			$otherdeduction=0;
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt+$otherdeduction;
			$netpay = $totalearnings - $totaldeductions;
	}
	else if( $calculatedsalary >20000)
	{       $ctc = $calculatedsalary;
	        $gross = $calculatedsalary;
			$basic = round($calculatedsalary * 0.52);
			$pfpayable = 0;
			$da = 0;			
			$pfemployer = 0;
			//other deduction condition when flag is 1
			if($odflag=='1'){
            $otherdeduction= round($calculatedsalary*0.025);
			}else{
            $otherdeduction=0;
			}
			
			$bonus = round($calculatedsalary * 0.15);
			$hra = round($basic * 0.40);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra+$otherdeduction);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;// other deduction already deducted in refreshmentns.
			$netpay = $totalearnings - $totaldeductions;
	}
}

?>

<tr>
	<td><?php echo $row[0];?></td>
	<td><?php echo $row[1];?></td>
	
	<td>
		
		<?php
if($row[8] == '4')
{
	echo "Housekeeping";
}
else 
{
	echo $row[7];
}


		?>

	</td>
	<td><?php echo $grosssalary;?></td>
	<td><?php echo $gross;?></td>	
	<td><?php echo $leavedeductions;?></td>
	<td><?php echo $hold;?></td>
	<td><?php echo $otherdeduction;?></td>
	<td><?php echo $loan; //THIS IS JV HOLD 1?></td>
	<td><?php echo $jvhold; //THIS IS JV HOLD 2?></td>
	<td><?php echo $calculatedsalary;?></td>
	<td><?php echo $basic;?></td>
	<td><?php echo $da;?></td>
	<td><?php echo $hra;?></td>
	<td><?php echo $bonus;?></td>
	<td><?php echo $refreshment;?></td>
	<td><?php echo $pfemployer;?></td>
	<td><?php echo $otherdeduction;?></td>
	<td><?php echo $pfpayable;?></td>
	<td><?php echo $otherdeduction;?></td>
	<td><?php echo $pt;?></td>
	<td><?php echo $netpay - ($loan+$jvhold);?></td>
	<td><?php echo $brow['name'];?></td>
	<td><?php echo $brow['code'];?></td>
	<td><?php echo $rrow['name'];?></td>
</tr>

<?php
}
?>
</table>

