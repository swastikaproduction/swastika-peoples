<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model {

    //
    protected $table = 'policies';

    public $timestamps = false;

}
