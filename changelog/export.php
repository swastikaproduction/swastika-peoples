<?php
include("../include/config.php");
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
$month = "0".$month;	
}

$emparray = Array();
$employees = Array();
$employees = getData('employee','*','name','ASC');	
foreach($employees as $val)
{
	$emparray[$val['id']] = $val['name']."/".$val['empid'];
}

$name = "changelog_".$month."_".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


$getdata = mysqli_query($con,"SELECT * FROM `changelog` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
?>
<table border="1">
	<tr>
		<th>Employee</th>
		<th>Date</th>
		<th>Old In Time</th>
		<th>Old Out Time</th>
		<th>Updated In Time</th>
		<th>Updated Out Time</th>
		<th>Changed On</th>
	</tr>

<?php
while($row = mysqli_fetch_array($getdata))
{
	?>
<tr>
	<td><?php echo $emparray[$row['empid']];?></td>
<td><?php echo $row['date'];?></td>
	<td><?php echo $row['oldin'];?></td>
	<td><?php echo $row['oldout'];?></td>
	<td><?php echo $row['newin'];?></td>
	<td><?php echo $row['newout'];?></td>
	<td><?php echo date("d-m-y h:i A",strtotime($row['createdate']));?></td>
</tr>
	<?php
}
?>
</table>