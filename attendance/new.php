<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
?>



<div class="moduleHead">
<br/>
<div style="float:right">
	<button class="btn btn-sm btn-primary" onclick="window.open('attendance/allthsheet.php?month='+document.getElementById('inp0').value+'&year='+document.getElementById('inp1').value,'_blank')">DOWNLOAD ALL EMPLOYEE SHEET</button>
	<button class="btn btn-sm btn-primary" onclick="window.open('attendance/thsheet.php?month='+document.getElementById('inp0').value+'&year='+document.getElementById('inp1').value,'_blank')">DOWNLOAD TRAINEE & HOUSKEEPING SHEET</button>
</div>

	<div class="moduleHeading" onclick="toogleFormTable();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Attendance

	</div>
</div>




<div class="tabelContainer shadow" style="height:auto;background:#fff;">


<div class="row" style="border-bottom: 1px #eee solid;">
<div class="col-sm-12 subHead">
	Upload a new sheet
</div>


<div class="col-sm-2 formLeft">
		Format
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="inp3">
	<option value="MATRIX">MATRIX</option>
	<option value="ELSS">ELSS</option>
	</select>
		
	</div>

	<div class="col-sm-2 formLeft">
		Month
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp0">
	<?php

	for($j=1;$j<13;$j++)
	{
		$t = "2017-".$j."-01";
		?>
<option value="<?php echo $j;?>"><?php echo date("M",strtotime($t));?></option>
		<?php
	}
	?>
	</select>
		
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		Year
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp1">
	<?php

	for($j=2017;$j<2025;$j++)
	{
		?>
<option value="<?php echo $j;?>"><?php echo $j;?></option>
		<?php
	}
	?>
	</select>
	</div>


</div>


<div class="row" >
	<div class="col-sm-2 formLeft">
		File
	</div>
	<div class="col-sm-10 formRight">
		<form action="fileupload.php?inputid=inp2&type=csv" method="post" target="fileuploadframe"  enctype="multipart/form-data">		
		<input type="file" name="image"  class="inputBox" onclick="$('#docSaveDiv').hide();">
		  <input type="text" name="" id="inp2" style="display:none"> 
		  <br/>
<center>
		 <button name="file_upload" class="btn btn-primary btn-sm" type="" id="upButton" onclick="showProcessing();">UPLOAD FILE</button>
		 </center>
		 <br/>
	</form>
	</div>	

</div>

<div class="row" style="display:none" id="docSaveDiv">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
	<br/><br/>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','inp',4,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;FILE UPLOADED, CLICK TO ALLOT</button>
			<br/><br/><br/>
	</div>	

</div>	


</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>