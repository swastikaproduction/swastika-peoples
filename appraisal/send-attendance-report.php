<?php
    include'../include/config.php';  
  
        //Do real escaping here

    $query = "SELECT  e.name,m.name as manager,b.name as branch,d.name as designation,ds.name as department,lvr.type,lvr.fromdate,lvr.todate,lvr.hodapproval,lvr.ondate,lvr.intime,lvr.outime   FROM leaverequests lvr 
INNER  JOIN employee e on e.id = lvr.empid INNER JOIN employee m on m.id = e.imdboss 
INNER JOIN branch b ON b.id = e.branch INNER JOIN designation d on d.id = e.designation 
INNER JOIN departments ds ON ds.id=e.department where lvr.ondate='2019-06-05'";
    
    $result = mysqli_query($con,$query);
    

if(mysqli_num_rows($result) > 0){
    $delimiter = ",";
    $filename = "members_" . date('Y-m-d') . ".csv";
    

    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Name','Manager','Branch','Designation','Department','From date','To date','HOD Approval','On date','Intime','Outtime','Request type');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = mysqli_fetch_array($result)){

    	 $status = ($row['hodapproval']=='1')?'Approve':'Pending';
         
         $time = $row['appraised_time'].'-'.$row['appraised_year'];

        $lineData = array($row['name'],$row['manager'],$row['branch'],$row['designation'],$row['department'],$row['fromdate'],$row['todate'],$status,$row['ondate'],$row['intime'],$row['outime'],$row['type']);
        fputcsv($f, $lineData, $delimiter);
    }

    
    //move back to beginning of file
    fseek($f, 0);
      
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f); 

   

  

}



exit;

?>
