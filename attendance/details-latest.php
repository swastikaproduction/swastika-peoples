<?php
include("../include/config.php");
$id = $_GET['id'];

$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowData = mysqli_fetch_array($getData);
$month = $rowData['month']; 
$year = $rowData['year'];
$empid = $rowData['empid'];

$tableName = "attendance_".$month."_".$year;


$getEmp = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);




$getData= mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid' ORDER BY `date` ASC") or die(mysqli_error($con));
?>


<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
	


	<div class="moduleHeading">
<?php echo $rowEmp['name'];?>

	</div>
</div>


<br/>
<div style="height:600px;overflow-y:scroll">
<table class="table table-hover fetchSmall table-bordered" cellpadding="0" cellspacing="0">

	<tr>
		<th>Date</th>
		<th>In</th>
		<th>Out</th>
		<th>Shift End</th>
		<th>Workhrs</th>
		<th>Allotedhrs</th>
		<th>Diff</th>
		<th>Late</th>
		<th>Consecutive</th>
		<th>Status</th>
		<th>Two Hours</th>
		<th>Fifteen Mins</th>
		<th>Deduction</th>
		<th>Remarks</th>
		<th>Forced Dedcution</th>
		<?php
		if($loggeduserid == '124')
		{
?>
		<th>Action</th>
<?php			
		}
		?>

	</tr>


<?php
while($row= mysqli_fetch_array($getData))
{

	$class = '';
	if($row['status'] == '2')
	{
		$class = 'bg-warning';
	}
	else if($row['status'] == '1')
	{
		$class = '';
	}
	else
	{
		$class = 'bg-danger';
	}


	if($row['twohourvalue'] != '0')
{
	$class = 'bg-info';
}


if($row['deduction'] != '0')
{
	$class = 'bg-danger';
}









	?>
<tr class="<?php echo $class;?>">
	<td><?php echo $row['date'];?></td>
	<td><?php echo $row['intime'];?></td>
	<td><?php echo $row['outime'];?></td>
	<td><?php echo $row['shiftend'];?></td>
	<td><?php echo $row['hours'] * 60;?></td>
	<td><?php echo $row['allotedshifthours'];?></td>
	<td><?php echo $row['difference'];?></td>
	<td><?php echo $row['late'];?></td>
	<td><?php echo $row['consecutive_counter'];?></td>
	<td><?php
	if($row['status'] == '2')
	{
		echo "Holiday";
	}
	else if($row['status'] == '1')
	{
		echo "Present";
	}
	else
	{
		echo "Absent";
	}

	?></td>
	<td><?php echo $row['twohourvalue'];?></td>
	<td><?php echo $row['fifteenminscounter'];?></td>
	<td>
<div style="float:right" onclick="getModal('leaves/req-modal.php?id=<?php echo $row['id'];?>&table=<?php echo $tableName;?>','formModal','tableModal','loading')">
	<span class="label label-success">RAISE ISSUE</span>
</div>
	<?php echo $row['deduction'];?>
		
		

	</td>
	<td><?php echo $row['remarks'];?></td>
	<td><?php 
	if($row['forced'] == '')
	{
		if($loggeduserid == '124')
		{

		?>
<span class="label label-info" onclick="getModal('attendance/forced.php?id=<?php echo $id;?>&tableid=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">
	<i class="fa fa-plus"></i>&nbsp;ADD
		</span>
		<?php
	}
	}
	else
	{
			if($loggeduserid == '124')
		{
?>
<span class="label label-default" style="font-size:10px;" onclick="getModal('attendance/forced.php?id=<?php echo $id;?>&tableid=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">
	<?php echo $row['forced'];	?>
		</span>

<?php
		}
		else
		{
			?>
<span class="label label-default" style="font-size:10px;">
	<?php echo $row['forced'];	?>
		</span>

			<?php
		}
?>

<?php
		
	}
	?></td>


		<?php
		if($loggeduserid == '124')
		{
?>
		<td>
	<span class="label label-warning" onclick="getModal('attendance/edit.php?id=<?php echo $id;?>&tableid=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">
	<i class="fa fa-edit"></i>&nbsp;EDIT
		</span>
	</td>
<?php			
		}
		?>

	
	

</tr>
<?php
}
?>
</table>
<br/><br/><br/><br/>
<br/><br/><br/><br/>

</div>
</div>
</div>
