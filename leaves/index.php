<?php
include("../include/config.php");
$data = Array();
if(!isset($_GET['my']))
{
if($loggeduserid == '124')
{
if(isset($_GET['pending']))
{
	$penStr = " AND leaverequests.status = '0'";
}
else
{
	$penStr = " AND (1=1)";	
}

if(isset($_GET['type']))
{
	$typeStr = " AND leaverequests.type = ".$_GET['type'];
}
else
{
	$typeStr = "";	
}

$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval,leaverequests.approval_date FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id ".$penStr.$typeStr." ORDER BY leaverequests.id DESC") or die(mysqli_error($con));

}
else
{
if(isset($_GET['pending']))
{
	$penStr = " AND leaverequests.status = '0'";
}
else
{
	$penStr = " AND (1=1)";	
}

if(isset($_GET['type']))
{
	$typeStr = " AND leaverequests.type = ".$_GET['type'];
}
else
{
	$typeStr = "";	
}

	if($profile == 20){

		$branch = [];   
      	$getZones = mysqli_query($con,"SELECT `zones` FROM employee WHERE id='$loggeduserid'") or die(mysqli_error($con));
      	$rowZones = mysqli_fetch_array($getZones);

      	$getData = mysqli_query($con,"SELECT id FROM `branch` WHERE zoneid IN ($rowZones[0])") or die(mysqli_error($con));
    	while($row=mysqli_fetch_array($getData)){
    	$branch[] .= $row[0];
    	}
    	$branches = implode(',', $branch);
    	$employees = mysqli_query($con,"SELECT * FROM employee where `branch` IN ($branches) AND `left`=0") or die(mysqli_error($con));

    	$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval,leaverequests.approval_date FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND `branch` IN ($branches) AND leaverequests.status = '0' ORDER BY leaverequests.id DESC") or die(mysqli_error($con));
	}
	else{
		$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval,leaverequests.approval_date FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.imdboss = '$loggeduserid' ".$penStr.$typeStr."  ORDER BY leaverequests.id DESC") or die(mysqli_error($con));

	}

}
}
else
{
	$getData = mysqli_query($con,"SELECT employee.name,branch.name,leaverequests.type,leaverequests.fromdate,leaverequests.todate,leaverequests.ondate,leaverequests.status,leaverequests.createdate,leaverequests.id,leaverequests.intime,leaverequests.outime,leaverequests.hodapproval,employee.id,leaverequests.approval_date FROM leaverequests,employee,branch WHERE leaverequests.empid = employee.id AND employee.branch = branch.id AND employee.id = '$loggeduserid' ".$penStr."  ORDER BY leaverequests.id DESC") or die(mysqli_error($con));
}
$newUrl= str_ireplace("index.php", "new.php",$urltocall);
?>


<div class="moduleHead">
<br/>
<div style="float:right">
	<button class="btn btn-primary"  onclick="getModule('leaves/new.php?leave=1','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>


	<div class="moduleHeading" onclick="toogleFormTable();">
	Leave & Attendance Change Requests
	</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-hover shadow" cellpadding="0" cellspacing="0" id="tableDiv">
<tr>
<th style="width:5%;">#</th>
<th style="width:15%">Employee</th>
<th style="width:10%">Branch</th>
<th style="width:15%;">Type</th>
<th style="width:30%">Details</th>
<th style="width:10%">HOD Approval</th>
<th style="width:10%">Status</th>
<th style="width:10%">Request Date</th>
<th style="width:10%">HOD Approval Date</th>
</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
?>
<?php
if($row[12] != $loggeduserid && $loggeduserid != '124')
{

	if(isset($_GET['type']))
	{
		$typeChangeStatus = "&type=".$_GET['type'];
	}
	else
	{
		$typeChangeStatus = "";	
	}

	?>

<?php if($profile != '20'){ ?>

<tr id="tableRow<?php echo $row[8];?>" onclick="getModal('leaves/changestatushod.php?id=<?php echo $row['id'].$typeChangeStatus;?>','formModalBig','tableModalBig','loading')">


	<?php }
}
?>
<td><?php echo $i;?></td>
<td><?php echo $row[0];?></td>
<td><?php echo $row[1];?></td>
<td>

<?php 
	if($row[2] == '1')
	{
		?>
	<i class="fa fa-circle" style="color:#ffb22b"></i>&nbsp;&nbsp;&nbsp;
		<?php
		echo "Leave";
	}
	else if($row[2] == '2')
	{
		?>
	<i class="fa fa-circle" style="color:#1976d2"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Timing Mismatch";
	}

	else if($row[2] == '3')
	{
		?>
	<i class="fa fa-circle" style="color:#22cabf"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Overtime";
	}

	else if($row[2] == '4')
	{
		?>
	<i class="fa fa-circle" style="color:#e32eab"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "OD Request";
	}

else if($row[2] == '5')
	{
		?>
	<i class="fa fa-circle" style="color:#fed3c5"></i>&nbsp;&nbsp;&nbsp;
		<?php

		echo "Half Day";
	}
?></td>
<td>
<?php
if($row[2] == 1)
{
	?>

<span class="label label-default">
	<?php echo date("D, d M Y",strtotime($row[3]));?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo date("D, d M Y",strtotime($row[4]));?>
</span>
	<?php
}
else
{
	?>

<span class="label label-default">
	<?php echo $row[9];?> </span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default"><?php echo $row[10];?></span><div style="display:inline-block;vertical-align:middle;height:5px;width:50px;margin:10px 0px;background:#ccc;"></div><span class="label label-default">
<?php echo date("D, d M Y",strtotime($row[5]));?>
</span>



	<?php
}
?>
</td>


<td style="padding-top:15px;" <?php if($loggeduserid == '124')
{
?>

<?php	
}
?>>
	<?php 
	if($row[11] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row[11] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row[11] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>



<td style="padding-top:15px;" <?php if($loggeduserid == '124')
{
	if(isset($_GET['type']))
	{
		$typeChangeStatus = "&type=".$_GET['type'];
	}
	else
	{
		$typeChangeStatus = "";	
	}

?>
onclick="getModal('leaves/changestatus.php?id=<?php echo $row['id'].$typeChangeStatus; ?>','formModalBig','tableModalBig','loading')"
<?php	
}
?>>
	<?php 
	if($row[6] == '1')
	{
		?>
<span class="label label-success">
<i class="fa fa-check"></i>&nbsp;
APPROVED</span>
		<?php
	}
		if($row[6] == '2')
	{
		?>
<span class="label label-danger">
<i class="fa fa-alert"></i>&nbsp;
REJECTED</span>
		<?php
	}
	if($row[6] == '0')
	{
		?>
<span class="label label-default">
<i class="fa fa-circle"></i>&nbsp;
NOT YET UPDATED</span>
		<?php
	}

	?>
</td>


<td>
<?php echo date("d/m/y h:i A",strtotime($row[7]));?>
</td>

<td>
<?php if(!empty($row['approval_date'])){ echo date("d/m/y h:i A",strtotime($row['approval_date'])); }else{echo ''; } ?>
</td>


</tr>

	<?php
	$i++;
}
?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>
