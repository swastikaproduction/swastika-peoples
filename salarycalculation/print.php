<?php
include("../include/connection.php");

$id = $_GET['id'];
$getEmp = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

$name = "deductions_".$rowEmp['name'].".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


$getData= mysqli_query($con,"SELECT * FROM `attendance` WHERE `empid` = '$id'") or die(mysqli_error($con));
?>

<table border="1">
<tr>
	<th colspan="6"><?php echo $rowEmp['name'];?></th>
</tr>
	<tr>
		<th>Date</th>
		<th>InTime</th>
		<th>Out Time</th>
		<th>Hours Working (In Mins)</th>
		<th>Alloted Hours</th>
		<th>Difference (In Mins)</th>
		<th>Late (In Mins)</th>
		<th>Consecutive Late Counter</th>
		<th>Status</th>
		<th>Two Hours Usage (Mins)</th>
		<th>Fifteen Mins Counter</th>
		<th>Deduction For The Day</th>
		<th>Remarks</th>
	</tr>


<?php
while($row= mysqli_fetch_array($getData))
{
?>
<tr>
	<td><?php echo $row['date'];?></td>
	<td><?php echo $row['intime'];?></td>
	<td><?php echo $row['outime'];?></td>
	<td><?php echo $row['hours'] * 60;?></td>
	<td><?php echo $row['allotedshifthours'];?></td>
	<td><?php echo $row['difference'];?></td>
	<td><?php echo $row['late'];?></td>
	<td><?php echo $row['consecutive_counter'];?></td>
	<td><?php
	if($row['status'] == '2')
	{
		echo "Holiday";
	}
	else if($row['status'] == '1')
	{
		echo "Present";
	}
	else
	{
		echo "Absent";
	}

	?></td>
	<td><?php echo $row['twohourvalue'];?></td>
	<td><?php echo $row['fifteenminscounter'];?></td>
	<td><?php echo $row['deduction'];?></td>
	<td><?php echo $row['remarks'];?></td>
	

</tr>
<?php
}
?>
</table>