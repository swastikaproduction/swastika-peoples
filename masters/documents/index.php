<?php
include("../../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
?>
<div class="moduleHead">
<br/>
<div style="float:right">		
<div class="btn-group">
</div>
</div>
	<div class="moduleHeading">

<span>
	<span style="font-size:20px;">
	</span>&nbsp;&nbsp; Generate Offer Letter
</span>
	</div>
</div>
<form target="_blank" method="post" onsubmit="return ValidateOfferLetter();" action="TCPDF/examples/example_049.php" id="myform" >
<div class="shadow" style="background:#fff">
<div class="row">
	<div class="col-sm-2 formLeft req">
		Employee Name
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" title="" id="inp0" name="name" class="inputBox input-field"  value="" >
		<span id="userName-info" class="info" style="color:red"></span><br />
	</div>	

	<div class="col-sm-2 formLeft req">
		Employee Email Address
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" title="" id="inp1" name="email" class="inputBox input-field"  value="">
		<span id="userEmail-info" class="info" style="color:red"></span><br />
	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Designation
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox input-field" id="inpDesignation" name="designation">
		<option>-- Select Designation -- </option>
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option  value="<?php echo $row2['name']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	<span id="userDesignation-info" class="info" style="color:red"></span><br />
	</div>	

	<div class="col-sm-2 formLeft req">
		Branch
	</div>
	<div class="col-sm-4 formRight">
    <select class="inputBox input-field" id="inpBranch" name="branch">
	<option>-- Select Branch -- </option>
		<?php
		$data = getData('branch','*','name','ASC');
		foreach($data as $row2){
		?>
		<option value="<?php echo $row2['name']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	<span id="userBranch-info" class="info" style="color:red"></span><br />
	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Revenue
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" id="revenue" name="revenue" class="inputBox" >
	</div>	

	<div class="col-sm-2 formLeft req">
		Funded
	</div>
	<div class="col-sm-4 formRight">
    	<input type="text" id="funded" name="funded" class="inputBox" >
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		A/cs
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" id="a_cs" name="a_cs" class="inputBox" >
	</div>	

	<div class="col-sm-2 formLeft req">
		Margins
	</div>
	<div class="col-sm-4 formRight">
    	<input type="text" id="margins" name="margins" class="inputBox" >
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Franchisee Acquisition
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" id="franchisee" name="franchisee" class="inputBox" >
	</div>	

	<div class="col-sm-2 formLeft req">
		Insurance 1x of salary
	</div>
	<div class="col-sm-4 formRight">
    	<input type="text" id="insurance_salary" name="insurance_salary" class="inputBox" >
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Cloud Telephony
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" id="cloud_telephony" name="cloud_telephony" class="inputBox" >
	</div>	

	<div class="col-sm-2 formLeft req">
		Per Day Traded Clients
	</div>
	<div class="col-sm-4 formRight">
    	<input type="text" id="per_day_traded_clients" name="per_day_traded_clients" class="inputBox" >
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Total traded Clients in Month
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" id="total_traded_clients" name="total_traded_clients" class="inputBox" >
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Salary
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" title="" id="inp2" class="inputBox input-field"  value="" onkeyup="salaryFunction()">
		<span id="userSallery-info" class="info" style="color:red"></span><br />
	</div>	
	<div class="col-sm-2 formLeft req">
    PF Deduction
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp3" onchange="changeDefaultPf('hideValuesOnSelect', this)">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>
	</div>	
</div>

<div class="row">

	<div id="inp4DefaultPF" style="display:none">
		<div class="col-sm-2 formLeft req"  >
			Default PF
		</div>
		<div class="col-sm-4 formRight">
			<input type="text" title="" id="inp4" class="inputBox"  value="">
		</div>	
	</div>

	<div class="col-sm-2 formLeft req">
    PT Deduction
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp5">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>
	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		OD Deduction
	</div>
	<div class="col-sm-2 formRight">
	<select class="inputBox" id="inp6">
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select>
	</div>	

	<div class="col-sm-2 formLeft req">
		Date Of Joining
	</div>
	<div class="col-sm-2 formRight">
		<input type="date" title="" name="doj" id="inpDOJ" class="inputBox input-field"  value="">
		<span id="userDOJ-info" class="info" style="color:red"></span><br />
	</div>	

	<div class="col-sm-4 formLeft req">
	<span onclick="SalaryBreakdowns()" class="label label-info">CALCULATE SALARY BREAKDOWN</span>
	</div>
</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Basic Salary
	</div>
	<div class="col-sm-2 formRight">
		<input type="text"  title="" id="inp7" class="inputBox" name="basic" value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
	    Bonus 
	</div>
	<div class="col-sm-2 formRight">
		<input type="text"  title="" id="inp8" class="inputBox" name="bonus"  value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		CTC
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp9" class="inputBox" name="ctc"  value="" readonly="readonly">
	</div>	

</div>

<div class="row">
	<!-- <div class="col-sm-2 formLeft req">
		DA
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp10" class="inputBox" name="da" value="">
	</div>	 -->

	<div class="col-sm-2 formLeft req">
	    Gross Salary
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp11" class="inputBox" name="grosssalary"  value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		HRA
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp12" class="inputBox"  name="hra" value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		Netpay
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp13" class="inputBox" name="netpay" value="" readonly="readonly">
	</div>	

</div>

<div class="row">
	

	<div class="col-sm-2 formLeft req">
	    Other Deduction
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp14" class="inputBox" name="otherdeduction"  value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		PF Employer
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp15" class="inputBox" name="pfemployer"  value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		PF Payable
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp16" class="inputBox" name="pfpayable"  value="" readonly="readonly">
	</div>	

</div>


<div class="row">
	

	<div class="col-sm-2 formLeft req">
	    Refreshment
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp17" class="inputBox" name="refreshment" value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		Total Deduction
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp18" class="inputBox" name="totaldeduction"  value="" readonly="readonly">
	</div>	

	<div class="col-sm-2 formLeft req">
		 PT Payable
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp20" class="inputBox" name="pt"  value="" readonly="readonly">
	</div>	

</div>

<div class="row">
	
	<div class="col-sm-2 formLeft req">
		Total Earnings
	</div>
	<div class="col-sm-2 formRight">
		<input type="text" title="" id="inp19" class="inputBox" name="totalearnings"  value="" readonly="readonly">
	</div>	
</div>

<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	<div class="col-sm-10 formRight">
		<input type="submit" class="btn btn-primary" name="submit" value="GENERATE OFFER LETTER" >
		
		<input type="submit" class="btn btn-primary" name="submit" value="SEND ON EMAIL">
		<br/><br/><br/>
	</div>	
</div>
</div>
</form>


