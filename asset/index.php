<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Asset Requirement</li>
</ol>
</section>
<!-- Main content -->
<section class="content">

            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<span class="label label-success"  style="float: right;font-size: 15px;margin-bottom: 6px;" data-toggle="modal" data-target="#myModal11">Apply</span>           
</div>  
	<?php
            if ( isset($_GET['success']) && $_GET['success'] == 1 )
{?>

    <div class="alert alert-danger"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Deleted successfully..!
</div>   <?php
}
            ?>      
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Asset Name</th>            
<th>Category</th>          
<th>Asset Tag</th>          
<th>Create date</th>
<th>Status</th>
<!-- <th>Action</th>  -->
<th>Action</th> 
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/config.php';
$query = mysqli_query($con,"SELECT   ac.name category,a.name assetname ,ar.createdate,ar.assettag ,ar.id ,ar.status FROM assetrequirement ar INNER JOIN assets a on a.id = ar.assetid INNER JOIN assetreqcategory ac on ac.id = ar.categoryid where ar.empid='$loggeduserid'");
while($row=mysqli_fetch_array($query)){
if($row['status']==0){
  $status='Pending';
  $color='orange';
}else if($row['status']==1){
  $status='Approve';
  $color='green';
}else{
  $status='Rejected';
  $color='Red';
}

?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['assetname'];?></td>
<td><?php echo $row['category'];?></td>
<td><?php echo $row['assettag'];?></td>
<td><?php echo $row['createdate'];?></td>  
<td><span style="background-color: <?php echo $color ?>;color: white;"><?php echo $status ?></span></td>               
<!-- <td><a onclick="getModule('officialcards/identity/edit-identity.php?id=<?php echo $row['id'];?>','tableDiv','formDiv','loading');"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;<a href="officialcards/identity/delete.php?id=<?php echo $row['id'];?>&message=delete" onclick="return confirm('Are you sure want to delete?')"><i class="fa fa-trash" style="color: red;"></i></a>

</td> -->


 <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal11" onclick="GetAssetdata('<?php echo $row['id'];?>')">
  Edit
</button>
</td>
</tr> 
<?php
}
?>                          

</tbody>               
</table>

</div>
<!-- /.box-body -->
</div>
</div>
</div>

<div class="modal fade" id="myModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog"> 
  
  <!-- Modal content-->
  
  <div class="modal-content"> 
  <form action="asset/post-asset-requirements.php" method="POST" enctype="multipart/form-data">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Apply for Assets</h4>
    </div>
    <div class="modal-body">
   
      
        <div class="row">
          <div class="col-md-6">
            <label>Assest Name</label>
            <select class="form-control" required="" name="assetid">
              <option value="">select name</option>
              <?php

  $query = mysqli_query($con,"select * from assets order by name asc");

  while($row = mysqli_fetch_array($query)){

    ?>
              <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
              <?php }

  ?>
            </select>
          </div>
          <div class="col-md-6">
            <label>Asset Category</label>
            <select class="form-control" required="" name="assetcategory" id="assetcategory" onchange="GetAssetCategory();">
              <option value="">select category</option>
              <?php

  $query = mysqli_query($con,"select * from assetreqcategory order by name asc");

  while($row = mysqli_fetch_array($query)){

    ?>
              <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
              <?php }

  ?>
            </select>
          </div>
        </div>
        <div class="row" id="assettagdiv" style="display: none;">
          <div class="col-md-6">
            <label>Asset Tag</label>
            <input type="text" name="assettag" class="form-control">
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit"  class="btn btn-success">Submit</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
     </form>
 
</div>
</div>
</div>



<div class="modal fade" id="editModal11" role="dialog" style="margin-top: 114px;">
<div class="modal-dialog"> 
  
  <!-- Modal content-->
  
  <div class="modal-content"> 
  <form action="asset/update-asset-requirements.php" method="POST" enctype="multipart/form-data">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Update Assets</h4>
    </div>
    <div class="modal-body">
   
      
        <div class="row">
          <div class="col-md-6">
            <input type="hidden" name="assetreqid" id="assetreqid">
            <label>Assest Name</label>
            <select class="form-control" required="" id="assetid" name="updateassetid">
              <option value="">select name</option>
              <?php

  $query = mysqli_query($con,"select * from assets order by name asc");

  while($row = mysqli_fetch_array($query)){

    ?>
              <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
              <?php }

  ?>
            </select>
          </div>
          <div class="col-md-6">
            <label>Asset Category</label>
            <select class="form-control" required="" name="updateassetcategory" id="assetcategoryid" onchange="GetAssetCategory();">
              <option value="">select category</option>
              <?php

  $query = mysqli_query($con,"select * from assetreqcategory order by name asc");

  while($row = mysqli_fetch_array($query)){

    ?>
              <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
              <?php }

  ?>
            </select>
          </div>
        </div>
        <div class="row" id="assettagdiv1">
          <div class="col-md-6">
            <label>Asset Tag</label>
            <input type="text" name="updateassettag" value="" id="tagname" class="form-control">
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit"  class="btn btn-success">Submit</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
     </form>
 
</div>
</div>
</div>


