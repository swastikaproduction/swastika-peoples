<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active" style="color: green;">Expense Manager</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<!-- <span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" onclick="getModule('appraisal/apraisal_mode.php?type=appraised_mode','formDiv','tableDiv','loading');">Start Goal Setting</span>   -->         
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Employee Code</th>
<th>Concern Person</th>            
<th>Branch Name</th>            
<th>For Branch</th>            
<th>For Admin</th>            
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/connection.php';
$query = mysqli_query($con,"select e.id,e.name,e.expensemanager,e.empid, b.name as bname,e.expenseadmin from employee e  INNER JOIN branch b on b.id = e.branch where `left`=0");
while($row=mysqli_fetch_array($query)){
     $empid = $row['id'];
	if($row['expensemanager']==0){

		$Status = "<span style='color: #f5f5f5;background-color: coral;border-radius: 81px;'>Disable</span>";
	}else{
		$Status = "<span style='color: #f5f5f5;background-color: green;border-radius: 81px;'>Enable</span>";
	}

	if($row['expenseadmin']==0){

		$admin = "<span style='color: #f5f5f5;background-color: coral;border-radius: 81px;'>Disable</span>";
	}else{
		$admin = "<span style='color: #f5f5f5;background-color: green;border-radius: 81px;'>Enable</span>";
	}
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['empid'];?></a></td>
<td><?php echo $row['name'];?></a></td>
<td><?php echo $row['bname'];?></td>
<td><a href='expense/change-expense-manager.php?empid=<?php echo $empid;?>&expensemanager=<?php echo $row['expensemanager'];?> &key=user' onclick="return confirm('Are you sure you want to change this item?');"><?php echo $Status?></a></td>
<td><a href='expense/change-expense-manager.php?empid=<?php echo $empid;?>&expenseadmin=<?php echo $row['expenseadmin'];?> & key=admin' onclick="return confirm('Are you sure you want to change this item?');"><?php echo $admin?></a></td>

    <?php
    }?>                      

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>