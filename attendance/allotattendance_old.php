<?php
$employeeList = Array();
global $month_start;
global $month_end;

$month_end = $year."-".$month."-25";
$stemp = strtotime($month_end) - (60*60*24*26);
$stempmonth = date("m",$stemp);
$stempYear = date("Y",$stemp);
$month_start = $stempYear."-".$stempmonth."-26";
$employee = Array();

$getEmployees = mysqli_query($con,"SELECT * FROM `employee` WHERE `active` = '1'") or die(mysqli_error($con));
while($rowEmployees = mysqli_fetch_array($getEmployees))
{
$employee[$rowEmployees['empid']] = $rowEmployees['id'];
$employeeShift[$rowEmployees['empid']] = $rowEmployees['shift'];
}

$shifts = Array();
$shiftEnd = Array();
$shiftlength = Array();
$getShifts = mysqli_query($con,"SELECT * FROM `shift`") or die(mysqli_error($con));
while($rowShifts = mysqli_fetch_array($getShifts))
{
$shift[$rowShifts['id']] = $rowShifts['starttime'];
$shiftlength[$rowShifts['id']] = $rowShifts['hours'];
$shiftEnd[$rowShifts['id']]= $rowShifts['endtime'];
}


$calender = Array();
$getCalender = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$month_start' AND '$month_end'") or die(mysqli_error($con));
while($rowCalender = mysqli_fetch_array($getCalender))
{
	if($rowCalender['type'] == '1')
	{
		$calender[$rowCalender['date']] = 'holiday';
	}
	else
	{
		$calender[$rowCalender['date']] = $rowCalender['from']."--".$rowCalender['to']."--".$rowCalender['hours'];
	}

}

$is_absent_on = Array();
$is_also_absent_on = Array();
$take_leave_on = Array();

$dateString = '';
$filepath = "../".$post[2];
$file = fopen($filepath,"r");
$k=0;
$thisEmpId= '0';
$valueString = '';
$thisfifteen = '0';
$oldempid = '0';
$fifteen = '0';
$oldlate = '0';
$conlate = '0';

$spclShift = Array();



while(! feof($file))
  {
  $data = fgetcsv($file);
	  if($data[0] == 'User')
	  {
	  	if($employee[$data[2]])
	  	{
	  		if($oldempid != $data[2])
	  		{
	  			$fifteen = '0';
				$conlate = '0';
	  		}
	  		$oldempid = $data[2];
	  		if($thisEmpId != '0' && $valueString != '')
	  		{
	  			$dateString = substr($dateString,0,-1);

	  			mysqli_query($con,"DELETE FROM `$tableName` WHERE `empid` = '$empid' AND `date` IN (".$dateString.")") or die(mysqli_error($con));
	  			$valueString  = substr($valueString,0,-1);
	  			mysqli_query($con,"INSERT INTO `$tableName` (`id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend`) VALUES ".$valueString) or die(mysqli_error($con));
	  			$valueString = '';
	  			$dateString = '';
	  		}
	  		$thisEmpId = $data[2];
			$empid = $employee[$data[2]];
			$empname = $data[5];  	
			$employeeList[] .= $empid;
			$spclShift = Array();
			$getSpclShift = mysqli_query($con,"SELECT * FROM `shiftallot` WHERE `empid` = '$empid'") OR die(mysqli_error($con));
			while($rowSpcl = mysqli_fetch_array($getSpclShift))
			{
				$dts = $rowSpcl['dates'];
				$thisSpclShift = $rowSpcl['shiftid'];
				$dts = explode(",",$dts);
				foreach($dts as $tvldt)
				{
					$spclShift[$tvldt] = 	$thisSpclShift;			
				}
	
			}
	  	}
	  	else
	  	{
			$empid = '0';
			$empname = '0'; 	
			$thisEmpId = '0';  		
	  	}
	  }
	  else if($data[1] != 'Shift' && $data[1] != 'hr' && $data[1] != '' && $empid != '0')
	  {
	  	$atdate = $data[0];
	  	$atdate = convertdate($atdate);
	  	$in = $data[2];
	  	if($data[8] != '')
	  	{
	  		$out = $data[8];
	  	}
	  	else if($data[6] != '')
	  	{
	  		$out = $data[6];
	  	}
	  	else
	  	{
	  		$out = $data[4];
	  	}
	  	$out;
	  	if($in != '' && $out != '' && date("D",strtotime($atdate)) != 'Sun')
	  	{
	  		$status = '1';
	  		if($calender[$atdate])
	  		{
	  			if($calender[$atdate] == 'holiday')
	  			{
	  				$status = '2';
	  			}
	  			else
	  			{
	  				$x = explode("--",$calender[$atdate]);
	  				$from = $x[0];
				  	$total_hours = $x[2];		
	  			}

	  		}
	  		else
	  		{
	  			if(!$spclShift[$atdate])
	  			{
				  	$from = $shift[$employeeShift[$thisEmpId]];
				  	$total_hours = $shiftlength[$employeeShift[$thisEmpId]];
				  	$thisshiftEnd = $shiftEnd[$employeeShift[$thisEmpId]];	  				
	  			}
	  			else
	  			{
				  	$from = $shift[$spclShift[$atdate]];
				  	$total_hours = $shiftlength[$spclShift[$atdate]];	  				
				  	$thisshiftEnd = $shiftEnd[$spclShift[$atdate]];
	  			}

	  			if($atdate == '2017-08-07')
	  			{
				  	$total_hours = $total_hours-2; 		
				  	$rakhi = "2017-08-07 ".$thisshiftEnd;
				  	$rakhi = strtotime($rakhi) - (60*60*2);
				  	$thisshiftEnd = date("H:i:s",$rakhi);
	  			}

	  		}
	  	$total_mins = $total_hours * 60;
	  	
	  	$late = lateminutes($from,$in);
	  	$temp = shifthours($in,$out,$total_hours);
	  	$temp = explode(":::",$temp);
	  	$workinghours = round($temp[1]/60,2);
	  	$shiftdifference = $temp[0];

	  	if($late > 5)
	  	{

	  			$conlate++;
	  	}
	  	else
	  	{
	  		$conlate = '0';
	  	}
	  	$oldlate = $late;
	  	if($late > 5 && $late <= 15 && $late > 0)
	  	{
	  		$fifteen++;
	  		$thisfifteen  = $fifteen;
	  	}
	  	else
	  	{
	  		$thisfifteen = '0';
	  	}


	  	}
	  	else
	  	{
	  	$conlate = '0';
	  	$late = 'NA';
	  	$workinghours = 'NA';
	  	$shiftdifference = 'NA';
	  	if(date("D",strtotime($atdate)) == 'Sun')
	  	{
			$status = '2';	  		
	  	}
	  	else if($calender[$atdate] == 'holiday')
	  	{
	  				$status = '2';
	  	}
	  	else
	  	{
			$status = '0';	
					
	  	}

	  	$thisfifteen = '0';
	  	$total_mins = '0';

	  	}
	  	$valueString .= "('', '$empid', '$atdate', '$in', '$out', '$workinghours', '$shiftdifference', '$late', '$status', '0', '$thisfifteen', '', '', '$datetime', '$total_mins','$conlate','$thisshiftEnd'),";
	  	$dateString .= "'".$atdate."',";

	  }
	  $k++;
  }
  				if($valueString != '')
  				{
	  			$dateString = substr($dateString,0,-1);
	  			mysqli_query($con,"DELETE FROM `$tableName` WHERE `empid` = '$empid' AND `date` IN (".$dateString.")") or die(mysqli_error($con));

	  				$valueString  = substr($valueString,0,-1);
	  	  			mysqli_query($con,"INSERT INTO `$tableName` (`id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend`) VALUES ".$valueString) or die(mysqli_error($con));
		  			$valueString = '';  					
  				}


fclose($file);
$employeeList = array_unique($employeeList);

foreach($employeeList as $emp)
{
	$delStr .= $emp.",";
	$insertStr .= "('', '$month', '$year', '$emp', '$datetime', '$loggeduserid'),";
}
$delStr = substr($delStr,0,-1);
$insertStr = substr($insertStr,0,-1);

mysqli_query($con,"DELETE FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' AND `empid` IN (".$delStr.")") or die(mysqli_error($con));


mysqli_query($con,"INSERT INTO `attendancelog` (`id`, `month`, `year`, `empid`, `createdate`, `updatedby`) VALUES ".$insertStr) or die(mysqli_error($con));


function convertdate($atdate)
{
	$temp =explode("/",$atdate);
	if(count($temp) > 1)
	{
	$atdate = explode("/",$atdate);
	return $atdate[2]."-".$atdate[1]."-".$atdate[0];		
	}
	else
	{
			$atdate = explode("-",$atdate);
			return '20'.$atdate[2]."-".$atdate[1]."-".$atdate[0];

	}
}


function lateminutes($from,$in)
{
	$from = "2017-01-01 ".$from;
	$in = "2017-01-01 ".$in.":00";
	$fromtime = strtotime($from);
	$intime = strtotime($in);
	$difference= $intime - $fromtime;
	$mindif = round($difference/60);
	return $mindif;
}

function shifthours($in,$out,$total_hours)
{
		$orgin = $in;
$orgout = $out;
	$in = "2017-01-01 ".$in.":00";
	$out = "2017-01-01 ".$out.":00";

		$outtime = strtotime($out);
		$intime = strtotime($in);
	$temp1 = explode(":",$orgin);

	$temp = explode(":",$orgout);
	if($temp[0] < 7)
	{
		$temp_tweleve_one = strtotime('2017-01-01 00:00:00');
		$temp_tweleve_two = strtotime('2017-01-02 00:00:00');

		$diff_one = $temp_tweleve_two - $intime;
		$diff_two = $outtime - $temp_tweleve_one;
		$difference = $diff_one+$diff_two;
	}
	else
	{
	
		$difference= $outtime - $intime;
	}


	$mindif = round($difference/60);

	$hourdifference = ($total_hours * 60) - $mindif;

	return $hourdifference.":::".$mindif;	
}


include("sandwich.php");

?>