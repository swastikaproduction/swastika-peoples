<?php
include("../include/config.php");
$type = $_GET['type'];
$month = $_GET['month'];

$year = $_GET['year'];


$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
}

$name = "bookingexport_".$month."_".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>

<table border="1">

<tr>
	<th>Employee Name</th>
	<th>Employee Code</th>
	<th>Employee Type</th>
	<th>Gross Salary</th>
	<th>Leave Deductions</th>
	<th>Hold Deductions</th>
	<th>Calculated Salary</th>
	<th>Basic</th>
	<th>Da</th>
	<th>Hra</th>
	<th>Bonus</th>
	<th>Refreshment</th>
	<th>Pf Contribution Employee</th>
	<th>Total Pf Payable</th>
	<th>Professional Tax</th>
	<th>Loan/Advances</th>
	<th>Netpay</th>

</tr>

<?php
$month = "0".$month;
if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}

while($row= mysqli_fetch_array($getData))
{


//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;

if($row[7] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}




if($pfflag == '1')
{

		if($calculatedsalary < 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = round($calculatedsalary * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		if($calculatedsalary < 8960 && $calculatedsalary >= 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = 1920;				
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		if($calculatedsalary >= 8960 && $calculatedsalary <= 10000)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 10000 && $calculatedsalary <= 13350)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = 500;
			$hra = $calculatedsalary - ($basic+$pfemployer+$bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary > 13350 && $calculatedsalary <= 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

						if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			$bonus = round($calculatedsalary * 0.05);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
		else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}



			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 8000)
	{
		
			$basic = $calculatedsalary;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

	}

	else if($calculatedsalary >= 8000 && $calculatedsalary < 9000)
	{
			$basic = 8000;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

	else if($calculatedsalary >= 9000 && $calculatedsalary <= 15000)
	{
			$basic = 8000;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.10);
			$hra = $calculatedsalary - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000)
	{
			$basic = round($calculatedsalary * 0.4);
			$pfpayable = 0;
			$da = round($basic * 0.35);
			$hra = round(($basic + $da) * 0.4);
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.15);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}
}

?>

<tr>
	<td><?php echo $row[0];?></td>
	<td><?php echo $row[1];?></td>
	<td>
		
		<?php
if($row[8] == '4')
{
	echo "Housekeeping";
}
else 
{
	echo $row[7];
}


		?>

	</td>
	<td><?php echo $row[2];?></td>
	<td><?php echo $leavedeductions;?></td>
	<td><?php echo $hold;?></td>
	<td><?php echo $calculatedsalary;?></td>
	<td><?php echo $basic;?></td>
	<td><?php echo $da;?></td>
	<td><?php echo $hra;?></td>
	<td><?php echo $bonus;?></td>
	<td><?php echo $refreshment;?></td>
	<td><?php echo $pfemployer;?></td>
	<td><?php echo $pfpayable;?></td>
	<td><?php echo $pt;?></td>
	<td><?php echo $loan;?></td>
	<td><?php echo $netpay-$loan;?></td>
</tr>

<?php
}
?>
</table>

