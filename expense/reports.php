<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active">Expense History</li>
</ol>
</section>
<section>
   <div class="row">
      <div class="col-md-2">
        <label>Status</label>
        <select id="expensestatus" name="expensestatus" class="form-control">
          <option value="">select status</option>
          <option value="0">Pending</option>
          <option value="1">Approved</option>
          <option value="2">Rejected</option>
          <option value="3">Paid</option>
        </select>
      </div>
      <div class="col-md-2">
        <label>From Date</label>
        <input type="date" name="fromdate" id="fromdate" class="form-control">
      </div>
      <div class="col-md-2">
        <label>To Date</label>
        <input type="date" name="todate" id="todate" class="form-control">
      </div>
      <div class="col-md-2">
       <!--  <input type="submit" class="btn btn-success" name="submit" value="Submit" style="margin-top:25px; " onclick="GetExpenseFilterData('<?php echo base64_decode($_GET['empid']);?>')"> -->
      </div>
    </div>
    <div style="margin-top: 30px;">
      <div class="row"> 
      <div class="col-md-2">
        <button class="btn btn-primary"  onclick="window.open('expense/booking-export.php?expensestatus='+$('#expensestatus').val()+'&fromdate='+$('#fromdate').val()+'&todate='+$('#todate').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;DETAILED REPORT</button>

 

      </div>
      <div class="col-md-2">
        <button class="btn btn-primary"  onclick="window.open('expense/tally-export.php?expensestatus='+$('#expensestatus').val()+'&fromdate='+$('#fromdate').val()+'&todate='+$('#todate').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;TALLY REPORT</button>
      </div>
      <div class="col-md-2">
        <button class="btn btn-primary"  onclick="window.open('expense/bank-export.php?expensestatus='+$('#expensestatus').val()+'&fromdate='+$('#fromdate').val()+'&todate='+$('#todate').val(),'_blank');"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;BANK REPORT</button> 
      </div>
    </div>
    </div>
    
</section>


