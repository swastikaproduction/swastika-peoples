 <div class="content-wrapper " style="background:#fff;height: 70%;" >
    <!-- Content Header (Page header) -->
   

     <section style="background: white">

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-2">
          <label>Department Name</label>
          <select class="form-control" id="departmentName">
            <option value="">select department</option>
            <?php 
            include'../include/config.php';
            $query = mysqli_query($con,"select distinct(name),id from departments where name !='None' order by name asc");

            while($row=mysqli_fetch_array($query)){
              ?>

            <option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
          <?php }
          ?>
          </select>
        </div>
        <div class="col-md-2">
          <label>Staus</label>
          <select class="form-control" id="status">
            <option value="">select status</option>
            <option value="false">false</option>
            <option value="submitted">submitted</option>
            <option value="save">save</option>
          </select>          
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">

        <button class="btn btn-success" style="margin-top: 13px;margin-bottom: 15px;" onclick="getGoalsUsers();">Submit</button>
      </div>        
      </div>
      
    </section>
       <div id="appraisal_users"></div>
    <div id="appUser">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <?PHP          
            $app_id = $_GET['app_id'];
            //check if the starting row variable was passed in the URL or not
            if (!isset($_GET['startrow']) or !is_numeric($_GET['startrow'])) {
            //we give the value of the starting row to 0 because nothing was found in URL
            $startrow = 0;
            //otherwise we take the value from the URL
            } else {
            $startrow = (int)$_GET['startrow'];
            }
            //this part goes after the checking of the $_GET var
            $fetch = mysqli_query($con,"SELECT a.id as appId ,e.mobile,e.id,e.username,e.name,a.created_at ,a.appraised_mode,a.appraised_time,a.appraised_year,au.issubmitted FROM appraisal_users au INNER JOIN appraisals a ON a.id=au.app_id INNER JOIN employee e ON e.id = au.emp_id WHERE a.id ='$app_id' LIMIT $startrow, 10")or
            die(mysqli_error());
            $num=Mysqli_num_rows($fetch);
          if($num>0)
          {?>
        <div class="box-body">
       <table border='2px;' class="table table-bordered table-striped">
        <thead style="background: #81b7ea;color: white;">
        <tr><td>S.NO</td><td>Name</td><td>Mobile</td><td>Appraisal Time</td><td>Create Date</td><td>Status</td><td>Action</td></tr></thead>
        <?php
         $count =1;

        for($i=0;$i<$num;$i++)
        {
        $row=mysqli_fetch_array($fetch);
        $appId = $row['appId'];
        ?>
        <tr>
          <td><?php echo $count++;?></td>
          <td><?php echo $row['name']?></td> 
          <td><?php echo $row['mobile']?></td>          
          <td><?php echo $row['appraised_time'] ?> &nbsp;&nbsp;<?php echo $row['appraised_year'] ?></td>
          <td><?php echo $row['created_at']?></td>
          <td><span class="label label-warning"><?php echo $row['issubmitted']?></span></td>
          <?php if(!$row['issubmitted'] =='false' && $row['issubmitted']=='submitted'){?>
         <td><span class="label label-success">Completed</span></td>
         <?php
       }else{
        ?><td><span class="label label-success" onclick="sendEmailEmployee('<?php echo $row['id']?>');">Send Reminder</span></td>
        <?php
      }?>

        </tr>
        <?php
        }//for
        ?>
      </table>
 
        <?php
        }
//now this is the link..
echo '<a href="default.php#&rtl=appraisal/goal-users.php?startrow='.($startrow+10).'&app_id='.$appId.'&rtl=formDiv&rtl=tableDiv&rtl=loading"><span class="label label-success">Next</a></span>';

$prev = $_GET['startrow'] - 10;

//only print a "Previous" link if a "Next" was clicked
if ($prev >= 0)
    echo '<a href="default.php#&rtl=appraisal/goal-users.php?startrow='.$prev.'&app_id='.$appId.'&rtl=formDiv&rtl=tableDiv&rtl=loading"><span class="label label-success" style="margin-left: 14px;">Previous</a></span>';
?>
       
    </section>

  </div>
    

  
  