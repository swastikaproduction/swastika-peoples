<?php
$tableName = 'attendance_'.$month."_".$year;

global $month_start;
global $month_end;

$month_end = $year."-".$month."-25";
$stemp = strtotime($month_end) - (60*60*24*26);
$stempmonth = date("m",$stemp);
$stempYear = date("Y",$stemp);
$month_start = $stempYear."-".$stempmonth."-26";


$employee = Array();

$empBranch = Array();
$getEmployees = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
while($rowEmployees = mysqli_fetch_array($getEmployees))
{
$employee[$rowEmployees['empid']] = $rowEmployees['id'];
$employeeShift[$rowEmployees['empid']] = $rowEmployees['shift'];
$currentShift = $rowEmployees['shift'];
$thisEmpId= $rowEmployees['empid'];
$empBranch[$rowEmployees['empid']]  = $rowEmployees['branch'];
}

$shifts = Array();
$shiftlength = Array();
$shiftEnd = Array();
$getShifts = mysqli_query($con,"SELECT * FROM `shift`") or die(mysqli_error($con));
while($rowShifts = mysqli_fetch_array($getShifts))
{
$shift[$rowShifts['id']] = $rowShifts['starttime'];
$shiftlength[$rowShifts['id']] = $rowShifts['hours'];
$shiftEnd[$rowShifts['id']]= $rowShifts['endtime'];
}



$calender = Array();
$calDate = Array();
$branchState = Array();
$p=0;
$getCalender = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$month_start' AND '$month_end'") or die(mysqli_error($con));
while($rowCalender = mysqli_fetch_array($getCalender))
{
	$calDate[$p] = $rowCalender['date'];
	if($rowCalender['type'] == '1')
	{
		
		$calender[$p] = 'holiday';
		$branchState[$p] = explode(",",$rowCalender['branch']);
	}
	else
	{
		$calender[$p] = $rowCalender['from']."--".$rowCalender['to']."--".$rowCalender['hours'];
		$branchState[$p] = explode(",",$rowCalender['branch']);
	}
$p++;
}


$is_absent_on = Array();
$is_also_absent_on = Array();
$take_leave_on = Array();


$dateString = '';
$k=0;

$valueString = '';
$thisfifteen = '0';
$oldempid = '0';
$fifteen = '0';
$oldlate = '0';
$conlate = '0';

$spclShift = Array();
			$getSpclShift = mysqli_query($con,"SELECT * FROM `shiftallot` WHERE `empid` = '$empid'") OR die(mysqli_error($con));
			while($rowSpcl = mysqli_fetch_array($getSpclShift))
			{
				$dts = $rowSpcl['dates'];
				$thisSpclShift = $rowSpcl['shiftid'];
				$dts = explode(",",$dts);
				
				foreach($dts as $tvldt)
				{
					$spclShift[$tvldt] = 	$thisSpclShift;			
				}
	
			}




$getData = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid'  ORDER BY `date` ASC") or die(mysqli_error($con));


while($row = mysqli_fetch_array($getData))
  {
  	$keycheck=Array();
	 	$thisAtId = $row['id'];
	  	 $atdate = $row['date'];
	  	 $in = $row['intime'];
	  	$out = $row['outime'];	  	
	  	if((date("D",strtotime($atdate)) != 'Sun') && ($in != '00:00:00') && ($out != '00:00:00'))
	  	{
	  		$status = '1';
if($spclShift[$atdate])
	  			{
				  	$from = $shift[$spclShift[$atdate]];
				  	$total_hours = $shiftlength[$spclShift[$atdate]];	  				
				  	$thisshiftEnd = $shiftEnd[$spclShift[$atdate]];
	  			}
	  		else if(in_array($atdate,$calDate))
	  		{
	  			foreach($calDate as $key => $val)
	  			{
	  				if($val == $atdate)
	  				{
	  					$keycheck[] .= $key;
	  				}
	  			}
	  			foreach($keycheck as $chk)
	  			{
	  				if(in_array($empBranch[$thisEmpId],$branchState[$chk]))
	  				{
	  					if($calender[$chk] == 'holiday')
			  			{
			  				$status = '2';
			  			}
			  			else
			  			{
			  				$x = explode("--",$calender[$chk]);
			  				$from = $x[0];
						  	$total_hours = $x[2];
						  	$thisshiftEnd = $x[1];	  						
			  			}
	  				}
	  			}

	  			

	  		}
	  		else
	  		{
				  	$from = $shift[$employeeShift[$thisEmpId]];
				  	$total_hours = $shiftlength[$employeeShift[$thisEmpId]];
				  	$thisshiftEnd = $shiftEnd[$employeeShift[$thisEmpId]];	  				
	  		}
	  	$total_mins = $total_hours * 60;
	  	
	  	$late = lateminutes($from,$in);
	  	$temp = shifthours($in,$out,$total_hours);
	  	$temp = explode(":::",$temp);
	  	$workinghours = round($temp[1]/60,2);
	  	$shiftdifference = $temp[0];

	  	if($late > 15)
	  	{

	  			$conlate++;
	  	}
	  	else
	  	{
	  		$conlate = '0';
	  	}
	  	$oldlate = $late;
	  	if($late > 15 && $late <= 30 && $late > 0)
	  	{
	  		$fifteen++;
	  		$thisfifteen  = $fifteen;
	  		
	  	}
	  	else
	  	{
	  		$thisfifteen = '0';
	  	}


	  	}
	  	else
	  	{
		  	$conlate = '0';
		  	$late = 'NA';
		  	$workinghours = 'NA';
		  	$shiftdifference = 'NA';
		  	if(date("D",strtotime($atdate)) == 'Sun')
		  	{
				$status = '2';	  		
		  	}
		  	else if(in_array($atdate,$calDate))
	  		{
	  			foreach($calDate as $key => $val)
	  			{
	  				if($val == $atdate)
	  				{
	  					$keycheck[] .= $key;
	  				}
	  			}
	  			foreach($keycheck as $chk)
	  			{
	  				if(in_array($empBranch[$thisEmpId],$branchState[$chk]))
	  				{
	  					if($calender[$chk] == 'holiday')
			  			{
			  				$status = '2';
			  			}
			  			else
			  			{
			  				$status = '0';
			  			}
	  				}
	  			}
                if(($in != '00:00:00') || ($out !='00:00:00')){
                  $status = '3';
		  		}
					

	  		}
		  	else
		  	{
				if(($in != '00:00:00') || ($out != '00:00:00')){
                  $status = '3';
		  		}else{
                 $status = '0';
		  		}
						
		  	}

	  	$thisfifteen = '0';
	  	$total_mins = '0';

	  	}
	  	mysqli_query($con,"UPDATE `$tableName` SET `hours` = '$workinghours', `allotedshifthours` = '$total_mins',`late` = '$late',`status` = '$status',`consecutive_counter` = '$conlate',`fifteenminscounter`= '$thisfifteen', `difference` = '$shiftdifference',`shiftend` = '$thisshiftEnd' WHERE `id` = '$thisAtId'");

	  
	  
  }
  





function lateminutes($from,$in)
{
	$from = "2017-01-01 ".$from;
	$in = "2017-01-01 ".$in;
	$fromtime = strtotime($from);
	$intime = strtotime($in);
	$difference= $intime - $fromtime;
	$mindif = round($difference/60);
	return $mindif;
}

function shifthours($in,$out,$total_hours)
{
		$orgin = $in;
$orgout = $out;
	$in = "2017-01-01 ".$in;
	$out = "2017-01-01 ".$out;

		$outtime = strtotime($out);
		$intime = strtotime($in);
//	$temp1 = explode(":",$orgin);

//	$temp = explode(":",$orgout);
if(($outtime - $intime) < 0)
	
	{
		$temp_tweleve_one = strtotime('2017-01-01 00:00:00');
		$temp_tweleve_two = strtotime('2017-01-02 00:00:00');

		$diff_one = $temp_tweleve_two - $intime;
		$diff_two = $outtime - $temp_tweleve_one;
		$difference = $diff_one+$diff_two;
	}
	else
	{
	
		$difference= $outtime - $intime;
	}


	$mindif = round($difference/60);

	$hourdifference = ($total_hours * 60) - $mindif;

	return $hourdifference.":::".$mindif;	
}



?>