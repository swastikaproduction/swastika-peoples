<?php

include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT * FROM `employee` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>
<div class="moduleHead">
<br/>
<div style="float:right">		
<div class="btn-group">
		<button class="btn btn-primary" style="border-right:1px #3987d0 solid !important" onclick="getModal('bankdetails/edit.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')">
        <i class="fa fa-file"></i>&nbsp;&nbsp;
		BANK DETAILS</button>&nbsp;&nbsp;

		<button class="btn btn-primary" style="border-right:1px #3987d0 solid !important"    onclick="getModal('documents/index.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')">
<i class="fa fa-file"></i>&nbsp;&nbsp;
		SUBMITTED DOCUMENTS</button>
		<button id="compdocs" class="btn btn-primary" style="border-right:1px #3987d0 solid !important"    onclick="getModal('employee/documents.php?id=<?php echo $id;?>','tableModalBig','formModalBig','loading')">
<i class="fa fa-file"></i>&nbsp;&nbsp;
		COMPANY DOCUMENTS</button>

		<button class="btn btn-primary" style="border-left:1px #3987d0 solid !important"  onclick="getModal('employee/diffshift.php?id=<?php echo $id;?>','tableModalBig','formModalBig','loading')">
<i class="fa fa-random"></i>&nbsp;&nbsp;
		TEMPORARY SHIFT</button>

		</div>
</div>
	<div class="moduleHeading">



<span onclick="toogleFormTable();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Employees
</span>
	</div>
</div>



<div class="shadow" style="background:#fff">



<div class="row" >

<div class="col-sm-12 subHead">

<div style="float:right">

	<?php if(in_array('emp_f_and_f_calculation',$permissions) && $row['left'] == 1) { ?>
	<span onclick="getModule('bankdetails/fnfCalculation.php?id=<?php echo $id;?>&type=employee','formDiv','tableDiv','loading')" class="label label-info">FnF Calculation</span>
	<?php } ?>

	<?php if(in_array('emp_increment_letter',$permissions)) { ?>
	<span onclick="getModal('bankdetails/increment_letter_modal.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')" class="label label-info">INCREMENT LETTER</span>
	<?php } ?>

	<?php if(in_array('emp_promotion_letter',$permissions)) { ?>
	<span onclick="getModal('bankdetails/promotion_letter_modal.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')" class="label label-info">PROMOTION LETTER</span>
	<?php } ?>

	<?php if(in_array('emp_appointment_letter',$permissions)) { ?>
	<span onclick="getModal('bankdetails/appointment_letter_modal.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')" class="label label-info">APPOINTMENT LETTER</span>
	<?php } ?>

	<?php if(in_array('emp_relieving_letter',$permissions)) { ?>
	<span onclick="GenerateRelievingLetter('<?php echo $id;?>')" class="label label-info">RELIEVING LETTER</span>
	<?php } ?>

	<?php if(in_array('emp_experience_letter',$permissions)) { ?>
	<span onclick="GenerateExperienceLetter('<?php echo $id;?>')" class="label label-info">EXPERIENCE LETTER</span>
	<?php } ?>

	<?php if(in_array('emp_confirmation_letter',$permissions)) { ?>
	<span onclick="getModal('bankdetails/confirmation_letter_modal.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')" class="label label-info">CONFIRMATION LETTER</span>
	<?php } ?>

			<?php
			if($row['empstatuslive'] == '' || $row['empstatuslive'] == 'CONFIRMED')
			{
				?>
<span class="label label-success">CONFIRMED EMPLOYEE</span>
				<?php
			}
			else
			{
				?>
<span class="label label-warning">EMPLOYEE ON PROBATION</span>
				<?php
			}
			?>
			&nbsp;&nbsp;
			<?php
				$doj = $row['doj'];
				$tt = strtotime($doj);
				$totalseconds = time()-$tt;
				$totaldays = round($totalseconds/(3600 * 24));
				if($totaldays > 31 && $totaldays < 365)
				{
					$exp = round(($totaldays/30),1);
					$exp = "~".$exp." months";
				}
				if($totaldays < 31)
				{
					$exp = $totaldays." days";
				}
				if($totaldays >= 365)
				{
					$exp = round(($totaldays/365),1);
					$exp = "~".$exp." years";
				}


			?>
<span class="label label-default">Total experience: <?php echo $exp;?> </span>
		</div>
Employee Details

</div>

	<div class="col-sm-2 formLeft req">
		Name
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_name',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" name="req" title="" id="inp0" class="inputBox" value="<?php echo $row['name'];?>" >
	<?php } elseif(in_array('emp_name',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp0" class="inputBox" value="<?php echo $row['name'];?>" readonly>
	<?php } else { ?>
		<input type="text" style="display:none" name="req" title="" id="inp0" class="inputBox" value="<?php echo $row['name'];?>" readonly>
	<?php } ?>	
	</div>	

    <div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft req">
		Mobile
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_mobile',$permissions) && in_array('emp_update',$permissions)) { ?>
     		<input type="text" name="req" title="" id="inp1" class="inputBox" value="<?php echo $row['mobile'];?>" >
	<?php } elseif(in_array('emp_mobile',$permissions)) { ?>
			<input type="text" name="req" title="" id="inp1" class="inputBox" value="<?php echo $row['mobile'];?>" readonly>
	<?php } else{ ?>
			<input type="text" style="display:none" name="req" title="" id="inp1" class="inputBox" value="<?php echo $row['mobile'];?>" readonly>
    <?php } ?>
    </div>	

</div>



<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Personal Email
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_peremail',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="email" name="req" title="" id="inp2" class="inputBox"  value="<?php echo $row['email'];?>" >
	<?php } elseif(in_array('emp_peremail',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp2" class="inputBox"  value="<?php echo $row['email'];?>" readonly>
	<?php } else { ?>
		<input type="text" style="display:none" name="req" title="" id="inp2" class="inputBox"  value="<?php echo $row['email'];?>" readonly>
	<?php } ?>
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
	Work Email
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_workemail',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="email" name="req" title="" id="inp3" class="inputBox"  value="<?php echo $row['workemail'];?>" > 
    <?php } elseif(in_array('emp_workemail',$permissions)) { ?>
	<input type="email" name="req" title="" id="inp3" class="inputBox"  value="<?php echo $row['workemail'];?>" readonly> 
    <?php } else { ?>
	<input type="text" style="display:none" name="req" title="" id="inp3" class="inputBox"  value="<?php echo $row['workemail'];?>" readonly> 
    <?php } ?>
	</div>	
    </div>

    <div class="row" >
		<div class="col-sm-2 formLeft req">
			Emergency Contact No.
		</div>
		<div class="col-sm-10 formRight">
			<input type="text" name="emergency_contact" value="<?php echo $row['emergency_contact_no'];?>" title="" id="inp46" class="inputBox" >
		</div>	

	</div>


<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Employee Code
	</div>
	<div class="col-sm-10 formRight">
	<?php if(in_array('emp_code',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp4" class="inputBox"  value="<?php echo $row['empid'];?>" > 
	<?php } elseif(in_array('emp_code',$permissions)) {  ?>
		<input type="text" name="req" title="" id="inp4" class="inputBox"  value="<?php echo $row['empid'];?>" readonly> 
    <?php } else {?>
		<input type="text" style="display:none" name="req" title="" id="inp4" class="inputBox"  value="<?php echo $row['empid'];?>" readonly> 
    <?php } ?>	
	</div>	
</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Department
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_department',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp5" name='req'>
		<?php	$data = getData('departments','*','name','ASC');
		foreach($data as $row1){?>
		<option <?php if($row['department'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
	<?php } elseif(in_array('emp_department',$permissions)) {  ?>
	<select class="inputBox" id="inp5" name='req' disabled="true">
		<?php	$data = getData('departments','*','name','ASC');
		foreach($data as $row1){?>
		<option <?php if($row['department'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
    <?php } else {?>
	<select class="inputBox" id="inp5" name='req' style="display:none;" disabled="true">
		<?php	$data = getData('departments','*','name','ASC');
		foreach($data as $row1){?>
		<option <?php if($row['department'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
    <?php } ?>	
	
	</div>	
<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft req">
		Designation
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_designation',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp6" name="req">
		<option>-- Select Designation -- </option>
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option <?php if($row['designation'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	
	<?php } elseif(in_array('emp_designation',$permissions)) {  ?>
	<select class="inputBox" id="inp6" name="req" disabled="true">
		<option>-- Select Designation -- </option>
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option <?php if($row['designation'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	
	<?php } else {?>
     <select class="inputBox" id="inp6" name="req" style="display:none;" disabled="true">
		<option>-- Select Designation -- </option>
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option <?php if($row['designation'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
  	</select>
	<?php } ?>	

	</div>	

</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Date Of Joining 
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_doj',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="date" name="req" title="" id="inp7" class="inputBox"  value="<?php echo $row['doj'];?>" >
	<?php } elseif(in_array('emp_doj',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp7" class="inputBox"  value="<?php echo $row['doj'];?>" readonly>
	<?php } else {	?>
		<input type="text" style="display:none" name="req" title="" id="inp7" class="inputBox"  value="<?php echo $row['doj'];?>" readonly>
	<?php } ?>
	</div>	

    <div class="col-sm-1"></div>


	<div class="col-sm-1 formLeft req">
		Branch
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_branch',$permissions) && in_array('emp_update',$permissions)) { ?>
    <select class="inputBox" id="inp8" name="req">
		<?php
		$data = getData('branch','*','name','ASC');
		foreach($data as $row2){
		?>
		<option <?php if($row['branch'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	
	<?php } elseif(in_array('emp_branch',$permissions)) {  ?>
    <select class="inputBox" id="inp8" name="req" disabled="true">
		<?php
		$data = getData('branch','*','name','ASC');
		foreach($data as $row2){
		?>
		<option <?php if($row['branch'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	
	<?php } else {?>
    <select class="inputBox" id="inp8" name="req" style="display: none" disabled="true">
		<?php
		$data = getData('branch','*','name','ASC');
		foreach($data as $row2){
		?>
		<option <?php if($row['branch'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
    <?php } ?>	

	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Salary
	</div>
	<div class="col-sm-4 formRight">
		<?php if(in_array('employee_salary',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp16" class="inputBox"  value="<?php echo $row['salary'];?>">
		<?php } elseif(in_array('employee_salary',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp16" class="inputBox"  value="<?php echo $row['salary'];?>" readonly>
	<?php } else { ?>
		<input type="text" style="display:none" name="req" title="" id="inp16" class="inputBox"  value="<?php echo $row['salary'];?>" readonly>
	<?php  } ?>
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
	Pool Bal
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('pool_bal',$permissions) && in_array('emp_update',$permissions)) { ?>
    <input type="text" name="req" title="" id="inp17" class="inputBox"  value="<?php echo $row['poolbalance'];?>">
	<?php } elseif(in_array('pool_bal',$permissions)) { ?>
    <input type="text" name="req" title="" id="inp17" class="inputBox"  value="<?php echo $row['poolbalance'];?>" readonly>
    <?php } else { ?>
    <input type="text" style="display:none" name="req" title="" id="inp17" class="inputBox"  value="<?php echo $row['poolbalance'];?>" readonly>
    <?php } ?> 	
	</div>	

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		PF Deduction
	</div>
	<div class="col-sm-2 formRight">
	<?php if(in_array('pf_deduction_flag',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox"  
	 id="inp18"	onchange="if(this.value == '0') {$('#pfyes').hide();} else {$('#pfyes').show();} ">
		<option value="0"  <?php if($row['pf'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pf'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    
	<?php } elseif(in_array('pf_deduction_flag',$permissions)) {  ?>
	<select class="inputBox" id="inp18" disabled="true"	onchange="if(this.value == '0') {$('#pfyes').hide();} else {$('#pfyes').show();} ">
		<option value="0"  <?php if($row['pf'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pf'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    
	<?php } else {?>
	<select class="inputBox" id="inp18" disabled="true" style="display: none;"	onchange="if(this.value == '0') {$('#pfyes').hide();} else {$('#pfyes').show();} ">
		<option value="0"  <?php if($row['pf'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pf'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    <?php } ?>	

	</div>
		<div class="col-sm-2 formLeft req">
		PT Deduction
	</div>
	<div class="col-sm-2 formRight">
	<?php if(in_array('pt_deduction_flag',$permissions) && in_array('emp_update',$permissions)) { ?>
    	<select  class="inputBox" id="inp40">   <option value="0"  <?php if($row['pt'] == '0') echo "selected='selected'";?> >No</option>
		<option value="0"  <?php if($row['pt'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pt'] == '1') echo "selected='selected'";?> >Yes</option>
      	</select>
	<?php } elseif(in_array('pt_deduction_flag',$permissions)) {  ?>
    	<select  class="inputBox" id="inp40" disabled="true">  
    	<option value="0"  <?php if($row['pt'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pt'] == '1') echo "selected='selected'";?> >Yes</option>
      	</select>
	<?php } else {?>
 	<select  class="inputBox" id="inp40" disabled="true" style="display: none;">  
    	<option value="0"  <?php if($row['pt'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['pt'] == '1') echo "selected='selected'";?> >Yes</option>
      	</select>
	<?php } ?>	
	
	</div>
		<div class="col-sm-2 formLeft req">
		Other Deduction
	</div>
	<div class="col-sm-2 formRight">
	<?php if(in_array('od_flag',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp41">
		<option value="0"  <?php if($row['od'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['od'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    <?php } elseif(in_array('od_flag',$permissions)) {  ?>
	<select class="inputBox" id="inp41" disabled="true">
		<option value="0"  <?php if($row['od'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['od'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    <?php } else {?>
	<select class="inputBox" id="inp41" disabled="true" style="display: none;">
		<option value="0"  <?php if($row['od'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['od'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
 	<?php } ?>	
	</div>

	</div>


<div id="pfyes" <?php if($row['pf'] == '0') { echo 'style="display:none"'; }?>>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Default PF<br/>
		<span style="font-size:10px">(Only if specific amount)</span>
	</div>
	<div class="col-sm-10 formRight defpf">
	<?php if(in_array('default_pf',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" class="inputBox" id="inp23" value="<?php echo $row['defpf'];?>" name="">
    <?php } elseif(in_array('default_pf',$permissions)) { ?>
	<input type="text" class="inputBox" id="inp23" value="<?php echo $row['defpf'];?>" name="" readonly>
    <?php } else { ?>
	<input type="text" style="display:none" class="inputBox" id="inp23" value="<?php echo $row['defpf'];?>" name="" readonly>
    <?php } ?> 	
	</div>	

</div>

<div class="row" >
	<div class="col-sm-2 formLeft">
		Name
						<br/>
		<span style="font-size:11px;">
As per aadhar
</span>

	</div>
	<div class="col-sm-10 formRight">
	<?php if(in_array('emp_aadhar_name',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" title="" id="inp31" class="inputBox"  style="border-left:2px #d05419 solid !important;">
    <?php } elseif(in_array('emp_aadhar_name',$permissions)) { ?>
	<input type="text" title="" id="inp31" class="inputBox"  style="border-left:2px #d05419 solid !important;"  readonly>
    <?php } else { ?>
	<input type="text" style="display:none" title="" id="inp31" class="inputBox"  style="border-left:2px #d05419 solid !important;" readonly>
    <?php } ?> 	
	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		Aadhar No.
	</div>
	<div class="col-sm-10 formRight">
    <?php if(in_array('emp_aadhar_number',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" title="" id="inp32" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } elseif(in_array('emp_aadhar_number',$permissions)) { ?>
	<input type="text" title="" id="inp32" class="inputBox" readonly="true" style="border-left:2px #d05419 solid !important;" >
	<?php } else { ?>
	<input type="text" style="display:none" title="" id="inp32" class="inputBox" readonly="true" style="border-left:2px #d05419 solid !important;" >
	<?php } ?> 	

	</div>	
</div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		DOB
	<br/>
	<span style="font-size:11px;">
As per aadhar
</span>

	</div>
	<div class="col-sm-10 formRight">
    <?php if(in_array('emp_aadhar_dob',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" title="" id="inp33" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } elseif(in_array('emp_aadhar_dob',$permissions)) { ?>
	<input type="text" title="" id="inp33" readonly="true" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } else { ?>
	<input type="text" style="display:none" title="" id="inp33" readonly="true" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } ?> 	
	</div>	
    </div>



<div class="row" >
	<div class="col-sm-2 formLeft">
		Father Name
	 <br/>
	 <span style="font-size:11px;">
As per aadhar
</span>
	</div>
	<div class="col-sm-10 formRight">
    <?php if(in_array('emp_aadhar_father',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" title="" id="inp34" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } elseif(in_array('emp_aadhar_father',$permissions)) { ?>
	<input type="text" title="" id="inp34" readonly="true" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } else { ?>
	<input type="text" style="display:none" title="" id="inp34" readonly="true" class="inputBox"  style="border-left:2px #d05419 solid !important;" >
	<?php } ?> 	
	</div>	
</div>


<div class="row" >
	<div class="col-sm-2 formLeft">
		UN Number
	</div>
	<div class="col-sm-10 formRight">
    <?php if(in_array('emp_uan',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" title="" id="inp35" class="inputBox" >
	<?php } elseif(in_array('emp_uan',$permissions)) { ?>
	<input type="text" title="" id="inp35" readonly="true" class="inputBox" >
	<?php } else { ?>
	<input type="text" style="display:none" title="" id="inp35" readonly="true" class="inputBox" >
	<?php } ?> 	
	</div>	
</div>

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Mark as resignation
	</div>
	<div class="col-sm-4 formRight">

	<?php if(in_array('mark_left',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp45" onchange="ToLeftResignationRemark();">
		<option value="0"  <?php if($row['left_resignation_remark'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left_resignation_remark'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    <?php } elseif(in_array('mark_left',$permissions)) {  ?>
	<select class="inputBox" disabled="true" id="inp45" onchange="ToLeftResignationRemark();">
		<option value="0"  <?php if($row['left_resignation_remark'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left_resignation_remark'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>

    <?php } else {?>
	<select class="inputBox" id="inp45" disabled="true" style="display: none;" onchange="ToLeftResignationRemark();">
		<option value="0"  <?php if($row['left_resignation_remark'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left_resignation_remark'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
 	<?php } ?>	
	</div>	

	<div class="col-sm-2 formLeft req">
		Resignation Date
	</div>
	<div class="col-sm-4 formRight">
	<div id="dateleftresignationvalidation">
    <?php if(in_array('emp_designation',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="date"  value="<?php echo $row['resignation_date'];?>" class="inputBox" id="inp44" name="">
	<?php } elseif(in_array('emp_designation',$permissions)) { ?>
		<input type="date" readonly="true" value="<?php echo $row['resignation_date'];?>" class="inputBox" id="inp44" name="">
	<?php } else { ?>
		<input type="text" style="display:none" readonly="true" value="<?php echo $row['resignation_date'];?>" class="inputBox" id="inp44" name="">
	<?php } ?> 	
	</div>
	</div>

</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Mark as left
	</div>
	<div class="col-sm-4 formRight">

	<?php if(in_array('mark_left',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp19" onchange="ToLeftRemark();">
		<option value="0"  <?php if($row['left'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
    <?php } elseif(in_array('mark_left',$permissions)) {  ?>
	<select class="inputBox" disabled="true" id="inp19" onchange="ToLeftRemark();">
		<option value="0"  <?php if($row['left'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>

    <?php } else {?>
	<select class="inputBox" id="inp19" disabled="true" style="display: none;" onchange="ToLeftRemark();">
		<option value="0"  <?php if($row['left'] == '0') echo "selected='selected'";?> >No</option>
		<option value="1"  <?php if($row['left'] == '1') echo "selected='selected'";?> >Yes</option>
	</select>
 	<?php } ?>	



	</div>	

	<div class="col-sm-2 formLeft req">
		Date Left (If any)
	</div>
	<div class="col-sm-4 formRight">
	<div id="dateleftvalidation">
    <?php if(in_array('date_left',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="date"  value="<?php echo $row['leavingdate'];?>" class="inputBox" id="inp37" name="">
	<?php } elseif(in_array('date_left',$permissions)) { ?>
		<input type="date" readonly="true" value="<?php echo $row['leavingdate'];?>" class="inputBox" id="inp37" name="">
	<?php } else { ?>
		<input type="text" style="display:none" readonly="true" value="<?php echo $row['leavingdate'];?>" class="inputBox" id="inp37" name="">
	<?php } ?> 	
	</div>
	</div>	

</div>
<div class="row" id="leftstatusdiv" style="display: none;">
	<div class="col-sm-2 formLeft">
		Left Remark
	</div>
	<div class="col-sm-4 formRight">
	<!-- <input type="text" title=""  value="<?php echo $row['leftremark'];?>" class="inputBox" id="inp39"> -->
	<select id="inp39" class="form-control">
		<option value="Resignation">Resignation</option>
		<option value="Termination">Termination</option>
		<option value="Absconding">Absconding</option>
	</select>

	</div>	


</div>

<div class="row">
	<div class="col-sm-2 formLeft req">
		Shift
	</div>
	<div class="col-sm-10 formRight">
		
	<?php if(in_array('working_shift',$permissions) && in_array('emp_update',$permissions)) { ?>
    <select class="inputBox" id="inp9" name="req">
		<?php $data = getData('shift','*','name','ASC');
		foreach($data as $row2) {	?>
		<option <?php if($row['shift'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
    <?php } elseif(in_array('working_shift',$permissions)) {  ?>
    <select class="inputBox" disabled="true"  id="inp9" name="req">
		<?php $data = getData('shift','*','name','ASC');
		foreach($data as $row2) {	?>
		<option <?php if($row['shift'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>

    <?php } else {?>
    <select class="inputBox" disabled="true" style="display: none" id="inp9" name="req">
		<?php $data = getData('shift','*','name','ASC');
		foreach($data as $row2) {	?>
		<option <?php if($row['shift'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
 	<?php } ?>	



	</div>	


</div>


<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
	Date of Birth
	</div>
	<div class="col-sm-4 formRight">
    <?php if(in_array('emp_dob',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="date" name="req" title="" id="inp11" class="inputBox" value="<?php echo $row['dob'];?>" >
	<?php } elseif(in_array('emp_dob',$permissions)) { ?>
	<input type="date"  readonly="true" name="req" title="" id="inp11" class="inputBox" value="<?php echo $row['dob'];?>" >
	<?php } else { ?>
	<input type="date" style="display:none" readonly="true" name="req" title="" id="inp11" class="inputBox" value="<?php echo $row['dob'];?>" >
	<?php } ?> 	
	</div>	

	<div class="col-sm-2 formLeft req">
		Gender
	</div>
	<div class="col-sm-4 formRight">
		<select name="req" id="inp10" class="inputBox">
			<option value="0">--Please select the gender--</option>
			<option value="Male" <?php if($row['gender'] == 'Male'){ echo "selected='selected'"; }  ?> >Male</option>
			<option value="Female" <?php if($row['gender'] == 'Female'){ echo "selected='selected'"; }  ?> >Female</option>
		</select>
	</div>
</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		 Address
	</div>
    <?php if(in_array('emp_address',$permissions) && in_array('emp_update',$permissions)) { ?>
	<div class="col-sm-10 formRight">
		<textarea class="inputBox" id="inp12" style="width:100%;height:150px;" ><?php echo $row['address'];?></textarea>
	</div>	

	<?php } elseif(in_array('emp_address',$permissions)) { ?>
	<div class="col-sm-10 formRight">
		<textarea class="inputBox" id="inp12" disabled style="width:100%;height:150px;" ><?php echo $row['address'];?></textarea>
	</div>	

	<?php } else { ?>
	<div class="col-sm-10 formRight">
		<textarea class="inputBox" id="inp12" disabled style="width:100%;height:150px;display: none" ><?php echo $row['address'];?></textarea>
	</div>	
	<?php } ?> 	


</div>


<div class="row">
	<div class="col-sm-6">
		<div class="col-sm-2 formLeft req">
		 HOD
	</div>
	<div class="col-sm-10 formRight">
	<?php if(in_array('emp_hod',$permissions) && in_array('emp_update',$permissions)) { ?>
    <select  class="inputBox" id="inp20" name='req'>
		<option value="">select hod</option>
		<?php
        $data = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0' ORDER BY `name` ASC") or die(mysqli_error($con));
		while($datarow = mysqli_fetch_array($data))
		{
		?>
		<option <?php if($row['imdboss'] == $datarow['id']) echo "selected='selected'";?> value="<?php echo $datarow['id']; ?>"><?php echo $datarow['name']." ".$datarow['empid']; ?></option>
		<?php } ?>
	</select>

    <?php } elseif(in_array('emp_hod',$permissions)) {  ?>
    <select disabled="true" class="inputBox" id="inp20" name='req'>
		<option value="">select hod</option>
		<?php
        $data = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0' ORDER BY `name` ASC") or die(mysqli_error($con));
		while($datarow = mysqli_fetch_array($data))
		{
		?>
		<option <?php if($row['imdboss'] == $datarow['id']) echo "selected='selected'";?> value="<?php echo $datarow['id']; ?>"><?php echo $datarow['name']." ".$datarow['empid']; ?></option>
		<?php } ?>
	</select>
    <?php } else {?>
     <select disabled="true" style="display: none" class="inputBox" id="inp20" name='req'>
		<option value="">select hod</option>
		<?php
        $data = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0' ORDER BY `name` ASC") or die(mysqli_error($con));
		while($datarow = mysqli_fetch_array($data))
		{
		?>
		<option <?php if($row['imdboss'] == $datarow['id']) echo "selected='selected'";?> value="<?php echo $datarow['id']; ?>"><?php echo $datarow['name']." ".$datarow['empid']; ?></option>
		<?php } ?>
	</select>

 	<?php } ?>	
	</div>
	</div>
		<div class="col-sm-6">
		<div class="col-sm-2 formLeft req">
		 Big Image
	</div>
	<div class="col-sm-10 formRight">
		
	<?php if(in_array('big_image',$permissions) && in_array('emp_update',$permissions)) { ?>
    <select class="inputBox" id="inp42" name='req'>
		<option value="">select big image status</option>
		<option value="1" <?php if($row['bigimage'] == '1') echo "selected='selected'";?>>YES</option>
		<option value="0" <?php if($row['bigimage'] == '0') echo "selected='selected'";?>>NO</option>
	</select>

    <?php } elseif(in_array('big_image',$permissions)) {  ?>
    <select disabled="true" class="inputBox" id="inp42" name='req'>
		<option value="">select big image status</option>
		<option value="1" <?php if($row['bigimage'] == '1') echo "selected='selected'";?>>YES</option>
		<option value="0" <?php if($row['bigimage'] == '0') echo "selected='selected'";?>>NO</option>
	</select>

    <?php } else {?>
    <select disabled="true" style="display: none" class="inputBox" id="inp42" name='req'>
		<option value="">select big image status</option>
		<option value="1" <?php if($row['bigimage'] == '1') echo "selected='selected'";?>>YES</option>
		<option value="0" <?php if($row['bigimage'] == '0') echo "selected='selected'";?>>NO</option>
	</select>
 	<?php } ?>	
	</div>
</div>




<div class="row" style="background:#f7f8f8">
	<div class="col-sm-2 formLeft req">
		Username
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_username',$permissions) && in_array('emp_update',$permissions)) { ?>
	<input type="text" name="req" title=""  id="inp14" class="inputBox"  value="<?php echo $row['username'];?>" >
	<?php } elseif(in_array('emp_username',$permissions)) { ?>
	<input type="text" readonly="true" name="req" title=""  id="inp14" class="inputBox"  value="<?php echo $row['username'];?>" >
	<?php } else { ?>
	<input type="text" style="display: none" readonly="true" name="req" title=""  id="inp14" class="inputBox"  value="<?php echo $row['username'];?>" >
	<?php } ?> 	
    </div>


	<div class="col-sm-1 formLeft req">
		Password
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_password',$permissions) && in_array('emp_update',$permissions)) { ?>
		<input type="text" name="req" title="" id="inp15" class="inputBox"  value="<?php echo $row['password'];?>" >
	<?php } elseif(in_array('emp_password',$permissions)) { ?>
		<input type="text" readonly="true" name="req" title="" id="inp15" class="inputBox"  value="<?php echo $row['password'];?>" >
	<?php } else { ?>
		<input type="text" style="display: none" readonly="true" name="req" title="" id="inp15" class="inputBox"  value="<?php echo $row['password'];?>" >
	<?php } ?> 	
	</div>	

</div>



<div class="row">
	<div class="col-sm-2 formLeft req">
		Rule Type
		<br/>
		<span style="font-size:11px;">
		(Only for housekeeping Department)
		</span>
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('rule_type',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select  class="inputBox" id="inp21">
	<option <?php if($row['ruletype'] == '') echo "selected='selected'";?> value="">Normal/Regular Rule</option>
		<option <?php if($row['ruletype'] == '9') echo "selected='selected'";?>  value="9">9 Hours Shift Rule</option>
		<option <?php if($row['ruletype'] == 'PA') echo "selected='selected'";?>  value="PA">Only Present & Absent</option>
	</select>

    <?php } elseif(in_array('rule_type',$permissions)) {  ?>
	<select disabled="true" class="inputBox" id="inp21">
	<option <?php if($row['ruletype'] == '') echo "selected='selected'";?> value="">Normal/Regular Rule</option>
		<option <?php if($row['ruletype'] == '9') echo "selected='selected'";?>  value="9">9 Hours Shift Rule</option>
		<option <?php if($row['ruletype'] == 'PA') echo "selected='selected'";?>  value="PA">Only Present & Absent</option>
	</select>

    <?php } else {?>
	<select disabled="true" style="display: none" class="inputBox" id="inp21">
	<option <?php if($row['ruletype'] == '') echo "selected='selected'";?> value="">Normal/Regular Rule</option>
		<option <?php if($row['ruletype'] == '9') echo "selected='selected'";?>  value="9">9 Hours Shift Rule</option>
		<option <?php if($row['ruletype'] == 'PA') echo "selected='selected'";?>  value="PA">Only Present & Absent</option>
	</select>
 	<?php } ?>	

	</div>	
	<div class="col-sm-1 formLeft req">
		Department Type
				<br/>
		<span style="font-size:11px;">
(For hold salary)
</span>
	</div>
	<div class="col-sm-4 formRight">


	<?php if(in_array('depart_type',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select  class="inputBox" id="inp22">
		<option <?php if($row['depttype'] == 'Marketing') echo "selected='selected'";?>  value="Marketing">Marketing</option>
		<option <?php if($row['depttype'] == 'Housekeeping') echo "selected='selected'";?>  value="Housekeeping">Housekeeping</option>
		<option <?php if($row['depttype'] == 'Backoffice') echo "selected='selected'";?>  value="Backoffice">Backoffice</option>
		<option <?php if($row['depttype'] == 'Insurance') echo "selected='selected'";?> value="Insurance">Swastika Insurance</option>
		<option <?php if($row['depttype'] == 'Securities') echo "selected='selected'";?> value="Securities">Swastika Securities</option>
		<option <?php if($row['depttype'] == 'Cash') echo "selected='selected'";?>  value="Cash">Cash</option>
		<option <?php if($row['depttype'] == 'Other') echo "selected='selected'";?>  value="Other">Other</option>
	</select>
    <?php } elseif(in_array('depart_type',$permissions)) {  ?>
	<select disabled="true" class="inputBox" id="inp22">
		<option <?php if($row['depttype'] == 'Marketing') echo "selected='selected'";?>  value="Marketing">Marketing</option>
		<option <?php if($row['depttype'] == 'Housekeeping') echo "selected='selected'";?>  value="Housekeeping">Housekeeping</option>
		<option <?php if($row['depttype'] == 'Backoffice') echo "selected='selected'";?>  value="Backoffice">Backoffice</option>
		<option <?php if($row['depttype'] == 'Insurance') echo "selected='selected'";?> value="Insurance">Swastika Insurance</option>
		<option <?php if($row['depttype'] == 'Securities') echo "selected='selected'";?> value="Securities">Swastika Securities</option>
		<option <?php if($row['depttype'] == 'Cash') echo "selected='selected'";?>  value="Cash">Cash</option>
		<option <?php if($row['depttype'] == 'Other') echo "selected='selected'";?>  value="Other">Other</option>
	</select>

    <?php } else {?>
	<select disabled="true" style="display: none" class="inputBox" id="inp22">
		<option <?php if($row['depttype'] == 'Marketing') echo "selected='selected'";?>  value="Marketing">Marketing</option>
		<option <?php if($row['depttype'] == 'Housekeeping') echo "selected='selected'";?>  value="Housekeeping">Housekeeping</option>
		<option <?php if($row['depttype'] == 'Backoffice') echo "selected='selected'";?>  value="Backoffice">Backoffice</option>
		<option <?php if($row['depttype'] == 'Insurance') echo "selected='selected'";?> value="Insurance">Swastika Insurance</option>
		<option <?php if($row['depttype'] == 'Securities') echo "selected='selected'";?> value="Securities">Swastika Securities</option>
		<option <?php if($row['depttype'] == 'Cash') echo "selected='selected'";?>  value="Cash">Cash</option>
		<option <?php if($row['depttype'] == 'Other') echo "selected='selected'";?>  value="Other">Other</option>
	</select>
 	<?php } ?>	
	</div>	
</div>




<div class="row">
	<div class="col-sm-2 formLeft req">
		Tally Cost Center
		<br/>
		<span style="font-size:11px;">
		(Branch Acc To Tally)
		</span>
	</div>
	<div class="col-sm-4 formRight">

	<?php if(in_array('tally_cast',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select  class="inputBox" id="inp27">
		<option value="">Please select tally cost center</option>
	<?php
		$data = getData('tallybranch','*','name','ASC');
    	foreach($data as $row1){?>
		<option <?php if($row['tallybranch'] == $row1['name']) echo "selected='selected'";?> value="<?php echo $row1['name']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>

    <?php } elseif(in_array('tally_cast',$permissions)) {  ?>
	<select disabled="true"  class="inputBox" id="inp27">
		<option value="">Please select tally cost center</option>
	<?php
		$data = getData('tallybranch','*','name','ASC');
    	foreach($data as $row1){?>
		<option <?php if($row['tallybranch'] == $row1['name']) echo "selected='selected'";?> value="<?php echo $row1['name']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>

    <?php } else {?>
	<select disabled="true" style="display: none" class="inputBox" id="inp27">
		<option value="">Please select tally cost center</option>
	<?php
		$data = getData('tallybranch','*','name','ASC');
    	foreach($data as $row1){?>
		<option <?php if($row['tallybranch'] == $row1['name']) echo "selected='selected'";?> value="<?php echo $row1['name']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
 	<?php } ?>	
	</div>	


	<div class="col-sm-2 formLeft req">
		Employee Status
		
	</div>
	<div class="col-sm-4 formRight">

	<?php if(in_array('employee_status',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp30">
	<option value="">Please select this</option>
	<option <?php if($row['empstatuslive'] == 'CONFIRMED') echo "selected='selected'";?> value="CONFIRMED">CONFIRMED</option>
	<option <?php if($row['empstatuslive'] == 'ON PROBATION') echo "selected='selected'";?>  value="ON PROBATION">ON PROBATION</option>
	</select>

    <?php } elseif(in_array('employee_status',$permissions)) {  ?>
	<select disabled="true" class="inputBox" id="inp30">
	<option value="">Please select this</option>
	<option <?php if($row['empstatuslive'] == 'CONFIRMED') echo "selected='selected'";?> value="CONFIRMED">CONFIRMED</option>
	<option <?php if($row['empstatuslive'] == 'ON PROBATION') echo "selected='selected'";?>  value="ON PROBATION">ON PROBATION</option>
	</select>

    <?php } else {?>
	<select disabled="true" style="display: none" class="inputBox" id="inp30">
	<option value="">Please select this</option>
	<option <?php if($row['empstatuslive'] == 'CONFIRMED') echo "selected='selected'";?> value="CONFIRMED">CONFIRMED</option>
	<option <?php if($row['empstatuslive'] == 'ON PROBATION') echo "selected='selected'";?>  value="ON PROBATION">ON PROBATION</option>
	</select>
 	<?php } ?>	


	</div>	
    </div>

    


	
<div class="row">
	<div class="col-sm-2 formLeft req">
		Profile
		<br/>
	</div>
    <div class="col-sm-4 formRight">
    	
    <?php if(in_array('emp_profiles',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select onchange="getZoneList(this.value)" class="inputBox selectInput38border" id="inp38">
		<option value="0">--Please select profile--</option>
	<?php
		$profilequery = mysqli_query($con,"select * from profiles");
    	while($row1=mysqli_fetch_array($profilequery))
		{
		?>
		<option <?php if($row['profileid'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>

    <?php } elseif(in_array('emp_profiles',$permissions)) {  ?>
	<select disabled="true" onchange="getZoneList(this.value)" class="inputBox selectInput38border" id="inp38">
		<option value="0">--Please select profile--</option>
	<?php
		$profilequery = mysqli_query($con,"select * from profiles");
    	while($row1=mysqli_fetch_array($profilequery))
		{
		?>
		<option <?php if($row['profileid'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
    <?php } else {?>
	<select style="display: none" disabled="true" onchange="getZoneList(this.value)" class="inputBox selectInput38border" id="inp38">
		<option value="0">--Please select profile--</option>
	<?php
		$profilequery = mysqli_query($con,"select * from profiles");
    	while($row1=mysqli_fetch_array($profilequery))
		{
		?>
		<option <?php if($row['profileid'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
 	<?php } ?>	
	
	</div>


	<div class="col-sm-2 formLeft req">
		Levels
	</div>
	<div class="col-sm-4 formRight">
	<?php if(in_array('emp_level',$permissions) && in_array('emp_update',$permissions)) { ?>
	<select class="inputBox" id="inp39">
	<?php $query = mysqli_query($con,"select * from levels");
	while($result = mysqli_fetch_array($query)){ ?>
	<option <?php if($row['levelid'] == $result['id']) echo "selected='selected'";?> value="<?php echo $result['id']?>"><?php echo $result['name']?></option>
	<?php } ?>
	</select>

    <?php } elseif(in_array('emp_level',$permissions)) {  ?>
	<select disabled="true"  class="inputBox" id="inp39">
	<?php $query = mysqli_query($con,"select * from levels");
	while($result = mysqli_fetch_array($query)){ ?>
	<option <?php if($row['levelid'] == $result['id']) echo "selected='selected'";?> value="<?php echo $result['id']?>"><?php echo $result['name']?></option>
	<?php } ?>
	</select>

    <?php } else {?>
	<select disabled="true" style="display: none" class="inputBox" id="inp39">
	<?php $query = mysqli_query($con,"select * from levels");
	while($result = mysqli_fetch_array($query)){ ?>
	<option <?php if($row['levelid'] == $result['id']) echo "selected='selected'";?> value="<?php echo $result['id']?>"><?php echo $result['name']?></option>
	<?php } ?>
	</select>
 	<?php } ?>	
	</div>	
    </div>

<div class="row" id="zone_lists" <?php echo ($row['zones']=='')? 'style="display: none"':'style="display: block"' ?> >
	<div class="col-sm-2 formLeft">Zones</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp43">
	<option value="">select zones</option>
	<?php $query = mysqli_query($con,"select * from zones");
  	while($result = mysqli_fetch_array($query)) { ?>
    <option <?php if($row['zones'] == $result['id']) echo "selected='selected'";?> value="<?php echo $result['id']; ?>"><?php echo $result['name']; ?></option>	
	<?php } ?>	
	</select>
	</div>	
</div>

<div class="row" style="display:none">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp13" style="width:100%;height:150px;"></textarea>
	</div>	


</div>
<div class="row">
	<div class="col-sm-2 formLeft">
		Extra Permissions
	</div>

	<div class="col-sm-10 formRight">
	<table class="table table-bordered">
		<tr>
			<td>Allow Hold Entries</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['holdperm'] == '1') echo "checked='checked'";?> type="checkbox" value="1" id="inp24" name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['holdperm'] == '1') echo "checked='checked'";?> type="checkbox" value="1" id="inp24" name=""  disabled="disabled">
            <?php } else {?>
				<input <?php if($row['holdperm'] == '1') echo "checked='checked'";?> style="display:none" type="checkbox" value="1" id="inp24" name=""  disabled="disabled">
           	<?php } ?>	
			</td>
		</tr>

		<tr>
			<td>Allow Incentive MF Entries</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['incentiveperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp25"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['incentiveperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp25"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['incentiveperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp25"  name="" disabled="disabled" style="display:none">
           	<?php } ?>	

			</td>
		</tr>

			<tr>
			<td>Allow Incentive Performance Entries</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp36"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp36"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp36"  name="" disabled="disabled" style="display: none">
           	<?php } ?>	

			</td>
		</tr>

		<tr>
			<td>Allow Loan/Advances Entries</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['loanperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp26"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['loanperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp26"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['loanperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp26"  name="" disabled="disabled" style="display: none;">
           	<?php } ?>	


			</td>
		</tr>
		<tr>
			<td>Allow Bank Details Approval</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['bcperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp28"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['bcperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp28"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['bcperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp28"  name="" disabled="disabled" style="display: none;">
           	<?php } ?>	
			</td>
		</tr>

		<tr>
			<td>Allow TDS Entries</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp29"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp29"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['jvholdperm'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp29"  name="" disabled="disabled" style="display: none;">
           	<?php } ?>	
			</td>
			
		</tr>
		<tr>
			<td>Manual Attendance</td>
			<td>
           	<?php if(in_array('emp_extra_permission',$permissions) && in_array('emp_update',$permissions)) { ?>
				<input <?php if($row['manualattendance'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp40"  name="">
            <?php } elseif(in_array('emp_extra_permission',$permissions)) {  ?>
				<input <?php if($row['manualattendance'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp40"  name="" disabled="disabled">
            <?php } else {?>
				<input <?php if($row['manualattendance'] == '1') echo "checked='checked'";?>  type="checkbox" value="1" id="inp40"  name="" disabled="disabled" style="display: none;">
           	<?php } ?>	
			</td>
		</tr>


	</table>
	</div>	


</div>


<div class="row">
	<div class="col-sm-2 formLeft">
	</div>
	<div class="col-sm-10 formRight">
		<?php if(in_array('emp_update',$permissions)) { ?>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="UpdateEmployee('<?php echo $updateurl;?>','<?php echo $callbackurl;?>');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>
		<?php } else{ ?>
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;DON'T HAVE UPDATE PERMISSION</button>
		<?php } ?> 	
			<br/><br/><br/>
	</div>	
</div>





</div>
<br />
<br />
<br />
<br />
<br />


