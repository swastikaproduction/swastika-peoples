<?php
function getMaxPoolAllowed($emp)
{
	global $con;
	global $rowData;
	global $rowEmp;
	global $month_end;
	global $lastpooldeduction;
	$currentBalance = $rowEmp['poolbalance'] + $lastpooldeduction;
	$doj = strtotime($rowEmp['doj']);
	$today = strtotime($month_end);
	$diff = $today - $doj;
	$days = $diff/(60*60*24);

	if($days >= 0 && $days <= 90)
	{
		if($currentBalance >= 1.5)
		{
			return 1.5;
		}
		else
		{
			return $currentBalance;
		}
	}
	else
	{
		if($currentBalance >= 6)
		{
			return 6;
		}
		else
		{
			return $currentBalance;
		}
	}
}

?>