<?php
include('include/connection.php');
$name = "Deductions_Cumilative.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

$empdata = Array();
$getData = mysqli_query($con,"SELECT * FROM `employee`") or die(msyqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	$empdata[$row['id']] = $row['name'];
}

?>
<table border="1">
	<tr>
		<th>Name</th>
		<th>Deduction From Pool</th>
		<th>Deduction From Salary</th>
		<th>Total Deduction</th>
		
	</tr>

<?php
$getData = mysqli_query($con,"SELECT * FROM `final_console`");
while($row = mysqli_fetch_array($getData))
{
?>
<tr>
	<td><?php echo $empdata[$row['empid']];?></td>
	<td><?php echo $row['pool'];?></td>
	<td><?php echo $row['salary'];?></td>
	<td><?php echo $row['pool']+$row['salary'];?></td>
</tr>
<?php
}
?>
</table>