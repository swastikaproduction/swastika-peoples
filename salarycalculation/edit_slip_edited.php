<?php
include("../include/config.php");
include("convert-to-word.php");
$id = $_GET['id'];
$getAtlog = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowLog = mysqli_fetch_array($getAtlog);
$empid = $rowLog['empid'];
$salary = $rowLog['edited_salary'];
$bifurcation = $rowLog['bifurcated'];
$bifurcation = explode("::",$bifurcation);


$getEmp = mysqli_query($con,"SELECT employee.name,designation.name,employee.pf FROM employee,designation WHERE employee.designation = designation.id AND employee.id = '$empid'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);


$getData = mysqli_query($con,"SELECT * FROM `bifurcation` WHERE `last` < '$salary' AND `this` >= '$salary'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$pf=$rowEmp[2];
$mth = "2017-".$rowLog['month']."-01";
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Salary Slip
	</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/6d9f21723b.js"></script>

<style type="text/css">
body
{
	overflow: auto !important;
}
	.inTab th
	{
		background:#999;
		color: #fff;
		font-weight: normal;
		text-align: left;
	}

	.inTab tr th:nth-child(2),.inTab tr th:nth-child(4)
	{
		text-align: right;
	}

	.inTab tr td:nth-child(1), .inTab tr td:nth-child(3)
	{
		font-weight: bold;
	}

	.inTab tr td:nth-child(2)
	{
		border-right:1px #eee solid;
		text-align: right;
	}

	.inTab tr td:nth-child(4)
	{
		text-align: right;
	}
	.inTab td
	{
		border-bottom:1px #eee solid;
	}

	.inputBoxSal
	{
		padding: 5px;
	}
</style>
<script type="text/javascript">
	function calculateSalary()
	{
		var totalEarning = 0;
		var totalDeductions = 0;
		var netpay = 0;
		var values = [];
		for(j=0;j<9;j++)
		{
			if(document.getElementById('sal'+j).value == '')
			{
				document.getElementById('sal'+j).value = 0;
			}
			if(j != 1 && j != 4)
			{
			totalEarning += parseInt(document.getElementById('sal'+j).value);				
			}
			else
			{
			totalDeductions += parseInt(document.getElementById('sal'+j).value);				
			}
		}
		document.getElementById('totalEarning').innerHTML= totalEarning;		
		document.getElementById('totalDeductions').innerHTML= totalDeductions;
		document.getElementById('netPay').innerHTML= totalEarning - totalDeductions;

	}
</script>
</head>
<body>
<center>
	<div style="width:900px;padding:40px;">
	<form action="savesalary.php?id=<?php echo $id;?>" method="post">
		<table cellpadding="10" cellspacing="0" style="width:100%;border:1px #eee solid;">
		<tr>
			<td colspan="2" align="center" style="border-bottom:1px #eee solid;font-size:20px;">
				<strong>Payslip for the Month of <?php echo date("M",strtotime($mth));?>, <?php echo $rowLog['year'];?></strong>
			</td>
		</tr>
			<tr>
				<td style="width:50%;text-align:left;border-right:1px #eee solid;">
					<table style="width:100%" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<td>Employee Name</td><td style="border-right:0px"><?php echo $rowEmp[0];?></td>
						</tr>
						<tr>
						<td style="border-bottom:0px;">
							Designation
						</td>
						<td style="border-right:0px;border-bottom:0px;">
							<?php echo $rowEmp[1];?>
						</td>
							
						</tr>
					</table>
				</td>
				<td style="text-align:center">
					<img src="https://www.swastika.co.in/images/blogo.png" style="height:40px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;border-top:1px #eee solid">
				<br/><br/>
					<table style="width:100%;" cellpadding="10" cellspacing="0" class="inTab">
						<tr>
							<th style="width:25%">Earnings & Remuneration</th>
							<th style="width:25%">Amount (Rs.)</th>
							<th style="width:25%">Deduction</th>
							<th style="width:25%">Amount (Rs.)</th>
						</tr>
<tr>
							<td>Earned Basic</td>
							<td><?php
							  $basic = $bifurcation[0];
							?>
							<input type="number" name="basic" value="<?php echo $basic;?>" class="inputBoxSal" name="" id="sal0">
							</td>
							<td>Provident Fund</td>
							<td>
								<?php 
								$pfval = $bifurcation[1];
								?>
							<input type="number" name="pf"  value="<?php echo $pfval;?>" class="inputBoxSal" name="" id="sal1">
							</td>
						</tr>
						<tr>
							<td>Dearness Allowance</td>
							<td>
								<?php 
								$da = $bifurcation[2];
								?>
							<input type="number" name="da"  value="<?php echo $da;?>" class="inputBoxSal" name="" id="sal2">								
							</td>
							<td>TDS</td>
							<td>0</td>
						</tr>
						<tr>
							<td>HRA</td>
							<td>
								<?php
								$hra = $bifurcation[3];
								?>
									<input type="number" name="hra"  value="<?php echo $hra;?>" class="inputBoxSal" name="" id="sal3">

							</td>
							<td>Professional Tax</td>
							<td>
								<?php
								$pt = $bifurcation[4];
								?>
								<input type="number" name="pt"  value="<?php echo $pt;?>" class="inputBoxSal" name="" id="sal4">
							</td>
						</tr>
						<tr>
							<td>Bonus</td>
							<td>
								<?php
								$bonus = $bifurcation[5];
								?>
<input type="number" value="<?php echo $bonus;?>" name="bonus"  class="inputBoxSal" name="" id="sal5">
							</td>
							<td>Leave Deducted</td>
							<td>0</td>
						</tr>
						<tr>
							<td>Domicilliary Allowance</td>
							<td><?php  $dom = $bifurcation[6];?>
								
<input type="number" value="<?php echo $dom;?>" name="dom"  class="inputBoxSal" name="" id="sal6">
							</td>
							<td>Other Deduction</td>
							<td>0</td>
						</tr>

						<tr>
							<td>Conveyance Allowance</td>
							<td><?php  $convey = $bifurcation[7];;?>
								<input type="number" name="convey"  value="<?php echo $convey;?>" class="inputBoxSal" name="" id="sal7">

							</td>
							<td></td><td></td>
						</tr>
						<tr>
							<td>Employee Daily Refreshment</td>
							<td>
								<?php $ref =  $bifurcation[8];
								?>
								<input type="number"  name="ref" value="<?php echo $ref;?>" class="inputBoxSal" name="" id="sal8">
							</td>
							<td></td><td></td>
						</tr>
						<tr>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td>Total Earning</td>
							<td id="totalEarning"><?php echo $salary;?></td>
							<td>Total Deductions</td>
							<td id="totalDeductions"><?php echo $total_deduction = $pfval+$pt;?></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Net Pay</td>
							<td style="font-weight:bold" id="netPay"><?php echo $netpay = $salary - $total_deduction;?></td>
						</tr>

					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<button style="width:100px;border:0px;background:#1976d2;color:#fff;padding:10px;">SAVE SALARY</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button" style="width:100px;border:0px;background:#ef5350;color:#fff;padding:10px;" onclick="location.reload();">RESET FORM</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button"  style="width:200px;border:0px;background:#ef5350;color:#fff;padding:10px;"  onclick="window.location = 'reset_original.php?id=<?php echo $id;?>'">RESET TO ORIGINAL VERSION</button>

				</td>
			</tr>
			
		</table>
		</form>
	</div>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('input').keyup(function(){
			calculateSalary();
		});
	})
</script>
</body>
</html>
