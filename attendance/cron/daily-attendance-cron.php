#!/usr/bin/php
<?php
error_reporting(0);
date_default_timezone_set('Asia/Kolkata');
global $con;
$con = mysqli_connect("localhost","root","");
if(!$con)
{
	die(mysqli_error($con));
}
mysqli_select_db($con,'hrm');

$datetime = date("Y-m-d H:i:s");

$yesterday = time() - (24 * 60 * 60);
$thisdate = date("Y-m-d",$yesterday);
//$thisdate = $_GET['date'];
//$date = "10/12/2018";
$date = date("d/m/Y",strtotime($thisdate));
$daterange = date("d",strtotime($thisdate)).date("m",strtotime($thisdate)).date("Y",strtotime($thisdate));
$daterange = $daterange."-".$daterange;
//$daterange = "10122018-10122018";
//$today = "26";
//$month = "04";
$today = date("d",strtotime($thisdate));
$month = date("m",strtotime($thisdate));
$year = date("Y",strtotime($thisdate));
if($today > 25)
{
	if($month == 12)
	{
		$month = "01";
		$year = $year+1;
		$tableName = "attendance_".$month."_".$year;
	}
	else
	{
		$month = $month+1;
		if($month < 10)
		{
			$month = "0".$month;
		}
		$tableName = "attendance_".$month."_".$year;	
	}
}
else
{
	$tableName = "attendance_".$month."_".$year;
}

$tables = Array();
$getTables = mysqli_query($con,"show tables");
while($row = mysqli_fetch_array($getTables))
{
	$tables[] = $row[0];
}


if(!in_array($tableName,$tables))
{
	mysqli_query($con,"CREATE TABLE IF NOT EXISTS `".$tableName."` (
  `id` int(11) NOT NULL,
  `empid` int(11) NOT NULL,
  `date` date NOT NULL,
  `intime` time NOT NULL,
  `outime` time NOT NULL,
  `hours` varchar(100) NOT NULL,
  `allotedshifthours` varchar(20) NOT NULL,
  `shiftend` time NOT NULL,
  `difference` varchar(100) NOT NULL,
  `late` varchar(100) NOT NULL,
  `consecutive_counter` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `twohourscounter` varchar(100) NOT NULL,
  `twohourvalue` varchar(20) NOT NULL,
  `fifteenminscounter` varchar(100) NOT NULL,
  `deduction` varchar(100) NOT NULL,
  `remarks` text NOT NULL,
  `forced` varchar(20) NOT NULL,
  `forceremarks` varchar(20) NOT NULL,
  `edited` int(1) NOT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1") or die(mysqli_error($con));


mysqli_query($con,"ALTER TABLE `".$tableName."` ADD PRIMARY KEY (`id`)") or die(mysqli_error($con));
mysqli_query($con,"ALTER TABLE `".$tableName."` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT") or die(mysqli_error($con));
}


$getEmployees = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0'") or die(mysqli_error($con));
while($rowEmployees = mysqli_fetch_array($getEmployees))
{
$employee[$rowEmployees['empid']] = $rowEmployees['name'];
}


$url = "http://sa:poiuytrewq123@192.168.0.241/cosec/api.svc/v2/attendance-daily?action=get;format=json;date-range=".$daterange;

//$url = "http://sa:poiuytrewq123@183.182.86.91:838/cosec/api.svc/v2/attendance-daily?action=get;format=json;date-range=".$daterange;
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);

$data = array(
    'username' => 'foo',
    'password' => 'bar'
);


curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
$contents = curl_exec($ch);
$contents = json_decode($contents,true);




$fulldata = $contents['attendance-daily'];
$filepath = "../public/files/sampledata.csv";
$file = fopen($filepath,"w");
foreach($fulldata as $val)
{  
     if($val['punch1']==''){

        echo $val['userid'];
     }

	// $firstline = Array("User","",$val['userid'],"","",$employee[$val['userid']],"");
	// $temp = $val['punch1'];
	// $temp = explode(" ",$temp);
	// $intime = $temp[1];

 //    $t = explode(":",$intime);
 //    $intime = $t[0].":".$t[1];


	// $temp = $val['outpunch'];
	// $temp = explode(" ",$temp);
	// $outtime = $temp[1];
 //    $t = explode(":",$outtime);
 //    $outtime = $t[0].":".$t[1];

	// $secondline = Array($date,"",$intime,"",$outtime,"","");

	// fputcsv($file,$firstline);
	// fputcsv($file,$secondline);

}
curl_close($ch);

fclose($file);



$employeeList = Array();
global $month_start;
global $month_end;

$month_end = $year."-".$month."-25";
$stemp = strtotime($month_end) - (60*60*24*26);
$stempmonth = date("m",$stemp);
$stempYear = date("Y",$stemp);
$month_start = $stempYear."-".$stempmonth."-26";
$employee = Array();

$empBranch = Array();
$getEmployees = mysqli_query($con,"SELECT * FROM `employee` WHERE `left` = '0'") or die(mysqli_error($con));
while($rowEmployees = mysqli_fetch_array($getEmployees))
{
$employee[$rowEmployees['empid']] = $rowEmployees['id'];
$employeeShift[$rowEmployees['empid']] = $rowEmployees['shift'];
$empBranch[$rowEmployees['empid']]  = $rowEmployees['branch'];
}

$shifts = Array();
$shiftEnd = Array();
$shiftlength = Array();
$getShifts = mysqli_query($con,"SELECT * FROM `shift`") or die(mysqli_error($con));
while($rowShifts = mysqli_fetch_array($getShifts))
{
$shift[$rowShifts['id']] = $rowShifts['starttime'];
$shiftlength[$rowShifts['id']] = $rowShifts['hours'];
$shiftEnd[$rowShifts['id']]= $rowShifts['endtime'];
}


$calender = Array();
$branchState = Array();
$calDate = Array();
$p=0;
$getCalender = mysqli_query($con,"SELECT * FROM `calendar` WHERE `date` BETWEEN '$month_start' AND '$month_end'") or die(mysqli_error($con));
while($rowCalender = mysqli_fetch_array($getCalender))
{
	$calDate[$p] = $rowCalender['date'];
	if($rowCalender['type'] == '1')
	{
		$calender[$p] = 'holiday';
		$branchState[$p] = explode(",",$rowCalender['branch']);
	}
	else
	{
		$calender[$p] = $rowCalender['from']."--".$rowCalender['to']."--".$rowCalender['hours'];
		$branchState[$p] = explode(",",$rowCalender['branch']);
	}

}

$is_absent_on = Array();
$is_also_absent_on = Array();
$take_leave_on = Array();
$dateString = '';
//$filepath = "../".$post[2];
$file = fopen($filepath,"r");
$k=0;
$thisEmpId= '0';
$valueString = '';
$thisfifteen = '0';
$oldempid = '0';
$fifteen = '0';
$oldlate = '0';
$conlate = '0';

$spclShift = Array();



while(! feof($file))
  {
  $data = fgetcsv($file);
	  if($data[0] == 'User')
	  {
	  	if($employee[$data[2]])
	  	{
	  		
	  		if($oldempid != $data[2])
	  		{
	  			$fifteen = '0';
				$conlate = '0';
	  		}
	  		$oldempid = $data[2];
	  		if($thisEmpId != '0' && $valueString != '')
	  		{
	  			$dateString = substr($dateString,0,-1);



	  			mysqli_query($con,"DELETE FROM `$tableName` WHERE `empid` = '$empid' AND `date` IN (".$dateString.")") or die(mysqli_error($con));
	  			$valueString  = substr($valueString,0,-1);

	  			mysqli_query($con,"INSERT INTO `$tableName` (`id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend`) VALUES ".$valueString) or die(mysqli_error($con));
	  			$valueString = '';
	  			$dateString = '';
	  		}
	  		$thisEmpId = $data[2];
			$empid = $employee[$data[2]];

			$editedDates = Array();
			$getEdited = mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid' AND `edited` = '1'") or die(mysqli_error($con));
			while($rowEdited = mysqli_fetch_array($getEdited))
			{
				$editedDates[] .= $rowEdited['date'];
			}

			$empname = $data[5];  	
			$employeeList[] .= $empid;
			$spclShift = Array();

			$getSpclShift = mysqli_query($con,"SELECT * FROM `shiftallot` WHERE `empid` = '$empid'") OR die(mysqli_error($con));
			while($rowSpcl = mysqli_fetch_array($getSpclShift))
			{
				$dts = $rowSpcl['dates'];
				$thisSpclShift = $rowSpcl['shiftid'];
				$dts = explode(",",$dts);
				foreach($dts as $tvldt)
				{
					$spclShift[$tvldt] = 	$thisSpclShift;			
				}
	
			}
	  	}
	  	else
	  	{
			$empid = '0';
			$empname = '0'; 	
			$thisEmpId = '0';  		
	  	}
	  }
	  else if($data[1] != 'Shift' && $data[1] != 'hr' && $data[0] != '' && $empid != '0')
	  {
	  	$atdate = $data[0];
	  	$atdate = convertdate($atdate);
		  	if(!in_array($atdate,$editedDates))
		  	{
		  	$in = $data[2];
		  	if($data[8] != '')
		  	{
		  		$out = $data[8];
		  	}
		  	else if($data[6] != '')
		  	{
		  		$out = $data[6];
		  	}
		  	else
		  	{
		  		$out = $data[4];
		  	}
		  	$out;
		  	if($in != '' && $out != '' && date("D",strtotime($atdate)) != 'Sun')
		  	{
		  		$status = '1';
		  		$brAr = Array();
		  		$brAr = explode(",",$branchState[$atdate]);

		  		if($spclShift[$atdate])
		  			{
					  	$from = $shift[$spclShift[$atdate]];
					  	$total_hours = $shiftlength[$spclShift[$atdate]];	  				
					  	$thisshiftEnd = $shiftEnd[$spclShift[$atdate]];
		  			}


		  		else if(in_array($atdate,$calDate))
	  		{
	  			foreach($calDate as $key => $val)
	  			{
	  				if($val == $atdate)
	  				{
	  					$keycheck[] .= $key;
	  				}
	  			}
	  			foreach($keycheck as $chk)
	  			{
	  				if(in_array($empBranch[$thisEmpId],$branchState[$chk]))
	  				{
	  					if($calender[$chk] == 'holiday')
			  			{
			  				$status = '2';
			  			}
			  			else
			  			{
			  				$x = explode("--",$calender[$chk]);
			  				$from = $x[0];
						  	$total_hours = $x[2];
						  	$thisshiftEnd = $x[1];	  						
			  			}
	  				}
	  			}

	  			

	  		}
		  		else
		  		{
					  	$from = $shift[$employeeShift[$thisEmpId]];
					  	$total_hours = $shiftlength[$employeeShift[$thisEmpId]];
					  	$thisshiftEnd = $shiftEnd[$employeeShift[$thisEmpId]];	  				
		  		}
		  		



		  	$total_mins = $total_hours * 60;
		  	
		  	$late = lateminutes($from,$in);
		  	$temp = shifthours($in,$out,$total_hours);
		  	$temp = explode(":::",$temp);
		  	$workinghours = round($temp[1]/60,2);
		  	$shiftdifference = $temp[0];

		  	if($late > 15)
		  	{

		  			$conlate++;
		  	}
		  	else
		  	{
		  		$conlate = '0';
		  	}
		  	$oldlate = $late;
		  	if($late > 15 && $late <= 30 && $late > 0)
		  	{
		  		$fifteen++;
		  		$thisfifteen  = $fifteen;
		  	}
		  	else
		  	{
		  		$thisfifteen = '0';
		  	}


		  	}
		  	else
		  	{
		  	$conlate = '0';
		  	$late = 'NA';
		  	$workinghours = 'NA';
		  	$shiftdifference = 'NA';
		  	if(date("D",strtotime($atdate)) == 'Sun')
		  	{
				$status = '2';	  		
		  	}
		  	else if(in_array($atdate,$calDate))
	  		{
	  			foreach($calDate as $key => $val)
	  			{
	  				if($val == $atdate)
	  				{
	  					$keycheck[] .= $key;
	  				}
	  			}
	  			foreach($keycheck as $chk)
	  			{
	  				if(in_array($empBranch[$thisEmpId],$branchState[$chk]))
	  				{
	  					if($calender[$chk] == 'holiday')
			  			{
			  				$status = '2';
			  			}
			  			else
			  			{
			  				$status = '0';
			  			}
	  				}
	  			}

	  			

	  		}
		  	else
		  	{
				$status = '0';	
						
		  	}

		  	$thisfifteen = '0';
		  	$total_mins = '0';

		  	}
		  	$valueString .= "('', '$empid', '$atdate', '$in', '$out', '$workinghours', '$shiftdifference', '$late', '$status', '0', '$thisfifteen', '', '', '$datetime', '$total_mins','$conlate','$thisshiftEnd'),";

		  	$dateString .= "'".$atdate."',";
	  	}
	  }
	  $k++;
  }
  				if($valueString != '')
  				{
	  			$dateString = substr($dateString,0,-1);
	  			mysqli_query($con,"DELETE FROM `$tableName` WHERE `empid` = '$empid' AND `date` IN (".$dateString.")") or die(mysqli_error($con));

	  				$valueString  = substr($valueString,0,-1);
	  	  			mysqli_query($con,"INSERT INTO `$tableName` (`id`, `empid`, `date`, `intime`, `outime`, `hours`, `difference`, `late`, `status`, `twohourscounter`, `fifteenminscounter`, `deduction`, `remarks`, `createdate`,`allotedshifthours`,`consecutive_counter`,`shiftend`) VALUES ".$valueString) or die(mysqli_error($con));
		  			$valueString = '';  					
  				}


fclose($file);
$employeeList = array_unique($employeeList);

foreach($employeeList as $emp)
{
	$delStr .= $emp.",";
	$insertStr .= "('', '$month', '$year', '$emp', '$datetime', '$loggeduserid'),";
}
$delStr = substr($delStr,0,-1);
$insertStr = substr($insertStr,0,-1);

mysqli_query($con,"DELETE FROM `attendancelog` WHERE `month` = '$month' AND `year` = '$year' AND `empid` IN (".$delStr.")") or die(mysqli_error($con));


mysqli_query($con,"INSERT INTO `attendancelog` (`id`, `month`, `year`, `empid`, `createdate`, `updatedby`) VALUES ".$insertStr) or die(mysqli_error($con));


function convertdate($atdate)
{
	$temp =explode("/",$atdate);
	if(count($temp) > 1)
	{
	$atdate = explode("/",$atdate);
	return $atdate[2]."-".$atdate[1]."-".$atdate[0];		
	}
	else
	{
			$atdate = explode("-",$atdate);
			return '20'.$atdate[2]."-".$atdate[1]."-".$atdate[0];

	}
}


function lateminutes($from,$in)
{
	$from = "2017-01-01 ".$from;
	$in = "2017-01-01 ".$in.":00";
	$fromtime = strtotime($from);
	$intime = strtotime($in);
	$difference= $intime - $fromtime;
	$mindif = round($difference/60);
	return $mindif;
}

function shifthours($in,$out,$total_hours)
{
		$orgin = $in;
$orgout = $out;
	$in = "2017-01-01 ".$in.":00";
	$out = "2017-01-01 ".$out.":00";

		$outtime = strtotime($out);
		$intime = strtotime($in);
	$temp1 = explode(":",$orgin);

	$temp = explode(":",$orgout);
if(($outime - $intime) < 0)
	{
		$temp_tweleve_one = strtotime('2017-01-01 00:00:00');
		$temp_tweleve_two = strtotime('2017-01-02 00:00:00');

		$diff_one = $temp_tweleve_two - $intime;
		$diff_two = $outtime - $temp_tweleve_one;
		$difference = $diff_one+$diff_two;
	}
	else
	{
	
		$difference= $outtime - $intime;
	}


	$mindif = round($difference/60);

	$hourdifference = ($total_hours * 60) - $mindif;

	return $hourdifference.":::".$mindif;	
}


$today = strtotime($year."-".$month."-01") - (24*60*60*7);

$premonth = date("m",$today);


$satArray = Array();
global $is_also_absent_on;
global $is_absent_on;
global $take_leave_on;

$k=1;
for($j=0;$j<32;$j++)
{
		$d = $j;
	$thisDate = $year."-".$premonth."-".$d;
	$day = date("D",strtotime($thisDate));
	if($day == 'Sat')
	{
		if($d > 25)
		{
			$mondayTemp = strtotime($thisDate) + (24*60*60*2);			
		$monday = date("Y-m-d",$mondayTemp);
		$is_absent_on[] .= $monday;

		}
		if($k == 1 || $k == 3 || $k == 5)
		{
			if($d>25)
			{
				$is_also_absent_on[$monday] = $thisDate;
				$sundayTemp = strtotime($thisDate) + (24*60*60*1);
				$sunday = date("Y-m-d",$sundayTemp);;
				$take_leave_on[$monday] = $sunday;				
			}
		}
		else
		{
			if($d > 25)
			{
				$fridayTemp = strtotime($thisDate) - (24*60*60*1);
				$friday = date("Y-m-d",$fridayTemp);
				$is_also_absent_on[$monday] = $friday;
				$sundayTemp = strtotime($thisDate) + (24*60*60*1);
				$sunday = date("Y-m-d",$sundayTemp);
				$take_leave_on[$monday] = $sunday.",".$thisDate;				
			}

		}
		$k++;
		
	}
}

$k=1;


for($j=0;$j<26;$j++)
{
	if($j<10)
	{
		$d = "0".$j;
	}
	else
	{
		$d = $j;
	}
	$thisDate = $year."-".$month."-".$d;
	$day = date("D",strtotime($thisDate));
	if($day == 'Sat')
	{
		$mondayTemp = strtotime($thisDate) + (24*60*60*2);
		$monday = date("Y-m-d",$mondayTemp);
		$is_absent_on[] .= $monday;
		if($k == 1 || $k == 3 || $k == 5)
		{
			$is_also_absent_on[$monday] = $thisDate;
			$sundayTemp = strtotime($thisDate) + (24*60*60*1);
			$sunday = date("Y-m-d",$sundayTemp);;
			$take_leave_on[$monday] = $sunday;
		}
		else
		{
			$fridayTemp = strtotime($thisDate) - (24*60*60*1);
			$friday = date("Y-m-d",$fridayTemp);
			$is_also_absent_on[$monday] = $friday;
			$sundayTemp = strtotime($thisDate) + (24*60*60*1);
			$sunday = date("Y-m-d",$sundayTemp);
			$take_leave_on[$monday] = $sunday.",".$thisDate;

		}
		$k++;
		
	}
}


mysqli_query($con,"DELETE FROM `sandwich` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));


foreach($is_also_absent_on as $key => $val)
{
	mysqli_query($con,"INSERT INTO `sandwich` (`id`, `subject`, `key`, `value`, `month`, `year`, `createdate`, `updatedby`) VALUES ('', 'is_also_absent_on', '$key', '$val', '$month', '$year', '$datetime', '$loggeduserid')") or die(mysqli_error($con));
}

foreach($is_absent_on as $key => $val)
{
	mysqli_query($con,"INSERT INTO `sandwich` (`id`, `subject`, `key`, `value`, `month`, `year`, `createdate`, `updatedby`) VALUES ('', 'is_absent_on', '$key', '$val', '$month', '$year', '$datetime', '$loggeduserid')") or die(mysqli_error($con));
}


foreach($take_leave_on as $key => $val)
{
	mysqli_query($con,"INSERT INTO `sandwich` (`id`, `subject`, `key`, `value`, `month`, `year`, `createdate`, `updatedby`) VALUES ('', 'take_leave_on', '$key', '$val', '$month', '$year', '$datetime', '$loggeduserid')") or die(mysqli_error($con));
}

?>
