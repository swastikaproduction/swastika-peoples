<?php
function has_completed_shift_hours($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	
	if($rowData['difference'] <=  0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function less_thn_halfshift($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	if(date("D",strtotime($date)) == 'Sat')
	{
		$half = '270';
	}
	else
	{
		$half = '300';
	}
	//$half = $shifthours/2;
	if($rowData['difference'] < $half)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function half_to_shift_minus_two($emp,$date)
{
	global $con;
	global $rowData;
	$shifthours = $rowData['allotedshifthours'];
	$half = $shifthours/2;
	if($rowData['difference'] > 120 && $rowData['difference'] <= $half)
	{
		return true;
	}
	else
	{
		return false;
	}	
}


function shift_minus_two_to_shift_minus_onethirty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 90 && $rowData['difference'] <= 120)
	{
		return true;
	}
	else
	{
		return false;
	}	
}



function shift_minus_onethirty_to_shift_minus_one($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 60 && $rowData['difference'] <= 90)
	{
		return true;
	}
	else
	{
		return false;
	}	
}


function shift_minus_one_to_shift_minus_thirty($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 30 && $rowData['difference'] <= 60)
	{
		return true;
	}
	else
	{
		return false;
	}	
}

function shift_minus_thirty_to_shift($emp,$date)
{
	global $con;
	global $rowData;
	if($rowData['difference'] > 0 && $rowData['difference'] <= 30)
	{
		return true;
	}
	else
	{
		return false;
	}	
}

?>