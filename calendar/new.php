<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
?>



<div class="moduleHead">
<br/>

	<div class="moduleHeading" onclick="toogleFormTable();">
	<span style="font-size:20px;">
		<i class="fa fa-caret-left"></i>
	</span>&nbsp;&nbsp;Calendar

	</div>
</div>







<div class="shadow" style="background:#fff;">


<div class="col-sm-12 subHead">New Calendar Entry</div>




<div class="row">
	<div class="col-sm-2 formLeft">
		Type
	</div>
	<div class="col-sm-10 formRight">
		<select class="inputBox" id="cal0" onchange="if(this.value == '1') { $('#holTime').slideUp(200);} else  { $('#holTime').slideDown(200);} ">
			<option value="1">Holiday</option>
			<option value="2">Different Timing</option>
		</select>
	</div>	

</div>


<div class="row">
	<div class="col-sm-2 formLeft">
		Name
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="Name" id="cal1" class="inputBox">
	</div>	
<div class="col-sm-1">
</div>


	<div class="col-sm-1 formLeft">
		On date
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="Date" id="cal2" class="inputBox">
	</div>

</div>






<div class="row" id="holTime" style="display:none">
	<div class="col-sm-2 formLeft">
		From Time
	</div>
	<div class="col-sm-4 formRight">
		<input type="time" name="req" title="From Time" id="cal3" class="inputBox" value="<?php echo date("H:i:s");?>">
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
		To Time
	</div>
	<div class="col-sm-4 formRight">
		<input type="time" name="req" title="To Time" id="cal4" class="inputBox" value="<?php echo date("H:i:s");?>">
	</div>


</div>



<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Branches
	</div>
	<div class="col-sm-10 formRight">

<div class="list-group-item" id="topLi">
<input type="search" class="inputBox" onkeyup="dynamicSearch('classTab1',this.value)" placeholder="Search Here" name="" style="width:50%">&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick="selectAllList('classTab1','cal5')">SELECT ALL</button>&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick="deselectAllList('classTab1','cal5')">DESELECT ALL</button>
</div>


<ul class="list-group" style="cursor:pointer;margin-top:0px !important;max-height:300px;overflow-y:auto;overflow-x:hidden;border:1px #eee solid;" id="classTab1">


<?php
$branches = getData('branch','*','name','ASC');
foreach($branches as $val)
{
	?>
<li id="li<?php echo $val['id'];?>" class="list-group-item" lang="<?php echo $val['id'];?>" onclick="collectMultiValues(this,'classTab1','cal5');">
<?php echo $val['name'];?></li>

	<?php
}
?>
		 
		 		

		
		    
		 </ul>

		 		<input type="" id="cal5" name="req" title="Branches" style="display:none" value="">
	</div>	

</div>





<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="cal6" style="width:100%;height:150px;"></textarea>
	</div>	


</div>




<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary" onclick="savedata('<?php echo $saveurl;?>','','','cal',7,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			<br/><br/><br/>
	</div>	



</div>





</div>


<br />
<br />

<br />
<br />
<br />
