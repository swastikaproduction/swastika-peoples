<?php
include("../include/connection.php");
$id = $_GET['id'];

$getData = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `id` = '$id'") or die(mysqli_error($con));
$rowData = mysqli_fetch_array($getData);
$month = $rowData['month']; 
$year = $rowData['year'];
$empid = $rowData['empid'];

$tableName = "attendance_".$month."_".$year;


$getEmp = mysqli_query($con,"SELECT * FROM `employee` WHERE `id` = '$empid'") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

$name = "deductions_".$rowEmp['name'].".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


$getData= mysqli_query($con,"SELECT * FROM `$tableName` WHERE `empid` = '$empid'") or die(mysqli_error($con));
?>


<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
	


	<div class="moduleHeading">
<?php echo $rowEmp['name'];?>

	</div>
</div>


<br/>
<table class="table table-striped table-hover fetchSmall table-bordered" cellpadding="0" cellspacing="0">

	<tr>
		<th>Date</th>
		<th>In</th>
		<th>Out</th>
		<th>Workhrs</th>
		<th>Allotedhrs</th>
		<th>Diff</th>
		<th>Late</th>
		<th>Consecutive</th>
		<th>Status</th>
		<th>Two Hours</th>
		<th>Fifteen Mins</th>
		<th>Deduction</th>
		<th>Remarks</th>
		<th>Forced Dedcution</th>
		<th>Action</th>
	</tr>


<?php
while($row= mysqli_fetch_array($getData))
{
?>
<tr>
	<td><?php echo $row['date'];?></td>
	<td><?php echo $row['intime'];?></td>
	<td><?php echo $row['outime'];?></td>
	<td><?php echo $row['hours'] * 60;?></td>
	<td><?php echo $row['allotedshifthours'];?></td>
	<td><?php echo $row['difference'];?></td>
	<td><?php echo $row['late'];?></td>
	<td><?php echo $row['consecutive_counter'];?></td>
	<td><?php
	if($row['status'] == '2')
	{
		echo "Holiday";
	}
	else if($row['status'] == '1')
	{
		echo "Present";
	}
	else
	{
		echo "Absent";
	}

	?></td>
	<td><?php echo $row['twohourvalue'];?></td>
	<td><?php echo $row['fifteenminscounter'];?></td>
	<td><?php echo $row['deduction'];?></td>
	<td><?php echo $row['remarks'];?></td>
	<td><?php echo $row['forced'];?></td>
	<td>
	<span class="label label-warning">
		

	<i class="fa fa-edit"></i>&nbsp;EDIT
		</span>
	</td>
	

</tr>
<?php
}
?>
</table>
</div>
</div>