<?php
include("../include/config.php");
$type = $_GET['type'];
$month = $_GET['month'];

$year = $_GET['year'];


$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
}


$name = "tallyexport_".$month."_".$year.".xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");


?>

<table border="1" cellpadding="5">
	<tr>
		<th>DATE</th>
		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>

		
		<?php
		for($j=0;$j<10;$j++)
		{
			?>
			<th>BRANCH</th>
			<th></th>
			<th>LEDGER</th>
			<th>AMOUNT</th>
			<?php
		}
		?>
		<th>NARRATION</th>
		<th>VOUCHER TYPE</th>
		<th>error message</th>

	</tr>

<?php
$month = "0".$month;
if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));


}
else if($type == 'PF')
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0'  ORDER BY employee.empid  ASC") or die(mysqli_error($con));

}
else
{
	$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.tallybranch,employee.depttype FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.left = '0' ORDER BY employee.empid ASC") or die(mysqli_error($con));


}



while($row= mysqli_fetch_array($getData))
{


//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;



if($row[8] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}


if($pfflag == '1')
{
			if($calculatedsalary < 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = round($calculatedsalary * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		if($calculatedsalary < 8960 && $calculatedsalary >= 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = 1920;				
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		if($calculatedsalary >= 8960 && $calculatedsalary <= 10000)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 10000 && $calculatedsalary <= 13350)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = 500;
			$hra = $calculatedsalary - ($basic+$pfemployer+$bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary > 13350 && $calculatedsalary <= 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

						if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			$bonus = round($calculatedsalary * 0.05);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
		else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}



			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 8000)
	{
		
			$basic = $calculatedsalary;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

	}

	else if($calculatedsalary >= 8000 && $calculatedsalary < 9000)
	{
			$basic = 8000;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

	else if($calculatedsalary >= 9000 && $calculatedsalary <= 15000)
	{
			$basic = 8000;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.10);
			$hra = $calculatedsalary - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000)
	{
			$basic = round($calculatedsalary * 0.4);
			$pfpayable = 0;
			$da = round($basic * 0.35);
			$hra = round(($basic + $da) * 0.4);
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.15);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}
}

$dataArray = Array();
for($k=0;$k<84;$k++)
{
	$dataArray[$k] = ''; 
}

$dataArray[0]= 'ABA';
$dataArray[1]= $row[7];

/*
for($l=1;$l<78;$l++)
{
	if($l%4 == 0)
	{
		$dataArray[$l+1] = $row[7];		
	}
}

*/

$dataArray[2] = 'DR.';
$dataArray[6] = 'DR.';
$dataArray[10] = 'DR.';
$dataArray[14] = 'DR.';
$dataArray[18] = 'DR.';
$dataArray[22] = 'DR.';
$dataArray[26] = 'DR.';
$dataArray[30] = 'DR.';
$dataArray[34] = 'DR.';
$dataArray[38] = 'DR.';

$dataArray[3] = 'Basic Salary';
$dataArray[4] = $basic;

if($da != 0)
{
$dataArray[5] = $row[7];
$dataArray[7] = 'Dearness Allowance';
$dataArray[8] = $da;	
}


if($hra != 0)
{
$dataArray[9] = $row[7];
$dataArray[11]  = 'House Rent Allowance';
$dataArray[12] = $hra;

}



if($bonus != 0)
{
$dataArray[13] = $row[7];	
$dataArray[15] = 'Bonus';
$dataArray[16] = $bonus;

}




if($pfemployer != 0)
{
$dataArray[17] = $row[7];	
$dataArray[19] = 'PF Contribution (Employers)';
$dataArray[20] = $pfemployer;

}




if($refreshment != 0)
{
$dataArray[21] = $row[7];		
$dataArray[23] = 'Employee Daily Refreshment';
$dataArray[24] = $refreshment;

}





$dataArray[42] = 'CR.';
$dataArray[46] = 'CR.';
$dataArray[50] = 'CR.';
$dataArray[54] = 'CR.';
$dataArray[58] = 'CR.';
$dataArray[62] = 'CR.';
$dataArray[66] = 'CR.';
$dataArray[70] = 'CR.';
$dataArray[74] = 'CR.';
$dataArray[78] = 'CR.';

$dataArray[41] = $row[7];
$dataArray[43] = $row[1];
$dataArray[44] = round($netpay-$loan);

if($pfpayable != 0)
{
$dataArray[45] = $row[7];
$dataArray[47] = 'PF PAYABLE';
$dataArray[48] = $pfpayable;	
}

if($pt != 0)
{
$dataArray[49] = $row[7];	
$dataArray[51] = 'Professional Tax Payable';
$dataArray[52]= $pt;

}
$dataArray[81] = 'Salary provided for the month of '.$month.'-'.$year;
$dataArray[82] = 'JOURNAL SIL';

?>
<tr>
	<?php
	for($p=0;$p<count($dataArray);$p++)
	{
		?>
<td><?php echo $dataArray[$p];?></td>
		<?php
	}
	?>
</tr>


<?php
}
?>

</table>