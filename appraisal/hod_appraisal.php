<?php  
include'../include/config.php';
$count = 1;   
$emp_id = $hrow['m_id'];
$id = $_COOKIE['emanagerid'];

$query = mysqli_query($con,"SELECT a.appraised_mode,a.appraised_time,a.appraised_year,e.name,ast.eDate,ast.mDate,ast.hDate,ast.createdAt,au.status,eg.emp_id,eg.app_id from employee_goals eg  INNER JOIN appraisals a ON a.id = eg.app_id INNER JOIN employee e on e.id = eg.emp_id inner join appraisalstart ast on ast.appID= a.id  inner join appraisalusers au on au.empId=eg.emp_id   where h_id='$id' and au.status='hsubmit' group by emp_id"); 

$query1 = mysqli_query($con,"SELECT a.appraised_mode,a.appraised_time,a.appraised_year,e.name,ast.eDate,ast.mDate,ast.hDate,ast.createdAt,au.status,eg.emp_id,eg.app_id from employee_goals eg  INNER JOIN appraisals a ON a.id = eg.app_id INNER JOIN employee e on e.id = eg.emp_id inner join appraisalstart ast on ast.appID= a.id  inner join appraisalusers au on au.empId=eg.emp_id   where h_id='$id' and (au.status='msubmit' or au.status='hsave') group by emp_id");        
                    

$query2 = mysqli_query($con,"SELECT a.appraised_mode,a.appraised_time,a.appraised_year,e.name,ast.eDate,ast.mDate,ast.hDate,ast.createdAt,au.status,eg.emp_id,eg.app_id from employee_goals eg  INNER JOIN appraisals a ON a.id = eg.app_id INNER JOIN employee e on e.id = eg.emp_id inner join appraisalstart ast on ast.appID= a.id  inner join appraisalusers au on au.empId=eg.emp_id   where m_id='$id' and ast.appID = au.appID and (au.status='submit' or au.status='msave' or au.status='msubmit') group by emp_id"); 



?>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 15px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">

<div class="box">
<div class="box-header">
<h5 class="box-title">HOD approval Completted</h5>
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>

<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>


</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query)){

?>

<tr>
<?php
if($row['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}
if($row['es']=='g'){
$stage = 'Goals pending';                      
}else if($row['es']=='a'){
$stage = 'Appraisal pending';
}else{
$stage = 'HOD Approval Completted';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row['eDate']; ?></td>
<td><?php echo $row['mDate']; ?></td>
<td><?php echo $row['hDate']; ?></td>

<td><?php echo $row['createdAt']; ?></td> 

<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/hod-appraisal.php?hissubmitted=<?php echo $row['status'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['emp_id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td> 




               
</tr> 
<?php
}?>                       

</tbody>

</table>
</div>
<!-- /.box-body -->
</div>
</div>
</section>

<section class="content" style="background: #fff;margin-top: 15px;">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">

<div class="box">
<div class="box-header">
<h5 class="box-title">HOD approval pending </h5>
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>

<tr>
<th style="background: #b90909;">S.No</th>
<th style="background: #b90909;">Employee name</th>
<th style="background: #b90909;">Appraisal Mode</th>
<th style="background: #b90909;">Period</th>                
<th style="background: #b90909;">Status</th>
<th style="background: #b90909;">Stage</th>
<th style="background: #b90909;">Employee End Date</th>
<th style="background: #b90909;">Managers End Date</th>
<th style="background: #b90909;">HOD's End Date</th>
<th style="background: #b90909;">Create Date</th>
<th style="background: #b90909;">Action</th>


</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row=mysqli_fetch_array($query1)){
?>
<tr>
<?php
if($row['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}
if($row['es']=='g'){
$stage = 'Goals pending';                      
}else if($row['es']=='a'){
$stage = 'Appraisal pending';
}else{
$stage = 'HOD Approval Pending';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row['name']; ?></td>
<td><?php echo $row['appraised_mode']; ?></td>
<td><?php echo $row['appraised_time']; ?> <?php echo $row['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row['eDate']; ?></td>
<td><?php echo $row['mDate']; ?></td>
<td><?php echo $row['hDate']; ?></td>

<td><?php echo $row['createdAt']; ?></td> 

<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/hod-appraisal.php?hissubmitted=<?php echo $row['status'];?>&app_id=<?php echo $row['app_id'];?>&emp_id=<?php echo $row['emp_id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td>


                
</tr> 
<?php
}?>                       

</tbody>
</table>
</div>
</div>
</div>
</section>
<!-- Main content -->
<section class="content" style="background: #fff;margin-top: 37px;">
	  <?php
    if ( isset($_GET['success']) && $_GET['success'] == 1 )
    {?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Submit Successfully..!
    </div>   <?php
    }
    ?> 
    <?php
    if ( isset($_GET['success']) && $_GET['success'] == 2 )
    {?>

    <div class="alert alert-success"  id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    Save Successfully..!
    </div>   <?php
    }
    ?> 
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">

<div class="box">
<div class="box-header">
<h5 class="box-title">Manager appraisal pending</h5>
</div>

<!-- /.box-header -->
<div class="box-body">
<table id="#" class="table table-bordered table-striped">
<thead>   
<tr>
<th>S.No</th>
<th>Employee name</th>
<th>Appraisal Mode</th>
<th>Period</th>                
<th>Status</th>
<th>Stage</th>
<th>Employee End Date</th>
<th>Managers End Date</th>
<th>HOD's End Date</th>
<th>Create Date</th>
<th>Action</th>

</tr>
</thead>
<tbody>
<?php                      
$count=1;
while($row1=mysqli_fetch_array($query2)){

?>

<tr>
<?php

if($row1['status']=='o'){
$status = 'open';
}else{
$status = 'close';
}

if($row1['es']=='g'){

$stage = 'Manager Goals Pending';

}else if($row1['es']=='a'){
$stage = 'Appraisal Pending';

}else{
$stage = 'Appraisal Completed';
}
?>
<td><?php echo $count++; ?></td>
<td><?php echo $row1['name']; ?></td>
<td><?php echo $row1['appraised_mode']; ?></td>
<td><?php echo $row1['appraised_time']; ?> <?php echo $row1['appraised_year']; ?></td>
<td><?php echo $status; ?></td>
<td><span style="background: orange;color: white;"><?php echo $stage; ?></span></td>
<td><?php echo $row1['eDate']; ?></td>
<td><?php echo $row1['mDate']; ?></td>
<td><?php echo $row1['hDate']; ?></td>                  
<td><?php echo $row1['createdAt']; ?></td> 


<td><span  class="label label-success unclickable" style="font-size: 14px;display: block;" onclick="getModule('appraisal/hod-as-manager-appraisal.php?missubmitted=<?php echo $row1['status'];?>&app_id=<?php echo $row1['app_id'];?>&emp_id=<?php echo $row1['emp_id'] ?>','formDiv','tableDiv','loading');"  >View Appraisal</span></td>             
</tr> 
<?php
}?>                       

</tbody>
</table>
</div>
<!-- /.box-body -->
</div>
</div>

</section>
