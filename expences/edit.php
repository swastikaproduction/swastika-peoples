<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];

$getData = mysqli_query($con, "SELECT * FROM `expenses` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-default btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>
			</div>

	<div class="moduleHeading">
	Expense Details
	</div>
</div>





<div class="shadow">



<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft">
Name	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="Name" id="inp0" class="inputBox" value="<?php echo $row['name'];?>">
	</div>	

</div>



<div class="row">
	<div class="col-sm-2 formLeft">
Amount	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="Name" id="inp1" class="inputBox" value="<?php echo $row['amount'];?>">
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
Date	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="Name" id="inp2" class="inputBox" value="<?php echo $row['date'];?>">
	</div>	

</div>



<div class="row">
	<div class="col-sm-2 formLeft">
From Account	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp3">

<?php
$account = getData('account','*','name','ASC');
foreach($account as $val)
{
	?>
<option <?php if($row['account'] == $val['id']) echo "selected='selected'"; ?> value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
	<?php
}
?>

</select>
	</div>	

<div class="col-sm-1"></div>
	<div class="col-sm-1 formLeft">
To Vendor	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp4">

<?php
$account = getData('vendors','*','name','ASC');
foreach($account as $val)
{
	?>
<option <?php if($row['vendor'] == $val['id']) echo "selected='selected'"; ?>  value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
	<?php
}
?>

</select>
	</div>	

</div>











<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp5" style="width:100%;height:150px;"><?php echo $row['notes'];?></textarea>
	</div>	


</div>




<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','inp',6,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>
			<br/><br/><br/>
	</div>	


</div>

</div>

<br />
<br />
<br />
<br />
<br />