<?php

namespace App\Modules\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model {

    //
    protected $table = 'shift';

    public $timestamps = false;

}
