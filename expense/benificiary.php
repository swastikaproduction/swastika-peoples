<div class="content-wrapper" style="background:#fff">
<!--Content Header (Page header)-->
<section class="content-header">      
<ol class="breadcrumb">        
<li class="active" style="color: green;">Benificiary List</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
	            
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">        
<div class="box">
<div class="box-header">
<span class="label label-success"  style="float: right;font-size: 12px;margin-bottom: 6px;" data-toggle="modal" data-target="#myModal12">Add New</span>           
</div>            
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
<th>S.NO</th>
<th>Branch Name</th>          
<th>Benificiary Name</th>            
<th>Head Name</th>            
<th>Account Number</th>            
<th>IFSC Code</th>
<th>Bank Statement</th>            
<th>Create Date</th>            
<th>Update Date</th>            
<th>Action</th>            
</tr>
</thead>
<tbody>
<?php  
$count=1;           
include'../include/config.php';
$query = mysqli_query($con,"select eb.updatedat,eb.id,eb.name,b.name as bname,e.name as cname,eb.accountnumber,eb.ifsc,eb.createdate,ep.name as headname,eb.bankstatement FROM expensebenificiary eb INNER JOIN employee e on e.id = eb.empid INNER JOIN branch b on b.id = e.branch INNER JOIN expensenature ep on ep.id = eb.headid where eb.empid='$loggeduserid' order by eb.name");
while($row=mysqli_fetch_array($query)){
    
?>
<tr>
<td><?php echo $count++;?></td>
<td><?php echo $row['bname'];?></a></td>
<td><?php echo $row['name'];?></td>
<td><?php echo $row['headname'];?></td>
<td><?php echo $row['accountnumber'];?></td>
<td><?php echo $row['ifsc'];?></td>
<td><a href="expense/benificiaryimages/<?php echo $row['bankstatement'];?>" download>Download</a></td>
<td><?php echo $row['createdate'];?></td>
<td><?php echo $row['updatedat'];?></td>
<td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="expense/delete-benificiary.php?beniid=<?php echo $row['id'];?>" onclick="return confirm('Are you sure you want to delete this?');">Delete</a></li>
      <li><a href="#" data-toggle="modal" data-target="#myModalUpdate" onclick="UpdateBenificiary('<?php echo $row['id'];?>');">Edit</a></li>
      
    </ul>
  </div>
</div></td>


    <?php
    }?>                      

</tbody>               
</table>
</div>
<!-- /.box-body -->
</div>
</div>
</div>

 <div class="modal fade" id="myModal12" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/post-benificiary.php" method="post" enctype="multipart/form-data">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Benificiary </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Benificiary Name</label>
<input type="hidden" name="keyvalue" value="listing">
<input type="hidden" name="pid" id="pid">
<input type="text" name="name" class="form-control" id="name" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="expenseheadid" name="expenseheadid" class="form-control" onchange="GetExpenseHeadData();" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Account Number</label>
<input type="text" name="accountnumber" class="form-control allownumericwithoutdecimal" required="">
</div>
<div class="col-md-6">
<label>IFSC Code</label>
<input type="text" name="ifsccode" class="form-control" required="">
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Proof Of Bank</label>
<input type="file" name="proofofbank" class="form-control" required="">
</div>
</div>
</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->

<!-- //update benificiary data  -->

<div class="modal fade" id="myModalUpdate" role="dialog">
<div class="modal-dialog" style="margin-top: 150px;">
<form action="expense/update-benificiary.php" method="post" enctype="multipart/form-data">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update  Benificiary </h4>
</div>
<div class="modal-body">
  
<div class="row">
<div class="col-md-6">

<label>Benificiary Name</label>
<input type="hidden" name="keyvalue" value="listing">
<input type="hidden" name="bid" id="bid">
<input type="text" name="uname" class="form-control" id="uname" placeholder="enter profile name" required="">
</div>
<div class="col-md-6">
<label>Expense Head</label>
<select id="uexpenseheadid" name="uexpenseheadid" class="form-control" onchange="GetExpenseHeadData();" required="">
  <option value="">select head</option>
                    <?php 
                    $query = mysqli_query($con,"select * from expensenature");
                    while($row = mysqli_fetch_array($query)){
                      ?>
                   
                     <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                
                <?php } ?>
</select>
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Account Number</label>
<input type="text" name="uaccountnumber" id="uaccountnumber" class="form-control allownumericwithoutdecimal" required="">
</div>
<div class="col-md-6">
<label>IFSC Code</label>
<input type="text" name="uifsccode" id="uifsccode" class="form-control" required="">
</div>
</div>
<div class="row">

<div class="col-md-6">
<label>Bank Statement</label>
<input type="file" name="proofofbank" id="proofofbank" class="form-control">
</div>
</div>
</div>
<div class="modal-footer">
<input type="submit" name="submit" value="Submit" class="btn btn-success">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
<!-- Modal -->