<?php
include("../include/config.php");
$empid = '134';
$month = '11';
$year = "2018";

/*
$getAtlog = mysqli_query($con,"SELECT * FROM `attendancelog` WHERE `empid` = '$empid' AND `month` = '$month' AND `year` = '$year'") or die(mysqli_error($con));
$rowLog = mysqli_fetch_array($getAtlog);
$empid = $rowLog['empid'];
$month = $rowLog['month'];
$year = $rowLog['year'];
*/
$loanArray = Array();
$holdArray = Array();
$incentiveArray = Array();
$jvholdarray = Array();
$getLoans = mysqli_query($con,"SELECT * FROM `salaryaddons` WHERE `month` = '$month' AND `year` = '$year' AND `empid` = '$empid'") or die(mysqli_error($con));
while($rowLoans = mysqli_fetch_array($getLoans))
{
	$loanArray[$rowLoans['empid']] = $rowLoans['loan'];
	$holdArray[$rowLoans['empid']] = $rowLoans['hold'];
	$incentiveArray[$rowLoans['empid']] = $rowLoans['incentive'];
	$jvholdarray[$rowLoans['empid']] = $rowLoans['jvhold'];
}





$getEmp = mysqli_query($con,"SELECT employee.name,designation.name,employee.pf,employee.empid,employee.branch,departments.name,employee.department,employee.depttype FROM employee,designation,departments WHERE employee.designation = designation.id AND employee.id = '$empid' AND employee.department = departments.id") or die(mysqli_error($con));
$rowEmp = mysqli_fetch_array($getEmp);

if($rowEmp['department'] == '4')
{
	$type = "Housekeeping";
}
else if($rowEmp['depttype'] == 'Cash')
{
	$type = 'Cash';
}
else if($rowEmp['pf'] == '1')
{
	$type = "PF";
}
else
{
	$type = $rowEmp['depttype'];
}




$monthArray = Array();
$monthArray['01'] = 'January';
$monthArray["02"] = 'February';
$monthArray['03'] = 'March';
$monthArray['04'] = 'April';
$monthArray['05'] = 'May';
$monthArray['06'] = 'June';
$monthArray['07'] = 'July';
$monthArray['08'] = 'August';
$monthArray['09'] = 'September';
$monthArray['10'] = 'October';
$monthArray['11'] = 'November';
$monthArray['12'] = 'December';


$pfyes=$rowEmp[2];
$mth = "2018-".$rowLog['month']."-01";
//include("bifur-logic.php");


//$month = "0".$month;
if($type == 'Housekeeping')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.department = '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.depttype != 'Cash' AND employee.id = '$empid'   ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else if($type == 'Cash')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = 'Cash' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid'  ORDER BY employee.empid ASC") or die(mysqli_error($con));

}
else if($type == 'PF')
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.pf = '1' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid'   ORDER BY employee.empid ASC") or die(mysqli_error($con));
}
else
{
$getData = mysqli_query($con,"SELECT employee.name,employee.empid,attendancelog.actual_salary,attendancelog.final_salary,employee.pf,employee.defpf,employee.id,employee.depttype,employee.department FROM employee,attendancelog WHERE attendancelog.empid = employee.id AND employee.depttype = '$type' AND employee.department != '4' AND attendancelog.month = '$month' AND attendancelog.year = '$year' AND employee.id = '$empid' ORDER BY employee.empid ASC") or die(mysqli_error($con));

}

$row= mysqli_fetch_array($getData);


//CALCULATED VARIABLES
$grosssalary = $row[2];
$leavedeductions = $row[2] - $row[3];

$pfflag = $row[4];
$loan = 0;
if($loanArray[$row[6]])
{
	$loan = $loanArray[$row[6]];
}
$incentive = 0;
if($incentiveArray[$row[6]])
{
	$incentive = $incentiveArray[$row[6]];
}
$hold = 0;
if($holdArray[$row[6]])
{
	$hold = $holdArray[$row[6]];
}

$jvhold = 0;
if($jvholdarray[$row[6]])
{
	$jvhold = $jvholdarray[$row[6]];
}


$calculatedsalary = $grosssalary - ($leavedeductions+$hold);

$pfpayable = $row[5];

//DEFAULT VARIABLES
$basic = 0;
$da = 0;
$hra = 0;
$bonus = 0;
$pfemployer = 0;
$pt = 0;
$refreshment = 0;
$totalearnings = 0;
$totaldeductions =0;
$refreshment = 0;

if($row[7] != 'Other')
{
if($grosssalary  <= 18750)
{
	$pt = 0;
}
else if($grosssalary > 18750 && $grosssalary <= 25000)
{
	$pt = 125;
}
else if($grosssalary > 25000 && $grosssalary <= 33333)
{
	$pt = 167;
}
else
{
	if($month != '03')
	{
		$pt = 208;
	}
	else
	{
		$pt = 212;
	}
}	
}
else
{
	$pt = 0;
}




if($pfflag == '1')
{

		if($calculatedsalary < 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = round($calculatedsalary * 0.24);		
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		if($calculatedsalary < 8960 && $calculatedsalary >= 8000)
		{
			if($pfpayable == '')
			{
				$pfpayable = 1920;				
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}
			$pfemployer = round($pfpayable/2);

			$basic = $calculatedsalary - $pfemployer;
			$hra = 0;
			$bonus = 0;
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		if($calculatedsalary >= 8960 && $calculatedsalary <= 10000)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
							$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 10000 && $calculatedsalary <= 13350)
		{
			$basic = 8000;
			$hra = 0;
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}


			$pfemployer = round($pfpayable/2);
			$bonus = 500;
			$hra = $calculatedsalary - ($basic+$pfemployer+$bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}

		else if($calculatedsalary > 13350 && $calculatedsalary <= 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
			else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

						if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}

			$pfemployer = round($pfpayable/2);
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
		}
		else if($calculatedsalary > 25000)
		{
			$basic = round($calculatedsalary * 0.6);
			$hra = round($basic * 0.4);
			$bonus = round($calculatedsalary * 0.05);
			if($pfpayable == '')
			{
			$pfpayable = round($basic * 0.24);
			}
		else
			{
				if($pfpayable == 3600)
				{
					$da = $basic - 15000;
					$basic = 15000; 
				}
			}

			if($pfpayable%2 != 0)
			{
				$pfpayable = $pfpayable+1;
			}



			$pfemployer = round($pfpayable/2);
			$refreshment = $calculatedsalary - ($basic + $hra+$bonus+$pfemployer+$da);
			$totalearnings = $basic + $pfemployer + $bonus + $hra+$refreshment+$da;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

		}


}
else
{	
	if($calculatedsalary < 8000)
	{
		
			$basic = $calculatedsalary;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;

	}

	else if($calculatedsalary >= 8000 && $calculatedsalary < 9000)
	{
			$basic = 8000;
			$hra = 0;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = $calculatedsalary - ($basic+$pfemployer+$hra);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

	else if($calculatedsalary >= 9000 && $calculatedsalary <= 15000)
	{
			$basic = 8000;
			$pfpayable = 0;
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.10);
			$hra = $calculatedsalary - ($basic + $bonus);
			$totalearnings = $basic + $pfemployer + $bonus + $hra;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}

else if($calculatedsalary > 15000)
	{
			$basic = round($calculatedsalary * 0.4);
			$pfpayable = 0;
			$da = round($basic * 0.35);
			$hra = round(($basic + $da) * 0.4);
			$pfemployer = 0;
			$bonus = round($calculatedsalary * 0.15);
			$refreshment = $calculatedsalary - ($basic + $da + $bonus + $hra);
			$totalearnings = $basic + $da + $bonus + $hra + $refreshment;
			$totaldeductions = $pfpayable +$pt;
			$netpay = $totalearnings - $totaldeductions;
	}
}

echo $netpay= ($basic+$da+$hra+$bonus+$refreshment+$pfemployer) - ($pfpayable+$jvhold+$loan+$pt);
?>


