<style type="text/css">
  .top-margin{
     margin-top: 15px;
  }
  .secmargin{      
    margin-left:60px;
    color: #2dda2a;
  }
  .col{
  	color: #2532d6;
  }
  .sec{
  	background: white; height: auto;
  }
  .lab{
  	color: #d43319;
  }
  .ptag{
  	    margin: 1px 63px 10px;
  	    color: orange;
  }
</style>
<section>
	<?php 
	include'../include/config.php';
	$appId = $_GET['app_id'];
	$submitted = $_GET['hissubmitted'];
	$sql = mysqli_query($con,"select * from appraisals where id='$appId'") or die(mysqli_error($con));
	$row = mysqli_fetch_array($sql);
	$empId = $_GET['emp_id'];       
    $query1 = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
    $finalscorresult = mysqli_fetch_array($query1);
	?>
	<div class="row">
		<div class="col-md-12">
			<p class="col">Appraisal start for <?php echo $row['appraised_time'] ?> <?php echo $row['appraised_year'] ?> </p>
		</div>		
	</div>
</section>
<section class="sec">
	<form action="appraisal/hodAppraisal.php" method="post" enctype="multipart/form-data">		
   <div class="row">
 	<div class="col-md-4">
 		<p class="ptag">Employee</p>
 	</div>
 	<div class="col-md-4">
 		<p class="ptag">Approver</p>
 	</div>
 	<div class="col-md-4">
 		<p class="ptag">Reviewer</p>
 	</div>l
 </div>
  <div class="row">
  	<?php 
		 $query = mysqli_query($con,"select * from employee_goals where emp_id='$empId' and app_id='$appId'");
         while($result = mysqli_fetch_array($query)){
         	?> 
  	<div class="col-md-4">  		
  		<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Subject</label>
		<input type="hidden" name="appId" value="<?php echo $appId;?>">
		<input type="hidden" name="kpid[]" value="<?php echo $result['id'];?>">
		<textarea  type="text" class="form-control" name="subject"  readonly=""><?php echo $result['h_subject'];?></textarea>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label class="top-margin">Target</label>
		<textarea type="text" class="form-control" name="target"  readonly=""><?php echo $result['h_target'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Achievement</label>
		<textarea type="text" class="form-control" name="achievement[]" readonly=""><?php echo $result['achievement'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Weightage</label>
		<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'];?>" readonly="">
	</div>
	<div class="col-md-3">
		<label class="top-margin">Score</label>
		<input type="text" class="form-control allownumeric" data-weight ="" value="<?php echo $result['score'];?>" name="yourscore[]" id="yscore" readonly>		
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Description</label>
		<textarea type="text" class="form-control" name="description" readonly=""><?php echo $result['h_description'];?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin">Your Comment's</label>
		<textarea type="text" class="form-control" name="yourcomment[]" readonly=""><?php echo $result['comments'];?></textarea>		
	</div>
</div>
  	</div>
<div class="col-md-4">
		<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Subject</label>
		<input type="hidden" name="appId" value="<?php echo $appId;?>">
		<input type="hidden" name="kpid[]" value="<?php echo $result['id'];?>">
		<textarea  type="text" class="form-control" name="subject"  readonly=""><?php echo $result['h_subject'];?></textarea>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<label class="top-margin">Target</label>
		<textarea type="text" class="form-control" name="target"  readonly=""><?php echo $result['h_target'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Achievement</label>
		<textarea type="text" class="form-control" name="achievement[]" readonly=""><?php echo $result['machievement'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Weightage</label>
		<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'];?>" readonly="">
	</div>
	<div class="col-md-3">
		<label class="top-margin">Score</label>
		<input type="text" class="form-control allownumeric" data-weight ="" value="<?php echo $result['mscore'];?>" name="yourscore[]" id="yscore" readonly>		
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Description</label>
		<textarea type="text" class="form-control" name="description"  readonly=""><?php echo $result['h_description'];?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin">Your Comment's</label>
		<textarea type="text" class="form-control" name="yourcomment[]" readonly=""><?php echo $result['mcomment'];?></textarea>		
	</div>
</div>
</div>
<div class="col-md-4">
	<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Subject</label>
		<input type="hidden" name="empId" value="<?php echo $empId;?>">
		<input type="hidden" name="appId" value="<?php echo $appId;?>">
		<input type="hidden" name="hkpid[]" value="<?php echo $result['id'];?>">
		<textarea  type="text" class="form-control" name="subject"  readonly=""><?php echo $result['h_subject'];?></textarea>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<label class="top-margin">Target</label>
		<textarea type="text" class="form-control" name="target"  readonly=""><?php echo $result['h_target'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Achievement</label>
		<textarea type="text" class="form-control" name="hachievement[]" <?php if($submitted=='hsubmit'){ echo'readonly';}else{} ?>><?php echo $result['hachievement'];?></textarea>
	</div>
	<div class="col-md-3">
		<label class="top-margin">Weightage</label>
		<input type="text" class="form-control"  name="weightage" value="<?php echo $result['h_weightage'];?>" readonly="">
	</div>
	<div class="col-md-3">
		<label class="top-margin">Score</label>
		<input type="text" class="form-control allownumeric yourscore" data-weight ="<?php echo $result['h_weightage'];?>" value="<?php echo $result['hscore'];?>" name="hscore[]" id="yscore" <?php if($submitted=='hsubmit'){ echo'readonly';}else{} ?>>		
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Description</label>
		<textarea type="text" class="form-control" name="description"  readonly=""><?php echo $result['h_description'];?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin">Your Comment's</label>
		<textarea type="text" class="form-control" name="hcomment[]" <?php if($submitted=='hsubmit'){ echo'readonly';}else{} ?>><?php echo $result['hcomment'];?></textarea>	
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label class="top-margin">Supporting Document</label>
		<input type="file" class="form-control" name="hkpifile[]">
	</div>
	<div class="col-md-4">
		<label class="top-margin">Manager Document</label>
		<a href="kpiImages/<?php echo $result['msupportDocument'] ?>" class="form-control" style="color: #38e835;" download> Download</a>
	</div>
</div>
</div>

<?php }?>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="row">
	<div class="col-md-6">
		<label class="top-margin lab">Final Comment's</label>
		<textarea type="text" class="form-control" name="finalcommnets" readonly=""><?php echo $finalscorresult['finalcomment'] ?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin lab">Final Score</label>
		<input type="text" class="form-control allownumeric"  name="finalscore" value="<?php echo $finalscorresult['finalscore'] ?>" id="mfscore" readonly="">		
	</div>
</div>
	</div>
	<div class="col-md-4">
		<div class="row">
	<div class="col-md-6">
		<label class="top-margin lab">Final Comment's</label>
		<textarea type="text" class="form-control" name="finalcommnets" readonly=""><?php echo $finalscorresult['mfinalcomment'] ?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin lab">Final Score</label>
		<input type="text" class="form-control allownumeric"  name="finalscore" value="<?php echo $finalscorresult['mfinalscore'] ?>" id="mfscore" readonly="">		
	</div>
</div>
	</div>
	<div class="col-md-4">
		<div class="row">
	<div class="col-md-6">
		<label class="top-margin lab">Final Comment's</label>
		<textarea type="text" class="form-control" name="hfinalcommnets" <?php if($submitted=='hsubmit'){ echo'readonly';}else{} ?>> <?php echo $finalscorresult['hfinalcomment'] ?></textarea>		
	</div>
	<div class="col-md-6">
		<label class="top-margin lab">Final Score</label>
		<input type="text" class="form-control allownumeric"  name="hfinalscore" value="<?php echo $finalscorresult['hfinalscore'] ?>" id="fscore" readonly="">		
	</div>
</div>
</div>	
</div>
<?php
 $empId = $_GET['emp_id'];;
 $check = mysqli_query($con,"select * from appraisalusers where empId='$empId'");
 $checkresult = mysqli_fetch_array($check);
 $status =$checkresult['status']; 
 if($status !=='hsubmit' ){
 	?>
<div class="row">
	<div class="col-md-12" style="margin-left: 19px;">
		<input type="submit" class="btn btn-success top-margin" name="submit" value="Save Draft">&nbsp;&nbsp;<input type="submit" class="btn btn-primary top-margin" name="submit" value="Submit">&nbsp;&nbsp;<input type="reset" class="btn btn-info top-margin" name="cancel" value="Cancel">
	</div>
</div>
<?php }?>
</form>
</section>
 