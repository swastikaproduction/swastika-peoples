<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT * FROM `employee` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>


<div class="moduleHead">


		<div style="float: right">

<button class="btn btn-default btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;&nbsp;&nbsp;
<div class="btn-group">
		<button class="btn btn-sm btn-default"  onclick="getModal('documents/index.php?id=<?php echo $id;?>&type=employee','tableModalBig','formModalBig','loading')">
<i class="fa fa-file"></i>&nbsp;&nbsp;
		DOCUMENTS</button>
		<button class="btn btn-sm btn-default"  onclick="getModal('employeesalary/index.php?id=<?php echo $id;?>','formModalBig','tableModalBig','loading')">
<i class="fa fa-rupee"></i>&nbsp;&nbsp;
		SALARY</button>

		</div>
		
			</div>

	<div class="moduleHeading">
	Edit Employee Details
	</div>
</div>

<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Employee Code
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="" id="inp0" class="inputBox" value="<?php echo $row['emp_code']?>" >
	</div>	

</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Department
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp1" name='req'>
		<option>-- Select Department -- </option>
		<?php
		$data = getData('department','*','name','ASC');
		foreach($data as $row1)
		{
		?>
		<option <?php if($row['departmentid'] == $row1['id']) echo "selected='selected'";?> value="<?php echo $row1['id']?>"><?php echo $row1['name']?></option>
		<?php } ?>
	</select>
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
		Designation
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp2" name="req">
		<option>-- Select Designation --</option>
		<?php
		$data = getData('designation','*','name','ASC');
		foreach($data as $row2)
		{
		?>
		<option <?php if($row['designationid'] == $row2['id']) echo "selected='selected'";?> value="<?php echo $row2['id']?>"><?php echo $row2['name']?></option>
		<?php } ?>
	</select>
	</div>	

</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Joining Date 
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="" title="" id="inp3" class="inputBox" value="<?php echo $row['joiningdate']?>" >
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
		Qualification 
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp4" class="inputBox" value="<?php echo $row['qualification']?>" >
	</div>	

</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Total Experience 
	</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp5" class="inputBox" value="<?php echo $row['experience']?>" >
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
	User Type
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp6" name="req">
		<option>--Select Type--</option>
		<?php
		$data = getData('user_type','*','name','ASC');
		foreach($data as $row3)
		{
		?>
		<option <?php if($row['usertypeid'] == $row3['id']) echo "selected='selected'";?> value="<?php echo $row3['id'];?>"><?php echo $row3['name'];?></option>
		<?php } ?>
	</select>
	</div>	

</div>


<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
	First Name
		</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp7" class="inputBox" value="<?php echo $row['name']?>">
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
	Middle Name
		</div>
	<div class="col-sm-4 formRight">
		<input type="text"  title="" id="inp8" class="inputBox"  value="<?php echo $row['middlename']?>">
	</div>	

</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
	Last Name
		</div>
	<div class="col-sm-4 formRight">
		<input type="text" name="req" title="" id="inp9" class="inputBox" value="<?php echo $row['lastname']?>">
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
	Date of Birth
	</div>
	<div class="col-sm-4 formRight">
		<input type="date" name="req" title="" id="inp10" class="inputBox" value="<?php echo $row['dob']?>">
	</div>	

</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
	Gender
	</div>
	<div class="col-sm-4 formRight">
		<select class="inputBox" id="inp11" name="req">
		<option>--Select Type--</option>
		<option <?php if($row['gender']=='Male')echo "selected='selected'";?> value="Male">Male</option>
		<option <?php if($row['gender']=='Female')echo "selected='selected'";?> value="Female">Female</option>
	</select>
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
		Present Address
	</div>
	<div class="col-sm-4 formRight">
		<input type="textarea" name="req" title="" id="inp12" class="inputBox" value="<?php echo $row['presentaddress']?>">
	</div>	

</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
		Permanant Address
	</div>
	<div class="col-sm-4 formRight">
		<input type="textarea" name="req" title="" id="inp13" class="inputBox" value="<?php echo $row['permaaddress']?>">
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
		City
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp14" name="req">
		<option>--Select City--</option>
		<?php
		$data = mysqli_query($con,"select * from city");
		while($row4= mysqli_fetch_assoc($data))
		{
		?>
		<option  <?php if($row['city'] == $row4['name']) echo "selected='selected'";?> value="<?php echo $row4['name']?>"><?php echo $row4['name']?></option>
		<?php } ?>
	</select>
	</div>	

</div>
<div class="row" >
	<div class="col-sm-2 formLeft req">
		Pin Code
	</div>
	<div class="col-sm-4 formRight">
		<input type=" number" name="req" title="" id="inp15" class="inputBox" value="<?php echo $row['pin']?>">
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">

	State
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp16" name="req">
		<option>--Select State--</option>
		<?php
		$data =  mysqli_query($con,"select * from State");
		while($row5= mysqli_fetch_assoc($data))
		{
		?>
		<option <?php if($row['state'] == $row5['name']) echo "selected='selected'";?> value="<?php echo $row5['name'];?>"><?php echo $row5['name'];?></option>
		<?php } ?>
	</select>
	</div>	

</div>
<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
	 Country
	</div>
	<div class="col-sm-4 formRight">
	<select class="inputBox" id="inp17" name="req">
		<option>--Select Country--</option>
		<?php
		$data = mysqli_query($con,"select * from country");
		echo $city; 
		while($row6 = mysqli_fetch_array($data))
		{
		?>
		<option <?php if($row['country'] == $row6['name']) echo "selected='selected'";?> value="<?php echo $row6['name'];?>"><?php echo $row6['name'];?></option>
		<?php } ?>
	</select>
	</div>	

    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
     Phone
	</div>
	<div class="col-sm-4 formRight">
			<input type="number"  name="reqisnum" title="" id="inp18" class="inputBox"  value="<?php echo $row['phone']?>">	
	</div>	
</div>

<div class="row" style="background:">
	<div class="col-sm-2 formLeft req">
     Mobile
	</div>
	<div class="col-sm-4 formRight">
			<input type="number" name="reqismobile" title="" id="inp19" class="inputBox" value="<?php echo $row['mobile']?>">	
	</div>	
    <div class="col-sm-1"></div>

	<div class="col-sm-1 formLeft req">
     Email
	</div>
	<div class="col-sm-4 formRight">
			<input type="email" name="reqisEmail" title="" id="inp20" class="inputBox" value="<?php echo $row['email']?>" >	
	</div>	
</div>

<div class="row" style="display:none">
	<div class="col-sm-2 formLeft req">
     Photo
	</div>
	<div class="col-sm-4 formRight">
			<input type="file"  title="" id="inp21" class="inputBox" value="<?php echo $row['photo']?>" >	
	</div>	
</div>



<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp22" style="width:100%;height:150px;"></textarea>
	</div>	


</div>



<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','inp',23,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>
			<br/><br/><br/>
	</div>	


</div>





</div>



<br />
<br />
<br />
<br />
<br />

