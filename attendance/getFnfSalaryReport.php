<?php
include("../include/config.php");

$emp = $_GET['emp'];
$month = $_GET['month'];
$year = $_GET['year'];
if($month < 10)
{
	$month = "0".$month;
}
$empArray = Array();
$employees = getData('employee','*','name','ASC');
foreach($employees as $val)
{
	$empArray[$val['id']]['name'] = $val['name'];
	$empArray[$val['id']]['code'] = $val['empid'];
}
$typeStr = '';
if($_GET['type'] != '0')
{
	// $typeStr = " AND `status` = '1'";
}

if($emp != 'ALL')
{
$getData = mysqli_query($con,"SELECT * FROM `fnfcalculation` WHERE `empid`= '$emp' AND `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `created_date`") or die(mysqli_error($con));
}
else
{
	$getData = mysqli_query($con,"SELECT * FROM `fnfcalculation` WHERE `month` = '$month' AND `year` = '$year' ".$typeStr." ORDER BY `created_date`") or die(mysqli_error($con));

}

$name = "fnf_salary_details.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>




<table border="1" style="background-color: #eef5f9;">
	<tr>
		<th style="width:5%">#</th>
		<th style="width:20%">Employee Name</th>
		<th style="width:10%">Employee Code</th>
		<th style="width:10%">Month-Year</th>
		<th style="width:15%">Pool Balance</th>
		<th style="width:15%">Total Leave</th>
		<th style="width:15%">Leave Balance Before resignation</th>
		<th style="width:15%">Leave Deduction</th>
		<th style="width:15%">Salary Leave Deduction</th>
		<th style="width:15%">Client Debit Amount</th>
		<th style="width:15%">RM Client Debit</th>
		<th style="width:15%">Loan Amount</th>
		<th style="width:15%">Training Impartment Contribution</th>
		<th style="width:15%">Professional Tax</th>
		<th style="width:10%">Actual Salary</th>
		<th style="width:5%">Salary Calculated By Attendance</th>
		<th style="width:5%">Salary Calculated By Heads Comment</th>
		<th style="width:5%">FnF Salary</th>
		<th style="width:5%">Released By</th>
		<th style="width:5%">Released</th>
	</tr>
	<?php
	$i=1;
	while($row = mysqli_fetch_array($getData))
	{
		?>
<tr id="atRow<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td><?php echo $empArray[$row['empid']]['name'];?></td>
	<td><?php echo $empArray[$row['empid']]['code'];?></td>
	<td><?php 

	$temp = $row['year']."-".$row['month']."-01";
	echo date("M-Y",strtotime($temp));
	?></td>
	
	<td><?php echo $row['pool_balance'];?></td>
	<td><?php echo $row['total_leave'];?></td>
	<td><?php echo $row['leave_balance_before_resignation'];?></td>
	<td><?php echo $row['leave_deduction'];?></td>
	<td><?php echo $row['salary_leave_deduction'];?></td>
	<td><?php echo $row['client_debit_amount'];?></td>

	<td>
		<?php if($row['rm_client_debit'] == '2')
		{
			?>
				<span class="">YES</span>
			<?php
		}
		else
		{
			?>
				<span class="">NO</span>
			<?php

		}
		?>
	</td>

	<td><?php echo $row['loan_amount'];?></td>
	<td><?php echo $row['training_deduction'];?></td>
	<td><?php echo $row['pt'];?></td>
	<td><?php echo $row['actual_salary'];?></td>
	<td><?php echo $row['salary_calculated_by_attendance'];?></td>
	<td><?php echo $row['salary_by_heads_comment'];?></td>
	<td><?php echo $row['fnf_salary'];?></td>

	<td>
		<?php if($row['salary_released_by'] == '1')
		{
			?>
				<span class="">By Attendance</span>
			<?php
		}
		else
		{
			?>
				<span class="">By Heads</span>
			<?php

		}
		?>
	</td>

	<td>
		<?php if($row['salary_release_status'] == '1')
		{
			?>
				<span class="label label-primary">YES</span>
			<?php
		}
		else
		{
			?>
				<span class="label label-danger">NO</span>
			<?php

		}
		?>
	</td>
</tr>
		<?php
		$i++;
	}
	?>
</table>
